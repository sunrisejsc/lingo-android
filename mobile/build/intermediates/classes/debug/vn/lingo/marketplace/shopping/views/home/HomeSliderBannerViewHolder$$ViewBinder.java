// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomeSliderBannerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.HomeSliderBannerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624206, "field 'view'");
    target.view = view;
    view = finder.findRequiredView(source, 2131624205, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624205, "field 'imageView'");
  }

  @Override public void unbind(T target) {
    target.view = null;
    target.imageView = null;
  }
}

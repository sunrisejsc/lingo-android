// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MicroSiteViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.MicroSiteViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624580, "field 'cardView'");
    target.cardView = finder.castView(view, 2131624580, "field 'cardView'");
    view = finder.findRequiredView(source, 2131624582, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624582, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624592, "field 'marketPrice'");
    target.marketPrice = finder.castView(view, 2131624592, "field 'marketPrice'");
    view = finder.findRequiredView(source, 2131624591, "field 'price'");
    target.price = finder.castView(view, 2131624591, "field 'price'");
    view = finder.findRequiredView(source, 2131624590, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624590, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624583, "field 'linearLayoutPercent'");
    target.linearLayoutPercent = finder.castView(view, 2131624583, "field 'linearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624584, "field 'appCompatTextViewPercent'");
    target.appCompatTextViewPercent = finder.castView(view, 2131624584, "field 'appCompatTextViewPercent'");
    view = finder.findRequiredView(source, 2131624585, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624585, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624586, "field 'appCompatTextViewHappyHourDiscount'");
    target.appCompatTextViewHappyHourDiscount = finder.castView(view, 2131624586, "field 'appCompatTextViewHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624587, "field 'linearLayoutDiscountGiftCards'");
    target.linearLayoutDiscountGiftCards = finder.castView(view, 2131624587, "field 'linearLayoutDiscountGiftCards'");
    view = finder.findRequiredView(source, 2131624588, "field 'appCompatTextViewDiscountGiftCards'");
    target.appCompatTextViewDiscountGiftCards = finder.castView(view, 2131624588, "field 'appCompatTextViewDiscountGiftCards'");
  }

  @Override public void unbind(T target) {
    target.cardView = null;
    target.imageView = null;
    target.marketPrice = null;
    target.price = null;
    target.appCompatTextViewDescription = null;
    target.linearLayoutPercent = null;
    target.appCompatTextViewPercent = null;
    target.linearLayoutHappyHourDiscount = null;
    target.appCompatTextViewHappyHourDiscount = null;
    target.linearLayoutDiscountGiftCards = null;
    target.appCompatTextViewDiscountGiftCards = null;
  }
}

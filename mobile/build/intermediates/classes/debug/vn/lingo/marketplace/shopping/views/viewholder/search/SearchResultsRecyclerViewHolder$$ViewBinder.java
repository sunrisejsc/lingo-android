// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder.search;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SearchResultsRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.search.SearchResultsRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624595, "field 'relativeLayoutRowItem'");
    target.relativeLayoutRowItem = finder.castView(view, 2131624595, "field 'relativeLayoutRowItem'");
    view = finder.findRequiredView(source, 2131624596, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624596, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624597, "field 'searchResultsProductionTitle'");
    target.searchResultsProductionTitle = finder.castView(view, 2131624597, "field 'searchResultsProductionTitle'");
    view = finder.findRequiredView(source, 2131624598, "field 'searchResultsPrices'");
    target.searchResultsPrices = finder.castView(view, 2131624598, "field 'searchResultsPrices'");
    view = finder.findRequiredView(source, 2131624599, "field 'searchResultsMarketPrice'");
    target.searchResultsMarketPrice = finder.castView(view, 2131624599, "field 'searchResultsMarketPrice'");
    view = finder.findRequiredView(source, 2131624600, "field 'searchResultsRatingBar'");
    target.searchResultsRatingBar = finder.castView(view, 2131624600, "field 'searchResultsRatingBar'");
  }

  @Override public void unbind(T target) {
    target.relativeLayoutRowItem = null;
    target.imageView = null;
    target.searchResultsProductionTitle = null;
    target.searchResultsPrices = null;
    target.searchResultsMarketPrice = null;
    target.searchResultsRatingBar = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.launcher;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SplashScreen$$ViewBinder<T extends vn.lingo.marketplace.shopping.launcher.SplashScreen> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624117, "field 'appCompatTextView'");
    target.appCompatTextView = finder.castView(view, 2131624117, "field 'appCompatTextView'");
  }

  @Override public void unbind(T target) {
    target.appCompatTextView = null;
  }
}

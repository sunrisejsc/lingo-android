// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProductDetailsGeneralProductInformationRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.ProductDetailsGeneralProductInformationRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624519, "field 'productDetailsGeneralProductInformationRecyclerViewItemProductAttributes'");
    target.productDetailsGeneralProductInformationRecyclerViewItemProductAttributes = finder.castView(view, 2131624519, "field 'productDetailsGeneralProductInformationRecyclerViewItemProductAttributes'");
  }

  @Override public void unbind(T target) {
    target.productDetailsGeneralProductInformationRecyclerViewItemProductAttributes = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CategoriesActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.CategoriesActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624077, "field 'recyclerView'");
    target.recyclerView = finder.castView(view, 2131624077, "field 'recyclerView'");
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
  }

  @Override public void unbind(T target) {
    target.recyclerView = null;
    target.toolbar = null;
  }
}

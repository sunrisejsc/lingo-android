// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder.search;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CustomerPaymentRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.search.CustomerPaymentRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624300, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624300, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624301, "field 'productName'");
    target.productName = finder.castView(view, 2131624301, "field 'productName'");
    view = finder.findRequiredView(source, 2131624304, "field 'productQuantity'");
    target.productQuantity = finder.castView(view, 2131624304, "field 'productQuantity'");
    view = finder.findRequiredView(source, 2131624305, "field 'addProduct'");
    target.addProduct = finder.castView(view, 2131624305, "field 'addProduct'");
    view = finder.findRequiredView(source, 2131624303, "field 'subProduct'");
    target.subProduct = finder.castView(view, 2131624303, "field 'subProduct'");
    view = finder.findRequiredView(source, 2131624298, "field 'removeProduct'");
    target.removeProduct = finder.castView(view, 2131624298, "field 'removeProduct'");
    view = finder.findRequiredView(source, 2131624302, "field 'productPrice'");
    target.productPrice = finder.castView(view, 2131624302, "field 'productPrice'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.productName = null;
    target.productQuantity = null;
    target.addProduct = null;
    target.subProduct = null;
    target.removeProduct = null;
    target.productPrice = null;
  }
}

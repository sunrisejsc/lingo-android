// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProductDetailsRelatedProductsRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.ProductDetailsRelatedProductsRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624534, "field 'relativeLayoutRowItem'");
    target.relativeLayoutRowItem = finder.castView(view, 2131624534, "field 'relativeLayoutRowItem'");
    view = finder.findRequiredView(source, 2131624536, "field 'productDetailsRelatedProductsImageViewMetaData'");
    target.productDetailsRelatedProductsImageViewMetaData = finder.castView(view, 2131624536, "field 'productDetailsRelatedProductsImageViewMetaData'");
    view = finder.findRequiredView(source, 2131624544, "field 'productDetailsRelatedProductsTitle'");
    target.productDetailsRelatedProductsTitle = finder.castView(view, 2131624544, "field 'productDetailsRelatedProductsTitle'");
    view = finder.findRequiredView(source, 2131624545, "field 'productDetailsRelatedProductsPrices'");
    target.productDetailsRelatedProductsPrices = finder.castView(view, 2131624545, "field 'productDetailsRelatedProductsPrices'");
    view = finder.findRequiredView(source, 2131624546, "field 'productDetailsRelatedProductsMarketPrice'");
    target.productDetailsRelatedProductsMarketPrice = finder.castView(view, 2131624546, "field 'productDetailsRelatedProductsMarketPrice'");
    view = finder.findRequiredView(source, 2131624537, "field 'linearLayoutPercent'");
    target.linearLayoutPercent = finder.castView(view, 2131624537, "field 'linearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624538, "field 'appCompatTextViewPercent'");
    target.appCompatTextViewPercent = finder.castView(view, 2131624538, "field 'appCompatTextViewPercent'");
    view = finder.findRequiredView(source, 2131624539, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624539, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624540, "field 'appCompatTextViewPercentHappyHourDiscount'");
    target.appCompatTextViewPercentHappyHourDiscount = finder.castView(view, 2131624540, "field 'appCompatTextViewPercentHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624541, "field 'linearLayoutGiftCards'");
    target.linearLayoutGiftCards = finder.castView(view, 2131624541, "field 'linearLayoutGiftCards'");
    view = finder.findRequiredView(source, 2131624542, "field 'appCompatTextViewGiftCards'");
    target.appCompatTextViewGiftCards = finder.castView(view, 2131624542, "field 'appCompatTextViewGiftCards'");
  }

  @Override public void unbind(T target) {
    target.relativeLayoutRowItem = null;
    target.productDetailsRelatedProductsImageViewMetaData = null;
    target.productDetailsRelatedProductsTitle = null;
    target.productDetailsRelatedProductsPrices = null;
    target.productDetailsRelatedProductsMarketPrice = null;
    target.linearLayoutPercent = null;
    target.appCompatTextViewPercent = null;
    target.linearLayoutHappyHourDiscount = null;
    target.appCompatTextViewPercentHappyHourDiscount = null;
    target.linearLayoutGiftCards = null;
    target.appCompatTextViewGiftCards = null;
  }
}

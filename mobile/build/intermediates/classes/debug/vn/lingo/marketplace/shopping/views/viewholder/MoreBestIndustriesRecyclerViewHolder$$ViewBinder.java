// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MoreBestIndustriesRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.MoreBestIndustriesRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624440, "field 'cardView'");
    target.cardView = finder.castView(view, 2131624440, "field 'cardView'");
    view = finder.findRequiredView(source, 2131624442, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624442, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624452, "field 'marketPrice'");
    target.marketPrice = finder.castView(view, 2131624452, "field 'marketPrice'");
    view = finder.findRequiredView(source, 2131624451, "field 'price'");
    target.price = finder.castView(view, 2131624451, "field 'price'");
    view = finder.findRequiredView(source, 2131624444, "field 'percent'");
    target.percent = finder.castView(view, 2131624444, "field 'percent'");
    view = finder.findRequiredView(source, 2131624450, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624450, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624446, "field 'appCompatTextViewHappyHourDiscount'");
    target.appCompatTextViewHappyHourDiscount = finder.castView(view, 2131624446, "field 'appCompatTextViewHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624448, "field 'appCompatTextViewDiscountGiftCards'");
    target.appCompatTextViewDiscountGiftCards = finder.castView(view, 2131624448, "field 'appCompatTextViewDiscountGiftCards'");
    view = finder.findRequiredView(source, 2131624443, "field 'linearLayoutPercent'");
    target.linearLayoutPercent = finder.castView(view, 2131624443, "field 'linearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624445, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624445, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624447, "field 'linearLayoutDiscountGiftCards'");
    target.linearLayoutDiscountGiftCards = finder.castView(view, 2131624447, "field 'linearLayoutDiscountGiftCards'");
  }

  @Override public void unbind(T target) {
    target.cardView = null;
    target.imageView = null;
    target.marketPrice = null;
    target.price = null;
    target.percent = null;
    target.appCompatTextViewDescription = null;
    target.appCompatTextViewHappyHourDiscount = null;
    target.appCompatTextViewDiscountGiftCards = null;
    target.linearLayoutPercent = null;
    target.linearLayoutHappyHourDiscount = null;
    target.linearLayoutDiscountGiftCards = null;
  }
}

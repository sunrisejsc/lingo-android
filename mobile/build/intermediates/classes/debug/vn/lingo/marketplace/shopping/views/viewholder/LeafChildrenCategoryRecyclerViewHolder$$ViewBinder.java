// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LeafChildrenCategoryRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.LeafChildrenCategoryRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624339, "field 'relativeLayoutRowItem'");
    target.relativeLayoutRowItem = finder.castView(view, 2131624339, "field 'relativeLayoutRowItem'");
    view = finder.findRequiredView(source, 2131624341, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624341, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624344, "field 'leafChildrenCategoryProductionTitle'");
    target.leafChildrenCategoryProductionTitle = finder.castView(view, 2131624344, "field 'leafChildrenCategoryProductionTitle'");
    view = finder.findRequiredView(source, 2131624345, "field 'leafChildrenCategoriesPrices'");
    target.leafChildrenCategoriesPrices = finder.castView(view, 2131624345, "field 'leafChildrenCategoriesPrices'");
    view = finder.findRequiredView(source, 2131624346, "field 'leafChildrenCategoriesMarketPrice'");
    target.leafChildrenCategoriesMarketPrice = finder.castView(view, 2131624346, "field 'leafChildrenCategoriesMarketPrice'");
    view = finder.findRequiredView(source, 2131624347, "field 'leafChildrenCategoryRatingBar'");
    target.leafChildrenCategoryRatingBar = finder.castView(view, 2131624347, "field 'leafChildrenCategoryRatingBar'");
    view = finder.findRequiredView(source, 2131624342, "field 'leafChildrenCategoryLinearLayoutPercent'");
    target.leafChildrenCategoryLinearLayoutPercent = finder.castView(view, 2131624342, "field 'leafChildrenCategoryLinearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624343, "field 'leafChildrenCategoryPercent'");
    target.leafChildrenCategoryPercent = finder.castView(view, 2131624343, "field 'leafChildrenCategoryPercent'");
  }

  @Override public void unbind(T target) {
    target.relativeLayoutRowItem = null;
    target.imageView = null;
    target.leafChildrenCategoryProductionTitle = null;
    target.leafChildrenCategoriesPrices = null;
    target.leafChildrenCategoriesMarketPrice = null;
    target.leafChildrenCategoryRatingBar = null;
    target.leafChildrenCategoryLinearLayoutPercent = null;
    target.leafChildrenCategoryPercent = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProductDetailsImageMetadataViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.ProductDetailsImageMetadataViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624528, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624528, "field 'imageView'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
  }
}

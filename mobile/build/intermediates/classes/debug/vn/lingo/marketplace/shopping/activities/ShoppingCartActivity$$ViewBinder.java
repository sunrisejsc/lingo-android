// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ShoppingCartActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.ShoppingCartActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624108, "field 'recyclerView'");
    target.recyclerView = finder.castView(view, 2131624108, "field 'recyclerView'");
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624111, "field 'appCompatTextViewShoppingCartSummary'");
    target.appCompatTextViewShoppingCartSummary = finder.castView(view, 2131624111, "field 'appCompatTextViewShoppingCartSummary'");
    view = finder.findRequiredView(source, 2131624112, "field 'appCompatTextViewShoppingCartSubtotalValue'");
    target.appCompatTextViewShoppingCartSubtotalValue = finder.castView(view, 2131624112, "field 'appCompatTextViewShoppingCartSubtotalValue'");
    view = finder.findRequiredView(source, 2131624113, "field 'appCompatTextViewShoppingCartTotalValue'");
    target.appCompatTextViewShoppingCartTotalValue = finder.castView(view, 2131624113, "field 'appCompatTextViewShoppingCartTotalValue'");
    view = finder.findRequiredView(source, 2131624114, "field 'appCompatTextViewShoppingCartTotalQuantity'");
    target.appCompatTextViewShoppingCartTotalQuantity = finder.castView(view, 2131624114, "field 'appCompatTextViewShoppingCartTotalQuantity'");
    view = finder.findRequiredView(source, 2131624115, "field 'appCompatButtonShoppingCartBuy'");
    target.appCompatButtonShoppingCartBuy = finder.castView(view, 2131624115, "field 'appCompatButtonShoppingCartBuy'");
  }

  @Override public void unbind(T target) {
    target.recyclerView = null;
    target.toolbar = null;
    target.appCompatTextViewShoppingCartSummary = null;
    target.appCompatTextViewShoppingCartSubtotalValue = null;
    target.appCompatTextViewShoppingCartTotalValue = null;
    target.appCompatTextViewShoppingCartTotalQuantity = null;
    target.appCompatButtonShoppingCartBuy = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProductDetailsProductReviewsRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.ProductDetailsProductReviewsRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624531, "field 'appCompatTextViewCommentTitle'");
    target.appCompatTextViewCommentTitle = finder.castView(view, 2131624531, "field 'appCompatTextViewCommentTitle'");
    view = finder.findRequiredView(source, 2131624532, "field 'appCompatTextViewCommentContent'");
    target.appCompatTextViewCommentContent = finder.castView(view, 2131624532, "field 'appCompatTextViewCommentContent'");
    view = finder.findRequiredView(source, 2131624533, "field 'appCompatTextViewFullName'");
    target.appCompatTextViewFullName = finder.castView(view, 2131624533, "field 'appCompatTextViewFullName'");
    view = finder.findRequiredView(source, 2131624530, "field 'appCompatTextViewDateCreated'");
    target.appCompatTextViewDateCreated = finder.castView(view, 2131624530, "field 'appCompatTextViewDateCreated'");
    view = finder.findRequiredView(source, 2131624529, "field 'ratingBar'");
    target.ratingBar = finder.castView(view, 2131624529, "field 'ratingBar'");
  }

  @Override public void unbind(T target) {
    target.appCompatTextViewCommentTitle = null;
    target.appCompatTextViewCommentContent = null;
    target.appCompatTextViewFullName = null;
    target.appCompatTextViewDateCreated = null;
    target.ratingBar = null;
  }
}

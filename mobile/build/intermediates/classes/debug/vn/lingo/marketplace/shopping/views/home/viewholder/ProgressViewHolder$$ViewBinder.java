// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProgressViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.viewholder.ProgressViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624549, "field 'progressBarLoadMore'");
    target.progressBarLoadMore = finder.castView(view, 2131624549, "field 'progressBarLoadMore'");
  }

  @Override public void unbind(T target) {
    target.progressBarLoadMore = null;
  }
}

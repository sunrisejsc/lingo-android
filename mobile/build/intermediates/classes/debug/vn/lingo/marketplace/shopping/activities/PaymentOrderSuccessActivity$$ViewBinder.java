// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PaymentOrderSuccessActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.PaymentOrderSuccessActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624323, "field 'textViewCodeOrder'");
    target.textViewCodeOrder = finder.castView(view, 2131624323, "field 'textViewCodeOrder'");
    view = finder.findRequiredView(source, 2131624324, "field 'textViewCustomerName'");
    target.textViewCustomerName = finder.castView(view, 2131624324, "field 'textViewCustomerName'");
    view = finder.findRequiredView(source, 2131624325, "field 'textViewCustomerEmail'");
    target.textViewCustomerEmail = finder.castView(view, 2131624325, "field 'textViewCustomerEmail'");
    view = finder.findRequiredView(source, 2131624327, "field 'textViewCustomerAddress'");
    target.textViewCustomerAddress = finder.castView(view, 2131624327, "field 'textViewCustomerAddress'");
    view = finder.findRequiredView(source, 2131624328, "field 'textViewCustomerNameReceive'");
    target.textViewCustomerNameReceive = finder.castView(view, 2131624328, "field 'textViewCustomerNameReceive'");
    view = finder.findRequiredView(source, 2131624329, "field 'textViewCustomerEmailReceive'");
    target.textViewCustomerEmailReceive = finder.castView(view, 2131624329, "field 'textViewCustomerEmailReceive'");
    view = finder.findRequiredView(source, 2131624331, "field 'textViewCustomerAddressReceive'");
    target.textViewCustomerAddressReceive = finder.castView(view, 2131624331, "field 'textViewCustomerAddressReceive'");
    view = finder.findRequiredView(source, 2131624326, "field 'textViewCustomerPhone'");
    target.textViewCustomerPhone = finder.castView(view, 2131624326, "field 'textViewCustomerPhone'");
    view = finder.findRequiredView(source, 2131624330, "field 'textViewCustomerPhoneReceive'");
    target.textViewCustomerPhoneReceive = finder.castView(view, 2131624330, "field 'textViewCustomerPhoneReceive'");
    view = finder.findRequiredView(source, 2131624332, "field 'textViewPaymentMethod'");
    target.textViewPaymentMethod = finder.castView(view, 2131624332, "field 'textViewPaymentMethod'");
    view = finder.findRequiredView(source, 2131624334, "field 'buttonCallback'");
    target.buttonCallback = finder.castView(view, 2131624334, "field 'buttonCallback'");
    view = finder.findRequiredView(source, 2131624333, "field 'textViewLoginShipping'");
    target.textViewLoginShipping = finder.castView(view, 2131624333, "field 'textViewLoginShipping'");
  }

  @Override public void unbind(T target) {
    target.textViewCodeOrder = null;
    target.textViewCustomerName = null;
    target.textViewCustomerEmail = null;
    target.textViewCustomerAddress = null;
    target.textViewCustomerNameReceive = null;
    target.textViewCustomerEmailReceive = null;
    target.textViewCustomerAddressReceive = null;
    target.textViewCustomerPhone = null;
    target.textViewCustomerPhoneReceive = null;
    target.textViewPaymentMethod = null;
    target.buttonCallback = null;
    target.textViewLoginShipping = null;
  }
}

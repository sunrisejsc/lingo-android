// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PromotionProgramViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.PromotionProgramViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624593, "field 'relativeLayout'");
    target.relativeLayout = finder.castView(view, 2131624593, "field 'relativeLayout'");
    view = finder.findRequiredView(source, 2131624594, "field 'imageViewPromotionProgram'");
    target.imageViewPromotionProgram = finder.castView(view, 2131624594, "field 'imageViewPromotionProgram'");
  }

  @Override public void unbind(T target) {
    target.relativeLayout = null;
    target.imageViewPromotionProgram = null;
  }
}

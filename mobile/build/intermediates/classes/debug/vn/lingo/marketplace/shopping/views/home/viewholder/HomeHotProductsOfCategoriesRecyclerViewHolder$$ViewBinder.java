// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomeHotProductsOfCategoriesRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.viewholder.HomeHotProductsOfCategoriesRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624248, "field 'appCompatTextView'");
    target.appCompatTextView = finder.castView(view, 2131624248, "field 'appCompatTextView'");
    view = finder.findRequiredView(source, 2131624247, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624247, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624246, "field 'cardViewHotProductsOfCategoriesItemId'");
    target.cardViewHotProductsOfCategoriesItemId = finder.castView(view, 2131624246, "field 'cardViewHotProductsOfCategoriesItemId'");
  }

  @Override public void unbind(T target) {
    target.appCompatTextView = null;
    target.imageView = null;
    target.cardViewHotProductsOfCategoriesItemId = null;
  }
}

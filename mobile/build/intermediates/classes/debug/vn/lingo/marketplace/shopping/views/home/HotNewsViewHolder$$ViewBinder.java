// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HotNewsViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.HotNewsViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624204, "field 'view'");
    target.view = view;
    view = finder.findRequiredView(source, 2131624203, "field 'appCompatTextViewHotNews'");
    target.appCompatTextViewHotNews = finder.castView(view, 2131624203, "field 'appCompatTextViewHotNews'");
    view = finder.findRequiredView(source, 2131624202, "field 'imageViewAvatar'");
    target.imageViewAvatar = finder.castView(view, 2131624202, "field 'imageViewAvatar'");
  }

  @Override public void unbind(T target) {
    target.view = null;
    target.appCompatTextViewHotNews = null;
    target.imageViewAvatar = null;
  }
}

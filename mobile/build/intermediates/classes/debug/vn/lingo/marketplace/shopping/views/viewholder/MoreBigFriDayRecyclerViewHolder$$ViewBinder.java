// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MoreBigFriDayRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.MoreBigFriDayRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624453, "field 'cardView'");
    target.cardView = finder.castView(view, 2131624453, "field 'cardView'");
    view = finder.findRequiredView(source, 2131624455, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624455, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624465, "field 'marketPrice'");
    target.marketPrice = finder.castView(view, 2131624465, "field 'marketPrice'");
    view = finder.findRequiredView(source, 2131624464, "field 'price'");
    target.price = finder.castView(view, 2131624464, "field 'price'");
    view = finder.findRequiredView(source, 2131624457, "field 'percent'");
    target.percent = finder.castView(view, 2131624457, "field 'percent'");
    view = finder.findRequiredView(source, 2131624463, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624463, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624459, "field 'appCompatTextViewHappyHourDiscount'");
    target.appCompatTextViewHappyHourDiscount = finder.castView(view, 2131624459, "field 'appCompatTextViewHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624461, "field 'appCompatTextViewDiscountGiftCards'");
    target.appCompatTextViewDiscountGiftCards = finder.castView(view, 2131624461, "field 'appCompatTextViewDiscountGiftCards'");
    view = finder.findRequiredView(source, 2131624456, "field 'linearLayoutPercent'");
    target.linearLayoutPercent = finder.castView(view, 2131624456, "field 'linearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624458, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624458, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624460, "field 'linearLayoutDiscountGiftCards'");
    target.linearLayoutDiscountGiftCards = finder.castView(view, 2131624460, "field 'linearLayoutDiscountGiftCards'");
  }

  @Override public void unbind(T target) {
    target.cardView = null;
    target.imageView = null;
    target.marketPrice = null;
    target.price = null;
    target.percent = null;
    target.appCompatTextViewDescription = null;
    target.appCompatTextViewHappyHourDiscount = null;
    target.appCompatTextViewDiscountGiftCards = null;
    target.linearLayoutPercent = null;
    target.linearLayoutHappyHourDiscount = null;
    target.linearLayoutDiscountGiftCards = null;
  }
}

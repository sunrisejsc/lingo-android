// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LingoHomePageBigFridayViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.viewholder.LingoHomePageBigFridayViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624380, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624380, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624381, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624381, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624382, "field 'appCompatTextViewPrice'");
    target.appCompatTextViewPrice = finder.castView(view, 2131624382, "field 'appCompatTextViewPrice'");
    view = finder.findRequiredView(source, 2131624383, "field 'appCompatTextViewMarketPrice'");
    target.appCompatTextViewMarketPrice = finder.castView(view, 2131624383, "field 'appCompatTextViewMarketPrice'");
    view = finder.findRequiredView(source, 2131624384, "field 'appCompatTextViewPercent'");
    target.appCompatTextViewPercent = finder.castView(view, 2131624384, "field 'appCompatTextViewPercent'");
    view = finder.findRequiredView(source, 2131624386, "field 'appCompatTextViewHappyHourDiscount'");
    target.appCompatTextViewHappyHourDiscount = finder.castView(view, 2131624386, "field 'appCompatTextViewHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624388, "field 'appCompatTextViewDiscountGiftCard'");
    target.appCompatTextViewDiscountGiftCard = finder.castView(view, 2131624388, "field 'appCompatTextViewDiscountGiftCard'");
    view = finder.findRequiredView(source, 2131624389, "field 'view'");
    target.view = view;
    view = finder.findRequiredView(source, 2131624385, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624385, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624387, "field 'linearLayoutDiscountGiftCard'");
    target.linearLayoutDiscountGiftCard = finder.castView(view, 2131624387, "field 'linearLayoutDiscountGiftCard'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.appCompatTextViewDescription = null;
    target.appCompatTextViewPrice = null;
    target.appCompatTextViewMarketPrice = null;
    target.appCompatTextViewPercent = null;
    target.appCompatTextViewHappyHourDiscount = null;
    target.appCompatTextViewDiscountGiftCard = null;
    target.view = null;
    target.linearLayoutHappyHourDiscount = null;
    target.linearLayoutDiscountGiftCard = null;
  }
}

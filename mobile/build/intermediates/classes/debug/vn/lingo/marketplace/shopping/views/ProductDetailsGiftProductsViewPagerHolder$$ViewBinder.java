// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProductDetailsGiftProductsViewPagerHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.ProductDetailsGiftProductsViewPagerHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624524, "field 'view'");
    target.view = view;
    view = finder.findRequiredView(source, 2131624520, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624520, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624521, "field 'appCompatTextViewGiftName'");
    target.appCompatTextViewGiftName = finder.castView(view, 2131624521, "field 'appCompatTextViewGiftName'");
    view = finder.findRequiredView(source, 2131624522, "field 'appCompatTextViewProductName'");
    target.appCompatTextViewProductName = finder.castView(view, 2131624522, "field 'appCompatTextViewProductName'");
    view = finder.findRequiredView(source, 2131624523, "field 'appCompatTextViewProductPrice'");
    target.appCompatTextViewProductPrice = finder.castView(view, 2131624523, "field 'appCompatTextViewProductPrice'");
  }

  @Override public void unbind(T target) {
    target.view = null;
    target.imageView = null;
    target.appCompatTextViewGiftName = null;
    target.appCompatTextViewProductName = null;
    target.appCompatTextViewProductPrice = null;
  }
}

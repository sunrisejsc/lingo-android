// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.adapter;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AbstractRecyclerViewFooterAdapter$ProgressViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.adapter.AbstractRecyclerViewFooterAdapter.ProgressViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624549, "field 'progressBar'");
    target.progressBar = finder.castView(view, 2131624549, "field 'progressBar'");
  }

  @Override public void unbind(T target) {
    target.progressBar = null;
  }
}

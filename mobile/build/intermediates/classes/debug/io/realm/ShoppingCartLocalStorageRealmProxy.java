package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.ShoppingCartLocalStorage;

public class ShoppingCartLocalStorageRealmProxy extends ShoppingCartLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_CARTID;
    private static long INDEX_ORDERQUANTITY;
    private static long INDEX_AVATAR;
    private static long INDEX_MARKETPRICE;
    private static long INDEX_PRODUCTPRICE;
    private static long INDEX_PRODUCTNAME;
    private static long INDEX_ISORDERED;
    private static long INDEX_MARKETPRICEFORMAT;
    private static long INDEX_PRODUCTPRICEFORMAT;
    private static long INDEX_PRODUCTPRICEDISCOUNTFORMAT;
    private static long INDEX_SKU;
    private static long INDEX_TIMESTAMP;
    private static long INDEX_VALUE;
    private static long INDEX_BIGTYPE;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("cartID");
        fieldNames.add("orderQuantity");
        fieldNames.add("avatar");
        fieldNames.add("marketPrice");
        fieldNames.add("productPrice");
        fieldNames.add("productName");
        fieldNames.add("isOrdered");
        fieldNames.add("marketPriceFormat");
        fieldNames.add("productPriceFormat");
        fieldNames.add("productPriceDiscountFormat");
        fieldNames.add("sku");
        fieldNames.add("timeStamp");
        fieldNames.add("value");
        fieldNames.add("bigType");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public int getCartID() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_CARTID);
    }

    @Override
    public void setCartID(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_CARTID, (long) value);
    }

    @Override
    public int getOrderQuantity() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_ORDERQUANTITY);
    }

    @Override
    public void setOrderQuantity(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_ORDERQUANTITY, (long) value);
    }

    @Override
    public String getAvatar() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_AVATAR);
    }

    @Override
    public void setAvatar(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_AVATAR);
            return;
        }
        row.setString(INDEX_AVATAR, (String) value);
    }

    @Override
    public int getMarketPrice() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_MARKETPRICE);
    }

    @Override
    public void setMarketPrice(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_MARKETPRICE, (long) value);
    }

    @Override
    public int getProductPrice() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_PRODUCTPRICE);
    }

    @Override
    public void setProductPrice(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_PRODUCTPRICE, (long) value);
    }

    @Override
    public String getProductName() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTNAME);
    }

    @Override
    public void setProductName(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTNAME);
            return;
        }
        row.setString(INDEX_PRODUCTNAME, (String) value);
    }

    @Override
    public int getIsOrdered() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_ISORDERED);
    }

    @Override
    public void setIsOrdered(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_ISORDERED, (long) value);
    }

    @Override
    public String getMarketPriceFormat() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MARKETPRICEFORMAT);
    }

    @Override
    public void setMarketPriceFormat(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MARKETPRICEFORMAT);
            return;
        }
        row.setString(INDEX_MARKETPRICEFORMAT, (String) value);
    }

    @Override
    public String getProductPriceFormat() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTPRICEFORMAT);
    }

    @Override
    public void setProductPriceFormat(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTPRICEFORMAT);
            return;
        }
        row.setString(INDEX_PRODUCTPRICEFORMAT, (String) value);
    }

    @Override
    public String getProductPriceDiscountFormat() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTPRICEDISCOUNTFORMAT);
    }

    @Override
    public void setProductPriceDiscountFormat(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTPRICEDISCOUNTFORMAT);
            return;
        }
        row.setString(INDEX_PRODUCTPRICEDISCOUNTFORMAT, (String) value);
    }

    @Override
    public String getSku() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_SKU);
    }

    @Override
    public void setSku(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_SKU);
            return;
        }
        row.setString(INDEX_SKU, (String) value);
    }

    @Override
    public long getTimeStamp() {
        realm.checkIfValid();
        return (long) row.getLong(INDEX_TIMESTAMP);
    }

    @Override
    public void setTimeStamp(long value) {
        realm.checkIfValid();
        row.setLong(INDEX_TIMESTAMP, (long) value);
    }

    @Override
    public int getValue() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_VALUE);
    }

    @Override
    public void setValue(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_VALUE, (long) value);
    }

    @Override
    public int getBigType() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_BIGTYPE);
    }

    @Override
    public void setBigType(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_BIGTYPE, (long) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_ShoppingCartLocalStorage")) {
            Table table = transaction.getTable("class_ShoppingCartLocalStorage");
            table.addColumn(ColumnType.INTEGER, "cartID", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "orderQuantity", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "avatar", Table.NULLABLE);
            table.addColumn(ColumnType.INTEGER, "marketPrice", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "productPrice", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "productName", Table.NULLABLE);
            table.addColumn(ColumnType.INTEGER, "isOrdered", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "marketPriceFormat", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productPriceFormat", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productPriceDiscountFormat", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "sku", Table.NULLABLE);
            table.addColumn(ColumnType.INTEGER, "timeStamp", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "value", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "bigType", Table.NOT_NULLABLE);
            table.addSearchIndex(table.getColumnIndex("cartID"));
            table.setPrimaryKey("cartID");
            return table;
        }
        return transaction.getTable("class_ShoppingCartLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_ShoppingCartLocalStorage")) {
            Table table = transaction.getTable("class_ShoppingCartLocalStorage");
            if (table.getColumnCount() != 14) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 14 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 14; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type ShoppingCartLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_CARTID = table.getColumnIndex("cartID");
            INDEX_ORDERQUANTITY = table.getColumnIndex("orderQuantity");
            INDEX_AVATAR = table.getColumnIndex("avatar");
            INDEX_MARKETPRICE = table.getColumnIndex("marketPrice");
            INDEX_PRODUCTPRICE = table.getColumnIndex("productPrice");
            INDEX_PRODUCTNAME = table.getColumnIndex("productName");
            INDEX_ISORDERED = table.getColumnIndex("isOrdered");
            INDEX_MARKETPRICEFORMAT = table.getColumnIndex("marketPriceFormat");
            INDEX_PRODUCTPRICEFORMAT = table.getColumnIndex("productPriceFormat");
            INDEX_PRODUCTPRICEDISCOUNTFORMAT = table.getColumnIndex("productPriceDiscountFormat");
            INDEX_SKU = table.getColumnIndex("sku");
            INDEX_TIMESTAMP = table.getColumnIndex("timeStamp");
            INDEX_VALUE = table.getColumnIndex("value");
            INDEX_BIGTYPE = table.getColumnIndex("bigType");

            if (!columnTypes.containsKey("cartID")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'cartID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("cartID") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'cartID' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_CARTID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'cartID' does support null values in the existing Realm file. Use corresponding boxed type for field 'cartID' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("cartID")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'cartID' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("cartID"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'cartID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("orderQuantity")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'orderQuantity' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("orderQuantity") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'orderQuantity' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ORDERQUANTITY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'orderQuantity' does support null values in the existing Realm file. Use corresponding boxed type for field 'orderQuantity' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("avatar")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'avatar' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("avatar") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'avatar' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_AVATAR)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'avatar' is required. Either set @Required to field 'avatar' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("marketPrice")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'marketPrice' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("marketPrice") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'marketPrice' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_MARKETPRICE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'marketPrice' does support null values in the existing Realm file. Use corresponding boxed type for field 'marketPrice' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("productPrice")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPrice' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPrice") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'productPrice' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_PRODUCTPRICE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPrice' does support null values in the existing Realm file. Use corresponding boxed type for field 'productPrice' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("productName")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productName") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productName' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTNAME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productName' is required. Either set @Required to field 'productName' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("isOrdered")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'isOrdered' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("isOrdered") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'isOrdered' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ISORDERED)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'isOrdered' does support null values in the existing Realm file. Use corresponding boxed type for field 'isOrdered' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("marketPriceFormat")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'marketPriceFormat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("marketPriceFormat") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'marketPriceFormat' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MARKETPRICEFORMAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'marketPriceFormat' is required. Either set @Required to field 'marketPriceFormat' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productPriceFormat")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPriceFormat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPriceFormat") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productPriceFormat' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTPRICEFORMAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPriceFormat' is required. Either set @Required to field 'productPriceFormat' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productPriceDiscountFormat")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPriceDiscountFormat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPriceDiscountFormat") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productPriceDiscountFormat' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTPRICEDISCOUNTFORMAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPriceDiscountFormat' is required. Either set @Required to field 'productPriceDiscountFormat' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("sku")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'sku' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("sku") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'sku' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_SKU)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'sku' is required. Either set @Required to field 'sku' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("timeStamp")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'timeStamp' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("timeStamp") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'long' for field 'timeStamp' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_TIMESTAMP)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'timeStamp' does support null values in the existing Realm file. Use corresponding boxed type for field 'timeStamp' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("value")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'value' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("value") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'value' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_VALUE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'value' does support null values in the existing Realm file. Use corresponding boxed type for field 'value' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("bigType")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'bigType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("bigType") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'bigType' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_BIGTYPE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'bigType' does support null values in the existing Realm file. Use corresponding boxed type for field 'bigType' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The ShoppingCartLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_ShoppingCartLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static ShoppingCartLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        ShoppingCartLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(ShoppingCartLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("cartID")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("cartID"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new ShoppingCartLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(ShoppingCartLocalStorage.class);
        }
        if (json.has("cartID")) {
            if (json.isNull("cartID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field cartID to null.");
            } else {
                obj.setCartID((int) json.getInt("cartID"));
            }
        }
        if (json.has("orderQuantity")) {
            if (json.isNull("orderQuantity")) {
                throw new IllegalArgumentException("Trying to set non-nullable field orderQuantity to null.");
            } else {
                obj.setOrderQuantity((int) json.getInt("orderQuantity"));
            }
        }
        if (json.has("avatar")) {
            if (json.isNull("avatar")) {
                obj.setAvatar(null);
            } else {
                obj.setAvatar((String) json.getString("avatar"));
            }
        }
        if (json.has("marketPrice")) {
            if (json.isNull("marketPrice")) {
                throw new IllegalArgumentException("Trying to set non-nullable field marketPrice to null.");
            } else {
                obj.setMarketPrice((int) json.getInt("marketPrice"));
            }
        }
        if (json.has("productPrice")) {
            if (json.isNull("productPrice")) {
                throw new IllegalArgumentException("Trying to set non-nullable field productPrice to null.");
            } else {
                obj.setProductPrice((int) json.getInt("productPrice"));
            }
        }
        if (json.has("productName")) {
            if (json.isNull("productName")) {
                obj.setProductName(null);
            } else {
                obj.setProductName((String) json.getString("productName"));
            }
        }
        if (json.has("isOrdered")) {
            if (json.isNull("isOrdered")) {
                throw new IllegalArgumentException("Trying to set non-nullable field isOrdered to null.");
            } else {
                obj.setIsOrdered((int) json.getInt("isOrdered"));
            }
        }
        if (json.has("marketPriceFormat")) {
            if (json.isNull("marketPriceFormat")) {
                obj.setMarketPriceFormat(null);
            } else {
                obj.setMarketPriceFormat((String) json.getString("marketPriceFormat"));
            }
        }
        if (json.has("productPriceFormat")) {
            if (json.isNull("productPriceFormat")) {
                obj.setProductPriceFormat(null);
            } else {
                obj.setProductPriceFormat((String) json.getString("productPriceFormat"));
            }
        }
        if (json.has("productPriceDiscountFormat")) {
            if (json.isNull("productPriceDiscountFormat")) {
                obj.setProductPriceDiscountFormat(null);
            } else {
                obj.setProductPriceDiscountFormat((String) json.getString("productPriceDiscountFormat"));
            }
        }
        if (json.has("sku")) {
            if (json.isNull("sku")) {
                obj.setSku(null);
            } else {
                obj.setSku((String) json.getString("sku"));
            }
        }
        if (json.has("timeStamp")) {
            if (json.isNull("timeStamp")) {
                throw new IllegalArgumentException("Trying to set non-nullable field timeStamp to null.");
            } else {
                obj.setTimeStamp((long) json.getLong("timeStamp"));
            }
        }
        if (json.has("value")) {
            if (json.isNull("value")) {
                throw new IllegalArgumentException("Trying to set non-nullable field value to null.");
            } else {
                obj.setValue((int) json.getInt("value"));
            }
        }
        if (json.has("bigType")) {
            if (json.isNull("bigType")) {
                throw new IllegalArgumentException("Trying to set non-nullable field bigType to null.");
            } else {
                obj.setBigType((int) json.getInt("bigType"));
            }
        }
        return obj;
    }

    public static ShoppingCartLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        ShoppingCartLocalStorage obj = realm.createObject(ShoppingCartLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("cartID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field cartID to null.");
                } else {
                    obj.setCartID((int) reader.nextInt());
                }
            } else if (name.equals("orderQuantity")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field orderQuantity to null.");
                } else {
                    obj.setOrderQuantity((int) reader.nextInt());
                }
            } else if (name.equals("avatar")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setAvatar(null);
                } else {
                    obj.setAvatar((String) reader.nextString());
                }
            } else if (name.equals("marketPrice")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field marketPrice to null.");
                } else {
                    obj.setMarketPrice((int) reader.nextInt());
                }
            } else if (name.equals("productPrice")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field productPrice to null.");
                } else {
                    obj.setProductPrice((int) reader.nextInt());
                }
            } else if (name.equals("productName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductName(null);
                } else {
                    obj.setProductName((String) reader.nextString());
                }
            } else if (name.equals("isOrdered")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field isOrdered to null.");
                } else {
                    obj.setIsOrdered((int) reader.nextInt());
                }
            } else if (name.equals("marketPriceFormat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMarketPriceFormat(null);
                } else {
                    obj.setMarketPriceFormat((String) reader.nextString());
                }
            } else if (name.equals("productPriceFormat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductPriceFormat(null);
                } else {
                    obj.setProductPriceFormat((String) reader.nextString());
                }
            } else if (name.equals("productPriceDiscountFormat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductPriceDiscountFormat(null);
                } else {
                    obj.setProductPriceDiscountFormat((String) reader.nextString());
                }
            } else if (name.equals("sku")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setSku(null);
                } else {
                    obj.setSku((String) reader.nextString());
                }
            } else if (name.equals("timeStamp")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field timeStamp to null.");
                } else {
                    obj.setTimeStamp((long) reader.nextLong());
                }
            } else if (name.equals("value")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field value to null.");
                } else {
                    obj.setValue((int) reader.nextInt());
                }
            } else if (name.equals("bigType")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field bigType to null.");
                } else {
                    obj.setBigType((int) reader.nextInt());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static ShoppingCartLocalStorage copyOrUpdate(Realm realm, ShoppingCartLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        ShoppingCartLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(ShoppingCartLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getCartID());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new ShoppingCartLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static ShoppingCartLocalStorage copy(Realm realm, ShoppingCartLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        ShoppingCartLocalStorage realmObject = realm.createObject(ShoppingCartLocalStorage.class, newObject.getCartID());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setCartID(newObject.getCartID());
        realmObject.setOrderQuantity(newObject.getOrderQuantity());
        realmObject.setAvatar(newObject.getAvatar());
        realmObject.setMarketPrice(newObject.getMarketPrice());
        realmObject.setProductPrice(newObject.getProductPrice());
        realmObject.setProductName(newObject.getProductName());
        realmObject.setIsOrdered(newObject.getIsOrdered());
        realmObject.setMarketPriceFormat(newObject.getMarketPriceFormat());
        realmObject.setProductPriceFormat(newObject.getProductPriceFormat());
        realmObject.setProductPriceDiscountFormat(newObject.getProductPriceDiscountFormat());
        realmObject.setSku(newObject.getSku());
        realmObject.setTimeStamp(newObject.getTimeStamp());
        realmObject.setValue(newObject.getValue());
        realmObject.setBigType(newObject.getBigType());
        return realmObject;
    }

    static ShoppingCartLocalStorage update(Realm realm, ShoppingCartLocalStorage realmObject, ShoppingCartLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setOrderQuantity(newObject.getOrderQuantity());
        realmObject.setAvatar(newObject.getAvatar());
        realmObject.setMarketPrice(newObject.getMarketPrice());
        realmObject.setProductPrice(newObject.getProductPrice());
        realmObject.setProductName(newObject.getProductName());
        realmObject.setIsOrdered(newObject.getIsOrdered());
        realmObject.setMarketPriceFormat(newObject.getMarketPriceFormat());
        realmObject.setProductPriceFormat(newObject.getProductPriceFormat());
        realmObject.setProductPriceDiscountFormat(newObject.getProductPriceDiscountFormat());
        realmObject.setSku(newObject.getSku());
        realmObject.setTimeStamp(newObject.getTimeStamp());
        realmObject.setValue(newObject.getValue());
        realmObject.setBigType(newObject.getBigType());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("ShoppingCartLocalStorage = [");
        stringBuilder.append("{cartID:");
        stringBuilder.append(getCartID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{orderQuantity:");
        stringBuilder.append(getOrderQuantity());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{avatar:");
        stringBuilder.append(getAvatar() != null ? getAvatar() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{marketPrice:");
        stringBuilder.append(getMarketPrice());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPrice:");
        stringBuilder.append(getProductPrice());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productName:");
        stringBuilder.append(getProductName() != null ? getProductName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{isOrdered:");
        stringBuilder.append(getIsOrdered());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{marketPriceFormat:");
        stringBuilder.append(getMarketPriceFormat() != null ? getMarketPriceFormat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPriceFormat:");
        stringBuilder.append(getProductPriceFormat() != null ? getProductPriceFormat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPriceDiscountFormat:");
        stringBuilder.append(getProductPriceDiscountFormat() != null ? getProductPriceDiscountFormat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{sku:");
        stringBuilder.append(getSku() != null ? getSku() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{timeStamp:");
        stringBuilder.append(getTimeStamp());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{value:");
        stringBuilder.append(getValue());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{bigType:");
        stringBuilder.append(getBigType());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCartLocalStorageRealmProxy aShoppingCartLocalStorage = (ShoppingCartLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aShoppingCartLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aShoppingCartLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aShoppingCartLocalStorage.row.getIndex()) return false;

        return true;
    }

}

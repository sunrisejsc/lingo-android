package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.OrderHistoryLocalStorage;

public class OrderHistoryLocalStorageRealmProxy extends OrderHistoryLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_ORDERNUMBERID;
    private static long INDEX_ORDERNUMBER;
    private static long INDEX_ORDERNUMBERENDPOINT;
    private static long INDEX_TIMESTAMP;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("orderNumberId");
        fieldNames.add("orderNumber");
        fieldNames.add("orderNumberEndpoint");
        fieldNames.add("timeStamp");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public String getOrderNumberId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ORDERNUMBERID);
    }

    @Override
    public void setOrderNumberId(String value) {
        realm.checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field orderNumberId to null.");
        }
        row.setString(INDEX_ORDERNUMBERID, (String) value);
    }

    @Override
    public String getOrderNumber() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ORDERNUMBER);
    }

    @Override
    public void setOrderNumber(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ORDERNUMBER);
            return;
        }
        row.setString(INDEX_ORDERNUMBER, (String) value);
    }

    @Override
    public String getOrderNumberEndpoint() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ORDERNUMBERENDPOINT);
    }

    @Override
    public void setOrderNumberEndpoint(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ORDERNUMBERENDPOINT);
            return;
        }
        row.setString(INDEX_ORDERNUMBERENDPOINT, (String) value);
    }

    @Override
    public long getTimeStamp() {
        realm.checkIfValid();
        return (long) row.getLong(INDEX_TIMESTAMP);
    }

    @Override
    public void setTimeStamp(long value) {
        realm.checkIfValid();
        row.setLong(INDEX_TIMESTAMP, (long) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_OrderHistoryLocalStorage")) {
            Table table = transaction.getTable("class_OrderHistoryLocalStorage");
            table.addColumn(ColumnType.STRING, "orderNumberId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "orderNumber", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "orderNumberEndpoint", Table.NULLABLE);
            table.addColumn(ColumnType.INTEGER, "timeStamp", Table.NOT_NULLABLE);
            table.addSearchIndex(table.getColumnIndex("orderNumberId"));
            table.setPrimaryKey("orderNumberId");
            return table;
        }
        return transaction.getTable("class_OrderHistoryLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_OrderHistoryLocalStorage")) {
            Table table = transaction.getTable("class_OrderHistoryLocalStorage");
            if (table.getColumnCount() != 4) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 4 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 4; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type OrderHistoryLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_ORDERNUMBERID = table.getColumnIndex("orderNumberId");
            INDEX_ORDERNUMBER = table.getColumnIndex("orderNumber");
            INDEX_ORDERNUMBERENDPOINT = table.getColumnIndex("orderNumberEndpoint");
            INDEX_TIMESTAMP = table.getColumnIndex("timeStamp");

            if (!columnTypes.containsKey("orderNumberId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'orderNumberId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("orderNumberId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'orderNumberId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ORDERNUMBERID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'orderNumberId' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'orderNumberId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("orderNumberId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'orderNumberId' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("orderNumberId"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'orderNumberId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("orderNumber")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'orderNumber' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("orderNumber") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'orderNumber' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ORDERNUMBER)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'orderNumber' is required. Either set @Required to field 'orderNumber' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("orderNumberEndpoint")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'orderNumberEndpoint' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("orderNumberEndpoint") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'orderNumberEndpoint' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ORDERNUMBERENDPOINT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'orderNumberEndpoint' is required. Either set @Required to field 'orderNumberEndpoint' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("timeStamp")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'timeStamp' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("timeStamp") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'long' for field 'timeStamp' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_TIMESTAMP)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'timeStamp' does support null values in the existing Realm file. Use corresponding boxed type for field 'timeStamp' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The OrderHistoryLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_OrderHistoryLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static OrderHistoryLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        OrderHistoryLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(OrderHistoryLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("orderNumberId")) {
                long rowIndex = table.findFirstString(pkColumnIndex, json.getString("orderNumberId"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new OrderHistoryLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(OrderHistoryLocalStorage.class);
        }
        if (json.has("orderNumberId")) {
            if (json.isNull("orderNumberId")) {
                obj.setOrderNumberId(null);
            } else {
                obj.setOrderNumberId((String) json.getString("orderNumberId"));
            }
        }
        if (json.has("orderNumber")) {
            if (json.isNull("orderNumber")) {
                obj.setOrderNumber(null);
            } else {
                obj.setOrderNumber((String) json.getString("orderNumber"));
            }
        }
        if (json.has("orderNumberEndpoint")) {
            if (json.isNull("orderNumberEndpoint")) {
                obj.setOrderNumberEndpoint(null);
            } else {
                obj.setOrderNumberEndpoint((String) json.getString("orderNumberEndpoint"));
            }
        }
        if (json.has("timeStamp")) {
            if (json.isNull("timeStamp")) {
                throw new IllegalArgumentException("Trying to set non-nullable field timeStamp to null.");
            } else {
                obj.setTimeStamp((long) json.getLong("timeStamp"));
            }
        }
        return obj;
    }

    public static OrderHistoryLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        OrderHistoryLocalStorage obj = realm.createObject(OrderHistoryLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("orderNumberId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setOrderNumberId(null);
                } else {
                    obj.setOrderNumberId((String) reader.nextString());
                }
            } else if (name.equals("orderNumber")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setOrderNumber(null);
                } else {
                    obj.setOrderNumber((String) reader.nextString());
                }
            } else if (name.equals("orderNumberEndpoint")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setOrderNumberEndpoint(null);
                } else {
                    obj.setOrderNumberEndpoint((String) reader.nextString());
                }
            } else if (name.equals("timeStamp")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field timeStamp to null.");
                } else {
                    obj.setTimeStamp((long) reader.nextLong());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static OrderHistoryLocalStorage copyOrUpdate(Realm realm, OrderHistoryLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        OrderHistoryLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(OrderHistoryLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (object.getOrderNumberId() == null) {
                throw new IllegalArgumentException("Primary key value must not be null.");
            }
            long rowIndex = table.findFirstString(pkColumnIndex, object.getOrderNumberId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new OrderHistoryLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static OrderHistoryLocalStorage copy(Realm realm, OrderHistoryLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        OrderHistoryLocalStorage realmObject = realm.createObject(OrderHistoryLocalStorage.class, newObject.getOrderNumberId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setOrderNumberId(newObject.getOrderNumberId());
        realmObject.setOrderNumber(newObject.getOrderNumber());
        realmObject.setOrderNumberEndpoint(newObject.getOrderNumberEndpoint());
        realmObject.setTimeStamp(newObject.getTimeStamp());
        return realmObject;
    }

    static OrderHistoryLocalStorage update(Realm realm, OrderHistoryLocalStorage realmObject, OrderHistoryLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setOrderNumber(newObject.getOrderNumber());
        realmObject.setOrderNumberEndpoint(newObject.getOrderNumberEndpoint());
        realmObject.setTimeStamp(newObject.getTimeStamp());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("OrderHistoryLocalStorage = [");
        stringBuilder.append("{orderNumberId:");
        stringBuilder.append(getOrderNumberId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{orderNumber:");
        stringBuilder.append(getOrderNumber() != null ? getOrderNumber() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{orderNumberEndpoint:");
        stringBuilder.append(getOrderNumberEndpoint() != null ? getOrderNumberEndpoint() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{timeStamp:");
        stringBuilder.append(getTimeStamp());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderHistoryLocalStorageRealmProxy aOrderHistoryLocalStorage = (OrderHistoryLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aOrderHistoryLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aOrderHistoryLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aOrderHistoryLocalStorage.row.getIndex()) return false;

        return true;
    }

}

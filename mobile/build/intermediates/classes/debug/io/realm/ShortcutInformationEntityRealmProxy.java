package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.ShortcutInformationEntity;

public class ShortcutInformationEntityRealmProxy extends ShortcutInformationEntity
    implements RealmObjectProxy {

    private static long INDEX_SHORTCUTID;
    private static long INDEX_SHORTCUT;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("shortcutId");
        fieldNames.add("shortcut");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public int getShortcutId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_SHORTCUTID);
    }

    @Override
    public void setShortcutId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_SHORTCUTID, (long) value);
    }

    @Override
    public int getShortcut() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_SHORTCUT);
    }

    @Override
    public void setShortcut(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_SHORTCUT, (long) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_ShortcutInformationEntity")) {
            Table table = transaction.getTable("class_ShortcutInformationEntity");
            table.addColumn(ColumnType.INTEGER, "shortcutId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "shortcut", Table.NOT_NULLABLE);
            table.addSearchIndex(table.getColumnIndex("shortcutId"));
            table.setPrimaryKey("shortcutId");
            return table;
        }
        return transaction.getTable("class_ShortcutInformationEntity");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_ShortcutInformationEntity")) {
            Table table = transaction.getTable("class_ShortcutInformationEntity");
            if (table.getColumnCount() != 2) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 2 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 2; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type ShortcutInformationEntity");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_SHORTCUTID = table.getColumnIndex("shortcutId");
            INDEX_SHORTCUT = table.getColumnIndex("shortcut");

            if (!columnTypes.containsKey("shortcutId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'shortcutId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("shortcutId") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'shortcutId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_SHORTCUTID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'shortcutId' does support null values in the existing Realm file. Use corresponding boxed type for field 'shortcutId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("shortcutId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'shortcutId' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("shortcutId"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'shortcutId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("shortcut")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'shortcut' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("shortcut") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'shortcut' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_SHORTCUT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'shortcut' does support null values in the existing Realm file. Use corresponding boxed type for field 'shortcut' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The ShortcutInformationEntity class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_ShortcutInformationEntity";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static ShortcutInformationEntity createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        ShortcutInformationEntity obj = null;
        if (update) {
            Table table = realm.getTable(ShortcutInformationEntity.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("shortcutId")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("shortcutId"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new ShortcutInformationEntityRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(ShortcutInformationEntity.class);
        }
        if (json.has("shortcutId")) {
            if (json.isNull("shortcutId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field shortcutId to null.");
            } else {
                obj.setShortcutId((int) json.getInt("shortcutId"));
            }
        }
        if (json.has("shortcut")) {
            if (json.isNull("shortcut")) {
                throw new IllegalArgumentException("Trying to set non-nullable field shortcut to null.");
            } else {
                obj.setShortcut((int) json.getInt("shortcut"));
            }
        }
        return obj;
    }

    public static ShortcutInformationEntity createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        ShortcutInformationEntity obj = realm.createObject(ShortcutInformationEntity.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("shortcutId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field shortcutId to null.");
                } else {
                    obj.setShortcutId((int) reader.nextInt());
                }
            } else if (name.equals("shortcut")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field shortcut to null.");
                } else {
                    obj.setShortcut((int) reader.nextInt());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static ShortcutInformationEntity copyOrUpdate(Realm realm, ShortcutInformationEntity object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        ShortcutInformationEntity realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(ShortcutInformationEntity.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getShortcutId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new ShortcutInformationEntityRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static ShortcutInformationEntity copy(Realm realm, ShortcutInformationEntity newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        ShortcutInformationEntity realmObject = realm.createObject(ShortcutInformationEntity.class, newObject.getShortcutId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setShortcutId(newObject.getShortcutId());
        realmObject.setShortcut(newObject.getShortcut());
        return realmObject;
    }

    static ShortcutInformationEntity update(Realm realm, ShortcutInformationEntity realmObject, ShortcutInformationEntity newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setShortcut(newObject.getShortcut());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("ShortcutInformationEntity = [");
        stringBuilder.append("{shortcutId:");
        stringBuilder.append(getShortcutId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{shortcut:");
        stringBuilder.append(getShortcut());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShortcutInformationEntityRealmProxy aShortcutInformationEntity = (ShortcutInformationEntityRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aShortcutInformationEntity.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aShortcutInformationEntity.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aShortcutInformationEntity.row.getIndex()) return false;

        return true;
    }

}

package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.checkout.StoreAddress;

public class StoreAddressRealmProxy extends StoreAddress
    implements RealmObjectProxy {

    private static long INDEX_STOREID;
    private static long INDEX_STORENAME;
    private static long INDEX_STOREADDRESS;
    private static long INDEX_INDEX;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("storeID");
        fieldNames.add("storeName");
        fieldNames.add("storeAddress");
        fieldNames.add("index");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public String getStoreID() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_STOREID);
    }

    @Override
    public void setStoreID(String value) {
        realm.checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field storeID to null.");
        }
        row.setString(INDEX_STOREID, (String) value);
    }

    @Override
    public String getStoreName() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_STORENAME);
    }

    @Override
    public void setStoreName(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_STORENAME);
            return;
        }
        row.setString(INDEX_STORENAME, (String) value);
    }

    @Override
    public String getStoreAddress() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_STOREADDRESS);
    }

    @Override
    public void setStoreAddress(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_STOREADDRESS);
            return;
        }
        row.setString(INDEX_STOREADDRESS, (String) value);
    }

    @Override
    public String getIndex() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_INDEX);
    }

    @Override
    public void setIndex(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_INDEX);
            return;
        }
        row.setString(INDEX_INDEX, (String) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_StoreAddress")) {
            Table table = transaction.getTable("class_StoreAddress");
            table.addColumn(ColumnType.STRING, "storeID", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "storeName", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "storeAddress", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "index", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("storeID"));
            table.setPrimaryKey("storeID");
            return table;
        }
        return transaction.getTable("class_StoreAddress");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_StoreAddress")) {
            Table table = transaction.getTable("class_StoreAddress");
            if (table.getColumnCount() != 4) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 4 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 4; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type StoreAddress");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_STOREID = table.getColumnIndex("storeID");
            INDEX_STORENAME = table.getColumnIndex("storeName");
            INDEX_STOREADDRESS = table.getColumnIndex("storeAddress");
            INDEX_INDEX = table.getColumnIndex("index");

            if (!columnTypes.containsKey("storeID")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'storeID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("storeID") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'storeID' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_STOREID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'storeID' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'storeID' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("storeID")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'storeID' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("storeID"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'storeID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("storeName")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'storeName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("storeName") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'storeName' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_STORENAME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'storeName' is required. Either set @Required to field 'storeName' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("storeAddress")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'storeAddress' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("storeAddress") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'storeAddress' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_STOREADDRESS)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'storeAddress' is required. Either set @Required to field 'storeAddress' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("index")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'index' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("index") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'index' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_INDEX)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'index' is required. Either set @Required to field 'index' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The StoreAddress class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_StoreAddress";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static StoreAddress createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        StoreAddress obj = null;
        if (update) {
            Table table = realm.getTable(StoreAddress.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("storeID")) {
                long rowIndex = table.findFirstString(pkColumnIndex, json.getString("storeID"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new StoreAddressRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(StoreAddress.class);
        }
        if (json.has("storeID")) {
            if (json.isNull("storeID")) {
                obj.setStoreID(null);
            } else {
                obj.setStoreID((String) json.getString("storeID"));
            }
        }
        if (json.has("storeName")) {
            if (json.isNull("storeName")) {
                obj.setStoreName(null);
            } else {
                obj.setStoreName((String) json.getString("storeName"));
            }
        }
        if (json.has("storeAddress")) {
            if (json.isNull("storeAddress")) {
                obj.setStoreAddress(null);
            } else {
                obj.setStoreAddress((String) json.getString("storeAddress"));
            }
        }
        if (json.has("index")) {
            if (json.isNull("index")) {
                obj.setIndex(null);
            } else {
                obj.setIndex((String) json.getString("index"));
            }
        }
        return obj;
    }

    public static StoreAddress createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        StoreAddress obj = realm.createObject(StoreAddress.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("storeID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setStoreID(null);
                } else {
                    obj.setStoreID((String) reader.nextString());
                }
            } else if (name.equals("storeName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setStoreName(null);
                } else {
                    obj.setStoreName((String) reader.nextString());
                }
            } else if (name.equals("storeAddress")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setStoreAddress(null);
                } else {
                    obj.setStoreAddress((String) reader.nextString());
                }
            } else if (name.equals("index")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setIndex(null);
                } else {
                    obj.setIndex((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static StoreAddress copyOrUpdate(Realm realm, StoreAddress object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        StoreAddress realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(StoreAddress.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (object.getStoreID() == null) {
                throw new IllegalArgumentException("Primary key value must not be null.");
            }
            long rowIndex = table.findFirstString(pkColumnIndex, object.getStoreID());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new StoreAddressRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static StoreAddress copy(Realm realm, StoreAddress newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        StoreAddress realmObject = realm.createObject(StoreAddress.class, newObject.getStoreID());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setStoreID(newObject.getStoreID());
        realmObject.setStoreName(newObject.getStoreName());
        realmObject.setStoreAddress(newObject.getStoreAddress());
        realmObject.setIndex(newObject.getIndex());
        return realmObject;
    }

    static StoreAddress update(Realm realm, StoreAddress realmObject, StoreAddress newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setStoreName(newObject.getStoreName());
        realmObject.setStoreAddress(newObject.getStoreAddress());
        realmObject.setIndex(newObject.getIndex());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("StoreAddress = [");
        stringBuilder.append("{storeID:");
        stringBuilder.append(getStoreID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{storeName:");
        stringBuilder.append(getStoreName() != null ? getStoreName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{storeAddress:");
        stringBuilder.append(getStoreAddress() != null ? getStoreAddress() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{index:");
        stringBuilder.append(getIndex() != null ? getIndex() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoreAddressRealmProxy aStoreAddress = (StoreAddressRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aStoreAddress.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aStoreAddress.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aStoreAddress.row.getIndex()) return false;

        return true;
    }

}

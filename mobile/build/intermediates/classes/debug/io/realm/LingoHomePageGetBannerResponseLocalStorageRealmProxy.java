package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.local.LingoHomePageGetBannerResponseLocalStorage;

public class LingoHomePageGetBannerResponseLocalStorageRealmProxy extends LingoHomePageGetBannerResponseLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_STORAGEID;
    private static long INDEX_MODULEID;
    private static long INDEX_AVATAR;
    private static long INDEX_NAME;
    private static long INDEX_TODATE;
    private static long INDEX_DESCRIPTION;
    private static long INDEX_MICROSITETYPE;
    private static long INDEX_MICROSITEKEY;
    private static long INDEX_STATUS;
    private static long INDEX_FROMDATE;
    private static long INDEX_CREATEDBY;
    private static long INDEX_CREATEDAT;
    private static long INDEX_UPDATEDBY;
    private static long INDEX_ID;
    private static long INDEX_UPDATEDAT;
    private static long INDEX_TYPE;
    private static long INDEX_ZONELEVEL;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("storageID");
        fieldNames.add("moduleId");
        fieldNames.add("avatar");
        fieldNames.add("name");
        fieldNames.add("toDate");
        fieldNames.add("description");
        fieldNames.add("microSiteType");
        fieldNames.add("microSiteKey");
        fieldNames.add("status");
        fieldNames.add("fromDate");
        fieldNames.add("createdBy");
        fieldNames.add("createdAt");
        fieldNames.add("updatedBy");
        fieldNames.add("id");
        fieldNames.add("updatedAt");
        fieldNames.add("type");
        fieldNames.add("zoneLevel");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public int getStorageID() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_STORAGEID);
    }

    @Override
    public void setStorageID(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_STORAGEID, (long) value);
    }

    @Override
    public int getModuleId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_MODULEID);
    }

    @Override
    public void setModuleId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_MODULEID, (long) value);
    }

    @Override
    public String getAvatar() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_AVATAR);
    }

    @Override
    public void setAvatar(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_AVATAR);
            return;
        }
        row.setString(INDEX_AVATAR, (String) value);
    }

    @Override
    public String getName() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_NAME);
    }

    @Override
    public void setName(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_NAME);
            return;
        }
        row.setString(INDEX_NAME, (String) value);
    }

    @Override
    public String getToDate() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_TODATE);
    }

    @Override
    public void setToDate(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_TODATE);
            return;
        }
        row.setString(INDEX_TODATE, (String) value);
    }

    @Override
    public String getDescription() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_DESCRIPTION);
    }

    @Override
    public void setDescription(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_DESCRIPTION);
            return;
        }
        row.setString(INDEX_DESCRIPTION, (String) value);
    }

    @Override
    public String getMicroSiteType() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MICROSITETYPE);
    }

    @Override
    public void setMicroSiteType(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MICROSITETYPE);
            return;
        }
        row.setString(INDEX_MICROSITETYPE, (String) value);
    }

    @Override
    public String getMicroSiteKey() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MICROSITEKEY);
    }

    @Override
    public void setMicroSiteKey(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MICROSITEKEY);
            return;
        }
        row.setString(INDEX_MICROSITEKEY, (String) value);
    }

    @Override
    public String getStatus() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_STATUS);
    }

    @Override
    public void setStatus(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_STATUS);
            return;
        }
        row.setString(INDEX_STATUS, (String) value);
    }

    @Override
    public String getFromDate() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_FROMDATE);
    }

    @Override
    public void setFromDate(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_FROMDATE);
            return;
        }
        row.setString(INDEX_FROMDATE, (String) value);
    }

    @Override
    public String getCreatedBy() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_CREATEDBY);
    }

    @Override
    public void setCreatedBy(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_CREATEDBY);
            return;
        }
        row.setString(INDEX_CREATEDBY, (String) value);
    }

    @Override
    public String getCreatedAt() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_CREATEDAT);
    }

    @Override
    public void setCreatedAt(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_CREATEDAT);
            return;
        }
        row.setString(INDEX_CREATEDAT, (String) value);
    }

    @Override
    public String getUpdatedBy() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_UPDATEDBY);
    }

    @Override
    public void setUpdatedBy(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_UPDATEDBY);
            return;
        }
        row.setString(INDEX_UPDATEDBY, (String) value);
    }

    @Override
    public String getId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ID);
    }

    @Override
    public void setId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ID);
            return;
        }
        row.setString(INDEX_ID, (String) value);
    }

    @Override
    public String getUpdatedAt() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_UPDATEDAT);
    }

    @Override
    public void setUpdatedAt(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_UPDATEDAT);
            return;
        }
        row.setString(INDEX_UPDATEDAT, (String) value);
    }

    @Override
    public String getType() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_TYPE);
    }

    @Override
    public void setType(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_TYPE);
            return;
        }
        row.setString(INDEX_TYPE, (String) value);
    }

    @Override
    public String getZoneLevel() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONELEVEL);
    }

    @Override
    public void setZoneLevel(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONELEVEL);
            return;
        }
        row.setString(INDEX_ZONELEVEL, (String) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_LingoHomePageGetBannerResponseLocalStorage")) {
            Table table = transaction.getTable("class_LingoHomePageGetBannerResponseLocalStorage");
            table.addColumn(ColumnType.INTEGER, "storageID", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "moduleId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "avatar", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "name", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "toDate", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "description", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "microSiteType", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "microSiteKey", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "status", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "fromDate", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "createdBy", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "createdAt", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "updatedBy", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "id", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "updatedAt", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "type", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneLevel", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("storageID"));
            table.setPrimaryKey("storageID");
            return table;
        }
        return transaction.getTable("class_LingoHomePageGetBannerResponseLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_LingoHomePageGetBannerResponseLocalStorage")) {
            Table table = transaction.getTable("class_LingoHomePageGetBannerResponseLocalStorage");
            if (table.getColumnCount() != 17) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 17 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 17; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type LingoHomePageGetBannerResponseLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_STORAGEID = table.getColumnIndex("storageID");
            INDEX_MODULEID = table.getColumnIndex("moduleId");
            INDEX_AVATAR = table.getColumnIndex("avatar");
            INDEX_NAME = table.getColumnIndex("name");
            INDEX_TODATE = table.getColumnIndex("toDate");
            INDEX_DESCRIPTION = table.getColumnIndex("description");
            INDEX_MICROSITETYPE = table.getColumnIndex("microSiteType");
            INDEX_MICROSITEKEY = table.getColumnIndex("microSiteKey");
            INDEX_STATUS = table.getColumnIndex("status");
            INDEX_FROMDATE = table.getColumnIndex("fromDate");
            INDEX_CREATEDBY = table.getColumnIndex("createdBy");
            INDEX_CREATEDAT = table.getColumnIndex("createdAt");
            INDEX_UPDATEDBY = table.getColumnIndex("updatedBy");
            INDEX_ID = table.getColumnIndex("id");
            INDEX_UPDATEDAT = table.getColumnIndex("updatedAt");
            INDEX_TYPE = table.getColumnIndex("type");
            INDEX_ZONELEVEL = table.getColumnIndex("zoneLevel");

            if (!columnTypes.containsKey("storageID")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'storageID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("storageID") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'storageID' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_STORAGEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'storageID' does support null values in the existing Realm file. Use corresponding boxed type for field 'storageID' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("storageID")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'storageID' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("storageID"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'storageID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("moduleId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'moduleId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("moduleId") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'moduleId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_MODULEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'moduleId' does support null values in the existing Realm file. Use corresponding boxed type for field 'moduleId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("avatar")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'avatar' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("avatar") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'avatar' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_AVATAR)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'avatar' is required. Either set @Required to field 'avatar' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("name")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'name' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("name") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'name' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_NAME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'name' is required. Either set @Required to field 'name' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("toDate")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'toDate' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("toDate") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'toDate' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_TODATE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'toDate' is required. Either set @Required to field 'toDate' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("description")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'description' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("description") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'description' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_DESCRIPTION)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'description' is required. Either set @Required to field 'description' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("microSiteType")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'microSiteType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("microSiteType") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'microSiteType' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MICROSITETYPE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'microSiteType' is required. Either set @Required to field 'microSiteType' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("microSiteKey")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'microSiteKey' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("microSiteKey") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'microSiteKey' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MICROSITEKEY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'microSiteKey' is required. Either set @Required to field 'microSiteKey' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("status")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'status' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("status") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'status' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_STATUS)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'status' is required. Either set @Required to field 'status' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("fromDate")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'fromDate' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("fromDate") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'fromDate' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_FROMDATE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'fromDate' is required. Either set @Required to field 'fromDate' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("createdBy")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'createdBy' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("createdBy") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'createdBy' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_CREATEDBY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'createdBy' is required. Either set @Required to field 'createdBy' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("createdAt")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'createdAt' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("createdAt") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'createdAt' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_CREATEDAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'createdAt' is required. Either set @Required to field 'createdAt' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("updatedBy")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'updatedBy' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("updatedBy") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'updatedBy' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_UPDATEDBY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'updatedBy' is required. Either set @Required to field 'updatedBy' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("id") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'id' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'id' is required. Either set @Required to field 'id' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("updatedAt")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'updatedAt' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("updatedAt") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'updatedAt' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_UPDATEDAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'updatedAt' is required. Either set @Required to field 'updatedAt' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("type")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'type' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("type") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'type' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_TYPE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'type' is required. Either set @Required to field 'type' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneLevel")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneLevel' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneLevel") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneLevel' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONELEVEL)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneLevel' is required. Either set @Required to field 'zoneLevel' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The LingoHomePageGetBannerResponseLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_LingoHomePageGetBannerResponseLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static LingoHomePageGetBannerResponseLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        LingoHomePageGetBannerResponseLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(LingoHomePageGetBannerResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("storageID")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("storageID"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new LingoHomePageGetBannerResponseLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(LingoHomePageGetBannerResponseLocalStorage.class);
        }
        if (json.has("storageID")) {
            if (json.isNull("storageID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field storageID to null.");
            } else {
                obj.setStorageID((int) json.getInt("storageID"));
            }
        }
        if (json.has("moduleId")) {
            if (json.isNull("moduleId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
            } else {
                obj.setModuleId((int) json.getInt("moduleId"));
            }
        }
        if (json.has("avatar")) {
            if (json.isNull("avatar")) {
                obj.setAvatar(null);
            } else {
                obj.setAvatar((String) json.getString("avatar"));
            }
        }
        if (json.has("name")) {
            if (json.isNull("name")) {
                obj.setName(null);
            } else {
                obj.setName((String) json.getString("name"));
            }
        }
        if (json.has("toDate")) {
            if (json.isNull("toDate")) {
                obj.setToDate(null);
            } else {
                obj.setToDate((String) json.getString("toDate"));
            }
        }
        if (json.has("description")) {
            if (json.isNull("description")) {
                obj.setDescription(null);
            } else {
                obj.setDescription((String) json.getString("description"));
            }
        }
        if (json.has("microSiteType")) {
            if (json.isNull("microSiteType")) {
                obj.setMicroSiteType(null);
            } else {
                obj.setMicroSiteType((String) json.getString("microSiteType"));
            }
        }
        if (json.has("microSiteKey")) {
            if (json.isNull("microSiteKey")) {
                obj.setMicroSiteKey(null);
            } else {
                obj.setMicroSiteKey((String) json.getString("microSiteKey"));
            }
        }
        if (json.has("status")) {
            if (json.isNull("status")) {
                obj.setStatus(null);
            } else {
                obj.setStatus((String) json.getString("status"));
            }
        }
        if (json.has("fromDate")) {
            if (json.isNull("fromDate")) {
                obj.setFromDate(null);
            } else {
                obj.setFromDate((String) json.getString("fromDate"));
            }
        }
        if (json.has("createdBy")) {
            if (json.isNull("createdBy")) {
                obj.setCreatedBy(null);
            } else {
                obj.setCreatedBy((String) json.getString("createdBy"));
            }
        }
        if (json.has("createdAt")) {
            if (json.isNull("createdAt")) {
                obj.setCreatedAt(null);
            } else {
                obj.setCreatedAt((String) json.getString("createdAt"));
            }
        }
        if (json.has("updatedBy")) {
            if (json.isNull("updatedBy")) {
                obj.setUpdatedBy(null);
            } else {
                obj.setUpdatedBy((String) json.getString("updatedBy"));
            }
        }
        if (json.has("id")) {
            if (json.isNull("id")) {
                obj.setId(null);
            } else {
                obj.setId((String) json.getString("id"));
            }
        }
        if (json.has("updatedAt")) {
            if (json.isNull("updatedAt")) {
                obj.setUpdatedAt(null);
            } else {
                obj.setUpdatedAt((String) json.getString("updatedAt"));
            }
        }
        if (json.has("type")) {
            if (json.isNull("type")) {
                obj.setType(null);
            } else {
                obj.setType((String) json.getString("type"));
            }
        }
        if (json.has("zoneLevel")) {
            if (json.isNull("zoneLevel")) {
                obj.setZoneLevel(null);
            } else {
                obj.setZoneLevel((String) json.getString("zoneLevel"));
            }
        }
        return obj;
    }

    public static LingoHomePageGetBannerResponseLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        LingoHomePageGetBannerResponseLocalStorage obj = realm.createObject(LingoHomePageGetBannerResponseLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("storageID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field storageID to null.");
                } else {
                    obj.setStorageID((int) reader.nextInt());
                }
            } else if (name.equals("moduleId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
                } else {
                    obj.setModuleId((int) reader.nextInt());
                }
            } else if (name.equals("avatar")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setAvatar(null);
                } else {
                    obj.setAvatar((String) reader.nextString());
                }
            } else if (name.equals("name")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setName(null);
                } else {
                    obj.setName((String) reader.nextString());
                }
            } else if (name.equals("toDate")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setToDate(null);
                } else {
                    obj.setToDate((String) reader.nextString());
                }
            } else if (name.equals("description")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setDescription(null);
                } else {
                    obj.setDescription((String) reader.nextString());
                }
            } else if (name.equals("microSiteType")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMicroSiteType(null);
                } else {
                    obj.setMicroSiteType((String) reader.nextString());
                }
            } else if (name.equals("microSiteKey")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMicroSiteKey(null);
                } else {
                    obj.setMicroSiteKey((String) reader.nextString());
                }
            } else if (name.equals("status")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setStatus(null);
                } else {
                    obj.setStatus((String) reader.nextString());
                }
            } else if (name.equals("fromDate")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setFromDate(null);
                } else {
                    obj.setFromDate((String) reader.nextString());
                }
            } else if (name.equals("createdBy")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setCreatedBy(null);
                } else {
                    obj.setCreatedBy((String) reader.nextString());
                }
            } else if (name.equals("createdAt")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setCreatedAt(null);
                } else {
                    obj.setCreatedAt((String) reader.nextString());
                }
            } else if (name.equals("updatedBy")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setUpdatedBy(null);
                } else {
                    obj.setUpdatedBy((String) reader.nextString());
                }
            } else if (name.equals("id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setId(null);
                } else {
                    obj.setId((String) reader.nextString());
                }
            } else if (name.equals("updatedAt")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setUpdatedAt(null);
                } else {
                    obj.setUpdatedAt((String) reader.nextString());
                }
            } else if (name.equals("type")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setType(null);
                } else {
                    obj.setType((String) reader.nextString());
                }
            } else if (name.equals("zoneLevel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneLevel(null);
                } else {
                    obj.setZoneLevel((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static LingoHomePageGetBannerResponseLocalStorage copyOrUpdate(Realm realm, LingoHomePageGetBannerResponseLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        LingoHomePageGetBannerResponseLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(LingoHomePageGetBannerResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getStorageID());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new LingoHomePageGetBannerResponseLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static LingoHomePageGetBannerResponseLocalStorage copy(Realm realm, LingoHomePageGetBannerResponseLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        LingoHomePageGetBannerResponseLocalStorage realmObject = realm.createObject(LingoHomePageGetBannerResponseLocalStorage.class, newObject.getStorageID());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setStorageID(newObject.getStorageID());
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setAvatar(newObject.getAvatar());
        realmObject.setName(newObject.getName());
        realmObject.setToDate(newObject.getToDate());
        realmObject.setDescription(newObject.getDescription());
        realmObject.setMicroSiteType(newObject.getMicroSiteType());
        realmObject.setMicroSiteKey(newObject.getMicroSiteKey());
        realmObject.setStatus(newObject.getStatus());
        realmObject.setFromDate(newObject.getFromDate());
        realmObject.setCreatedBy(newObject.getCreatedBy());
        realmObject.setCreatedAt(newObject.getCreatedAt());
        realmObject.setUpdatedBy(newObject.getUpdatedBy());
        realmObject.setId(newObject.getId());
        realmObject.setUpdatedAt(newObject.getUpdatedAt());
        realmObject.setType(newObject.getType());
        realmObject.setZoneLevel(newObject.getZoneLevel());
        return realmObject;
    }

    static LingoHomePageGetBannerResponseLocalStorage update(Realm realm, LingoHomePageGetBannerResponseLocalStorage realmObject, LingoHomePageGetBannerResponseLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setAvatar(newObject.getAvatar());
        realmObject.setName(newObject.getName());
        realmObject.setToDate(newObject.getToDate());
        realmObject.setDescription(newObject.getDescription());
        realmObject.setMicroSiteType(newObject.getMicroSiteType());
        realmObject.setMicroSiteKey(newObject.getMicroSiteKey());
        realmObject.setStatus(newObject.getStatus());
        realmObject.setFromDate(newObject.getFromDate());
        realmObject.setCreatedBy(newObject.getCreatedBy());
        realmObject.setCreatedAt(newObject.getCreatedAt());
        realmObject.setUpdatedBy(newObject.getUpdatedBy());
        realmObject.setId(newObject.getId());
        realmObject.setUpdatedAt(newObject.getUpdatedAt());
        realmObject.setType(newObject.getType());
        realmObject.setZoneLevel(newObject.getZoneLevel());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("LingoHomePageGetBannerResponseLocalStorage = [");
        stringBuilder.append("{storageID:");
        stringBuilder.append(getStorageID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{moduleId:");
        stringBuilder.append(getModuleId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{avatar:");
        stringBuilder.append(getAvatar() != null ? getAvatar() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{name:");
        stringBuilder.append(getName() != null ? getName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{toDate:");
        stringBuilder.append(getToDate() != null ? getToDate() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{description:");
        stringBuilder.append(getDescription() != null ? getDescription() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{microSiteType:");
        stringBuilder.append(getMicroSiteType() != null ? getMicroSiteType() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{microSiteKey:");
        stringBuilder.append(getMicroSiteKey() != null ? getMicroSiteKey() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{status:");
        stringBuilder.append(getStatus() != null ? getStatus() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fromDate:");
        stringBuilder.append(getFromDate() != null ? getFromDate() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{createdBy:");
        stringBuilder.append(getCreatedBy() != null ? getCreatedBy() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{createdAt:");
        stringBuilder.append(getCreatedAt() != null ? getCreatedAt() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{updatedBy:");
        stringBuilder.append(getUpdatedBy() != null ? getUpdatedBy() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{id:");
        stringBuilder.append(getId() != null ? getId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{updatedAt:");
        stringBuilder.append(getUpdatedAt() != null ? getUpdatedAt() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{type:");
        stringBuilder.append(getType() != null ? getType() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneLevel:");
        stringBuilder.append(getZoneLevel() != null ? getZoneLevel() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LingoHomePageGetBannerResponseLocalStorageRealmProxy aLingoHomePageGetBannerResponseLocalStorage = (LingoHomePageGetBannerResponseLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aLingoHomePageGetBannerResponseLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aLingoHomePageGetBannerResponseLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aLingoHomePageGetBannerResponseLocalStorage.row.getIndex()) return false;

        return true;
    }

}

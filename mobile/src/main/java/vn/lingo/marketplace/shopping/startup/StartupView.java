package vn.lingo.marketplace.shopping.startup;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by longtran on 22/09/2015.
 */
public class StartupView extends RelativeLayout {

    public StartupView(Context context) {
        this(context, null);
    }

    public StartupView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StartupView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Context localContext = getContext();
        if ((localContext instanceof StartupActivity)) {
            ((StartupActivity) localContext).getStartupMediator().start();
        }
    }
}

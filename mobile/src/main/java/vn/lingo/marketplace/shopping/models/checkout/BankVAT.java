package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lenhan on 02/04/2016.
 */
public class BankVAT implements Serializable {
    @SerializedName("BANK_ID")
    int bankID;
    @SerializedName("BANK_NAME")
    String bankName;
    @SerializedName("BANK_NUMBER")
    String bankNunber;
    @SerializedName("LO_GO")
    String imageLogo;
    @SerializedName("ACCOUNT_NAME")
    String accountName;

    public int getBankID() {
        return bankID;
    }

    public void setBankID(int bankID) {
        this.bankID = bankID;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNunber() {
        return bankNunber;
    }

    public void setBankNunber(String bankNunber) {
        this.bankNunber = bankNunber;
    }

    public String getImageLogo() {
        return imageLogo;
    }

    public void setImageLogo(String imageLogo) {
        this.imageLogo = imageLogo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}

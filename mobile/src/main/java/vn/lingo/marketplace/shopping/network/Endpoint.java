package vn.lingo.marketplace.shopping.network;


import vn.lingo.marketplace.shopping.BuildConfig;

/**
 * Created by longtran on 12/10/2015.
 */
public class Endpoint {
    public static final String ENDPOINT = BuildConfig.ENDPOINT;
    public static final String ENDPOINT_CHECK_OUT = BuildConfig.ENDPOINT_CHECK_OUT;
    public static final String ENDPOINT_CHECK_OUT_TEST = BuildConfig.ENDPOINT_CHECK_OUT_TEST;
    public static final String ENDPOINT_CHECK_ECOUPON=BuildConfig.ENDPOINT_CHECK_ECOUPON;
    /***
     * Simple Storage Service (Lingo S3)
     */
    public static String LINGO_SIMPLE_STORAGE_SERVICE = "http://static.lingo.vn/";
    public static String LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE = "http://static.lingo.vn/";
    public static final String CLIENT_ID = "7HgP9/1Lt3eTy4E59d10vKBst1m6n+M3HSaxCrjq8cs=";
}

package vn.lingo.marketplace.shopping.listeners;

import android.content.Intent;

/**
 * Created by longtran on 21/12/2015.
 */
public interface ConfigurationFragmentCallbacks {

    void onFrictionChanged(float friction);

    void openDialog(float friction);

    void openActivity(Intent intent);
}

package vn.lingo.marketplace.shopping.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by longtran on 30/12/2015.
 */
public class HackyViewPager extends ViewPager {

    private final String TAG = HackyViewPager.class.getName();

    public HackyViewPager(Context context) {
        super(context);
    }

    public HackyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (Throwable t) {
            Log.e(TAG, t.toString());
            return false;
        }
    }
}

package vn.lingo.marketplace.shopping.models.local;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import vn.lingo.marketplace.shopping.models.HomeSliderBannerResponse;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by longtran on 22/01/2016.
 */
public class HomeSliderBannerResponseLocalStorage extends RealmObject {

    @PrimaryKey
    private int id;
    private int moduleId;
    private String description;
    private String name;
    private String galleryId;
    private String imagePath;
    private String url;
    private String galleryKey;
    private String parameter1;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGalleryId() {
        return galleryId;
    }

    public void setGalleryId(String galleryId) {
        this.galleryId = galleryId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getGalleryKey() {
        return galleryKey;
    }

    public void setGalleryKey(String galleryKey) {
        this.galleryKey = galleryKey;
    }

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public static HomeSliderBannerResponseLocalStorage convertFromHomeSliderBannerResponse(HomeSliderBannerResponse homeSliderBannerResponse) {
        HomeSliderBannerResponseLocalStorage homeSliderBannerResponseLocalStorage = new HomeSliderBannerResponseLocalStorage();
        try {
            homeSliderBannerResponseLocalStorage.setId(Integer.parseInt(homeSliderBannerResponse.getGalleryId()));
        } catch (Exception exception) {
            homeSliderBannerResponseLocalStorage.setId(Integer.MAX_VALUE);
        }
        homeSliderBannerResponseLocalStorage.setModuleId(Constant.MODULE_HOME_SLIDES_ID);
        homeSliderBannerResponseLocalStorage.setDescription(homeSliderBannerResponse.getDescription());
        homeSliderBannerResponseLocalStorage.setName(homeSliderBannerResponse.getName());
        homeSliderBannerResponseLocalStorage.setGalleryId(homeSliderBannerResponse.getGalleryId());
        homeSliderBannerResponseLocalStorage.setImagePath(homeSliderBannerResponse.getImagePath());
        homeSliderBannerResponseLocalStorage.setGalleryKey(homeSliderBannerResponse.getGalleryKey());
        homeSliderBannerResponseLocalStorage.setParameter1(homeSliderBannerResponse.getParameter1());
        return homeSliderBannerResponseLocalStorage;
    }
}

package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.ProductDetailsRelatedProductsEntity;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.viewholder.ProductDetailsRelatedProductsRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class ProductDetailsRelatedProductsRecyclerViewAdapter extends RecyclerView.Adapter<ProductDetailsRelatedProductsRecyclerViewHolder> {
    private final String TAG = ProductDetailsRelatedProductsRecyclerViewAdapter.class.getName();
    private List<ProductDetailsRelatedProductsEntity> itemList;
    private Context context;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private DisplayImageOptions displayImageOptions;

    public ProductDetailsRelatedProductsRecyclerViewAdapter(Context context, List<ProductDetailsRelatedProductsEntity> itemList,
                                                            RecyclerViewItemClickListener recyclerViewItemClickListener,
                                                            DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.context = context;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public ProductDetailsRelatedProductsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_details_related_products_recycler_view_item_layout, null);
        ProductDetailsRelatedProductsRecyclerViewHolder viewHolder = new ProductDetailsRelatedProductsRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ProductDetailsRelatedProductsRecyclerViewHolder holder, final int position) {
        final ProductDetailsRelatedProductsEntity productDetailsRelatedProductsEntity = itemList.get(position);
        if (!StringUtils.isBlank(productDetailsRelatedProductsEntity.getPromotionPercent())) {
            holder.linearLayoutPercent.setVisibility(View.VISIBLE);
            holder.appCompatTextViewPercent.setText(productDetailsRelatedProductsEntity.getPromotionPercent());
        } else {
            holder.linearLayoutPercent.setVisibility(View.GONE);
        }
        if (!StringUtils.isBlank(productDetailsRelatedProductsEntity.getProductGoldHour())) {
            holder.linearLayoutHappyHourDiscount.setVisibility(View.VISIBLE);
            holder.appCompatTextViewPercentHappyHourDiscount.setText("Giờ vàng");
        } else {
            holder.linearLayoutHappyHourDiscount.setVisibility(View.GONE);
        }
        if (!StringUtils.isBlank(productDetailsRelatedProductsEntity.getProductGift())) {
            holder.linearLayoutGiftCards.setVisibility(View.VISIBLE);
            holder.appCompatTextViewGiftCards.setText("Kèm theo quà tặng");
        } else {
            holder.linearLayoutGiftCards.setVisibility(View.GONE);
        }
        try {
            holder.productDetailsRelatedProductsTitle.setText(productDetailsRelatedProductsEntity.getProductName());
            holder.productDetailsRelatedProductsPrices.setText(productDetailsRelatedProductsEntity.getProductPriceFormat());
            if (StringUtils.isBlank(productDetailsRelatedProductsEntity.getMarketPriceFormat())) {
                holder.productDetailsRelatedProductsMarketPrice.setVisibility(View.GONE);
            } else {
                holder.productDetailsRelatedProductsMarketPrice.setVisibility(View.VISIBLE);
                holder.productDetailsRelatedProductsMarketPrice.setText(productDetailsRelatedProductsEntity.getMarketPriceFormat());
            }
        } catch (Exception exception) {
            Log.e(TAG, exception.toString());
        }
//        ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + productDetailsRelatedProductsEntity.getAvatar(),
//                holder.productDetailsRelatedProductsImageViewMetaData, displayImageOptions);
        String urlAvatar = Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + productDetailsRelatedProductsEntity.getAvatar();
        try {
            ImageSize targetSize = new ImageSize(256, 256);
            ImageLoader.getInstance().loadImage(urlAvatar, targetSize,
                    displayImageOptions, new SimpleImageLoadingListener() {

                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            super.onLoadingStarted(imageUri, view);
                            holder.productDetailsRelatedProductsImageViewMetaData.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            super.onLoadingFailed(imageUri, view, failReason);
                            holder.productDetailsRelatedProductsImageViewMetaData.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            super.onLoadingCancelled(imageUri, view);
                            holder.productDetailsRelatedProductsImageViewMetaData.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.productDetailsRelatedProductsImageViewMetaData.setImageBitmap(loadedImage);
                        }
                    });
        } catch (OutOfMemoryError outOfMemoryError) {
            Log.e(TAG, outOfMemoryError.toString());
        }
        holder.relativeLayoutRowItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, Integer.parseInt(productDetailsRelatedProductsEntity.getProductId()),
                            Integer.MAX_VALUE, Integer.MAX_VALUE, productDetailsRelatedProductsEntity.getProductName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}

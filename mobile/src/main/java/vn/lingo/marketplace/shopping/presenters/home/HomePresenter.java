package vn.lingo.marketplace.shopping.presenters.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.thefinestartist.finestwebview.FinestWebView;

import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;
import java.util.List;

import io.realm.RealmQuery;
import io.realm.RealmResults;
import me.relex.seamlessviewpagerheader.tools.ScrollableListener;
import me.relex.seamlessviewpagerheader.tools.ViewPagerHeaderHelper;
import me.relex.seamlessviewpagerheader.widget.TouchCallbackLayout;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.MainActivity;
import vn.lingo.marketplace.shopping.adapter.HomeSliderBannerPagerAdapter;
import vn.lingo.marketplace.shopping.listeners.EventDealFixDealsEventByTypesListener;
import vn.lingo.marketplace.shopping.models.BannerByDate;
import vn.lingo.marketplace.shopping.models.HomeAPI;
import vn.lingo.marketplace.shopping.models.HomeSliderAPI;
import vn.lingo.marketplace.shopping.models.HomeSliderBannerResponse;
import vn.lingo.marketplace.shopping.models.IndustryAPI;
import vn.lingo.marketplace.shopping.models.IndustryResponse;
import vn.lingo.marketplace.shopping.models.local.HomeSliderBannerResponseLocalStorage;
import vn.lingo.marketplace.shopping.models.local.IndustryResponseLocalStorage;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.presenters.AbstractPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.home.HomePageCategoriesFragment;
import vn.lingo.marketplace.shopping.views.home.HomeView;
import vn.lingo.marketplace.shopping.views.home.LingoHomePageIndustryFragment;

/**
 * Created by longtran on 05/11/2015.
 */
public class HomePresenter extends AbstractPresenter<HomeView> implements EventDealFixDealsEventByTypesListener,
        TouchCallbackLayout.TouchEventListener, ViewPagerHeaderHelper.OnViewPagerTouchListener {
    private String TAG = HomePresenter.class.getName();
    private static final long DEFAULT_DURATION = 300L;
    private static final float DEFAULT_DAMPING = 1.5f;
    private ViewPagerHeaderHelper viewPagerHeaderHelper;
    private int touchSlop;
    private int tabHeight;
    private int headerHeight;
    private Interpolator mInterpolator = new DecelerateInterpolator();
    private long downtime = -1;
    private int countPushEnd = 0;
    private int countPullEnd = 0;
    private int spaceBetweenBox = 0;
    private HomeView homeView;
    private boolean reloadsHomeSlides = false;
    private boolean homeIndustryAdapter = false;
    public ViewPager viewPager;

    public HomePresenter(final HomeView homeView) {
        super(homeView);
        this.homeView = homeView;
        touchSlop = ViewConfiguration.get(homeView.getContext()).getScaledTouchSlop();
        tabHeight = homeView.getContext().getResources().getDimensionPixelSize(R.dimen.tab_height);
        headerHeight = homeView.getContext().getResources().getDimensionPixelSize(R.dimen.parallax_top_image_height);
        spaceBetweenBox = homeView.getContext().getResources().getDimensionPixelSize(R.dimen.space_between_the_outside_of_the_drawer_box);

        viewPagerHeaderHelper = new ViewPagerHeaderHelper(homeView.getContext(), this);
        homeView.getTouchCallbackLayout().setTouchEventListener(this);
        ViewCompat.setTranslationY(homeView.getHackyViewPagerIndustry(), headerHeight);
        ViewCompat.setTranslationY(homeView.getParallaxTopHeaderLinearLayout(), tabHeight);
        ViewCompat.setTranslationY(homeView.getSmartTabLayoutIndustry(), 0);
        homeView.getAppCompatImageViewClose().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeView.getRelativeLayoutLingoAdvertising().setVisibility(View.GONE);
            }
        });
    }


    /****
     *
     */
    private void setHomeSlideAdapter() {
        RealmQuery<HomeSliderBannerResponseLocalStorage> realmQueryHomeSliderBannerResponseLocalStorage =
                realm.where(HomeSliderBannerResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_HOME_SLIDES_ID);
        RealmResults<HomeSliderBannerResponseLocalStorage> realmResultsHomeSliderBannerResponseLocalStorage = realmQueryHomeSliderBannerResponseLocalStorage.findAll();
        homeView.getAutoScrollViewPager().setAdapter(new HomeSliderBannerPagerAdapter(homeView.getFragmentActivity(), realmResultsHomeSliderBannerResponseLocalStorage,
                HomePresenter.this, homeView.getDisplayImageOptions()));
        homeView.getCirclePageIndicator().setViewPager(homeView.getAutoScrollViewPager());
        homeView.getAutoScrollViewPager().setInterval(Constant.SLIDE_TIME_OUT);
        homeView.getAutoScrollViewPager().startAutoScroll();
        homeView.getAutoScrollViewPager().setCurrentItem(0/*Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % imageIdList.size()*/);
        homeView.hideProcessing();
    }

    /***
     *
     */
    public void getHomeSlides() {
        homeView.showProcessing();
        RealmQuery<HomeSliderBannerResponseLocalStorage> realmQueryHomeSliderBannerResponseLocalStorage =
                realm.where(HomeSliderBannerResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_HOME_SLIDES_ID);
        RealmResults<HomeSliderBannerResponseLocalStorage> realmResultsHomeSliderBannerResponseLocalStorage = realmQueryHomeSliderBannerResponseLocalStorage.findAll();
        if (realmResultsHomeSliderBannerResponseLocalStorage.size() > 0) {
            Log.i(TAG, "get home slides load data from cache.............");
            setHomeSlideAdapter();
            reloadsHomeSlides = false;
        } else {
            Log.i(TAG, "get home slides load data from server.............");
            reloadsHomeSlides = true;
        }
        HomeSliderAPI homeSliderAPI = homeView.getRetrofit().create(HomeSliderAPI.class);
        Call<List<HomeSliderBannerResponse>> call = homeSliderAPI.getHomeSlider();
        call.enqueue(new Callback<List<HomeSliderBannerResponse>>() {
            @Override
            public void onResponse(Response<List<HomeSliderBannerResponse>> response, Retrofit retrofit) {
                if (response.code() == 200 && null != response.body()) {
                    try {
                        realm.beginTransaction();
                        List<HomeSliderBannerResponse> imageIdList = response.body();
                        Iterator<HomeSliderBannerResponse> iteratorHomeSliderBannerResponse = imageIdList.iterator();
                        while (iteratorHomeSliderBannerResponse.hasNext()) {
                            HomeSliderBannerResponse homeSliderBannerResponse = iteratorHomeSliderBannerResponse.next();
                            HomeSliderBannerResponseLocalStorage homeSliderBannerResponseLocalStorage = HomeSliderBannerResponseLocalStorage.
                                    convertFromHomeSliderBannerResponse(homeSliderBannerResponse);
                            realm.copyToRealmOrUpdate(homeSliderBannerResponseLocalStorage);
                        }

                        realm.commitTransaction();
                        if (reloadsHomeSlides) {
                            setHomeSlideAdapter();
                        }
                    } catch (Exception exception) {
                        Log.i(TAG, exception.toString());
                    }
                }
                homeView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                homeView.hideProcessing();
            }
        });
    }

    /*****
     * @param
     */
    private void setHomeIndustryAdapter() {
        RealmQuery<IndustryResponseLocalStorage> realmQueryIndustryResponseLocalStorage =
                realm.where(IndustryResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_INDUSTRY);
        RealmResults<IndustryResponseLocalStorage> realmResultsIndustryResponseLocalStorage = realmQueryIndustryResponseLocalStorage.findAll();
        FragmentPagerItems fragmentPagerItems = new FragmentPagerItems(homeView.getContext());
        Bundle bundleHomePageIndustryFragment = new Bundle();
        bundleHomePageIndustryFragment.putString(Constant.EVENT, String.valueOf(Integer.MAX_VALUE));
        fragmentPagerItems.add(FragmentPagerItem.of(homeView.getContext().getResources().getString(R.string.home_page),
                LingoHomePageIndustryFragment.class, bundleHomePageIndustryFragment));
        for (IndustryResponseLocalStorage industryResponseLocalStorage : realmResultsIndustryResponseLocalStorage) {
            AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", industryResponseLocalStorage.getZoneName(), true);
            AnalyticsHelper.logEvent(homeView.getContext(), industryResponseLocalStorage.getZoneName(),
                    industryResponseLocalStorage.getZoneName());
            Bundle bundle = new Bundle();
            bundle.putString(Constant.Categories.ZONE_ID, industryResponseLocalStorage.getZoneId());
            bundle.putString(Constant.Categories.ZONE_LEVEL, industryResponseLocalStorage.getZoneLevel());
            fragmentPagerItems.add(FragmentPagerItem.of(industryResponseLocalStorage.getZoneName(), HomePageCategoriesFragment.class, bundle));
        }
        final FragmentStatePagerItemAdapter fragmentStatePagerItemAdapter = new FragmentStatePagerItemAdapter(homeView.getFragment().getFragmentManager(),
                fragmentPagerItems);
        viewPager = homeView.getHackyViewPagerIndustry();
        viewPager.setAdapter(fragmentStatePagerItemAdapter);
        homeView.getSmartTabLayoutIndustry().setViewPager(viewPager);
        AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", homeView.getContext().getResources().getString(R.string.home_page), true);
        AnalyticsHelper.logEvent(homeView.getContext(), homeView.getContext().getResources().getString(R.string.home_page),
                homeView.getContext().getResources().getString(R.string.home_page));
        homeView.hideProcessing();


    }
    /****
     *
     */
    public void fetchingIndustry() {
        homeView.showProcessing();
        RealmQuery<IndustryResponseLocalStorage> realmQueryIndustryResponseLocalStorage =
                realm.where(IndustryResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_INDUSTRY);
        RealmResults<IndustryResponseLocalStorage> realmResultsIndustryResponseLocalStorage = realmQueryIndustryResponseLocalStorage.findAll();
        if (realmResultsIndustryResponseLocalStorage.size() > 0) {
            Log.i(TAG, "get home fetching industry load data from cache.............");
            setHomeIndustryAdapter();
            homeIndustryAdapter = false;
        } else {
            Log.i(TAG, "get home fetching industry load data from server.............");
            homeIndustryAdapter = true;
        }
        IndustryAPI industryAPI = homeView.getRetrofit().create(IndustryAPI.class);
        Call<List<IndustryResponse>> callIndustryResponse = industryAPI.getIndustryFull();
        callIndustryResponse.enqueue(new Callback<List<IndustryResponse>>() {
            @Override
            public void onResponse(Response<List<IndustryResponse>> response, Retrofit retrofit) {
                if (response.code() == 200 && null != response.body()) {
                    try {
                        realm.beginTransaction();
                        List<IndustryResponse> listIndustryResponse = response.body();
                        Iterator<IndustryResponse> iteratorIndustryResponse = listIndustryResponse.iterator();
                        while (iteratorIndustryResponse.hasNext()) {
                            IndustryResponse industryResponse = iteratorIndustryResponse.next();
                            IndustryResponseLocalStorage industryResponseLocalStorage =
                                    IndustryResponseLocalStorage.getIndustryResponseLocalStorageFromIndustryResponse(industryResponse);
                            realm.copyToRealmOrUpdate(industryResponseLocalStorage);
                        }
                        realm.commitTransaction();
                        if (homeIndustryAdapter) {
                            setHomeIndustryAdapter();
                        }
                    } catch (Exception exception) {
                        Log.i(TAG, exception.toString());
                    }
                }
                homeView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                homeView.hideProcessing();
            }
        });
    }

    @Override
    public void onItemClickedEventDealFixDealsEventByTypes(int position, int id, String micrositeKey, String micrositeType) {
        homeView.onItemClickedEventDealFixDealsEventByTypes(position, id, micrositeKey, micrositeType);
    }

    @Override
    public boolean onLayoutInterceptTouchEvent(MotionEvent event) {
        return viewPagerHeaderHelper.onLayoutInterceptTouchEvent(event,
                tabHeight + headerHeight);
    }


    @Override
    public boolean onLayoutTouchEvent(MotionEvent event) {
        return viewPagerHeaderHelper.onLayoutTouchEvent(event);
    }

    @Override
    public boolean isViewBeingDragged(MotionEvent event) {
        MainActivity activity = (MainActivity) homeView.getContext();
        ScrollableListener listener = null;
        if (null != activity && null != activity.getScrollableListeners()) {
            try {
                listener = activity.getScrollableListeners().valueAt(homeView.getHackyViewPagerIndustry().getCurrentItem());
            } catch (Exception exception) {
                listener = null;
            }
        }
        if (listener == null) {
            //return true;
        } else {
            //return listener.isViewBeingDragged(event);
        }
        return true;
    }

    @Override
    public void onMoveStarted(float y) {

    }

    @Override
    public void onMove(float y, float yDx) {
        float headerTranslationY = ViewCompat.getTranslationY(homeView.getParallaxTopHeaderLinearLayout()) + yDx;
        if (headerTranslationY >= 0) { // pull end
            headerExpand(0L);
            if (countPullEnd >= 1) {
                if (countPullEnd == 1) {
                    downtime = SystemClock.uptimeMillis();
                    simulateTouchEvent(homeView.getHackyViewPagerIndustry(), downtime, SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 250, y + headerHeight);
                }
                simulateTouchEvent(homeView.getHackyViewPagerIndustry(), downtime, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, 250, y + headerHeight);
            }
            countPullEnd++;
        } else if (headerTranslationY <= -headerHeight) { // push end
            headerFold(0L);
            if (countPushEnd >= 1) {
                if (countPushEnd == 1) {
                    downtime = SystemClock.uptimeMillis();
                    simulateTouchEvent(homeView.getHackyViewPagerIndustry(), downtime, SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 250, y + headerHeight);
                }
                simulateTouchEvent(homeView.getHackyViewPagerIndustry(), downtime, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, 250, y + headerHeight);
            }
            countPushEnd++;
        } else {
            ViewCompat.animate(homeView.getParallaxTopHeaderLinearLayout())
                    .translationY(headerTranslationY)
                    .setDuration(0)
                    .start();
            ViewCompat.animate(homeView.getHackyViewPagerIndustry())
                    .translationY(headerTranslationY + headerHeight)
                    .setDuration(0)
                    .start();
        }
    }

    /****
     * @param dispatcher
     * @param downTime
     * @param eventTime
     * @param action
     * @param x
     * @param y
     */
    private void simulateTouchEvent(View dispatcher, long downTime, long eventTime, int action, float x, float y) {
        MotionEvent motionEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), action, x, y, 0);
        try {
            dispatcher.dispatchTouchEvent(motionEvent);
        } catch (Throwable e) {
            Log.e("kaede", "simulateTouchEvent error: " + e.toString());
        } finally {
            motionEvent.recycle();
        }
    }

    @Override
    public void onMoveEnded(boolean isFling, float flingVelocityY) {
        countPushEnd = countPullEnd = 0;
        float headerY = ViewCompat.getTranslationY(homeView.getParallaxTopHeaderLinearLayout());
        if (headerY == 0 || headerY == -headerHeight) {
            return;
        }
        if (viewPagerHeaderHelper.getInitialMotionY() - viewPagerHeaderHelper.getLastMotionY()
                < -touchSlop) {  // pull > touchSlop = expand
            headerExpand(headerMoveDuration(true, headerY, isFling, flingVelocityY));
        } else if (viewPagerHeaderHelper.getInitialMotionY()
                - viewPagerHeaderHelper.getLastMotionY()
                > touchSlop) { // push > touchSlop = fold
            headerFold(headerMoveDuration(false, headerY, isFling, flingVelocityY));
        } else {
            if (headerY > -headerHeight / 2f) {  // headerY > header/2 = expand
                headerExpand(headerMoveDuration(true, headerY, isFling, flingVelocityY));
            } else { // headerY < header/2= fold
                headerFold(headerMoveDuration(false, headerY, isFling, flingVelocityY));
            }
        }
    }

    /****
     * @param isExpand
     * @param currentHeaderY
     * @param isFling
     * @param velocityY
     * @return
     */
    private long headerMoveDuration(boolean isExpand, float currentHeaderY, boolean isFling,
                                    float velocityY) {
        long defaultDuration = DEFAULT_DURATION;
        if (isFling) {
            float distance = isExpand ? Math.abs(headerHeight) - Math.abs(currentHeaderY)
                    : Math.abs(currentHeaderY);
            velocityY = Math.abs(velocityY) / 1000;
            defaultDuration = (long) (distance / velocityY * DEFAULT_DAMPING);
            defaultDuration =
                    defaultDuration > DEFAULT_DURATION ? DEFAULT_DURATION : defaultDuration;
        }
        return defaultDuration;
    }

    /****
     * @param duration
     */
    private void headerFold(long duration) {
        ViewCompat.animate(homeView.getParallaxTopHeaderLinearLayout())
                .translationY(-headerHeight)
                //.setDuration(duration)
                .setInterpolator(mInterpolator)
                .start();

        ViewCompat.animate(homeView.getHackyViewPagerIndustry())
                .translationY(0)
                //.setDuration(duration)
                .setInterpolator(mInterpolator).start();

        viewPagerHeaderHelper.setHeaderExpand(false);
    }

    /****
     * @param duration
     */
    private void headerExpand(long duration) {
        ViewCompat.animate(homeView.getParallaxTopHeaderLinearLayout())
                .translationY(tabHeight)
                //.setDuration(duration)
                .setInterpolator(mInterpolator)
                .start();
        ViewCompat.animate(homeView.getHackyViewPagerIndustry())
                .translationY(headerHeight)
                //.setDuration(duration)
                .setInterpolator(mInterpolator)
                .start();
        viewPagerHeaderHelper.setHeaderExpand(true);
    }

    /**
     * This function will take an URL as input and return the file name.
     * <p>Examples :</p>
     * <ul>
     * <li>http://example.com/a/b/c/test.txt -> test.txt</li>
     * <li>http://example.com/ -> an empty string </li>
     * <li>http://example.com/test.txt?param=value -> test.txt</li>
     * <li>http://example.com/test.txt#anchor -> test.txt</li>
     * </ul>
     *
     * @param urlString The input String
     * @return The file name
     */
    private String getFileNameFromUrl(String urlString) {
        return urlString.substring(urlString.lastIndexOf('/') + 1).split("\\?")[0].split("#")[0];
    }

    /**
     * Getting file name from url without extension
     *
     * @param url string
     * @return file name
     */
    private String getFileName(String url) {
        String fileName;
        int slashIndex = url.lastIndexOf("/");
        int qIndex = url.lastIndexOf("?");
        if (qIndex > slashIndex) {//if has parameters
            fileName = url.substring(slashIndex + 1, qIndex);
        } else {
            fileName = url.substring(slashIndex + 1);
        }
        if (fileName.contains(".")) {
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
        }
        return fileName;
    }

    /****
     *
     */
    public void fetchingLingoAdvertising() {
        homeView.showProcessing();
        HomeAPI homeAPI = homeView.getRetrofit().create(HomeAPI.class);
        Call<List<BannerByDate>> callAllEventsResponse = homeAPI.getBannerByDate();
        callAllEventsResponse.enqueue(new retrofit.Callback<List<BannerByDate>>() {
            @Override
            public void onResponse(Response<List<BannerByDate>> response, Retrofit retrofit) {
                if (response.code() == 200 && response.body().size() > 0) {
                    final BannerByDate bannerByDate = response.body().get(0);
                    ImageLoader.getInstance().displayImage(Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE + bannerByDate.getBannerImage(),
                            homeView.getAppCompatImageViewLingoAdvertising(), homeView.getDisplayImageOptions());
                    homeView.getAppCompatImageViewLingoAdvertising().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String microSiteKey = getFileName(bannerByDate.getLink());
                            String microSiteType = "1";
                            if (!StringUtils.isBlank(microSiteKey)) {
                                homeView.onItemClickedEventDealFixDealsEventByTypes(Integer.MAX_VALUE, Integer.MAX_VALUE, microSiteKey, microSiteType);
                            } else {
                                new FinestWebView.Builder(homeView.getFragmentActivity()).show(bannerByDate.getLink());
                            }
                        }
                    });
                } else {
                    homeView.getRelativeLayoutLingoAdvertising().setVisibility(View.GONE);
                }
                homeView.showProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                homeView.showProcessing();
                homeView.getRelativeLayoutLingoAdvertising().setVisibility(View.GONE);
            }
        });
    }
}
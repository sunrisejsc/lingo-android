package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 09/12/2015.
 */
public class CollectionDisplay implements Serializable {

    @SerializedName("ROW")
    private String row;
    @SerializedName("COLUMN")
    private String column;

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }
}

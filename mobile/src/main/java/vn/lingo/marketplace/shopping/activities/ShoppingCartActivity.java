package vn.lingo.marketplace.shopping.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.delegate.Singleton;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.presenters.ShoppingCartPresenter;
import vn.lingo.marketplace.shopping.service.RetrieveSiteData;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.views.ShoppingCartView;

/**
 * Created by longtran on 11/12/2015.
 */
public class ShoppingCartActivity extends AbstractAppCompatActivityNoToolbar implements ShoppingCartView {

    private final String TAG = ShoppingCartActivity.class.getName();

    @Bind(R.id.activity_shopping_cart_list)
    RecyclerView recyclerView;

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.shopping_cart_summary)
    AppCompatTextView appCompatTextViewShoppingCartSummary;

    @Bind(R.id.shopping_cart_subtotal_value)
    AppCompatTextView appCompatTextViewShoppingCartSubtotalValue;

    @Bind(R.id.shopping_cart_total_value)
    AppCompatTextView appCompatTextViewShoppingCartTotalValue;

    @Bind(R.id.shopping_cart_total_quantity)
    AppCompatTextView appCompatTextViewShoppingCartTotalQuantity;

    @Bind(R.id.shopping_cart_buy)
    AppCompatButton appCompatButtonShoppingCartBuy;

    private ShoppingCartPresenter shoppingCartPresenter;
    private ShoppingCartReceiver shoppingCartReceiver;

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public RecyclerView getRecyclerViewShoppingCart() {
        recyclerView.addItemDecoration(new DividerItemDecoration(1));
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        return recyclerView;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        ProductDetailsParcelable productDetailsParcelable = new ProductDetailsParcelable();
        productDetailsParcelable.setId(id);
        productDetailsParcelable.setTitle(title);
        Singleton.getSingleton().setSetUpdateProduct(1);
        Intent intent = new Intent();
        intent.setClassName(this, ProductDetailsActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productDetailsParcelable);
        intent.putExtras(bundle);
        this.startActivity(intent);
    }

    @Override
    public AppCompatTextView getAppCompatTextViewShoppingCartSummary() {
        return appCompatTextViewShoppingCartSummary;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewShoppingCartSubtotalValue() {
        return appCompatTextViewShoppingCartSubtotalValue;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewShoppingCartTotalValue() {
        return appCompatTextViewShoppingCartTotalValue;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewShoppingCartTotalQuantity() {
        return appCompatTextViewShoppingCartTotalQuantity;
    }

    @Override
    public AppCompatButton getAppCompatButtonShoppingCartBuy() {
        return appCompatButtonShoppingCartBuy;
    }

    @Override
    public Retrofit getRetrofitProductCheckout() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofitCheckOutTest();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {

    }

    @Override
    public void hideProcessing() {

    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public MaterialDialog getMaterialDialogAlert(AppCompatActivity appCompatActivity, String title, String content, String positiveText) {
        return super.getMaterialDialog(appCompatActivity, title, content, positiveText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart_screen);
        ButterKnife.bind(this);
        shoppingCartPresenter = new ShoppingCartPresenter(this);
        shoppingCartPresenter.fetchingShoppingCart();

        final IntentFilter filter = new IntentFilter(Constant.ACTION.ADD_TO_CART_ACTION);
        filter.addAction(Constant.ACTION.ADD_TO_CART_ACTION);
        shoppingCartReceiver = new ShoppingCartReceiver();
        registerReceiver(shoppingCartReceiver, filter);
    }

    private class ShoppingCartReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            shoppingCartPresenter.setAdapter();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AnalyticsHelper.logPageViews();
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        shoppingCartPresenter.invalidateToolbar();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(shoppingCartReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "requestCode : " + requestCode + "resultCode : " + resultCode);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("url");
                Log.i(TAG, "requestCode : " + requestCode + "resultCode : " + resultCode + "result : " + result);
                shoppingCartPresenter.truncateShoppingCart();
                new RetrieveSiteData().execute(result);
                Intent intentMainActivity = new Intent(this, MainActivity.class);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentMainActivity);
                finishAffinity();
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }
}

package vn.lingo.marketplace.shopping.views.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.viewpagerindicator.CirclePageIndicator;

import net.sourceforge.android.view.autoscrollviewpager.AutoScrollViewPager;

import butterknife.Bind;
import me.relex.seamlessviewpagerheader.widget.TouchCallbackLayout;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.CategoriesActivity;
import vn.lingo.marketplace.shopping.activities.HomeMoreAllEventsActivity;
import vn.lingo.marketplace.shopping.activities.LeafChildrenCategoryActivity;
import vn.lingo.marketplace.shopping.activities.MicroSiteActivity;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionParcelable;
import vn.lingo.marketplace.shopping.presenters.home.HomePresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.AbstractFragment;
import vn.lingo.marketplace.shopping.widget.HackyViewPager;

/**
 * Created by longtran on 03/11/2015.
 */
public class HomeFragment extends AbstractFragment implements HomeView {

    private final String TAG = HomeFragment.class.getName();

    @Bind(R.id.relative_layout_loading)
    RelativeLayout relativeLayoutLoading;

    @Bind(R.id.home_page_industry_fragment_view_pager_carousel_items)
    AutoScrollViewPager autoScrollViewPager;

    @Bind(R.id.indicator)
    CirclePageIndicator circlePageIndicator;

    @Bind(R.id.parallax_top_header_linear_layout_id)
    LinearLayout parallaxTopHeaderLinearLayout;

    @Bind(R.id.smart_tab_layout_industry_id)
    SmartTabLayout smartTabLayoutIndustry;

    @Bind(R.id.view_pager_industry_id)
    HackyViewPager hackyViewPagerIndustry;

    @Bind(R.id.layout)
    TouchCallbackLayout touchCallbackLayout;

    @Bind(R.id.lingo_home_page_industry_fragment_lingo_advertising_on_mobile_close_id)
    AppCompatImageView appCompatImageViewClose;

    @Bind(R.id.lingo_home_page_industry_fragment_lingo_advertising_on_mobile_id)
    AppCompatImageView appCompatImageViewLingoAdvertising;

    @Bind(R.id.lingo_home_page_industry_fragment_relative_layout_lingo_advertising_on_mobile_id)
    RelativeLayout relativeLayoutLingoAdvertising;

    private HomePresenter homePresenter;
    private HomeBroadCastReceiver homeBroadCastReceiver;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home_view;
    }

    @Override
    public RelativeLayout getRelativeLayoutLoading() {
        return relativeLayoutLoading;
    }

    @Override
    public TouchCallbackLayout getTouchCallbackLayout() {
        return touchCallbackLayout;
    }

    @Override
    public LinearLayout getParallaxTopHeaderLinearLayout() {
        return parallaxTopHeaderLinearLayout;
    }

    @Override
    public FragmentActivity getFragmentActivity() {
        return getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");
        homePresenter = new HomePresenter(this);
        new ProcessingTimes().execute();


    }
    public class HomeBroadCastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
                homePresenter.viewPager.setCurrentItem(0);
        }
    }

    /****
     *
     */
    private class ProcessingTimes extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onPreExecute() {
            homePresenter.getHomeSlides();
            homePresenter.fetchingIndustry();
            homePresenter.fetchingLingoAdvertising();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated");
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        relativeLayoutLoading.setVisibility(View.GONE);
    }

    @Override
    public void hideProcessing() {
        relativeLayoutLoading.setVisibility(View.GONE);
    }

    @Override
    public HackyViewPager getHackyViewPagerIndustry() {
        return hackyViewPagerIndustry;
    }

    @Override
    public AutoScrollViewPager getAutoScrollViewPager() {
        return autoScrollViewPager;
    }

    @Override
    public CirclePageIndicator getCirclePageIndicator() {
        return circlePageIndicator;
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getActivity().getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public DisplayImageOptions getDisplayImageOptions() {
        try {
            LingoApplication lingoApplication = (LingoApplication) getActivity().getApplication();
            return lingoApplication.getDisplayImageOptions();
        } catch (Exception exception) {
            return null;
        }
    }

    @Override
    public SmartTabLayout getSmartTabLayoutIndustry() {
        return smartTabLayoutIndustry;
    }

    @Override
    public AbstractFragment getFragment() {
        return this;
    }

    @Override
    public int getFragmentViewId() {
        return 0;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        if (zoneLevel == 3) {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.setId(id);
            categoriesParcelable.setTitle(title);
            categoriesParcelable.setMappingZoneId(mappingZoneId);
            Intent intent = new Intent();
            intent.setClassName(getContext(), LeafChildrenCategoryActivity.class.getName());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
            intent.putExtras(bundle);
            getContext().startActivity(intent);
        } else {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.setId(id);
            categoriesParcelable.setTitle(title);
            Intent intent = new Intent();
            intent.setClassName(getContext(), CategoriesActivity.class.getName());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
            intent.putExtras(bundle);
            getContext().startActivity(intent);
        }
    }

    @Override
    public void onItemClickedEventDealFixDealsEventByTypes(int position, int id, String micrositeKey, String micrositeType) {
        EventDealFixDealsCollectionParcelable eventDealFixDealsCollectionParcelable = new EventDealFixDealsCollectionParcelable();
        eventDealFixDealsCollectionParcelable.setId(id);
        eventDealFixDealsCollectionParcelable.setPosition(position);
        eventDealFixDealsCollectionParcelable.setMicrositeKey(micrositeKey);
        eventDealFixDealsCollectionParcelable.setMicrositeType(micrositeType);
        Intent intent = new Intent();
        intent.setClassName(getContext(), MicroSiteActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.EventDeal.EVENT_DEAL_FIX_DEALS_COLLECTION, eventDealFixDealsCollectionParcelable);
        intent.putExtras(bundle);
        getContext().startActivity(intent);
    }

    @Override
    public void onItemClickedMoreAllEvents() {
        Intent intent = new Intent();
        intent.setClassName(getContext(), HomeMoreAllEventsActivity.class.getName());
        getContext().startActivity(intent);
    }

    @Override
    public RelativeLayout getRelativeLayoutLingoAdvertising() {
        return relativeLayoutLingoAdvertising;
    }

    @Override
    public AppCompatImageView getAppCompatImageViewClose() {
        return appCompatImageViewClose;
    }

    @Override
    public AppCompatImageView getAppCompatImageViewLingoAdvertising() {
        return appCompatImageViewLingoAdvertising;
    }

    @Override
    public void onPause() {
        super.onPause();
        /***
         * stop auto scroll when onPause
         */
        autoScrollViewPager.stopAutoScroll();
    }

    @Override
    public void onResume() {
        AnalyticsHelper.logPageViews();
        super.onResume();
        /***
         * start auto scroll when onResume
         */
        autoScrollViewPager.startAutoScroll();
        final IntentFilter filter = new IntentFilter(Constant.ACTION.SET_CURRENT_PAGER);
        filter.addAction(Constant.ACTION.SET_CURRENT_PAGER);
        homeBroadCastReceiver = new HomeBroadCastReceiver();
        getFragmentActivity().registerReceiver(homeBroadCastReceiver, filter);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getFragmentActivity().unregisterReceiver(homeBroadCastReceiver);
    }
}

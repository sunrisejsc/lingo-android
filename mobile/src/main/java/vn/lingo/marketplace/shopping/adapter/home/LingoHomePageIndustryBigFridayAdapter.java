package vn.lingo.marketplace.shopping.adapter.home;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import net.sourceforge.android.view.autoscrollviewpager.RecyclingPagerAdapter;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.local.LingoHomePageBigFridayResponseLocalStorage;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.home.viewholder.LingoHomePageBigFridayViewHolder;

/**
 * Created by longtran on 08/11/2015.
 */
public class LingoHomePageIndustryBigFridayAdapter extends RecyclingPagerAdapter {

    private Context context;
    private List<LingoHomePageBigFridayResponseLocalStorage> listGoldenBrandResponse;
    private DisplayImageOptions displayImageOptions;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;

    public LingoHomePageIndustryBigFridayAdapter(Context context, List<LingoHomePageBigFridayResponseLocalStorage> listGoldenBrandResponse,
                                                 DisplayImageOptions displayImageOptions, RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.context = context;
        this.listGoldenBrandResponse = listGoldenBrandResponse;
        this.displayImageOptions = displayImageOptions;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
    }

    @Override
    public int getCount() {
        return listGoldenBrandResponse == null ? 0 : listGoldenBrandResponse.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        final LingoHomePageBigFridayViewHolder goldenBrandViewHolder;
        if (convertView == null) {
            /***
             * inflate the layout
             */
            convertView = LayoutInflater.from(container.getContext())
                    .inflate(R.layout.lingo_home_page_industry_view_pager_big_fri_day_layout, container, false);
            /***
             * well set up the HotNewsViewHolder
             */
            goldenBrandViewHolder = new LingoHomePageBigFridayViewHolder(convertView);
            /***
             * store the holder with the view.
             */
            convertView.setTag(goldenBrandViewHolder);
        } else {
            /***
             * we've just avoided calling findViewById() on resource everytimejust use the viewHolder
             */
            goldenBrandViewHolder = (LingoHomePageBigFridayViewHolder) convertView.getTag();
        }
        final LingoHomePageBigFridayResponseLocalStorage lingoHomePageBigFridayResponseLocalStorage = listGoldenBrandResponse.get(position);
        goldenBrandViewHolder.appCompatTextViewDescription.setText(lingoHomePageBigFridayResponseLocalStorage.getProductName());
        goldenBrandViewHolder.appCompatTextViewPrice.setText(lingoHomePageBigFridayResponseLocalStorage.getProductPriceLastFormat());
        if (StringUtils.isBlank(lingoHomePageBigFridayResponseLocalStorage.getMarketPriceFormat())) {
            goldenBrandViewHolder.appCompatTextViewMarketPrice.setVisibility(View.GONE);
        } else {
            goldenBrandViewHolder.appCompatTextViewMarketPrice.setVisibility(View.VISIBLE);
            goldenBrandViewHolder.appCompatTextViewMarketPrice.setText(lingoHomePageBigFridayResponseLocalStorage.getMarketPriceFormat());
        }
        if (StringUtils.isBlank(lingoHomePageBigFridayResponseLocalStorage.getProductDiscountLast())) {
            goldenBrandViewHolder.appCompatTextViewPercent.setVisibility(View.GONE);
        } else {
            goldenBrandViewHolder.appCompatTextViewPercent.setVisibility(View.VISIBLE);
            goldenBrandViewHolder.appCompatTextViewPercent.setText(lingoHomePageBigFridayResponseLocalStorage.getProductDiscountLast());
        }
        if (StringUtils.isBlank(lingoHomePageBigFridayResponseLocalStorage.getHappyHourDiscount())) {
            goldenBrandViewHolder.linearLayoutHappyHourDiscount.setVisibility(View.GONE);
        } else {
            goldenBrandViewHolder.linearLayoutHappyHourDiscount.setVisibility(View.VISIBLE);
            goldenBrandViewHolder.appCompatTextViewHappyHourDiscount.setText("Giờ vàng");
        }
        if (StringUtils.isBlank(lingoHomePageBigFridayResponseLocalStorage.getGiftCard())) {
            goldenBrandViewHolder.linearLayoutDiscountGiftCard.setVisibility(View.GONE);
        } else {
            goldenBrandViewHolder.linearLayoutDiscountGiftCard.setVisibility(View.VISIBLE);
            goldenBrandViewHolder.appCompatTextViewDiscountGiftCard.setText("Kèm theo quà tặng");
        }
        try {
            ImageSize targetSize = new ImageSize((int) context.getResources().getDimension(R.dimen.lingo_home_page_big_fri_day_image_height),
                    (int) context.getResources().getDimension(R.dimen.lingo_home_page_big_fri_day_image_height));
            ImageLoader.getInstance().loadImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + lingoHomePageBigFridayResponseLocalStorage.getAvatar(), targetSize,
                    displayImageOptions, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            goldenBrandViewHolder.imageView.setImageBitmap(loadedImage);
                        }
                    });
        } catch (OutOfMemoryError outOfMemoryError) {
            outOfMemoryError.printStackTrace();
        }
        goldenBrandViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(0, Integer.parseInt(lingoHomePageBigFridayResponseLocalStorage.getProductId()),
                            Integer.MAX_VALUE, Integer.MAX_VALUE, lingoHomePageBigFridayResponseLocalStorage.getProductName());
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        return convertView;
    }
}

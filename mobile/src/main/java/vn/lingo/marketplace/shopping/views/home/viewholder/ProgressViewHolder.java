package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 16/11/2015.
 */
public class ProgressViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.progress_bar_load_more)
    public ProgressBar progressBarLoadMore;

    public ProgressViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

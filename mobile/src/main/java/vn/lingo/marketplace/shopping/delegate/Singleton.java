package vn.lingo.marketplace.shopping.delegate;

/**
 * Created by lenhan on 06/04/2016.
 */
public class Singleton {
    private static Singleton singleton=null;

    public static Singleton getSingleton(){
        if (singleton==null){
            singleton=new Singleton();
        }
        return singleton;
    }

    private String attribute;
    private String codeOrder;
    private String paymentMethod;
    private int receiveGoods;
    private float shippingAmount;
    private float couponAmount;
    private String cityName;
    private String districtName;
    private int setUpdateProduct;
    private int isStarIntent;
    private String codeOrderStorage;

    public int getIsStarIntent() {
        return isStarIntent;
    }

    public void setIsStarIntent(int isStarIntent) {
        this.isStarIntent = isStarIntent;
    }

    public int getSetUpdateProduct() {
        return setUpdateProduct;
    }

    public void setSetUpdateProduct(int setUpdateProduct) {
        this.setUpdateProduct = setUpdateProduct;
    }

    public String getCodeOrderStorage() {
        return codeOrderStorage;
    }

    public void setCodeOrderStorage(String codeOrderStorage) {
        this.codeOrderStorage = codeOrderStorage;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public float getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(float couponAmount) {
        this.couponAmount = couponAmount;
    }

    public float getShippingAmount() {
        return shippingAmount;
    }

    public void setShippingAmount(float shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public int getReceiveGoods() {
        return receiveGoods;
    }

    public void setReceiveGoods(int receiveGoods) {
        this.receiveGoods = receiveGoods;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCodeOrder() {
        return codeOrder;
    }

    public void setCodeOrder(String codeOrder) {
        this.codeOrder = codeOrder;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }
}

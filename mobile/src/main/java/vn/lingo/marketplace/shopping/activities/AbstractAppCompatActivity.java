package vn.lingo.marketplace.shopping.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.mikepenz.actionitembadge.library.ActionItemBadgeAdder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ShoppingCartLocalStorage;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.JVMRuntime;

/**
 * Created by longtran on 03/11/2015.
 */
public abstract class AbstractAppCompatActivity extends AppCompatActivity {

    private final String TAG = AbstractAppCompatActivity.class.getName();
    private ShoppingCartReceiver shoppingCartReceiver;
    private final int CART_BADGE_ID = 34535;
    private int badgeCount = 0;
    private MenuItem menuItemShoppingCart;
    private static final String SHOWCASE_ID = "AbstractAppCompatActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the Currency
        //AppsFlyerLib.setCurrencyCode("VND");
        // The Dev key cab be set here or in the manifest.xml
        //AppsFlyerLib.setAppsFlyerKey("wyrVieWjHQUKx3HtBnVwoc");
        //AppsFlyerLib.sendTracking(this);
        Log.i(TAG, String.valueOf(getResources().getDimension(R.dimen.screen_size_tests)));
        final IntentFilter filter = new IntentFilter(Constant.ACTION.ADD_TO_CART_ACTION);
        filter.addAction(Constant.ACTION.ADD_TO_CART_ACTION);
        shoppingCartReceiver = new ShoppingCartReceiver();
        registerReceiver(shoppingCartReceiver, filter);
    }

    /***
     *
     */
    private void checkYourShoppingCart() {
        if (null != menuItemShoppingCart) {
            Realm realm = Realm.getDefaultInstance();
            RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class).equalTo("isOrdered", 1);
            RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
            badgeCount = realmResultsShoppingCartLocalStorage.sum("orderQuantity").intValue();
            if (badgeCount > 0) {
                ActionItemBadge.update(menuItemShoppingCart, badgeCount);
            } else {
                ActionItemBadge.hide(menuItemShoppingCart);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        AnalyticsHelper.onActivityResume(this);
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.onActivityResume(this);
        AnalyticsHelper.logPageViews();
        super.onResume();
        checkYourShoppingCart();
    }

    @Override
    public void onPause() {
        AnalyticsHelper.onActivityPause(this);
        super.onPause();
    }

    /****
     * @param newFragment
     * @param args
     * @param fragmentByTag
     */
    public void switchFragments(Fragment newFragment, Bundle args, String fragmentByTag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentPrevious = getSupportFragmentManager().findFragmentByTag(fragmentByTag);
        if (fragmentPrevious != null) {
            transaction.remove(fragmentPrevious);
            JVMRuntime.getVMRuntime().clearGrowthLimit();
        }
        if (null != args) {
            newFragment.setArguments(args);
        }
        /***
         * Replace whatever is in the fragment_container view with this fragment,
         * and add the transaction to the back stack so the user can navigate back
         */
        transaction.replace(getFragmentContainerViewId(), newFragment, fragmentByTag);
        transaction.addToBackStack(null);
        /***
         * Commit the transaction
         */
        transaction.commit();
    }

    /**
     * Every fragment has to inflate a layout in the onCreateView method. We have added this method to
     * avoid duplicate all the inflate code in every fragment. You only have to return the layout to
     * inflate in this method when extends BaseFragment.
     */
    public abstract int getFragmentContainerViewId();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_search).setIcon(R.drawable.icon_home_search);
        menu.findItem(R.id.action_shopping_cart).setIcon(R.drawable.icon_shopping_cart);
        menuItemShoppingCart = menu.findItem(R.id.action_shopping_cart);
        ActionItemBadge.update(this, menuItemShoppingCart,
                getResources().getDrawable(R.drawable.icon_shopping_cart),
                ActionItemBadge.BadgeStyles.GREY, badgeCount);
        new ActionItemBadgeAdder().act(this).menu(menu).title(R.string.app_name)
                .itemDetails(0, CART_BADGE_ID, 1)
                .showAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        checkYourShoppingCart();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            redirectSearchActivity();
            return true;
        } else if (id == R.id.action_shopping_cart) {
            redirectShoppingCartActivity();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false; //super.onOptionsItemSelected(item);
    }


    /***
     *
     */
    public void redirectSearchActivity() {
        Intent intent = new Intent();
        intent.setClassName(this, SearchActivity.class.getName());
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    /***
     *
     */
    public void redirectShoppingCartActivity() {
        Intent intent = new Intent();
        intent.setClassName(this, ShoppingCartActivity.class.getName());
        this.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onDestroy();
        unregisterReceiver(shoppingCartReceiver);
    }

    @Override
    protected void onStop() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onStop();
    }

    /***
     *
     */
    private class ShoppingCartReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Realm realm = Realm.getDefaultInstance();
            RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class).equalTo("isOrdered", 1);
            RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
            badgeCount = realmResultsShoppingCartLocalStorage.sum("orderQuantity").intValue();
            if (badgeCount > 0) {
                ActionItemBadge.update(menuItemShoppingCart, badgeCount);
            } else {
                ActionItemBadge.hide(menuItemShoppingCart);
            }
        }
    }

    /***
     * @return
     */
    public MaterialDialog getMaterialDialog() {
        MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                //.title(R.string.progress_dialog)
                //.content(R.string.please_wait)
                .customView(R.layout.progress_indicator_layout, true)
                //.progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .backgroundColorRes(R.color.transparent)
                .titleColorRes(R.color.transparent)
                .contentColor(Color.TRANSPARENT)
                .progressIndeterminateStyle(false)
                .show();
        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return materialDialog;
    }

    /***
     * @param appCompatActivity
     * @param title
     * @param content
     * @param positiveText
     * @return
     */
    public MaterialDialog getMaterialDialog(AppCompatActivity appCompatActivity, String title, String content, String positiveText) {
        return new MaterialDialog.Builder(appCompatActivity)
                .cancelable(true)
                .backgroundColorRes(R.color.white)
                .title(title)
                .titleColorRes(R.color.black)
                .content(content)
                .contentColor(Color.BLACK)
                .positiveText(positiveText)
                .positiveColorRes(R.color.black)
                .onAny(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /***
     * @return
     */
    public DisplayImageOptions getDisplayImageOptions() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getDisplayImageOptions();
    }

    /**
     * Method to get internet connection status
     *
     * @param context Context of the current activity
     * @return true     if {@link java.lang.Boolean} internet connection is established else false
     */
    public boolean isInternetConnected(Context context) {
        boolean isConnected;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = (activeNetwork != null)
                && (activeNetwork.isConnectedOrConnecting());
        return isConnected;
    }
}

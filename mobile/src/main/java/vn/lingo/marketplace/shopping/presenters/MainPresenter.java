package vn.lingo.marketplace.shopping.presenters;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.CategoriesActivity;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.MainView;
import vn.lingo.marketplace.shopping.views.OrderHistoryFragment;
import vn.lingo.marketplace.shopping.views.home.HomeFragment;

/**
 * Created by longtran on 05/11/2015.
 */
public class MainPresenter {

    private final String TAG = MainPresenter.class.getName();
    private int tempLeftMenu = Category.home.id;

    private enum Category {
        all(0x1010),
        cart(0x1011),
        category(0x1012),
        notifications(0x1013),
        purchase_history(0x1014),
        update_account(0x1015),
        sign_up(0x1016),
        sign_in(0x1017),
        settings(0x1018),
        help(0x1019),
        share(0x10120),
        profile(0x10122),
        profile_setting(0x10123),
        home(0x10124);
        public final int id;

        private Category(int id) {
            this.id = id;
        }
    }

    private MainView mainView;
    /***
     * save our header or result
     */
    private AccountHeader accountHeader;
    private Drawer drawer;

    public MainPresenter(final MainView mainView) {
        this.mainView = mainView;
    }

    /***
     *
     */
    public void invalidateToolbar() {
        //mainView.getToolbar().setLogo(R.drawable.lingo_logo_tool_bar);
        mainView.getToolbar().refreshDrawableState();
        mainView.getToolbar().setTitle("");
        mainView.getToolbar().setTitleTextColor(mainView.getContext().getResources().getColor(R.color.primary));
        mainView.getToolbar().invalidate();// restore toolbar
    }

    public void createSlidingMenu(Bundle savedInstanceState) {
        mainView.getToolbar().setTitle("");
        mainView.getToolbar().setTitleTextColor(mainView.getContext().getResources().getColor(R.color.primary));
        mainView.getAppCompatActivity().setSupportActionBar(mainView.getToolbar());
        /***
         * Create a profile
         */
        final IProfile profile = new ProfileDrawerItem()
                .withName("Anonymous").withEmail("anonymous@gmail.com")
                .withIcon(R.drawable.icon_avatar)
                .withIdentifier(Category.profile.id);
        /**
         * Create the AccountHeader
         */
        accountHeader = mainView.getAccountHeaderBuilder().addProfiles(
                //profile
                /***
                 * don't ask but google uses 14dp for the add account icon in gmail but 20dp for the normal icons (like manage account)
                 */
//                new ProfileSettingDrawerItem().withName("Add Account")
//                        .withDescription("Add New Account")
//                        .withIcon(R.drawable.icon_avatar)
//                        .withIdentifier(Category.profile_setting.id))
//                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
//                    @Override
//                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
//                        /***
//                         * IMPORTANT! notify the MiniDrawer about the profile click
//                         */
//
//                        /***
//                         * sample usage of the onProfileChanged listener if the clicked item has the identifier 1 add a new profile ;)
//                         * false if you have not consumed the event and it should close the drawer
//                         */
//                        return false;
//                    }
//                }
        )
                .withSavedInstance(savedInstanceState)
                .build();
        /***
         * Create the drawer
         */
        drawer = mainView.getDrawerBuilder()
                .withAccountHeader(accountHeader)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.sliding_menu_home)
                                .withIcon(R.drawable.icon_home_circle_blue)
                                .withIdentifier(Category.home.id),
                        new PrimaryDrawerItem().withName(R.string.sliding_menu_cart)
                                .withIcon(R.drawable.cart)
                                .withIdentifier(Category.cart.id),
                        new PrimaryDrawerItem().withName(R.string.sliding_menu_category)
                                .withIcon(R.drawable.category)
                                .withIdentifier(Category.category.id),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.sliding_menu_notifications)
                                .withIcon(R.drawable.notifications)
                                .withIdentifier(Category.notifications.id),
                        new PrimaryDrawerItem().withName(R.string.sliding_menu_purchase_history)
                                .withIcon(R.drawable.purchase_history)
                                .withIdentifier(Category.purchase_history.id),
//                        new PrimaryDrawerItem().withName(R.string.sliding_menu_update_account)
//                                .withIcon(R.drawable.update_account)
//                                .withIdentifier(Category.update_account.id),
//                        new PrimaryDrawerItem().withName(R.string.sliding_menu_sign_up)
//                                .withIcon(R.drawable.sign_up)
//                                .withIdentifier(Category.sign_up.id),
//                        new PrimaryDrawerItem().withName(R.string.sliding_menu_sign_in)
//                                .withIcon(R.drawable.sign_in)
//                                .withIdentifier(Category.sign_in.id),
                        new DividerDrawerItem(),
//                        new PrimaryDrawerItem().withName(R.string.sliding_menu_settings)
//                                .withIcon(R.drawable.settings)
//                                .withIdentifier(Category.settings.id),
                        new PrimaryDrawerItem().withName(R.string.sliding_menu_help)
                                .withIcon(R.drawable.help)
                                .withIdentifier(Category.help.id),
                        new PrimaryDrawerItem().withName(R.string.sliding_menu_share)
                                .withIcon(R.drawable.share)
                                .withIdentifier(Category.share.id)

                ) // add the items we want to use with our Drawer
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        invalidateToolbar();
                        /***
                         * check if the drawerItem is set.
                         * there are different reasons for the drawerItem to be null
                         * --> click on the header
                         * --> click on the footer
                         * those items don't contain a drawerItem
                         */
                        if (drawerItem != null) {
                            if (!mainView.isInternetConnected(mainView.getContext())) {
                                return true;
                            }
                            if (drawerItem instanceof Nameable) {
                                //mainView.getToolbar().setTitle(((Nameable) drawerItem).getName().getText(mainView.getContext()));
                                AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", ((Nameable) drawerItem).getName().getText(mainView.getContext()), true);
                            }
                            if (drawerItem.getIdentifier() == Category.cart.id) {
                                tempLeftMenu = Category.cart.id;
                                mainView.redirectShoppingCartActivity();
                            } else if (drawerItem.getIdentifier() == Category.home.id) {
                                Intent intent = new Intent();
                                intent.setAction(Constant.ACTION.SET_CURRENT_PAGER);
                                mainView.getAppCompatActivity().sendBroadcast(intent);
                                if (tempLeftMenu != Category.home.id) {
                                    mainView.switchFragments(new HomeFragment(), null, HomeFragment.class.getName());

                                }
                                tempLeftMenu = Category.home.id;
                            } else if (drawerItem.getIdentifier() == Category.category.id) {
                                CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
                                categoriesParcelable.setTitle(mainView.getContext().getResources().getString(R.string.sliding_menu_category));
                                categoriesParcelable.setId(0);
                                Intent intent = new Intent();
                                intent.setClassName(mainView.getContext(), CategoriesActivity.class.getName());
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
                                intent.putExtras(bundle);
                                mainView.getContext().startActivity(intent);
                            } else if (drawerItem.getIdentifier() == Category.purchase_history.id) {
                                if (tempLeftMenu != Category.purchase_history.id) {
                                    mainView.switchFragments(new OrderHistoryFragment(), null, OrderHistoryFragment.class.getName());
                                }
                                tempLeftMenu = Category.purchase_history.id;
                            } else if (drawerItem.getIdentifier() == Category.help.id) {
                                try {
                                    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://lingo.vn"));
                                    mainView.getAppCompatActivity().startActivity(myIntent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(mainView.getAppCompatActivity(), "No application can handle this request."
                                            + " Please install a webbrowser", Toast.LENGTH_LONG).show();
                                    Log.e(TAG, e.toString());
                                }
                            } else if (drawerItem.getIdentifier() == Category.share.id) {
                                try {
                                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                                    share.setType("text/plain");
                                    share.putExtra(Intent.EXTRA_SUBJECT, mainView.getContext().getResources().getString(R.string.lingo_mobile_app_share_title));
                                    share.putExtra(Intent.EXTRA_TEXT, mainView.getContext().getResources().getString(R.string.lingo_mobile_app_share_link));
                                    mainView.getContext().startActivity(Intent.createChooser(share, "Share link!"));
                                } catch (Exception exception) {
                                    Log.e(TAG, exception.toString());
                                }
                            } else {
                                //mainView.switchFragments(new FragmentComingSoon(), null, FragmentComingSoon.class.getName());
                            }
                        }
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();
    }

    public Drawer getDrawer() {
        return drawer;
    }


    public AccountHeader getAccountHeader() {
        return accountHeader;
    }
}

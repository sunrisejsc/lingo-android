package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 13/11/2015.
 */
public class LeafChildrenCategoryResponse implements Serializable {

    @SerializedName("IN_STORE")
    private String inStore;
    @SerializedName("TOTAL_VIEW")
    private String totalView;
    @SerializedName("AVATAR")
    private String avatar;
    @SerializedName("PRODUCT_ID")
    private String productId;
    @SerializedName("PRODUCT_NAME")
    private String productName;
    @SerializedName("NAME_LEVEL3")
    private String nameLevel3;
    @SerializedName("ATTRIBUTES_SET_ID")
    private String attributesSetId;
    @SerializedName("NAME_LEVEL2")
    private String nameLevel2;
    @SerializedName("NAME_LEVEL1")
    private String nameLevel1;
    @SerializedName("ATTRIBUTE_ID")
    private String attributeId;
    @SerializedName("RATING_NUM")
    private String ratingNum;
    @SerializedName("DISCOUNTS_TYPE")
    private String discountsType;
    @SerializedName("CREATEDON")
    private String createDon;
    @SerializedName("R")
    private String r;
    @SerializedName("PRODUCT_PRICE")
    private String productPrice;
    @SerializedName("PRODUCT_PRICEDISCOUNT")
    private String productPriceDiscount;
    @SerializedName("ISINSTALLMENT")
    private String isInstallment;
    @SerializedName("PRODUCT_QUANTITY")
    private String productQuantity;
    @SerializedName("MARKET_PRICE")
    private String marketPrice;
    @SerializedName("PROMOTIONPERCENT")
    private String promotionPercent;
    @SerializedName("RATING_STAR")
    private String ratingStar;
    @SerializedName("NUMBER_VALUE")
    private String numberValue;
    @SerializedName("PROMOTION_DISCOUNT")
    private String promotionDiscount;
    @SerializedName("SHIP_FREE")
    private String shipFree;
    @SerializedName("SECTOR_CAT_ID")
    private String sectorCatId;
    @SerializedName("PARENT_LEVEL1")
    private String parentLevel1;
    @SerializedName("TRADEMARK")
    private String trademark;
    @SerializedName("ISACTIVE")
    private String isActive;
    @SerializedName("PARENT_LEVEL2")
    private String parentLevel2;
    @SerializedName("MARKET_PRICE_FORMAT")
    private String marketPriceFormat;
    @SerializedName("PRODUCT_PRICE_FORMAT")
    private String productPriceFormat;
    @SerializedName("PRODUCT_PRICEDISCOUNT_FORMAT")
    private String productPriceDiscountFormat;
    @SerializedName("LOGO_THUONGHIEU")
    private String logoBrand;

    public String getLogoBrand() {
        return logoBrand;
    }

    public void setLogoBrand(String logoBrand) {
        this.logoBrand = logoBrand;
    }

    public String getInStore() {
        return inStore;
    }

    public void setInStore(String inStore) {
        this.inStore = inStore;
    }

    public String getTotalView() {
        return totalView;
    }

    public void setTotalView(String totalView) {
        this.totalView = totalView;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getNameLevel3() {
        return nameLevel3;
    }

    public void setNameLevel3(String nameLevel3) {
        this.nameLevel3 = nameLevel3;
    }

    public String getAttributesSetId() {
        return attributesSetId;
    }

    public void setAttributesSetId(String attributesSetId) {
        this.attributesSetId = attributesSetId;
    }

    public String getNameLevel2() {
        return nameLevel2;
    }

    public void setNameLevel2(String nameLevel2) {
        this.nameLevel2 = nameLevel2;
    }

    public String getNameLevel1() {
        return nameLevel1;
    }

    public void setNameLevel1(String nameLevel1) {
        this.nameLevel1 = nameLevel1;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getRatingNum() {
        return ratingNum;
    }

    public void setRatingNum(String ratingNum) {
        this.ratingNum = ratingNum;
    }

    public String getDiscountsType() {
        return discountsType;
    }

    public void setDiscountsType(String discountsType) {
        this.discountsType = discountsType;
    }

    public String getCreateDon() {
        return createDon;
    }

    public void setCreateDon(String createDon) {
        this.createDon = createDon;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductPriceDiscount() {
        return productPriceDiscount;
    }

    public void setProductPriceDiscount(String productPriceDiscount) {
        this.productPriceDiscount = productPriceDiscount;
    }

    public String getIsInstallment() {
        return isInstallment;
    }

    public void setIsInstallment(String isInstallment) {
        this.isInstallment = isInstallment;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getPromotionPercent() {
        return promotionPercent;
    }

    public void setPromotionPercent(String promotionPercent) {
        this.promotionPercent = promotionPercent;
    }

    public String getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(String ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(String numberValue) {
        this.numberValue = numberValue;
    }

    public String getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(String promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public String getShipFree() {
        return shipFree;
    }

    public void setShipFree(String shipFree) {
        this.shipFree = shipFree;
    }

    public String getSectorCatId() {
        return sectorCatId;
    }

    public void setSectorCatId(String sectorCatId) {
        this.sectorCatId = sectorCatId;
    }

    public String getParentLevel1() {
        return parentLevel1;
    }

    public void setParentLevel1(String parentLevel1) {
        this.parentLevel1 = parentLevel1;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getParentLevel2() {
        return parentLevel2;
    }

    public void setParentLevel2(String parentLevel2) {
        this.parentLevel2 = parentLevel2;
    }

    public String getMarketPriceFormat() {
        return marketPriceFormat;
    }

    public void setMarketPriceFormat(String marketPriceFormat) {
        this.marketPriceFormat = marketPriceFormat;
    }

    public String getProductPriceFormat() {
        return productPriceFormat;
    }

    public void setProductPriceFormat(String productPriceFormat) {
        this.productPriceFormat = productPriceFormat;
    }

    public String getProductPriceDiscountFormat() {
        return productPriceDiscountFormat;
    }

    public void setProductPriceDiscountFormat(String productPriceDiscountFormat) {
        this.productPriceDiscountFormat = productPriceDiscountFormat;
    }
}

package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lenhan on 06/04/2016.
 */
public class OrderShippingInfo implements Serializable {
    @SerializedName("USER_LOGIN_ID")
    int userLoginID;
    @SerializedName("SHIP_TYPE")
    int shipType;
    @SerializedName("RECEIVE_ORDER_TYPE")
    int receiveOrderType;
    @SerializedName("RECEIVE_CITY_ID")
    int receiveCityID;
    @SerializedName("RECEIVE_DISTRICT_ID")
    int receiveDistrictID;

    public OrderShippingInfo(int userLoginID, int shipType, int receiveOrderType, int receiveCityID, int receiveDistrictID) {
        this.userLoginID = userLoginID;
        this.shipType = shipType;
        this.receiveOrderType = receiveOrderType;
        this.receiveCityID = receiveCityID;
        this.receiveDistrictID = receiveDistrictID;
    }

    public int getShipType() {
        return shipType;
    }

    public void setShipType(int shipType) {
        this.shipType = shipType;
    }

    public int getUserLoginID() {
        return userLoginID;
    }

    public void setUserLoginID(int userLoginID) {
        this.userLoginID = userLoginID;
    }

    public int getReceiveOrderType() {
        return receiveOrderType;
    }

    public void setReceiveOrderType(int receiveOrderType) {
        this.receiveOrderType = receiveOrderType;
    }

    public int getReceiveCityID() {
        return receiveCityID;
    }

    public void setReceiveCityID(int receiveCityID) {
        this.receiveCityID = receiveCityID;
    }

    public int getReceiveDistrictID() {
        return receiveDistrictID;
    }

    public void setReceiveDistrictID(int receiveDistrictID) {
        this.receiveDistrictID = receiveDistrictID;
    }
}

package vn.lingo.marketplace.shopping;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.flurry.android.FlurryAgent;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.network.Endpoint;

/**
 * Application class for the contact explorer app.
 * Created by longtran on 15/09/2015.
 */
public class LingoApplication extends Application {

    private Retrofit retrofit;
    private Retrofit retrofitProductCheckoutOld;
    private Retrofit retrofitCheckoutTest;
    private DisplayImageOptions options;

    @Override
    public void onCreate() {
        super.onCreate();
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        retrofit = new Retrofit.Builder()
                .baseUrl(Endpoint.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
//        retrofitProductCheckoutOld = new Retrofit.Builder()
//                .baseUrl(Endpoint.ENDPOINT_CHECK_OUT)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
        retrofitCheckoutTest = new Retrofit.Builder()
                .baseUrl(Endpoint.ENDPOINT_CHECK_OUT_TEST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        if (isSDPresent) {
            try {

                RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                        .name("lingo")
                        .schemaVersion(18)
                        .deleteRealmIfMigrationNeeded()
                        .build();
                Realm.setDefaultConfiguration(realmConfiguration);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setTitle("Lỗi SDCard");
            builder.setMessage("Kiểm tra SDCard");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        }

        initImageLoader(this);
        //initialize and create the image loader logic
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Glide.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Glide.clear(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                //define different placeholders for different imageView targets
                //default tags are accessible via the DrawerImageLoader.Tags
                //custom ones can be checked via string. see the CustomUrlBasePrimaryDrawerItem LINE 111
                if (DrawerImageLoader.Tags.PROFILE.name().equals(tag)) {
                    return DrawerUIUtils.getPlaceHolder(ctx);
                } else if (DrawerImageLoader.Tags.ACCOUNT_HEADER.name().equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(com.mikepenz.materialdrawer.R.color.primary).sizeDp(56);
                } else if ("customUrlItem".equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(R.color.md_red_500).sizeDp(56);
                }
                //we use the default one for
                //DrawerImageLoader.Tags.PROFILE_DRAWER_ITEM.name()
                return super.placeholder(ctx, tag);
            }
        });
        try {
            FlurryAgent.setLogEnabled(true);
            FlurryAgent.setLogLevel(Log.INFO);
            FlurryAgent.setVersionName("1.1.6");
            FlurryAgent.init(this, "424VJM7W95M64NW28JN3");
        }catch (Exception e){
            e.printStackTrace();
        }


        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
        // Add your initialization code here
        Parse.initialize(this);
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        // Optionally enable public read access.
        // defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);
    }

    /***
     * @param context
     */
    public void initImageLoader(Context context) {
        /***
         * This configuration tuning is custom. You can tune every option, you may tune some of them,
         * or you can create default configuration by ImageLoaderConfiguration.createDefault(this); method.
         */
        File cacheDir = StorageUtils.getCacheDirectory(context);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(480, 768)
                .diskCacheExtraOptions(480, 768, null)
                .threadPoolSize(3) // default
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(5 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCacheSize(200 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(context)) // default
                .imageDecoder(new BaseImageDecoder(true)) // default
                .diskCache(new UnlimitedDiskCache(cacheDir)) // default
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(context)) // default
                //.defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }

    /****
     * @return
     */
    public DisplayImageOptions getDisplayImageOptions() {
        BitmapFactory.Options resizeOptions = new BitmapFactory.Options();
        resizeOptions.inSampleSize = 3; // decrease size 3 times
        resizeOptions.inScaled = true;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon_loading_throbber)
                .showImageForEmptyUri(R.drawable.icon_loading_throbber)
                .showImageOnFail(R.drawable.icon_loading_throbber)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .cacheOnDisk(true)
                .displayer(new FadeInBitmapDisplayer(100))
                .decodingOptions(resizeOptions)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        return options;
    }

    /***
     * @return
     */
    public Retrofit getRetrofit() {
        return retrofit;
    }

    /***
     * @return
     */
//    public Retrofit getRetrofitProductCheckoutOld() {
//        return retrofitProductCheckoutOld;
//    }

    public Retrofit getRetrofitCheckOutTest() {
        return retrofitCheckoutTest;
    }
}
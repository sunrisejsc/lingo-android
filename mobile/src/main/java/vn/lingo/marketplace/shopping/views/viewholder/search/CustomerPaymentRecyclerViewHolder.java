package vn.lingo.marketplace.shopping.views.viewholder.search;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class CustomerPaymentRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.item_product_payment_image)
    public ImageView imageView;

    @Bind(R.id.item_product_payment_product_name)
    public AppCompatTextView productName;

    @Bind(R.id.item_product_payment_product_quantity)
    public AppCompatTextView productQuantity;

    @Bind(R.id.item_product_payment_add)
    public ImageView addProduct;

    @Bind(R.id.item_product_payment_sub)
    public ImageView subProduct;

    @Bind(R.id.item_product_payment_remove)
    public ImageView removeProduct;

    @Bind(R.id.item_product_payment_price)
    public AppCompatTextView productPrice;

    public CustomerPaymentRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

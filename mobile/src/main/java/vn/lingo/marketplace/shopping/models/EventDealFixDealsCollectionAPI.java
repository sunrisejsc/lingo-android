package vn.lingo.marketplace.shopping.models;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 09/12/2015.
 */
public interface EventDealFixDealsCollectionAPI {

    @GET("/v1/mobile/Deal/GetEventDealDetail")
    Call<EventDealFixDealsCollectionTemplateResponse> getEventDealDetail(@Query("p_key") String p_key, @Query("p_type") int p_type);

    @GET("/v1/mobile/Deal/GetBoxInfoAndItemInfo")
    Call<BoxInfoAndItemInfo> getBoxInfoAndItemInfo(@Query("p_boxtempid") int p_boxtempid, @Query("p_detailid") int p_detailid);
}

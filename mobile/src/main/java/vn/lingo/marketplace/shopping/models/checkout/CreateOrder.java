package vn.lingo.marketplace.shopping.models.checkout;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;

/**
 * Created by zon on 07/04/2016.
 */
public class CreateOrder implements Serializable {
    @SerializedName("Order")
    private Order order;
    @SerializedName("InstallmentOrder")
    private InstallmentOrder installmentOrder;
    @SerializedName("MobileInfo")
    private MobileInfo mobileInfo;
    @SerializedName("ListOrderDetail")
    List<OrderDetail> listOrderDetail;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public InstallmentOrder getInstallmentOrder() {
        return installmentOrder;
    }

    public void setInstallmentOrder(InstallmentOrder installmentOrder) {
        this.installmentOrder = installmentOrder;
    }

    public MobileInfo getMobileInfo() {
        return mobileInfo;
    }

    public void setMobileInfo(MobileInfo mobileInfo) {
        this.mobileInfo = mobileInfo;
    }

    public List<OrderDetail> getListOrderDetail() {
        return listOrderDetail;
    }

    public void setListOrderDetail(List<OrderDetail> listOrderDetail) {
        this.listOrderDetail = listOrderDetail;
    }

    public static String getJsonCreateOrder(Order order,InstallmentOrder installmentOrder,MobileInfo mobileInfo,List<OrderDetail> listOrderDetail){
        CreateOrder createOrder=new CreateOrder();
        createOrder.setOrder(order);
        createOrder.setInstallmentOrder(installmentOrder);
        createOrder.setMobileInfo(mobileInfo);
        createOrder.setListOrderDetail(listOrderDetail);
        String jsonObject=new Gson().toJson(createOrder);
        HashMap<String, String> fetchPhotoStatEventParams = new HashMap<String, String>();
        fetchPhotoStatEventParams.put(AnalyticsHelper.EVENT_PRODUCT_ORDER_DATA_REQUEST, jsonObject);
        AnalyticsHelper.logEvent(AnalyticsHelper.EVENT_PRODUCT_ORDER, fetchPhotoStatEventParams, true);
        Log.e("Json_create_order", jsonObject);

        return jsonObject;
    }

}

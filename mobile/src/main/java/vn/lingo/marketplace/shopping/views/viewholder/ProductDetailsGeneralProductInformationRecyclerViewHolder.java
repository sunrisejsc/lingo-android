package vn.lingo.marketplace.shopping.views.viewholder;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class ProductDetailsGeneralProductInformationRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.product_details_general_product_information_recycler_view_item_product_attributes)
    public AppCompatTextView productDetailsGeneralProductInformationRecyclerViewItemProductAttributes;

    public ProductDetailsGeneralProductInformationRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        productDetailsGeneralProductInformationRecyclerViewItemProductAttributes.setEllipsize(TextUtils.TruncateAt.END);
        productDetailsGeneralProductInformationRecyclerViewItemProductAttributes.setMaxLines(2);
    }
}

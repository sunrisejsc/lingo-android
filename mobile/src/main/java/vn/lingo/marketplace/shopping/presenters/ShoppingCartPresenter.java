package vn.lingo.marketplace.shopping.presenters;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.thefinestartist.finestwebview.FinestWebView;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.CustomerPaymentOrdersActivity;
import vn.lingo.marketplace.shopping.adapter.ShoppingCartRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.listeners.ShoppingCartListener;
import vn.lingo.marketplace.shopping.models.ProductCheckoutAPI;
import vn.lingo.marketplace.shopping.models.ProductCheckoutRequest;
import vn.lingo.marketplace.shopping.models.ProductCheckoutResponse;
import vn.lingo.marketplace.shopping.models.ShoppingCartEntity;
import vn.lingo.marketplace.shopping.models.ShoppingCartLocalStorage;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.NumberFormat;
import vn.lingo.marketplace.shopping.views.ShoppingCartView;

/**
 * Created by longtran on 12/11/2015.
 */
public class ShoppingCartPresenter extends AbstractPresenter<ShoppingCartView> implements RecyclerViewItemClickListener, ShoppingCartListener {
    private final String TAG = ShoppingCartPresenter.class.getName();
    private ShoppingCartView shoppingCartView;
    private ShoppingCartRecyclerViewAdapter shoppingCartRecyclerViewAdapter;
    private List<ShoppingCartEntity> listShoppingCartEntity;

    /***
     * @param shoppingCartView
     */
    public ShoppingCartPresenter(final ShoppingCartView shoppingCartView) {
        this.shoppingCartView = shoppingCartView;
        listShoppingCartEntity = new ArrayList<ShoppingCartEntity>();
        shoppingCartRecyclerViewAdapter = new ShoppingCartRecyclerViewAdapter(shoppingCartView.getAppCompatActivity(),
                listShoppingCartEntity, ShoppingCartPresenter.this, ShoppingCartPresenter.this, shoppingCartView.getDisplayImageOptions());
        shoppingCartView.getRecyclerViewShoppingCart().setAdapter(shoppingCartRecyclerViewAdapter);
        shoppingCartView.getAppCompatButtonShoppingCartBuy().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                Intent intent = new Intent();
                intent.setClassName(shoppingCartView.getContext(), CustomerPaymentOrdersActivity.class.getName());
                intent.putExtra(Constant.LIST_SHOPPING_CART_ENTITY, (Serializable) listShoppingCartEntity);
                shoppingCartView.getContext().startActivity(intent);
                shoppingCartView.getAppCompatActivity().finish();
//////
//                RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class).equalTo("isOrdered", 1);
//                RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
//                ProductCheckoutAPI productCheckoutAPI = shoppingCartView.getRetrofitProductCheckout().create(ProductCheckoutAPI.class);
//                AnalyticsHelper.logEventOrder(shoppingCartView.getContext(), "PURCHASE", realmResultsShoppingCartLocalStorage);
//                Call<ProductCheckoutResponse> callProductCheckoutResponse = productCheckoutAPI.productCheckout(ProductCheckoutRequest.getProductCheckoutRequest(
//                        realmResultsShoppingCartLocalStorage));
//                callProductCheckoutResponse.enqueue(new Callback<ProductCheckoutResponse>() {
//                    @Override
//                    public void onResponse(Response<ProductCheckoutResponse> response, Retrofit retrofit) {
//                        ProductCheckoutResponse productCheckoutResponse = response.body();
//                        if (response.code() == 200) {
//                            if (!StringUtils.isBlank(productCheckoutResponse.getRedirectWapURL())) {
//                                new FinestWebView.Builder(shoppingCartView.getAppCompatActivity()).show(productCheckoutResponse.getRedirectWapURL());
//
//                            }
//                        } else {
//                            shoppingCartView.getMaterialDialogAlert(shoppingCartView.getAppCompatActivity(),
//                                    shoppingCartView.getContext().getResources().getString(R.string.app_name),
//                                    shoppingCartView.getContext().getResources().getString(R.string.product_invalidate),
//                                    shoppingCartView.getContext().getResources().getString(R.string.material_dialog_positive_text));
//                        }
//                        shoppingCartView.hideProcessing();
//                    }
//
//                    @Override
//                    public void onFailure(Throwable t) {
//                        shoppingCartView.hideProcessing();
//                    }
//                });

            }
        });
    }

    /****
     *
     */
    public void truncateShoppingCart() {
        RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class).equalTo("isOrdered", 1);
        RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
        // All changes to data must happen in a transaction
        realm.beginTransaction();
        realmResultsShoppingCartLocalStorage.clear();
        realm.commitTransaction();
    }

    /***
     *
     */
    public void fetchingShoppingCart() {
        shoppingCartView.showProcessing();
        shoppingCartView.getToolbar().setTitle("");
        shoppingCartView.getToolbar().setTitleTextColor(shoppingCartView.getContext().getResources().getColor(R.color.primary));
        shoppingCartView.getAppCompatActivity().setSupportActionBar(shoppingCartView.getToolbar());
        shoppingCartView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        shoppingCartView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        shoppingCartView.getToolbar().setNavigationIcon(R.drawable.close_delete_remove_icon);
        shoppingCartView.getToolbar().invalidate();// restore toolbar
        setAdapter();
    }

    /***
     *
     */
    public void invalidateToolbar() {
        //mainView.getToolbar().setLogo(R.drawable.lingo_logo_tool_bar);
        shoppingCartView.getToolbar().setTitle("");
        shoppingCartView.getToolbar().setTitleTextColor(shoppingCartView.getContext().getResources().getColor(R.color.primary));
        shoppingCartView.getToolbar().invalidate();// restore toolbar
    }

    /***
     *
     */
    public void setAdapter() {
        listShoppingCartEntity.clear();
        shoppingCartRecyclerViewAdapter.notifyDataSetChanged();
        RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class);
        RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
        realmResultsShoppingCartLocalStorage.sort("timeStamp", RealmResults.SORT_ORDER_DESCENDING);
        Iterator<ShoppingCartLocalStorage> iterator = realmResultsShoppingCartLocalStorage.iterator();
        while (iterator.hasNext()) {
            ShoppingCartLocalStorage shoppingCartLocalStorage = iterator.next();
            ShoppingCartEntity shoppingCartEntity = new ShoppingCartEntity();
            shoppingCartEntity.setCartID(shoppingCartLocalStorage.getCartID());
            shoppingCartEntity.setOrderQuantity(shoppingCartLocalStorage.getOrderQuantity());
            shoppingCartEntity.setValueWeight(shoppingCartLocalStorage.getValue());
            shoppingCartEntity.setProductName(shoppingCartLocalStorage.getProductName());
            shoppingCartEntity.setMarketPrice(shoppingCartLocalStorage.getMarketPrice());
            shoppingCartEntity.setProductPrice(shoppingCartLocalStorage.getProductPrice());
            shoppingCartEntity.setAvatar(shoppingCartLocalStorage.getAvatar());
            shoppingCartEntity.setIsOrdered(shoppingCartLocalStorage.getIsOrdered());
            shoppingCartEntity.setProductPriceFormat(shoppingCartLocalStorage.getProductPriceFormat());
            shoppingCartEntity.setMarketPriceFormat(shoppingCartLocalStorage.getMarketPriceFormat());
            shoppingCartEntity.setSku(shoppingCartLocalStorage.getSku());
            shoppingCartEntity.setTimeStamp(shoppingCartLocalStorage.getTimeStamp());
            shoppingCartEntity.setBigType(shoppingCartLocalStorage.getBigType());
            listShoppingCartEntity.add(shoppingCartEntity);
            shoppingCartRecyclerViewAdapter.notifyDataSetChanged();
        }
        fetchingSummaryInformation();
    }

    /***
     *
     */
    private void fetchingSummaryInformation() {
        RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class).equalTo("isOrdered", 1);
        RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
        int totalOrdered = realmResultsShoppingCartLocalStorage.sum("orderQuantity").intValue();
        shoppingCartView.getAppCompatTextViewShoppingCartSummary().setText(String.format(shoppingCartView.getContext().getResources().getString(
                R.string.shopping_cart_summary), totalOrdered));
        Iterator<ShoppingCartLocalStorage> iterator = realmResultsShoppingCartLocalStorage.iterator();
        float subTotal = 0;//realmQueryShoppingCartLocalStorage.sum("productPrice").floatValue();
        while (iterator.hasNext()) {
            ShoppingCartLocalStorage shoppingCartLocalStorage = iterator.next();
            subTotal += shoppingCartLocalStorage.getOrderQuantity() * shoppingCartLocalStorage.getProductPrice();
        }
        BigDecimal bigDecimal = new BigDecimal(subTotal);
        shoppingCartView.getAppCompatTextViewShoppingCartTotalValue().setText(String.format(shoppingCartView.getContext().getResources().getString(
                R.string.shopping_cart_total_value), NumberFormat.priceToString(bigDecimal.floatValue())));
        //shoppingCartView.getAppCompatTextViewShoppingCartSubtotalValue().setText(NumberFormat.priceToString(bigDecimal.floatValue()));
        shoppingCartView.getAppCompatTextViewShoppingCartTotalQuantity().setText(String.format(shoppingCartView.getContext().getResources().getString(
                R.string.shopping_cart_total_quantity_label), NumberFormat.priceToString(bigDecimal.floatValue())));
        if (totalOrdered > 0) {
            shoppingCartView.getAppCompatButtonShoppingCartBuy().setEnabled(true);
        } else {
            shoppingCartView.getAppCompatButtonShoppingCartBuy().setEnabled(false);
        }
    }

    /****
     * @param shoppingCartEntity
     */
    private void deleteItemShoppingCart(ShoppingCartEntity shoppingCartEntity) {
        RealmQuery<ShoppingCartLocalStorage> realmQuery = realm.where(ShoppingCartLocalStorage.class)
                .equalTo("cartID", shoppingCartEntity.getCartID());
        RealmResults<ShoppingCartLocalStorage> realmResults = realmQuery.findAll();
        realm.beginTransaction();
        realmResults.clear();
        realm.commitTransaction();
        setAdapter();
    }

    /***
     * @param shoppingCartEntity
     */
    private void updateItemShoppingCart(ShoppingCartEntity shoppingCartEntity) {
        ShoppingCartLocalStorage shoppingCartLocalStorage = new ShoppingCartLocalStorage();
        shoppingCartLocalStorage.setCartID(shoppingCartEntity.getCartID());
        shoppingCartLocalStorage.setOrderQuantity(shoppingCartEntity.getOrderQuantity());
        shoppingCartLocalStorage.setAvatar(shoppingCartEntity.getAvatar());
        shoppingCartLocalStorage.setProductName(shoppingCartEntity.getProductName());
        shoppingCartLocalStorage.setMarketPrice(shoppingCartEntity.getMarketPrice());
        shoppingCartLocalStorage.setProductPrice(shoppingCartEntity.getProductPrice());
        shoppingCartLocalStorage.setIsOrdered(shoppingCartEntity.getIsOrdered());
        shoppingCartLocalStorage.setMarketPriceFormat(shoppingCartEntity.getMarketPriceFormat());
        shoppingCartLocalStorage.setProductPriceFormat(shoppingCartEntity.getProductPriceFormat());
        shoppingCartLocalStorage.setSku(shoppingCartEntity.getSku());
        shoppingCartLocalStorage.setTimeStamp(shoppingCartEntity.getTimeStamp());
        shoppingCartLocalStorage.setBigType(shoppingCartEntity.getBigType());
        shoppingCartLocalStorage.setValue(shoppingCartEntity.getValueWeight());
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(shoppingCartLocalStorage);
            realm.commitTransaction();
        } catch (Exception exception) {
            exception.printStackTrace();
            Log.e(TAG, exception.toString());
        }
        fetchingSummaryInformation();
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        shoppingCartView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }

    @Override
    public void setOnListener(int actionID, int position, ShoppingCartEntity shoppingCartEntity) {
        switch (actionID) {
            case Constant.ON_CLICK_PROCESS_ORDERED:
                updateItemShoppingCart(shoppingCartEntity);
                break;
            case Constant.ON_CLICK_DELETED_ORDER:
                deleteItemShoppingCart(shoppingCartEntity);
                Intent intent = new Intent();
                intent.setAction(Constant.ACTION.ADD_TO_CART_ACTION);
                shoppingCartView.getAppCompatActivity().sendBroadcast(intent);
                break;
            default:
                break;
        }

    }
}

package vn.lingo.marketplace.shopping.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.delegate.Singleton;
import vn.lingo.marketplace.shopping.interfaces.CustomerPaymentOrdersView;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.presenters.CustomerPaymentOrdersPresenter;
import vn.lingo.marketplace.shopping.service.RetrieveSiteData;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;

/**
 * Created by Administrator on 3/23/2016.
 */
public class CustomerPaymentOrdersActivity extends AbstractAppCompatActivity implements CustomerPaymentOrdersView, Animation.AnimationListener {
    private final String TAG = PaymentOrderSuccessActivity.class.getName();

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;
    @Bind(R.id.your_order_information_part_order_information_recycler_product_purchase)
    RecyclerView recyclerViewProductPurchase;
    @Bind(R.id.customer_order_information_image_view_recycler)
    AppCompatImageView imageViewRecycler;
    @Bind(R.id.customer_order_information_button_apply_code_sale)
    AppCompatButton buttonApplyCodeSale;
    @Bind(R.id.customer_order_information_editText_code_sale)
    AppCompatEditText editTextCodeSale;
    @Bind(R.id.customer_order_information_all_money_product)
    AppCompatTextView textViewAllMoneyProduct;
    @Bind(R.id.customer_order_information_transport_fee)
    AppCompatTextView textViewTransportFee;
    @Bind(R.id.customer_order_information_price_discounted)
    AppCompatTextView textViewPriceDiscounted;
    @Bind(R.id.customer_order_information_money_pay)
    AppCompatTextView textViewMoneyPay;
    @Bind(R.id.customer_authentication_edittext_name_consignee)
    AppCompatEditText editTextNameConsignee;
    @Bind(R.id.customer_authentication_edittext_phone_consignee)
    AppCompatEditText editTextPhoneConsignee;
    @Bind(R.id.customer_authentication_edittext_email_consignee)
    AppCompatEditText editTextEmailConsignee;
    @Bind(R.id.customer_authentication_radio_button_shipping_address_option)
    AppCompatRadioButton radioButtonShippingAddressOption;
    @Bind(R.id.customer_authentication_radio_button_shipping_lingo_address)
    AppCompatRadioButton radioButtonShippingAddressLingo;
    @Bind(R.id.customer_authentication_spinner_shipping_city)
    AppCompatSpinner spinnerSippingCity;
    @Bind(R.id.customer_authentication_spinner_shipping_district)
    AppCompatSpinner spinnerSippingDistrict;
    @Bind(R.id.customer_authentication_edittext_address)
    AppCompatEditText editTextShippingAddress;
    @Bind(R.id.customer_authentication_spinner_shipping_method)
    AppCompatSpinner spinnerShippingMethod;
    @Bind(R.id.customer_authentication_edittext_note)
    AppCompatEditText editTextNote;
    @Bind(R.id.customer_authentication_checkbox_receive_bill)
    AppCompatCheckBox checkBoxReceiveBill;
    @Bind(R.id.customer_authentication_edittext_name_company)
    AppCompatEditText editTextNameCompany;
    @Bind(R.id.customer_authentication_edittext_code_tax_company)
    AppCompatEditText editTextTaxCompany;
    @Bind(R.id.customer_authentication_edittext_address_company)
    AppCompatEditText editTextAddressCompany;
    @Bind(R.id.customer_authentication_spinner_shipping_city_company)
    AppCompatSpinner spinnerCityCompany;
    @Bind(R.id.customer_authentication_spinner_shipping_district_company)
    AppCompatSpinner spinnerDistrictCompany;
    @Bind(R.id.customer_payment_method_radio_payment_receipt_goods)
    AppCompatRadioButton radioButtonPaymentReceiptGoods;
    @Bind(R.id.customer_payment_method_radio_internet_banking)
    AppCompatRadioButton radioButtonInternetBanking;
    @Bind(R.id.customer_payment_method_radio_transfer_get_bill)
    AppCompatRadioButton radioButtonTransferGetBill;
    @Bind(R.id.customer_payment_method_radio_visa_mater)
    AppCompatRadioButton radioButtonVisaMater;
    @Bind(R.id.customer_payment_method_radio_account_lingo)
    AppCompatRadioButton radioButtonAccountLingo;
    @Bind(R.id.customer_payment_method_spinner_name_banking)
    AppCompatSpinner spinnerNameBanking;
    @Bind(R.id.customer_payment_method_name_banking)
    AppCompatTextView textViewNameBanking;
    @Bind(R.id.customer_payment_method_number_account_banking)
    AppCompatTextView textViewNumberAccountBanking;
    @Bind(R.id.customer_payment_method_account_owner_banking)
    AppCompatTextView textViewAccountOwnerBanking;
    @Bind(R.id.customer_payment_method_button_complete_payment)
    AppCompatButton buttonCompletePayment;
    @Bind(R.id.customer_authentication_layout_view_receipt_goods_location_desired)
    LinearLayout linearLayoutViewReceiptGoodsLocationDesired;
    @Bind(R.id.customer_payment_method_layout_view_transfer_receipt_bill)
    LinearLayout linearLayoutViewTransferReceiptBill;
    @Bind(R.id.customer_payment_method_radio_group_payments)
    RadioGroup radioGroupPayments;
    @Bind(R.id.customer_authentication_radio_group_shipping_address)
    RadioGroup radioGroupShippingAddress;
    @Bind(R.id.customer_authentication_information_view_receive_bill)
    LinearLayout linearLayoutViewReceiveBill;
    @Bind(R.id.customer_authentication_radio_group_store_address)
    RadioGroup radioGroupStoreAddress;
    @Bind(R.id.customer_order_information_layout_show_list_product)
    RelativeLayout linearLayoutShowListProduct;
    CustomerPaymentOrdersPresenter customerPaymentOrdersPresenter;
    Animation animationVisible;
    Animation animationGone;
    private boolean isViewRecycler = false;
    private MaterialDialog materialDialog;
    private Retrofit retrofitCheckCoupon;

    @Override
    public int getFragmentContainerViewId() {
        return R.id.frame_container;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_customer_payment_orders);
        ButterKnife.bind(this);


        retrofitCheckCoupon = new Retrofit.Builder()
                .baseUrl(Endpoint.ENDPOINT_CHECK_ECOUPON)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        animationVisible = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.visible_recycler);
        animationVisible.setAnimationListener(this);
        animationGone = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.gone_recycler);
        animationGone.setAnimationListener(this);
        linearLayoutShowListProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isViewRecycler == false) {
                    recyclerViewProductPurchase.setVisibility(View.VISIBLE);
                    recyclerViewProductPurchase.startAnimation(animationVisible);
                    imageViewRecycler.setRotation(90);
                    isViewRecycler = true;
                } else {
                    recyclerViewProductPurchase.setVisibility(View.GONE);
                    recyclerViewProductPurchase.startAnimation(animationGone);
                    imageViewRecycler.setRotation(0);
                    isViewRecycler = false;
                }
            }
        });


        customerPaymentOrdersPresenter = new CustomerPaymentOrdersPresenter(this);
        customerPaymentOrdersPresenter.fetchingShoppingCart();


    }

    protected void hideSoftKeyboard(AppCompatEditText input) {
        input.setInputType(0);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        customerPaymentOrdersPresenter.deleteRealmCity();
//        customerPaymentOrdersPresenter.deleteRealmDistrict();
    }


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public AppCompatImageView getImageViewRecycler() {
        return imageViewRecycler;
    }

    @Override
    public RecyclerView getRecyclerViewProductPurchase() {
        recyclerViewProductPurchase.addItemDecoration(new DividerItemDecoration(1));
        recyclerViewProductPurchase.setLayoutManager(new GridLayoutManager(getContext(), 1));
        return recyclerViewProductPurchase;
    }

    @Override
    public AppCompatButton getButtonApplyCodeSale() {
        return buttonApplyCodeSale;
    }

    @Override
    public AppCompatEditText getEditTextCodeSale() {
        return editTextCodeSale;
    }

    @Override
    public AppCompatTextView getTextViewAllMoneyProduct() {
        return textViewAllMoneyProduct;
    }

    @Override
    public AppCompatTextView getTextViewTransportFee() {
        return textViewTransportFee;
    }

    @Override
    public AppCompatTextView getTextViewPriceDiscounted() {
        return textViewPriceDiscounted;
    }

    @Override
    public AppCompatTextView getTextViewMoneyPay() {
        return textViewMoneyPay;
    }

    @Override
    public AppCompatEditText getEditTextFullNameConsignee() {
        return editTextNameConsignee;
    }

    @Override
    public AppCompatEditText getEditTextNumberPhoneConsignee() {
        return editTextPhoneConsignee;
    }

    @Override
    public AppCompatEditText getEditTextEmailConsignee() {
        return editTextEmailConsignee;
    }

    @Override
    public AppCompatRadioButton getRadioButtonReceiveAtHouse() {
        return radioButtonShippingAddressOption;
    }

    @Override
    public AppCompatRadioButton getRadioButtonReceiveAtLingo() {
        return radioButtonShippingAddressLingo;
    }

    @Override
    public AppCompatSpinner getSpinnerCityReceiveGoods() {
        return spinnerSippingCity;
    }

    @Override
    public AppCompatSpinner getSpinnerDistrictReceiptGoods() {
        return spinnerSippingDistrict;
    }

    @Override
    public AppCompatSpinner getSpinnerMethodShippingGoods() {
        return spinnerShippingMethod;
    }

    @Override
    public AppCompatEditText getEditTextAddressReceiptGoods() {
        return editTextShippingAddress;
    }

    @Override
    public AppCompatEditText getEditTextNote() {
        return editTextNote;
    }

    @Override
    public AppCompatCheckBox getCheckBoxGrabBill() {
        return checkBoxReceiveBill;
    }

    @Override
    public AppCompatEditText getEditTextNameCompany() {
        return editTextNameCompany;
    }

    @Override
    public AppCompatEditText getEditTextTaxCompany() {
        return editTextTaxCompany;
    }

    @Override
    public AppCompatEditText getEditTextAddressCompany() {
        return editTextAddressCompany;
    }

    @Override
    public AppCompatSpinner getSpinnerCityCompany() {
        return spinnerCityCompany;
    }

    @Override
    public AppCompatSpinner getSpinnerDistrictCompany() {
        return spinnerDistrictCompany;
    }

    @Override
    public AppCompatRadioButton getRadioButtonPaymentReceiveGoods() {
        return radioButtonPaymentReceiptGoods;
    }

    @Override
    public AppCompatRadioButton getRadioButtonATMInternetBanking() {
        return radioButtonInternetBanking;
    }

    @Override
    public AppCompatRadioButton getRadioButtonCard() {
        return radioButtonVisaMater;
    }

    @Override
    public AppCompatRadioButton getRadioButtonTransferGetBill() {
        return radioButtonTransferGetBill;
    }

    @Override
    public AppCompatRadioButton getRadioButtonAccountLingo() {
        return radioButtonAccountLingo;
    }

    @Override
    public AppCompatSpinner getSpinnerNameBanking() {
        return spinnerNameBanking;
    }


    @Override
    public AppCompatTextView getTextViewNameBanking() {
        return textViewNameBanking;
    }

    @Override
    public AppCompatTextView getTextViewNumberAccountBanking() {
        return textViewNumberAccountBanking;
    }

    @Override
    public AppCompatTextView getTextViewAccountOwner() {
        return textViewAccountOwnerBanking;
    }

    @Override
    public AppCompatButton getButtonPaymentComplete() {
        return buttonCompletePayment;
    }

    @Override
    public LinearLayout getLinearLayoutViewTransferReceiveBill() {
        return linearLayoutViewTransferReceiptBill;
    }

    @Override
    public LinearLayout getLinearLayoutReceiveGoodsLocationDesired() {
        return linearLayoutViewReceiptGoodsLocationDesired;
    }

    @Override
    public RadioGroup getRadioGroupPayments() {
        return radioGroupPayments;
    }

    @Override
    public RadioGroup getRadioGroupSippingAdress() {
        return radioGroupShippingAddress;
    }

    @Override
    public LinearLayout getLinearLayoutViewReceiveBill() {
        return linearLayoutViewReceiveBill;
    }

    @Override
    public RadioGroup getRadioGroupStoreAddress() {
        return radioGroupStoreAddress;
    }

    @Override
    public Retrofit getRetrofitCheckCoupon() {
        if (retrofitCheckCoupon == null) {
            return null;
        }
        return retrofitCheckCoupon;
    }

//    @Override
//    public Retrofit getRetrofitProductCheckoutOld() {
//        LingoApplication lingoApplication = (LingoApplication) getApplication();
//        return lingoApplication.getRetrofitProductCheckoutOld();
//    }

    @Override
    public MaterialDialog getMaterialDialogAlert(AppCompatActivity appCompatActivity, String title, String content, String positiveText) {
        return super.getMaterialDialog(appCompatActivity, title, content, positiveText);
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication application = (LingoApplication) getApplication();
        return application.getRetrofitCheckOutTest();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AnalyticsHelper.logPageViews();
        super.onBackPressed();
        Intent intent = new Intent();
        intent.setClass(this, ShoppingCartActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        customerPaymentOrdersPresenter.invalidateToolbar();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_shopping_cart).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "requestCode : " + requestCode + "resultCode : " + resultCode);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("url");
                Log.i(TAG, "requestCode : " + requestCode + "resultCode : " + resultCode + "result : " + result);
                // paymentOrderSuccessPrecenter.deleteShoppingCart();
                new RetrieveSiteData().execute(result);
                Singleton.getSingleton().setIsStarIntent(0);
                Intent intentMainActivity = new Intent(this, PaymentOrderSuccessActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("OrderSerializable", customerPaymentOrdersPresenter.getOrder());
                intentMainActivity.putExtras(bundle);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentMainActivity);
                finishAffinity();
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

}

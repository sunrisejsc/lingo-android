package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 09/12/2015.
 */
public class EventDealFixDealsCollectionMicroInfo implements Serializable {

    @SerializedName("TEMPLATE_CSS")
    private String templateCss;
    @SerializedName("META_TITLE")
    private String metaTitle;
    @SerializedName("META_KEYWORD")
    private String metaKeyword;
    @SerializedName("MICROSITE_DES")
    private String microSiteDescription;
    @SerializedName("MICROSITE_PHONE")
    private String microSitePhone;
    @SerializedName("TEMPLATE_JS")
    private String templateJs;
    @SerializedName("MICROSITE_ID")
    private String microSiteId;
    @SerializedName("MICROSITE_TYPE")
    private String microSiteType;
    @SerializedName("MICROSITE_YAHOO")
    private String microSiteYahoo;
    @SerializedName("MICROSITE_DOMAIN")
    private String microSiteDomain;
    @SerializedName("MICROSITE_KEY")
    private String microSiteKey;
    @SerializedName("TEMPLATE_ID")
    private String templateId;
    @SerializedName("TEMPLATE_MENU")
    private String templateMenu;
    @SerializedName("TEMPLATE_MENU_ITEM")
    private String templateMenuItem;
    @SerializedName("MICROSITE_NAME")
    private String microSiteName;
    @SerializedName("META_DESCRIPTION")
    private String metaDescription;
    @SerializedName("TEMPLATE_PATH")
    private String templatePath;
    @SerializedName("MICROSITE_BODY")
    private String microSiteBody;
    @SerializedName("MICROSITE_STATUS")
    private String microSiteStatus;
    @SerializedName("MENU_FOOTER")
    private String menuFooter;
    @SerializedName("TEMPLATENAME")
    private String templateName;

    public String getTemplateCss() {
        return templateCss;
    }

    public void setTemplateCss(String templateCss) {
        this.templateCss = templateCss;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaKeyword() {
        return metaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        this.metaKeyword = metaKeyword;
    }

    public String getMicroSiteDescription() {
        return microSiteDescription;
    }

    public void setMicroSiteDescription(String microSiteDescription) {
        this.microSiteDescription = microSiteDescription;
    }

    public String getMicroSitePhone() {
        return microSitePhone;
    }

    public void setMicroSitePhone(String microSitePhone) {
        this.microSitePhone = microSitePhone;
    }

    public String getTemplateJs() {
        return templateJs;
    }

    public void setTemplateJs(String templateJs) {
        this.templateJs = templateJs;
    }

    public String getMicroSiteId() {
        return microSiteId;
    }

    public void setMicroSiteId(String microSiteId) {
        this.microSiteId = microSiteId;
    }

    public String getMicroSiteType() {
        return microSiteType;
    }

    public void setMicroSiteType(String microSiteType) {
        this.microSiteType = microSiteType;
    }

    public String getMicroSiteYahoo() {
        return microSiteYahoo;
    }

    public void setMicroSiteYahoo(String microSiteYahoo) {
        this.microSiteYahoo = microSiteYahoo;
    }

    public String getMicroSiteDomain() {
        return microSiteDomain;
    }

    public void setMicroSiteDomain(String microSiteDomain) {
        this.microSiteDomain = microSiteDomain;
    }

    public String getMicroSiteKey() {
        return microSiteKey;
    }

    public void setMicroSiteKey(String microSiteKey) {
        this.microSiteKey = microSiteKey;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateMenu() {
        return templateMenu;
    }

    public void setTemplateMenu(String templateMenu) {
        this.templateMenu = templateMenu;
    }

    public String getTemplateMenuItem() {
        return templateMenuItem;
    }

    public void setTemplateMenuItem(String templateMenuItem) {
        this.templateMenuItem = templateMenuItem;
    }

    public String getMicroSiteName() {
        return microSiteName;
    }

    public void setMicroSiteName(String microSiteName) {
        this.microSiteName = microSiteName;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getMicroSiteBody() {
        return microSiteBody;
    }

    public void setMicroSiteBody(String microSiteBody) {
        this.microSiteBody = microSiteBody;
    }

    public String getMicroSiteStatus() {
        return microSiteStatus;
    }

    public void setMicroSiteStatus(String microSiteStatus) {
        this.microSiteStatus = microSiteStatus;
    }

    public String getMenuFooter() {
        return menuFooter;
    }

    public void setMenuFooter(String menuFooter) {
        this.menuFooter = menuFooter;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
}

package vn.lingo.marketplace.shopping.models.local;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import vn.lingo.marketplace.shopping.models.LingoHomePageGetBannerResponse;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by longtran on 23/12/2015.
 */
public class LingoHomePageGetBannerResponseLocalStorage extends RealmObject {

    @PrimaryKey
    private int storageID;
    private int moduleId;
    private String avatar;
    private String name;
    private String toDate;
    private String description;
    private String microSiteType;
    private String microSiteKey;
    private String status;
    private String fromDate;
    private String createdBy;
    private String createdAt;
    private String updatedBy;
    private String id;
    private String updatedAt;
    private String type;
    private String zoneLevel;

    public int getStorageID() {
        return storageID;
    }

    public void setStorageID(int storageID) {
        this.storageID = storageID;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMicroSiteType() {
        return microSiteType;
    }

    public void setMicroSiteType(String microSiteType) {
        this.microSiteType = microSiteType;
    }

    public String getMicroSiteKey() {
        return microSiteKey;
    }

    public void setMicroSiteKey(String microSiteKey) {
        this.microSiteKey = microSiteKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getType() {
        return type;
    }

    public String getZoneLevel() {
        return zoneLevel;
    }

    public void setZoneLevel(String zoneLevel) {
        this.zoneLevel = zoneLevel;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static LingoHomePageGetBannerResponseLocalStorage convertFromLingoHomePageGetBannerResponse(LingoHomePageGetBannerResponse lingoHomePageGetBannerResponse) {
        LingoHomePageGetBannerResponseLocalStorage lingoHomePageGetBannerResponseLocalStorage = new LingoHomePageGetBannerResponseLocalStorage();
        try {
            lingoHomePageGetBannerResponseLocalStorage.setStorageID(Integer.parseInt(lingoHomePageGetBannerResponse.getId()));
        } catch (Exception exception) {
            lingoHomePageGetBannerResponseLocalStorage.setStorageID(Integer.MIN_VALUE);
        }
        lingoHomePageGetBannerResponseLocalStorage.setModuleId(Constant.MODULE_GET_BANNER);
        lingoHomePageGetBannerResponseLocalStorage.setAvatar(lingoHomePageGetBannerResponse.getAvatar());
        lingoHomePageGetBannerResponseLocalStorage.setName(lingoHomePageGetBannerResponse.getName());
        lingoHomePageGetBannerResponseLocalStorage.setAvatar(lingoHomePageGetBannerResponse.getAvatar());
        lingoHomePageGetBannerResponseLocalStorage.setToDate(lingoHomePageGetBannerResponse.getToDate());
        lingoHomePageGetBannerResponseLocalStorage.setDescription(lingoHomePageGetBannerResponse.getDescription());
        lingoHomePageGetBannerResponseLocalStorage.setMicroSiteType(lingoHomePageGetBannerResponse.getMicroSiteType());
        lingoHomePageGetBannerResponseLocalStorage.setMicroSiteKey(lingoHomePageGetBannerResponse.getMicroSiteKey());
        lingoHomePageGetBannerResponseLocalStorage.setStatus(lingoHomePageGetBannerResponse.getStatus());
        lingoHomePageGetBannerResponseLocalStorage.setFromDate(lingoHomePageGetBannerResponse.getFromDate());
        lingoHomePageGetBannerResponseLocalStorage.setCreatedBy(lingoHomePageGetBannerResponse.getCreatedBy());
        lingoHomePageGetBannerResponseLocalStorage.setUpdatedBy(lingoHomePageGetBannerResponse.getUpdatedBy());
        lingoHomePageGetBannerResponseLocalStorage.setCreatedAt(lingoHomePageGetBannerResponse.getCreatedAt());
        lingoHomePageGetBannerResponseLocalStorage.setUpdatedBy(lingoHomePageGetBannerResponse.getUpdatedBy());
        lingoHomePageGetBannerResponseLocalStorage.setId(lingoHomePageGetBannerResponse.getId());
        lingoHomePageGetBannerResponseLocalStorage.setUpdatedAt(lingoHomePageGetBannerResponse.getUpdatedAt());
        lingoHomePageGetBannerResponseLocalStorage.setType(lingoHomePageGetBannerResponse.getType());
        lingoHomePageGetBannerResponseLocalStorage.setZoneLevel(lingoHomePageGetBannerResponse.getZoneLevel());
        return lingoHomePageGetBannerResponseLocalStorage;
    }
}

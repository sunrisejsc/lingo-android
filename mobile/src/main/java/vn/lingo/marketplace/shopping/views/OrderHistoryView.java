package vn.lingo.marketplace.shopping.views;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;

/**
 * Created by longtran on 01/01/2016.
 */
public interface OrderHistoryView extends AbstractView {

    public FragmentActivity getFragmentActivity();

    public RecyclerView getRecyclerViewOrderHistory();

    public AppCompatTextView getAppCompatTextViewHotLine();

    public void onItemClicked(int position, int id, String endpoint);
}

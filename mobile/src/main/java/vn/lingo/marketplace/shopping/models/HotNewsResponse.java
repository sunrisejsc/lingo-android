package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 08/11/2015.
 */
public class HotNewsResponse implements Serializable {

    @SerializedName("CONTENT_ID")
    private String contentId;

    @SerializedName("CONTENT_HEADLINE")
    private String contentHeadline;

    @SerializedName("CONTENT_BIGAVATAR")
    private String contentBigAvatar;

    @SerializedName("CONTENT_AVATAR")
    private String contentAvatar;

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getContentHeadline() {
        return contentHeadline;
    }

    public void setContentHeadline(String contentHeadline) {
        this.contentHeadline = contentHeadline;
    }

    public String getContentBigAvatar() {
        return contentBigAvatar;
    }

    public void setContentBigAvatar(String contentBigAvatar) {
        this.contentBigAvatar = contentBigAvatar;
    }

    public String getContentAvatar() {
        return contentAvatar;
    }

    public void setContentAvatar(String contentAvatar) {
        this.contentAvatar = contentAvatar;
    }
}

package vn.lingo.marketplace.shopping.interfaces;

import vn.lingo.marketplace.shopping.models.ShoppingCartEntity;

/**
 * Created by lenhan on 04/04/2016.
 */
public interface EventClickListenerProductOrder {

    public void clickItemAdapter(int event,int position, int quantity, float price, ShoppingCartEntity shoppingCartEntity);
}

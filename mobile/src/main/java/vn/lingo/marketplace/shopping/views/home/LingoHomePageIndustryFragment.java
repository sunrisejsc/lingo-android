package vn.lingo.marketplace.shopping.views.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import net.sourceforge.android.view.autoscrollviewpager.AutoScrollViewPager;

import butterknife.Bind;
import me.relex.seamlessviewpagerheader.delegate.ScrollViewDelegate;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.ProductDetailsActivity;
import vn.lingo.marketplace.shopping.delegate.Singleton;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.presenters.LingoHomePageIndustryPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.JVMRuntime;
import vn.lingo.marketplace.shopping.views.BaseViewPagerFragment;
import vn.lingo.marketplace.shopping.views.LingoHomePageIndustryView;

/**
 * Created by longtran on 18/12/2015.
 */
public class LingoHomePageIndustryFragment extends BaseViewPagerFragment implements LingoHomePageIndustryView {

    private final String TAG = LingoHomePageIndustryFragment.class.getName();

    @Override
    protected int getFragmentLayout() {
        return R.layout.home_page_industry_fragment_layout;
    }

    @Override
    public int getFragmentViewId() {
        return R.id.home_page_industry_fragment_layout_id;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        if (null != homeFragment) {
            homeFragment.getRelativeLayoutLoading().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProcessing() {
        if (null != homeFragment) {
            homeFragment.getRelativeLayoutLoading().setVisibility(View.GONE);
        }
    }

    @Override
    public Retrofit getRetrofit() {
        if (getActivity() == null) {
            return null;
        }
        LingoApplication lingoApplication = (LingoApplication) getActivity().getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public DisplayImageOptions getDisplayImageOptions() {
        if (getActivity() == null) {
            return null;
        }
        LingoApplication lingoApplication = (LingoApplication) getActivity().getApplication();
        return lingoApplication.getDisplayImageOptions();
    }

    @Override
    public void onDestroy() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onDestroy();
    }

    private LingoHomePageIndustryPresenter lingoHomePageIndustryPresenter;
    private ScrollViewDelegate scrollViewDelegate = new ScrollViewDelegate();

    @Bind(R.id.lingo_home_page_industry_fragment_scroll_view_id)
    ScrollView scrollView;

    @Bind(R.id.view_pager_auto_scroll_golden_brand_items)
    AutoScrollViewPager autoScrollViewPagerGoldenBrand;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_first_layout_card_view_item_title_id)
    AppCompatTextView appCompatTextViewBestIndustrialFirst;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_first_layout_card_view_item_recycler_view_id)
    RecyclerView recyclerViewBestIndustrialFirst;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_second_layout_card_view_item_title_id)
    AppCompatTextView appCompatTextViewBestIndustrialSecond;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_second_layout_card_view_item_recycler_view_id)
    RecyclerView recyclerViewBestIndustrialSecond;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_third_layout_card_view_item_title_id)
    AppCompatTextView appCompatTextViewBestIndustrialThird;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_third_layout_card_view_item_recycler_view_id)
    RecyclerView recyclerViewBestIndustrialThird;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_fourth_layout_card_view_item_title_id)
    AppCompatTextView appCompatTextViewBestIndustrialFourth;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_fourth_layout_card_view_item_recycler_view_id)
    RecyclerView recyclerViewBestIndustrialFourth;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_fifth_layout_card_view_item_title_id)
    AppCompatTextView appCompatTextViewBestIndustrialFifth;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_fifth_layout_card_view_item_recycler_view_id)
    RecyclerView recyclerViewBestIndustrialFifth;

    @Bind(R.id.lingo_home_page_industry_fragment_banner_first_id)
    AppCompatImageView appCompatImageViewBannerFirst;

    @Bind(R.id.lingo_home_page_industry_fragment_banner_second_id)
    AppCompatImageView appCompatImageViewBannerSecond;

    @Bind(R.id.lingo_home_page_industry_fragment_banner_third_id)
    AppCompatImageView appCompatImageViewBannerThird;

    @Bind(R.id.lingo_home_page_industry_fragment_banner_fourth_id)
    AppCompatImageView appCompatImageViewBannerFourth;

    @Bind(R.id.lingo_home_page_industry_big_fri_day_app_compat_text_view_title_id)
    AppCompatTextView appCompatTextViewBigFriDayTitle;

    @Bind(R.id.lingo_home_page_industry_big_fri_day_relative_layout_id)
    RelativeLayout relativeLayoutBigFriDay;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_first_layout_card_view_item_title_layout_id)
    RelativeLayout relativeLayoutFirstLayoutCardTitle;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_second_layout_card_view_item_title_layout_id)
    RelativeLayout relativeLayoutSecondLayoutCardTitle;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_third_layout_card_view_item_title_layout_id)
    RelativeLayout relativeLayoutThirdLayoutCardTitle;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_fourth_layout_card_view_item_title_layout_id)
    RelativeLayout relativeLayoutFourthLayoutCardTitle;

    @Bind(R.id.lingo_home_page_industry_fragment_best_industrial_fifth_layout_card_view_item_title_layout_id)
    RelativeLayout relativeLayoutFifthLayoutCardTitle;

    private HomeFragment homeFragment;

    @Override
    public AppCompatTextView getAppCompatTextViewBigFriDayTitle() {
        return appCompatTextViewBigFriDayTitle;
    }

    @Override
    public RelativeLayout getRelativeLayoutBigFriDay() {
        return relativeLayoutBigFriDay;
    }

    @Override
    public AutoScrollViewPager getAutoScrollViewPagerGoldenBrand() {
        return autoScrollViewPagerGoldenBrand;
    }

    @Override
    public ScrollView getScrollView() {
        return scrollView;
    }

    @Override
    public AppCompatImageView getAppCompatImageViewBannerFirst() {
        return appCompatImageViewBannerFirst;
    }

    @Override
    public AppCompatImageView getAppCompatImageViewBannerSecond() {
        return appCompatImageViewBannerSecond;
    }

    @Override
    public AppCompatImageView getAppCompatImageViewBannerThird() {
        return appCompatImageViewBannerThird;
    }

    @Override
    public AppCompatImageView getAppCompatImageViewBannerFourth() {
        return appCompatImageViewBannerFourth;
    }

    @Override
    public RelativeLayout getRelativeLayoutFirstLayoutCardTitle() {
        return relativeLayoutFirstLayoutCardTitle;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewBestIndustrialFirst() {
        return appCompatTextViewBestIndustrialFirst;
    }

    @Override
    public RecyclerView getRecyclerViewBestIndustrialFirst() {
        recyclerViewBestIndustrialFirst.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewBestIndustrialFirst.setLayoutManager(gridLayoutManager);
        return recyclerViewBestIndustrialFirst;
    }

    @Override
    public RelativeLayout getRelativeLayoutSecondLayoutCardTitle() {
        return relativeLayoutSecondLayoutCardTitle;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewBestIndustrialSecond() {
        return appCompatTextViewBestIndustrialSecond;
    }

    @Override
    public RecyclerView getRecyclerViewBestIndustrialSecond() {
        recyclerViewBestIndustrialSecond.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewBestIndustrialSecond.setLayoutManager(gridLayoutManager);
        return recyclerViewBestIndustrialSecond;
    }

    @Override
    public RelativeLayout getRelativeLayoutThirdLayoutCardTitle() {
        return relativeLayoutThirdLayoutCardTitle;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewBestIndustrialThird() {
        return appCompatTextViewBestIndustrialThird;
    }

    @Override
    public RecyclerView getRecyclerViewBestIndustrialThird() {
        recyclerViewBestIndustrialThird.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewBestIndustrialThird.setLayoutManager(gridLayoutManager);
        return recyclerViewBestIndustrialThird;
    }

    @Override
    public RelativeLayout getRelativeLayoutFourthLayoutCardTitle() {
        return relativeLayoutFourthLayoutCardTitle;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewBestIndustrialFourth() {
        return appCompatTextViewBestIndustrialFourth;
    }

    @Override
    public RecyclerView getRecyclerViewBestIndustrialFourth() {
        recyclerViewBestIndustrialFourth.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewBestIndustrialFourth.setLayoutManager(gridLayoutManager);
        return recyclerViewBestIndustrialFourth;
    }

    @Override
    public RelativeLayout getRelativeLayoutFifthLayoutCardTitle() {
        return relativeLayoutFifthLayoutCardTitle;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewBestIndustrialFifth() {
        return appCompatTextViewBestIndustrialFifth;
    }

    @Override
    public RecyclerView getRecyclerViewBestIndustrialFifth() {
        recyclerViewBestIndustrialFifth.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewBestIndustrialFifth.setLayoutManager(gridLayoutManager);
        return recyclerViewBestIndustrialFifth;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            homeFragment = (HomeFragment) getFragmentManager().getFragments().get(0);
        } catch (Exception exception) {
            Log.e(TAG, exception.toString());
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");
        lingoHomePageIndustryPresenter = new LingoHomePageIndustryPresenter(this);
        try {
            lingoHomePageIndustryPresenter.fetchingLingoHomePageBanner();
        } catch (Exception exception) {
            Log.e(TAG, exception.toString());
        }
        try {
            lingoHomePageIndustryPresenter.fetchingLingoHomePageBigFriday();
        } catch (Exception exception) {
            Log.e(TAG, exception.toString());
        }
        try {
            lingoHomePageIndustryPresenter.fetchingBestIndustries();
        } catch (Exception exception) {
            Log.e(TAG, exception.toString());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult");
    }

    @Override
    public boolean isViewBeingDragged(MotionEvent event) {
        return scrollViewDelegate.isViewBeingDragged(event, getScrollView());
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        ProductDetailsParcelable productDetailsParcelable = new ProductDetailsParcelable();
        productDetailsParcelable.setId(id);
        productDetailsParcelable.setTitle(title);
        Singleton.getSingleton().setSetUpdateProduct(0);
        Intent intent = new Intent();
        intent.setClassName(getActivity(), ProductDetailsActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productDetailsParcelable);
        intent.putExtras(bundle);
        this.startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        AnalyticsHelper.logPageViews();
    }
}

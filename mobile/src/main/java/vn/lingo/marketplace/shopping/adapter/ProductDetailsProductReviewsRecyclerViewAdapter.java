package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ProductReviewResponse;
import vn.lingo.marketplace.shopping.views.home.viewholder.ProgressViewHolder;
import vn.lingo.marketplace.shopping.views.viewholder.ProductDetailsProductReviewsRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class ProductDetailsProductReviewsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String TAG = ProductDetailsProductReviewsRecyclerViewAdapter.class.getName();
    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_LOADING = 1;
    private List<ProductReviewResponse> itemList;
    private Context context;
    private DisplayImageOptions displayImageOptions;

    /***
     * @param context
     * @param itemList
     */
    public ProductDetailsProductReviewsRecyclerViewAdapter(Context context, List<ProductReviewResponse> itemList, DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.context = context;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_details_product_reviews_recycler_view_item_layout, null);
            ProductDetailsProductReviewsRecyclerViewHolder viewHolder = new ProductDetailsProductReviewsRecyclerViewHolder(view);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);
            ProgressViewHolder progressViewHolder = new ProgressViewHolder(view);
            return progressViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ProductDetailsProductReviewsRecyclerViewHolder) {
            ProductDetailsProductReviewsRecyclerViewHolder productDetailsProductReviewsRecyclerViewHolder = (ProductDetailsProductReviewsRecyclerViewHolder) holder;
            final ProductReviewResponse productReviewResponse = itemList.get(position);
            try {
                productDetailsProductReviewsRecyclerViewHolder.ratingBar.setRating(Float.parseFloat(productReviewResponse.getTotalLike()));
                productDetailsProductReviewsRecyclerViewHolder.ratingBar.setNumStars(5);
                productDetailsProductReviewsRecyclerViewHolder.appCompatTextViewCommentTitle.setText(productReviewResponse.getCommentTitle());
                productDetailsProductReviewsRecyclerViewHolder.appCompatTextViewCommentContent.setText(productReviewResponse.getCommentContent());
                productDetailsProductReviewsRecyclerViewHolder.appCompatTextViewFullName.setText(productReviewResponse.getFullName());
                productDetailsProductReviewsRecyclerViewHolder.appCompatTextViewDateCreated.setText(productReviewResponse.getDateCreated());
            } catch (Exception exception) {
                Log.e(TAG, exception.toString());
            }
        } else {
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            progressViewHolder.progressBarLoadMore.setIndeterminate(true);
        }
    }
}

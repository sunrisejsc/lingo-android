package vn.lingo.marketplace.shopping.utils;

/**
 * Created by longtran on 12/11/2015.
 */
public final class Constant {

    public interface ACTION {
        public static String ADD_TO_CART_ACTION = "vn.lingo.marketplace.shopping.add.to.cart";
        public static String START_FOREGROUND_ACTION = "START_FOREGROUND_ACTION";
        public static String MAIN_ACTION = "MAIN_ACTION";
        public static String STOP_FOREGROUND_ACTION = "STOP_FOREGROUND_ACTION";
        public static String SET_CURRENT_PAGER="setCurrentPager";
        public static String UPDATE_LIST_CART="update_list_cart";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 2031984;
    }

    public static final class Categories {
        public static final String ZONE_ID = "ZONE_ID";
        public static final String ZONE_LEVEL = "ZONE_LEVEL";
        public static final String CATEGORY_KEY = "CATEGORY_KEY";
        public static final String FRAGMENT_PREVIOUS = "FRAGMENT_PREVIOUS";
    }

    public static final class ProductDetails {
        public static final String PRODUCT_DETAILS_KEY = "PRODUCT_DETAILS_KEY";
        public static final String PRODUCT_DETAIL_INFORMATION = "PRODUCT_DETAIL_INFORMATION";
    }

    public static final class EventDeal {
        public static final String EVENT_DEAL_FIX_DEALS_COLLECTION = "EVENT_DEAL_FIX_DEALS_COLLECTION";
    }

    public static final class Article {
        public static final String ARTICLE_KEY = "ARTICLE_KEY";
    }

    public static final String EVENT = "EVENT";
    public static final String EVENT_ID = "EVENT_ID";
    public static final String EMPTY_VALUES = "EMPTY_VALUES";
    public static final int ORDERED_VALUES = 1;

    public static final int ON_CLICK_PROCESS_ORDERED = 111;
    public static final int ON_CLICK_DELETED_ORDER = 112;
    public static final int SLIDE_TIME_OUT = 10 * 1000;

    public static final int TOTAL_STAR = 5;

    public static final int MODULE_HOME_SLIDES_ID = 0x11001;
    public static final int MODULE_INDUSTRY = 0x11002;
    public static final int MODULE_GET_BANNER = 0x11003;
    public static final int MODULE_EVENT_BY_TYPE = 0x11004;
    public static final int MODULE_BEST_INDUSTRY = 0x11005;
    public static final int MODULE_HOME_PAGE_BIG_FRIDAY = 0x11006;

    public static final int REQUEST_SEREVE = 200;

    public static final String LIST_SHOPPING_CART_ENTITY ="listShoppingCartEntity";

    public static final int EVENT_SUB_PRODUCT = 1990;
    public static final int EVENT_ADD_PRODUCT = 1991;
    public static final int EVENT_REMOVE_PRODUCT = 1992;

}

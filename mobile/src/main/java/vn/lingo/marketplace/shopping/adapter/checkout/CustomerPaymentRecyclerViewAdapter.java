package vn.lingo.marketplace.shopping.adapter.checkout;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.math.BigDecimal;
import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.interfaces.EventClickListenerProductOrder;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.listeners.ShoppingCartListener;
import vn.lingo.marketplace.shopping.models.ShoppingCartEntity;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.NumberFormat;
import vn.lingo.marketplace.shopping.views.viewholder.search.CustomerPaymentRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class CustomerPaymentRecyclerViewAdapter extends RecyclerView.Adapter<CustomerPaymentRecyclerViewHolder> {

    private final String TAG = CustomerPaymentRecyclerViewAdapter.class.getName();

    private final String SHOWCASE_ID = CustomerPaymentRecyclerViewAdapter.class.getName();
    private List<ShoppingCartEntity> itemList;
    private Context activity;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private ShoppingCartListener shoppingCartListener;
    private EventClickListenerProductOrder eventClickListenerProductOrder;
    private DisplayImageOptions displayImageOptions;

    public CustomerPaymentRecyclerViewAdapter(Context activity, List<ShoppingCartEntity> itemList, RecyclerViewItemClickListener recyclerViewItemClickListener,
                                              ShoppingCartListener shoppingCartListener, DisplayImageOptions displayImageOptions, EventClickListenerProductOrder eventClickListenerProductOrder) {
        this.itemList = itemList;
        this.activity = activity;
        this.displayImageOptions = displayImageOptions;
        this.eventClickListenerProductOrder = eventClickListenerProductOrder;
    }

    @Override
    public CustomerPaymentRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_product_payment, null);
        CustomerPaymentRecyclerViewHolder viewHolder = new CustomerPaymentRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomerPaymentRecyclerViewHolder holder, final int position) {
        final ShoppingCartEntity shoppingCartEntity = itemList.get(position);
        holder.productName.setText(shoppingCartEntity.getProductName());
        holder.productQuantity.setText(shoppingCartEntity.getOrderQuantity() + "");
        float productPrice = shoppingCartEntity.getProductPrice();
        BigDecimal bigDecimal = new BigDecimal(productPrice);
        holder.productPrice.setText(String.format(activity.getResources().getString(
                R.string.prices), NumberFormat.priceToString(bigDecimal.floatValue())));

        holder.addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int addProduct = Integer.parseInt(holder.productQuantity.getText().toString().trim());
                if (addProduct < 5) {
                    int quantity = addProduct + 1;
                    holder.productQuantity.setText(quantity + "");
                    eventClickListenerProductOrder.clickItemAdapter(Constant.EVENT_ADD_PRODUCT, position, quantity, shoppingCartEntity.getProductPrice(), shoppingCartEntity);
                }
            }
        });
        holder.subProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int subProduct = Integer.parseInt(holder.productQuantity.getText().toString().trim());
                if (subProduct > 1) {
                    int quantity = subProduct - 1;
                    holder.productQuantity.setText(quantity + "");
                    eventClickListenerProductOrder.clickItemAdapter(Constant.EVENT_SUB_PRODUCT, position, quantity, shoppingCartEntity.getProductPrice(), shoppingCartEntity);
                }
            }
        });
        holder.removeProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(holder.productQuantity.getText().toString().trim());
                eventClickListenerProductOrder.clickItemAdapter(Constant.EVENT_REMOVE_PRODUCT, position, quantity, shoppingCartEntity.getProductPrice(), shoppingCartEntity);
            }
        });

//        ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + shoppingCartEntity.getAvatar(),
//                holder.imageView, displayImageOptions);
        String urlAvatar = Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + shoppingCartEntity.getAvatar();
        try {
            ImageSize targetSize = new ImageSize(256, 256);
            ImageLoader.getInstance().loadImage(urlAvatar, targetSize,
                    displayImageOptions, new SimpleImageLoadingListener() {

                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            super.onLoadingStarted(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            super.onLoadingFailed(imageUri, view, failReason);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            super.onLoadingCancelled(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.imageView.setImageBitmap(loadedImage);
                        }
                    });
        } catch (OutOfMemoryError outOfMemoryError) {
            Log.e(TAG, outOfMemoryError.toString());
        }

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    /***
     *
     */
}

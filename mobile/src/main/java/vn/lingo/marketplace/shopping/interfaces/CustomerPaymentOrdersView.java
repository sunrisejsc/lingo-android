package vn.lingo.marketplace.shopping.interfaces;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.views.AbstractView;

/**
 * Created by Administrator on 3/23/2016.
 */
public interface CustomerPaymentOrdersView extends AbstractView {

    public Toolbar getToolbar();

    public AppCompatActivity getAppCompatActivity();

    public AppCompatImageView getImageViewRecycler();

    public RecyclerView getRecyclerViewProductPurchase();

    public AppCompatButton getButtonApplyCodeSale();

    public AppCompatEditText getEditTextCodeSale();

    public AppCompatTextView getTextViewAllMoneyProduct();

    public AppCompatTextView getTextViewTransportFee();

    public AppCompatTextView getTextViewPriceDiscounted();

    public AppCompatTextView getTextViewMoneyPay();

    public AppCompatEditText getEditTextFullNameConsignee();
    public AppCompatEditText getEditTextNumberPhoneConsignee();
    public AppCompatEditText getEditTextEmailConsignee();

    public AppCompatRadioButton getRadioButtonReceiveAtHouse();
    public AppCompatRadioButton getRadioButtonReceiveAtLingo();
    public AppCompatSpinner getSpinnerCityReceiveGoods();
    public AppCompatSpinner getSpinnerDistrictReceiptGoods();
    public AppCompatSpinner getSpinnerMethodShippingGoods();
    public AppCompatEditText getEditTextAddressReceiptGoods();
    public AppCompatEditText getEditTextNote();
    public AppCompatCheckBox getCheckBoxGrabBill();
    public AppCompatEditText getEditTextNameCompany();
    public AppCompatEditText getEditTextTaxCompany();
    public AppCompatEditText getEditTextAddressCompany();
    public AppCompatSpinner getSpinnerCityCompany();
    public AppCompatSpinner getSpinnerDistrictCompany();
    public AppCompatRadioButton getRadioButtonPaymentReceiveGoods();
    public AppCompatRadioButton getRadioButtonATMInternetBanking();
    public AppCompatRadioButton getRadioButtonCard();
    public AppCompatRadioButton getRadioButtonTransferGetBill();
    public AppCompatRadioButton getRadioButtonAccountLingo();
    public AppCompatSpinner getSpinnerNameBanking();
    public AppCompatTextView getTextViewNameBanking();
    public AppCompatTextView getTextViewNumberAccountBanking();
    public AppCompatTextView getTextViewAccountOwner();
    public AppCompatButton getButtonPaymentComplete();
    public LinearLayout getLinearLayoutViewTransferReceiveBill();
    public LinearLayout getLinearLayoutReceiveGoodsLocationDesired();
    public RadioGroup getRadioGroupPayments();
    public RadioGroup getRadioGroupSippingAdress();
    public LinearLayout getLinearLayoutViewReceiveBill();

    public RadioGroup getRadioGroupStoreAddress();

    public Retrofit getRetrofitCheckCoupon();

//    public Retrofit getRetrofitProductCheckoutOld();

    public MaterialDialog getMaterialDialogAlert(AppCompatActivity appCompatActivity, String title, String content, String positiveText);

}

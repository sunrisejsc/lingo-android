package vn.lingo.marketplace.shopping.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by longtran on 17/12/2015.
 */
public class ShortcutInformationEntity extends RealmObject {

    @PrimaryKey
    private int shortcutId;
    private int shortcut;

    public int getShortcutId() {
        return shortcutId;
    }

    public void setShortcutId(int shortcutId) {
        this.shortcutId = 0x1984;
    }

    public int getShortcut() {
        return shortcut;
    }

    public void setShortcut(int shortcut) {
        this.shortcut = shortcut;
    }
}

package vn.lingo.marketplace.shopping.models;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import vn.lingo.marketplace.shopping.models.checkout.Order;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;

/**
 * Created by longtran on 10/12/2015.
 */
public class ProductCheckoutRequest implements Serializable {

    private static final String TAG = ProductCheckoutRequest.class.getName();

    @SerializedName("reqId")
    private String reqId;
    @SerializedName("numberOfItems")
    private int numberOfItems;
    @SerializedName("morId")
    private int morId;
    @SerializedName("items")
    private List<ProductItemCheckoutRequest> listProductItemCheckoutRequest;
    @SerializedName("orderAmount")
    private int orderAmount;
    @SerializedName("reqTime")
    private String reqTime;
    @SerializedName("noted")
    private String noted;
    @SerializedName("clientInfo")
    private ProductClientInfoCheckoutRequest productClientInfoCheckoutRequest;
    @SerializedName("MD5Mac")
    private String mD5Mac;
    @SerializedName("currency")
    private String currency;
    @SerializedName("morUser")
    private String morUser;
    @SerializedName("Order")
    private Order order;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public int getMorId() {
        return morId;
    }

    public void setMorId(int morId) {
        this.morId = morId;
    }

    public List<ProductItemCheckoutRequest> getListProductItemCheckoutRequest() {
        return listProductItemCheckoutRequest;
    }

    public void setListProductItemCheckoutRequest(List<ProductItemCheckoutRequest> listProductItemCheckoutRequest) {
        this.listProductItemCheckoutRequest = listProductItemCheckoutRequest;
    }

    public int getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(int orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getReqTime() {
        return reqTime;
    }

    public void setReqTime(String reqTime) {
        this.reqTime = reqTime;
    }

    public String getNoted() {
        return noted;
    }

    public void setNoted(String noted) {
        this.noted = noted;
    }

    public ProductClientInfoCheckoutRequest getProductClientInfoCheckoutRequest() {
        return productClientInfoCheckoutRequest;
    }

    public void setProductClientInfoCheckoutRequest(ProductClientInfoCheckoutRequest productClientInfoCheckoutRequest) {
        this.productClientInfoCheckoutRequest = productClientInfoCheckoutRequest;
    }

    public String getMD5Mac() {
        return mD5Mac;
    }

    public void setMD5Mac(String mD5Mac) {
        this.mD5Mac = mD5Mac;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMorUser() {
        return morUser;
    }

    public void setMorUser(String morUser) {
        this.morUser = morUser;
    }

    /****
     * @param productId
     * @param sellPrice
     * @param skuId
     * @return
     */
    public static String getProductCheckoutRequest(int productId, int sellPrice, String skuId) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssZ");
        ProductCheckoutRequest productCheckoutRequest = new ProductCheckoutRequest();
        productCheckoutRequest.setReqId("MobileAppLingo");
        productCheckoutRequest.setMorId(0);
        productCheckoutRequest.setMorUser("Anonymous");
        ProductClientInfoCheckoutRequest productClientInfoCheckoutRequest = new ProductClientInfoCheckoutRequest();
        productClientInfoCheckoutRequest.setDevId(2);
        productClientInfoCheckoutRequest.setDevOS("Android");
        productClientInfoCheckoutRequest.setDevLocalTime(String.valueOf(simpleDateFormat.format(new Date())));
        productCheckoutRequest.setProductClientInfoCheckoutRequest(productClientInfoCheckoutRequest);
        productCheckoutRequest.setNumberOfItems(1);
        productCheckoutRequest.setCurrency("vnd");
        productCheckoutRequest.setOrderAmount(0);
        ProductItemCheckoutRequest productItemCheckoutRequest = new ProductItemCheckoutRequest();
        productItemCheckoutRequest.setProductId(productId);
        productItemCheckoutRequest.setSellPrice(sellPrice);
        productItemCheckoutRequest.setQuantity(1);
        productItemCheckoutRequest.setSkuId(skuId);
        productItemCheckoutRequest.setItemAmount(0);
        List<ProductItemCheckoutRequest> listProductItemCheckoutRequest = new ArrayList<ProductItemCheckoutRequest>();
        listProductItemCheckoutRequest.add(productItemCheckoutRequest);
        productCheckoutRequest.setListProductItemCheckoutRequest(listProductItemCheckoutRequest);
        productCheckoutRequest.setReqTime(String.valueOf(simpleDateFormat.format(new Date())));
        productCheckoutRequest.setNoted("lingo");
        productCheckoutRequest.setMD5Mac("lingo");
        String jsonObject = new Gson().toJson(productCheckoutRequest);
        Log.i(TAG, jsonObject);
        HashMap<String, String> fetchPhotoStatEventParams = new HashMap<String, String>();
        fetchPhotoStatEventParams.put(AnalyticsHelper.EVENT_PRODUCT_ORDER_DATA_REQUEST, jsonObject);
        AnalyticsHelper.logEvent(AnalyticsHelper.EVENT_PRODUCT_ORDER, fetchPhotoStatEventParams, true);
        return jsonObject;
    }

    /***
     * @param listShoppingCartLocalStorage
     * @return
     */
    public static String getProductCheckoutRequest(List<ShoppingCartLocalStorage> listShoppingCartLocalStorage,Order order) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssZ");
        ProductCheckoutRequest productCheckoutRequest = new ProductCheckoutRequest();
        productCheckoutRequest.setReqId("MobileAppLingo");
        productCheckoutRequest.setMorId(0);
        productCheckoutRequest.setMorUser("Anonymous");
        ProductClientInfoCheckoutRequest productClientInfoCheckoutRequest = new ProductClientInfoCheckoutRequest();
        productClientInfoCheckoutRequest.setDevId(2);
        productClientInfoCheckoutRequest.setDevOS("Android");
        productClientInfoCheckoutRequest.setDevLocalTime(String.valueOf(simpleDateFormat.format(new Date())));
        productCheckoutRequest.setProductClientInfoCheckoutRequest(productClientInfoCheckoutRequest);
        productCheckoutRequest.setNumberOfItems(1);
        productCheckoutRequest.setCurrency("vnd");
        productCheckoutRequest.setOrderAmount(0);
        productCheckoutRequest.setOrder(order);
        List<ProductItemCheckoutRequest> listProductItemCheckoutRequest = new ArrayList<ProductItemCheckoutRequest>();
        Iterator<ShoppingCartLocalStorage> iterator = listShoppingCartLocalStorage.iterator();
        while (iterator.hasNext()) {
            ShoppingCartLocalStorage shoppingCartLocalStorage = iterator.next();
            ProductItemCheckoutRequest productItemCheckoutRequest = new ProductItemCheckoutRequest();
            productItemCheckoutRequest.setProductId(shoppingCartLocalStorage.getCartID());
            productItemCheckoutRequest.setSellPrice(shoppingCartLocalStorage.getProductPrice());
            productItemCheckoutRequest.setQuantity(shoppingCartLocalStorage.getOrderQuantity());
            productItemCheckoutRequest.setSkuId(shoppingCartLocalStorage.getSku());
            productItemCheckoutRequest.setItemAmount(shoppingCartLocalStorage.getOrderQuantity());
            listProductItemCheckoutRequest.add(productItemCheckoutRequest);
        }
        productCheckoutRequest.setListProductItemCheckoutRequest(listProductItemCheckoutRequest);
        productCheckoutRequest.setReqTime(String.valueOf(simpleDateFormat.format(new Date())));
        productCheckoutRequest.setNoted("lingo");
        productCheckoutRequest.setMD5Mac("lingo");
        String jsonObject = new Gson().toJson(productCheckoutRequest);
        Log.i(TAG, jsonObject);
        HashMap<String, String> fetchPhotoStatEventParams = new HashMap<String, String>();
        fetchPhotoStatEventParams.put(AnalyticsHelper.EVENT_PRODUCT_ORDER_DATA_REQUEST, jsonObject);
        AnalyticsHelper.logEvent(AnalyticsHelper.EVENT_PRODUCT_ORDER, fetchPhotoStatEventParams, true);
        return jsonObject;
    }
}

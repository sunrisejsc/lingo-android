package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShotStateStore;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.DrawerBuilder;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.relex.seamlessviewpagerheader.tools.ScrollableFragmentListener;
import me.relex.seamlessviewpagerheader.tools.ScrollableListener;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.presenters.MainPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.views.MainView;
import vn.lingo.marketplace.shopping.views.NoInternetAccessFragment;
import vn.lingo.marketplace.shopping.views.home.HomeFragment;

/**
 * Created by longtran on 03/11/2015.
 */
public class MainActivity extends AbstractAppCompatActivity implements MainView, ScrollableFragmentListener {

    private final String TAG = MainActivity.class.getName();

    private Handler handlerTutorialHelper;
    private Runnable runnableTutorialHelper;
    private MainPresenter mainPresenter;

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.relative_layout_tutorial_helper)
    RelativeLayout relativeLayoutTutorialHelper;

    @Bind(R.id.app_compat_button_tutorial_helper)
    AppCompatButton appCompatButtonTutorialHelper;

    private AccountHeaderBuilder accountHeaderBuilder = null;
    private DrawerBuilder drawerBuilder = null;
    private SparseArrayCompat<ScrollableListener> scrollableListenerArrays;

    @Override
    public int getFragmentContainerViewId() {
        return R.id.frame_container;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        ButterKnife.bind(this);
        scrollableListenerArrays = new SparseArrayCompat<>();
        accountHeaderBuilder = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.icon_account_header);
        drawerBuilder = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(getToolbar())
                .withTranslucentStatusBar(false)
//                .withDrawerWidthDp(72)
                .withActionBarDrawerToggle(true)
                .withSavedInstance(savedInstanceState);
        mainPresenter = new MainPresenter(this);
        mainPresenter.createSlidingMenu(savedInstanceState);
        if (isInternetConnected(this)) {
            switchFragments(new HomeFragment(), null, HomeFragment.class.getName());
        } else {
            switchFragments(new NoInternetAccessFragment(), null, NoInternetAccessFragment.class.getName());
        }
        mainPresenter.getDrawer().closeDrawer();
//        ParseAnalytics.trackAppOpenedInBackground(getIntent());
//        ParseInstallation.getCurrentInstallation().saveInBackground();
        runnableTutorialHelper = new Runnable() {
            @Override
            public void run() {
                ShotStateStore shotStateStore = new ShotStateStore(MainActivity.this);
                shotStateStore.setSingleShot(0x222);
                if (!shotStateStore.hasShot()) {
                    showcaseViewShoppingCart();
                }
            }
        };
        handlerTutorialHelper = new Handler();
        handlerTutorialHelper.postDelayed(runnableTutorialHelper, 2000);
    }

    /****
     *
     */
    private void showcaseViewShoppingCart() {
        new ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .singleShot(0x222)
                .setTarget(new ToolbarActionItemTarget(toolbar, R.id.action_shopping_cart))
                .setStyle(R.style.CustomShowcaseTheme2)
                .setContentTitle(getString(R.string.view_your_order_check_out))
                .setContentText(getString(R.string.view_your_order_check_out_description))
                .setShowcaseEventListener(new OnShowcaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                        showcaseViewSearch();
                    }

                    @Override
                    public void onShowcaseViewShow(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

                    }
                })
                .build()
                .show();
    }

    /****
     *
     */
    private void showcaseViewHomePage() {
        getRelativeLayoutTutorialHelper().setVisibility(View.VISIBLE);
        getAppCompatButtonTutorialHelper().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRelativeLayoutTutorialHelper().setVisibility(View.GONE);

            }
        });
    }

    /***
     *
     */
    private void showcaseViewSearch() {
        new ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .singleShot(0x333)
                .setTarget(new ToolbarActionItemTarget(toolbar, R.id.action_search))
                .setStyle(R.style.CustomShowcaseTheme2)
                .setContentTitle(R.string.search_here)
                .setContentText(getString(R.string.search_here_description))
                .setShowcaseEventListener(new OnShowcaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                        showcaseViewHomePage();
                    }

                    @Override
                    public void onShowcaseViewShow(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

                    }
                })
                .build()
                .show();
    }

    /****
     *
     */
    private class ToolbarActionItemTarget implements Target {

        private final Toolbar toolbar;
        private final int menuItemId;

        public ToolbarActionItemTarget(Toolbar toolbar, @IdRes int itemId) {
            this.toolbar = toolbar;
            this.menuItemId = itemId;
        }

        @Override
        public Point getPoint() {
            return new ViewTarget(toolbar.findViewById(menuItemId)).getPoint();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.invalidateToolbar();
        AnalyticsHelper.logPageViews();
        Log.i(TAG, "MainActivity -> onResume()");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        /****
         * add the values which need to be saved from the drawer to the bundle
         */
        outState = mainPresenter.getDrawer().saveInstanceState(outState);
        /***
         * add the values which need to be saved from the accountHeader to the bundle
         */
        outState = mainPresenter.getAccountHeader().saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mainPresenter.getDrawer() != null && mainPresenter.getDrawer().isDrawerOpen()) {
            mainPresenter.getDrawer().closeDrawer();
        } else {
            finish();
            System.exit(0);
        }
    }

    @Override
    protected void onDestroy() {
        handlerTutorialHelper.removeCallbacks(runnableTutorialHelper);
        super.onDestroy();
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AccountHeaderBuilder getAccountHeaderBuilder() {
        return accountHeaderBuilder;
    }

    @Override
    public DrawerBuilder getDrawerBuilder() {
        return drawerBuilder;
    }

    @Override
    public RelativeLayout getRelativeLayoutTutorialHelper() {
        relativeLayoutTutorialHelper.setClickable(false);
        relativeLayoutTutorialHelper.setEnabled(false);
        return relativeLayoutTutorialHelper;
    }

    @Override
    public AppCompatButton getAppCompatButtonTutorialHelper() {
        return appCompatButtonTutorialHelper;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {

    }

    @Override
    public void hideProcessing() {

    }

    @Override
    public Retrofit getRetrofit() {
        return ((LingoApplication) getApplicationContext()).getRetrofit();
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public void onFragmentAttached(ScrollableListener listener, int position) {
        if (null != listener) {
            //position = 0, crash app
            if (position>0) {
                scrollableListenerArrays.put(position, listener);
            }
        }
    }

    @Override
    public void onFragmentDetached(ScrollableListener listener, int position) {
        scrollableListenerArrays.remove(position);
    }

    public SparseArrayCompat<ScrollableListener> getScrollableListeners() {
        return scrollableListenerArrays;
    }

}

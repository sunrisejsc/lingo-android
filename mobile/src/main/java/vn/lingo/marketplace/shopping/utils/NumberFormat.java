package vn.lingo.marketplace.shopping.utils;

import java.text.DecimalFormat;

/**
 * Created by longtran on 12/12/2015.
 */
public class NumberFormat {

    public static String priceWithDecimal(float price) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(price);
    }

    public static String priceWithoutDecimal(float price) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(price);
    }

    public static String priceToString(float price) {
        String toShow = priceWithoutDecimal(price);
        if (toShow.indexOf(".") > 0) {
            return priceWithDecimal(price).replaceAll(",", ".");
        } else {
            return priceWithoutDecimal(price).replaceAll(",",".");
        }
    }
}

package vn.lingo.marketplace.shopping.activities;


/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.viewpagerindicator.CirclePageIndicator;

import net.sourceforge.android.view.autoscrollviewpager.AutoScrollViewPager;
import net.sourceforge.widgets.CountdownView;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.OnFragmentInteractionListener;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.presenters.ProductDetailsPresenter;
import vn.lingo.marketplace.shopping.service.RetrieveSiteData;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.FullyGridLayoutManager;
import vn.lingo.marketplace.shopping.views.FullyLinearLayoutManager;
import vn.lingo.marketplace.shopping.views.ProductDetailsView;

/**
 * Created by longtran on 09/12/2015.
 */
public class ProductDetailsActivity extends AbstractAppCompatActivity implements ProductDetailsView,
        ObservableScrollViewCallbacks, OnFragmentInteractionListener {

    private final String TAG = ProductDetailsActivity.class.getName();

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.id_product_details_image_metadata_layout)
    FrameLayout frameLayoutImageMetadata;

    @Bind(R.id.product_details_auto_scroll_view_pager)
    AutoScrollViewPager autoScrollViewPager;

    @Bind(R.id.product_details_circle_page_indicator)
    CirclePageIndicator circlePageIndicator;

    @Bind(R.id.scroll)
    ObservableScrollView observableScrollView;

    @Bind(R.id.product_details_description)
    AppCompatTextView productDetailsDescription;

    @Bind(R.id.product_details_product_brand)
    AppCompatTextView productBrand;

    @Bind(R.id.product_code)
    AppCompatTextView productCode;

    @Bind(R.id.price_value)
    AppCompatTextView productPrice;

    @Bind(R.id.prices_golden_hour_label_id)
    AppCompatTextView productPriceGoldenHourLabel;

    @Bind(R.id.market_price_value)
    AppCompatTextView productMarketPrice;

    @Bind(R.id.product_details_related_products_recycler_view)
    RecyclerView recyclerViewRelatedProducts;

    @Bind(R.id.product_details_general_product_information)
    RecyclerView recyclerViewProductDetailsGeneralProductInformation;

    @Bind(R.id.product_details_product_for_review_recycler_view)
    RecyclerView recyclerViewProductForReview;

    @Bind(R.id.leaf_children_category_rating_bar)
    RatingBar ratingBar;

    @Bind(R.id.app_compat_button_buy_now)
    AppCompatButton appCompatButtonBuyNow;

    @Bind(R.id.app_compat_button_add_to_cart)
    AppCompatButton appCompatButtonAddToCart;

    @Bind(R.id.product_details_detailed_information_id)
    AppCompatTextView appCompatTextViewDetailedInformation;

    @Bind(R.id.view_pager_auto_scroll_gift_products_items)
    AutoScrollViewPager autoScrollViewPagerGiftProducts;

    @Bind(R.id.scores_label)
    AppCompatTextView appCompatTextViewScores;

    @Bind(R.id.count_down_golden_hour)
    CountdownView countdownViewGoldenHour;

    @Bind(R.id.product_details_product_discount)
    AppCompatTextView appCompatTextViewDiscount;

    @Bind(R.id.linear_layout_count_down_golden_hour)
    LinearLayout linearLayoutCountDownGoldenHour;

    @Bind(R.id.product_details_linear_layout_view_pager_auto_scroll_gift_products_items_id)
    LinearLayout linearLayoutViewPagerAutoScrollGiftProducts;

    @Bind(R.id.product_details_more_product_for_review_linear_layout_id)
    LinearLayout linearLayoutMoreProductReviews;

    @Bind(R.id.product_details_detailed_information_view_more_id)
    AppCompatTextView appCompatTextViewProductDetailedInformationViewMore;

    private ProductDetailsPresenter productDetailsPresenter;
    private int lastScroll = 0;
    private int mParallaxImageHeight;
    private MaterialDialog materialDialog;

    @Override
    public AppCompatTextView getAppCompatTextViewProductDetailedInformationViewMore() {
        return appCompatTextViewProductDetailedInformationViewMore;
    }

    @Override
    public LinearLayout getLinearLayoutMoreProductProductReviews() {
        return linearLayoutMoreProductReviews;
    }

    @Override
    public LinearLayout getLinearLayoutCountDownGoldenHour() {
        return linearLayoutCountDownGoldenHour;
    }

    @Override
    public AppCompatTextView getProductDiscount() {
        return appCompatTextViewDiscount;
    }

    @Override
    public CountdownView getCountdownViewGoldenHour() {
        return countdownViewGoldenHour;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewScores() {
        return appCompatTextViewScores;
    }

    @Override
    public AppCompatTextView getProductBrand() {
        return productBrand;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewDetailedInformation() {
        return appCompatTextViewDetailedInformation;
    }

    @Override
    public RatingBar getRatingBar() {
        return ratingBar;
    }

    @Override
    public AutoScrollViewPager getAutoScrollViewPagerGiftProducts() {
        return autoScrollViewPagerGiftProducts;
    }

    @Override
    public LinearLayout getLinearLayoutViewPagerAutoScrollGiftProducts() {
        return linearLayoutViewPagerAutoScrollGiftProducts;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details_screen);
        ButterKnife.bind(this);
        observableScrollView.setScrollViewCallbacks(this);
        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);
        productDetailsPresenter = new ProductDetailsPresenter(this);
        ProductDetailsParcelable productDetailsParcelable = getIntent().getParcelableExtra(Constant.ProductDetails.PRODUCT_DETAILS_KEY);
        if (null != productDetailsParcelable) {
            //toolbar.setTitle(productDetailsParcelable.getTitle());
            productDetailsPresenter.retrievingProductInformation(productDetailsParcelable);
        } else {
            productDetailsParcelable = new ProductDetailsParcelable();
            productDetailsParcelable.setId(135635);
            productDetailsParcelable.setTitle("Mode debug");
            productDetailsPresenter.retrievingProductInformation(productDetailsParcelable);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(observableScrollView.getCurrentScrollY(), false, false);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.primary);
        float alpha = Math.max(0.28f, (float) scrollY / mParallaxImageHeight);
        toolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        frameLayoutImageMetadata.setTranslationY(scrollY / 2);
        float translationY = toolbar.getTranslationY();
        if (dragging) {
            int scroll;
            int delta = lastScroll - scrollY;
            if (delta < 0) {
                scroll = Math.max(-toolbar.getHeight(), (int) translationY + delta);
            } else {
                scroll = Math.min((int) translationY + delta, 0);
            }
            if (delta != 0) {
                lastScroll = scrollY;
            }
            toolbar.setTranslationY(scroll);
        }
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        productDetailsPresenter.invalidateToolbar();
        super.onResume();
    }

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    public void onFragmentInteraction(Object action, Fragment fragment) {

    }

    @Override
    public RecyclerView getRecyclerViewRelatedProducts() {
        //recyclerViewRelatedProducts.addItemDecoration(new MarginDecoration(this));
        recyclerViewRelatedProducts.setHasFixedSize(true);
        recyclerViewRelatedProducts.setItemAnimator(new DefaultItemAnimator());
        FullyLinearLayoutManager fullyLinearLayoutManager = new FullyLinearLayoutManager(getApplicationContext(), 1);
        fullyLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewRelatedProducts.setLayoutManager(fullyLinearLayoutManager);
        return recyclerViewRelatedProducts;
    }

    @Override
    public RecyclerView getRecyclerViewProductDetailsGeneralProductInformation() {
        FullyGridLayoutManager fullyGridLayoutManager = new FullyGridLayoutManager(getApplicationContext(), 1);
        recyclerViewProductDetailsGeneralProductInformation.setLayoutManager(fullyGridLayoutManager);
        return recyclerViewProductDetailsGeneralProductInformation;
    }

    @Override
    public RecyclerView getRecyclerViewProductForReview() {
        FullyGridLayoutManager fullyGridLayoutManager = new FullyGridLayoutManager(getApplicationContext(), 1);
        recyclerViewProductForReview.setLayoutManager(fullyGridLayoutManager);
        return recyclerViewProductForReview;
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AutoScrollViewPager getAutoScrollViewPager() {
        return autoScrollViewPager;
    }

    @Override
    public CirclePageIndicator getCirclePageIndicator() {
        return circlePageIndicator;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public MaterialDialog getMaterialDialogAlert(AppCompatActivity appCompatActivity, String title, String content, String positiveText) {
        return super.getMaterialDialog(appCompatActivity, title, content, positiveText);
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        ProductDetailsParcelable productDetailsParcelable = new ProductDetailsParcelable();
        productDetailsParcelable.setId(id);
        productDetailsParcelable.setTitle(title);
        Intent intent = new Intent();
        intent.setClassName(this, ProductDetailsActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productDetailsParcelable);
        intent.putExtras(bundle);
        this.startActivity(intent);
        this.finish();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public Retrofit getRetrofitProductCheckout() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofitCheckOutTest();
    }

    @Override
    public AppCompatTextView getProductDetailsDescription() {
        return productDetailsDescription;
    }

    @Override
    public AppCompatTextView getProductCode() {
        return productCode;
    }

    @Override
    public AppCompatTextView getProductPrice() {
        return productPrice;
    }

    @Override
    public AppCompatTextView getProductPriceGoldenHourLabel() {
        return productPriceGoldenHourLabel;
    }

    @Override
    public AppCompatTextView getProductMarketPrice() {
        productMarketPrice.setPaintFlags(productMarketPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        return productMarketPrice;
    }

    @Override
    public AppCompatButton getAppCompatButtonBuyNow() {
        return appCompatButtonBuyNow;
    }

    @Override
    public AppCompatButton getAppCompatButtonAddToCart() {
        return appCompatButtonAddToCart;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "requestCode : " + requestCode + "resultCode : " + resultCode);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("url");
                new RetrieveSiteData().execute(result);
                Log.i(TAG, "requestCode : " + requestCode + "resultCode : " + resultCode + "result : " + result);
                productDetailsPresenter.truncateShoppingCart();
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }
}

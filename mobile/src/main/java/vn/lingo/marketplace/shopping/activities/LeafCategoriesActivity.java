package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

import org.jsoup.helper.StringUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.presenters.CategoriesPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.views.CategoriesView;

/**
 * Created by longtran on 16/11/2015.
 */
public class LeafCategoriesActivity extends AbstractAppCompatActivity implements CategoriesView {

    @Bind(R.id.activity_leaf_categories_layout_categories_result_list)
    RecyclerView searchResultList;

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.activity_leaf_categories_data_filter_card_view_id)
    CardView cardView;

    private CategoriesPresenter categoriesPresenter;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaf_categories_layout);
        ButterKnife.bind(this);
        cardView.setVisibility(View.GONE);
        categoriesPresenter = new CategoriesPresenter(this);
        CategoriesParcelable categoriesParcelable = getIntent().getParcelableExtra(Constant.Categories.CATEGORY_KEY);
        if (null != categoriesParcelable) {
            toolbar.setTitle(categoriesParcelable.getTitle());
            categoriesPresenter.setAdapter(categoriesParcelable);
        }
    }

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public RecyclerView getRecyclerViewCategories() {
//        searchResultList.addItemDecoration(new MarginDecoration(this));
//        searchResultList.addItemDecoration(new DividerItemDecoration(1));
        searchResultList.setHasFixedSize(true);
//        searchResultList.setItemAnimator(new DefaultItemAnimator());
//        searchResultList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        searchResultList.setLayoutManager(new LinearLayoutManager(this));
        return searchResultList;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        if (!StringUtil.isBlank(title)) {
            AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", title, true);
        }
        if (zoneLevel == 3) {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.setId(id);
            categoriesParcelable.setTitle(title);
            categoriesParcelable.setZoneLevel(zoneLevel);
            categoriesParcelable.setMappingZoneId(mappingZoneId);
            Intent intent = new Intent();
            intent.setClassName(this, LeafChildrenCategoryActivity.class.getName());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
            intent.putExtras(bundle);
            this.startActivity(intent);
        } else {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.setId(id);
            categoriesParcelable.setTitle(title);
            categoriesParcelable.setZoneLevel(zoneLevel);
            Intent intent = new Intent();
            intent.setClassName(this, LeafCategoriesActivity.class.getName());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
            intent.putExtras(bundle);
            this.startActivity(intent);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        super.onResume();
    }
}

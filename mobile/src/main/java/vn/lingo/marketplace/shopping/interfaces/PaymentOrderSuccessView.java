package vn.lingo.marketplace.shopping.interfaces;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;

import vn.lingo.marketplace.shopping.views.AbstractView;

/**
 * Created by lenhan on 12/04/2016.
 */
public interface PaymentOrderSuccessView extends AbstractView {

    public AppCompatTextView getTextViewOrderCode();

    public AppCompatTextView getTextViewCustomerName();

    public AppCompatTextView getTextViewCustomerEmail();

    public AppCompatTextView getTextViewCustomerAddress();

    public AppCompatTextView getTextViewCustomerPhone();

    public AppCompatTextView getTextViewCustomerNameReceive();

    public AppCompatTextView getTextViewCustomerEmailReceive();

    public AppCompatTextView getTextViewCustomerAddressReceive();

    public AppCompatTextView getTextViewCustomerPhoneReceive();

    public AppCompatTextView getTextViewMethod();

    public AppCompatButton getButtonCallback();

    public AppCompatActivity getAppCompatActivity();

}

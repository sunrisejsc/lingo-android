package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by zon on 08/04/2016.
 */
public class InstallmentOrder implements Serializable {
    @SerializedName("IDENTIFY_CARD")
    private String identifyCard;
    @SerializedName("BANK_CARD")
    private String bankCard;
    @SerializedName("INSTALLMENT_PROGRAM_ID")
    int installmentProgramID;

    public InstallmentOrder(String identifyCard, String bankCard, int installmentProgramID) {
        this.identifyCard = identifyCard;
        this.bankCard = bankCard;
        this.installmentProgramID = installmentProgramID;
    }

    public String getIdentifyCard() {
        return identifyCard;
    }

    public void setIdentifyCard(String identifyCard) {
        this.identifyCard = identifyCard;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public int getInstallmentProgramID() {
        return installmentProgramID;
    }

    public void setInstallmentProgramID(int installmentProgramID) {
        this.installmentProgramID = installmentProgramID;
    }
}

package vn.lingo.marketplace.shopping.presenters;

import android.content.Intent;
import android.view.View;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.MainActivity;
import vn.lingo.marketplace.shopping.interfaces.PaymentOrderSuccessView;
import vn.lingo.marketplace.shopping.models.ShoppingCartLocalStorage;
import vn.lingo.marketplace.shopping.models.checkout.Order;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by lenhan on 12/04/2016.
 */
public class PaymentOrderSuccessPrecenter {

    PaymentOrderSuccessView paymentOrderSuccessView;

    public PaymentOrderSuccessPrecenter(final PaymentOrderSuccessView paymentOrderSuccessView) {
        this.paymentOrderSuccessView = paymentOrderSuccessView;

        paymentOrderSuccessView.getButtonCallback().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(paymentOrderSuccessView.getAppCompatActivity(), MainActivity.class);
                paymentOrderSuccessView.getAppCompatActivity().startActivity(intent);
                paymentOrderSuccessView.getAppCompatActivity().finish();
                deleteShoppingCart();
            }
        });
    }

    public void deleteShoppingCart() {
        Realm realm=Realm.getDefaultInstance();
        RealmQuery<ShoppingCartLocalStorage> realmQuery = realm.where(ShoppingCartLocalStorage.class);
        RealmResults<ShoppingCartLocalStorage> realmResults = realmQuery.findAll();
        realm.beginTransaction();
        realmResults.clear();
        realm.commitTransaction();
    }

}

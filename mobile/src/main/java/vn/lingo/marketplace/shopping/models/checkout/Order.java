package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by zon on 07/04/2016.
 */
public class Order implements Serializable {
    @SerializedName("ORDER_RECEIVE_SEX")
    private int sex;
    @SerializedName("ORDER_RECEIVE_EMAIL")
    private String orderReceiveMail;
    @SerializedName("SHIP_TYPE")
    private int shipType;
    @SerializedName("ORDER_RECEIVE_ADDRESS")
    private String orderReceiveAddress;
    @SerializedName("ORDER_RECEIVE_NAME")
    private String orderReceiveName;
    @SerializedName("ORDER_RECEIVE_MOBILE")
    private String orderReceiveMobile;
    @SerializedName("ORDER_RECEIVE_NOTE")
    private String orderReceiveNote;
    @SerializedName("PM_ID")
    private int pmID;
    @SerializedName("STORE_GUID")
    private String storeGuid;
    @SerializedName("RECEIVE_ORDER_TYPE")
    private int receiveOrderType;
    @SerializedName("RECEIVE_CITY_ID")
    private int receiveCityID;
    @SerializedName("RECEIVE_DISTRICT_ID")
    private int receiveDistrictID;
    @SerializedName("RECEIVE_CITY_NAME")
    private String receiveCityName;
    @SerializedName("RECEIVE_DISTRICT_NAME")
    private String receiveDistrictName;
    @SerializedName("SHIP_AMOUNT")
    private int shipMount;
    @SerializedName("COMPANY_DISTRICT_ID")
    private int companyDistrictID;
    @SerializedName("TOTAL_WEIGHT")
    private int totalWeight;
    @SerializedName("COMPANY_CITY_ID")
    private int companyCityID;
    @SerializedName("IS_VAT")
    private int isVat;
    @SerializedName("ORDER_OWNER_COMPANY")
    private String orderOwnerCompany;
    @SerializedName("ORDER_OWNER_COMPANY_TAXCODE")
    private String orderOwnerCompanyTaxCode;
    @SerializedName("ORDER_OWNER_COMPANY_ADDRESS")
    private String orderOwnerCompanyAddress;
    @SerializedName("ECOUPON_CODE")
    private String ecopounCode;
    @SerializedName("HAD_USER_LOGIN")
    private boolean hadUserLogin;
    @SerializedName("USER_LOGIN_EMAIL")
    private String userLoginMail;
    @SerializedName("USER_LOGIN_GENDER")
    private int userLoginGender;
    @SerializedName("USER_LOGIN_ID")
    private int userLoginID;
    @SerializedName("USER_LOGIN_FULLNAME")
    private String userLoginFullName;
    @SerializedName("USER_LOGIN_MOBILE")
    private String userLoginMobile;
    @SerializedName("RECEIVE_FULL_ADDRESS")
    private String receiveFullAddress;

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getOrderReceiveMail() {
        return orderReceiveMail;
    }

    public void setOrderReceiveMail(String orderReceiveMail) {
        this.orderReceiveMail = orderReceiveMail;
    }

    public int getShipType() {
        return shipType;
    }

    public void setShipType(int shipType) {
        this.shipType = shipType;
    }

    public String getOrderReceiveAddress() {
        return orderReceiveAddress;
    }

    public void setOrderReceiveAddress(String orderReceiveAddress) {
        this.orderReceiveAddress = orderReceiveAddress;
    }

    public String getOrderReceiveName() {
        return orderReceiveName;
    }

    public void setOrderReceiveName(String orderReceiveName) {
        this.orderReceiveName = orderReceiveName;
    }

    public String getOrderReceiveMobile() {
        return orderReceiveMobile;
    }

    public void setOrderReceiveMobile(String orderReceiveMobile) {
        this.orderReceiveMobile = orderReceiveMobile;
    }

    public String getOrderReceiveNote() {
        return orderReceiveNote;
    }

    public void setOrderReceiveNote(String orderReceiveNote) {
        this.orderReceiveNote = orderReceiveNote;
    }

    public int getPmID() {
        return pmID;
    }

    public void setPmID(int pmID) {
        this.pmID = pmID;
    }

    public String getStoreGuid() {
        return storeGuid;
    }

    public void setStoreGuid(String storeGuid) {
        this.storeGuid = storeGuid;
    }

    public int getReceiveOrderType() {
        return receiveOrderType;
    }

    public void setReceiveOrderType(int receiveOrderType) {
        this.receiveOrderType = receiveOrderType;
    }

    public int getReceiveCityID() {
        return receiveCityID;
    }

    public void setReceiveCityID(int receiveCityID) {
        this.receiveCityID = receiveCityID;
    }

    public int getReceiveDistrictID() {
        return receiveDistrictID;
    }

    public void setReceiveDistrictID(int receiveDistrictID) {
        this.receiveDistrictID = receiveDistrictID;
    }

    public String getReceiveCityName() {
        return receiveCityName;
    }

    public void setReceiveCityName(String receiveCityName) {
        this.receiveCityName = receiveCityName;
    }

    public String getReceiveDistrictName() {
        return receiveDistrictName;
    }

    public void setReceiveDistrictName(String receiveDistrictName) {
        this.receiveDistrictName = receiveDistrictName;
    }

    public int getShipMount() {
        return shipMount;
    }

    public void setShipMount(int shipMount) {
        this.shipMount = shipMount;
    }

    public int getCompanyDistrictID() {
        return companyDistrictID;
    }

    public void setCompanyDistrictID(int companyDistrictID) {
        this.companyDistrictID = companyDistrictID;
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(int totalWeight) {
        this.totalWeight = totalWeight;
    }

    public int getCompanyCityID() {
        return companyCityID;
    }

    public void setCompanyCityID(int companyCityID) {
        this.companyCityID = companyCityID;
    }

    public int getIsVat() {
        return isVat;
    }

    public void setIsVat(int isVat) {
        this.isVat = isVat;
    }

    public String getOrderOwnerCompany() {
        return orderOwnerCompany;
    }

    public void setOrderOwnerCompany(String orderOwnerCompany) {
        this.orderOwnerCompany = orderOwnerCompany;
    }

    public String getOrderOwnerCompanyTaxCode() {
        return orderOwnerCompanyTaxCode;
    }

    public void setOrderOwnerCompanyTaxCode(String orderOwnerCompanyTaxCode) {
        this.orderOwnerCompanyTaxCode = orderOwnerCompanyTaxCode;
    }

    public String getOrderOwnerCompanyAddress() {
        return orderOwnerCompanyAddress;
    }

    public void setOrderOwnerCompanyAddress(String orderOwnerCompanyAddress) {
        this.orderOwnerCompanyAddress = orderOwnerCompanyAddress;
    }

    public String getEcopounCode() {
        return ecopounCode;
    }

    public void setEcopounCode(String ecopounCode) {
        this.ecopounCode = ecopounCode;
    }

    public boolean isHadUserLogin() {
        return hadUserLogin;
    }

    public void setHadUserLogin(boolean hadUserLogin) {
        this.hadUserLogin = hadUserLogin;
    }

    public String getUserLoginMail() {
        return userLoginMail;
    }

    public void setUserLoginMail(String userLoginMail) {
        this.userLoginMail = userLoginMail;
    }

    public int getUserLoginGender() {
        return userLoginGender;
    }

    public void setUserLoginGender(int userLoginGender) {
        this.userLoginGender = userLoginGender;
    }

    public int getUserLoginID() {
        return userLoginID;
    }

    public void setUserLoginID(int userLoginID) {
        this.userLoginID = userLoginID;
    }

    public String getUserLoginFullName() {
        return userLoginFullName;
    }

    public void setUserLoginFullName(String userLoginFullName) {
        this.userLoginFullName = userLoginFullName;
    }

    public String getUserLoginMobile() {
        return userLoginMobile;
    }

    public void setUserLoginMobile(String userLoginMobile) {
        this.userLoginMobile = userLoginMobile;
    }

    public String getReceiveFullAddress() {
        return receiveFullAddress;
    }

    public void setReceiveFullAddress(String receiveFullAddress) {
        this.receiveFullAddress = receiveFullAddress;
    }
}

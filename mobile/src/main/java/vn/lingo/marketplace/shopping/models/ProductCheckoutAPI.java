package vn.lingo.marketplace.shopping.models;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by longtran on 10/12/2015.
 */
public interface ProductCheckoutAPI {

    @Headers({
            "Content-Type: application/x-www-form-urlencoded; charset=UTF-8"
    })
    @FormUrlEncoded
    @POST("/v1/lingo/mobileapp/checkout")
    Call<ProductCheckoutResponse> productCheckout(@Field("reqData") String reqData);

    @FormUrlEncoded
    @POST("/v1/lingo/mobileapp/CHECKOUT")
    Call<ProductCheckoutResponse> productCheckoutWeb(@Field("reqData") String reqData);
}

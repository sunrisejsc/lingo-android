package vn.lingo.marketplace.shopping.models;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 12/11/2015.
 */
public interface HomeAPI {

    @GET("/v1/mobile/Home/GetBannerByDate")
    Call<List<BannerByDate>> getBannerByDate();

    @GET("/v1/mobile/Home/GetNewestArticle")
    Call<List<HotNewsResponse>> getNewestArticle();

    @GET("/v1/mobile/Home/GetHotCategory")
    Call<List<HotProductsOfCategoriesResponse>> getHotCategory(@Query("i_priority") int iPriority);

    @GET("/v1/mobile/Deal/GetFixDeal")
    Call<List<FixDealResponse>> getFixDeal();

    @GET("/v1/mobile/Deal/GetEventDeal")
    Call<AllEventDealsResponse> getEventDeal();

    @GET("/v1/mobile/Deal/GetCollection")
    Call<CollectionsResponse> getCollection();

    @GET("/v1/mobile/Deal/GetAllEvent")
    Call<AllEventsResponse> getAllEvent();
}

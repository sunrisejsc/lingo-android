package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ProductDetailsResponse;
import vn.lingo.marketplace.shopping.views.viewholder.ProductDetailsGeneralProductInformationRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class ProductDetailsGeneralProductInformationRecyclerViewAdapter extends RecyclerView.Adapter<ProductDetailsGeneralProductInformationRecyclerViewHolder> {

    private List<ProductDetailsResponse.GeneralProductInformationNameValue> itemList;
    private Context context;

    public ProductDetailsGeneralProductInformationRecyclerViewAdapter(Context context, List<ProductDetailsResponse.GeneralProductInformationNameValue> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public ProductDetailsGeneralProductInformationRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_details_general_product_information_recycler_view_item_layout, null);
        ProductDetailsGeneralProductInformationRecyclerViewHolder viewHolder = new ProductDetailsGeneralProductInformationRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductDetailsGeneralProductInformationRecyclerViewHolder holder, final int position) {
        final ProductDetailsResponse.GeneralProductInformationNameValue productDetailsResponseGeneralProductInformationNameValue = itemList.get(position);
        try {
            String label = String.format(context.getResources().getString(R.string.string_key_value),
                    productDetailsResponseGeneralProductInformationNameValue.getName(),
                    productDetailsResponseGeneralProductInformationNameValue.getValue());
            holder.productDetailsGeneralProductInformationRecyclerViewItemProductAttributes.setText(label);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}

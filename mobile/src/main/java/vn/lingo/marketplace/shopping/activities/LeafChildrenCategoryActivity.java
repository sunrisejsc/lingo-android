package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;

import org.jsoup.helper.StringUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.presenters.LeafChildrenCategoryPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.views.LeafChildrenCategoryView;

/**
 * Created by longtran on 07/12/2015.
 */
public class LeafChildrenCategoryActivity extends AbstractAppCompatActivity implements LeafChildrenCategoryView {

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.activity_leaf_categories_layout_categories_result_list)
    RecyclerView recyclerViewLeafChildrenCategory;

    @Bind(R.id.activity_leaf_categories_data_filter_spinner_id)
    AppCompatSpinner appCompatSpinnerDataFilter;

    private LeafChildrenCategoryPresenter leafChildrenCategoryPresenter;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaf_categories_layout);
        ButterKnife.bind(this);
        leafChildrenCategoryPresenter = new LeafChildrenCategoryPresenter(this);
        CategoriesParcelable categoriesParcelable = getIntent().getParcelableExtra(Constant.Categories.CATEGORY_KEY);
        if (null != categoriesParcelable) {
            toolbar.setTitle(categoriesParcelable.getTitle());
            leafChildrenCategoryPresenter.fetchingLeafChildrenCategory(categoriesParcelable);
        }
    }

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    @Override
    public RecyclerView getRecyclerViewLeafChildrenCategory() {
        recyclerViewLeafChildrenCategory.addItemDecoration(new DividerItemDecoration(1));
        recyclerViewLeafChildrenCategory.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewLeafChildrenCategory.setLayoutManager(linearLayoutManager);
        return recyclerViewLeafChildrenCategory;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public AppCompatSpinner getAppCompatSpinnerDataFilter() {
        return appCompatSpinnerDataFilter;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        if (!StringUtil.isBlank(title)) {
            AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", title, true);
            AnalyticsHelper.logEvent(this, title, String.valueOf(mappingZoneId));
        }
        ProductDetailsParcelable productDetailsParcelable = new ProductDetailsParcelable();
        productDetailsParcelable.setId(id);
        productDetailsParcelable.setTitle(title);
        Intent intent = new Intent();
        intent.setClassName(this, ProductDetailsActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productDetailsParcelable);
        intent.putExtras(bundle);
        this.startActivity(intent);
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        super.onResume();
    }
}

package vn.lingo.marketplace.shopping.views;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;

import retrofit.Retrofit;

/**
 * Created by longtran on 12/11/2015.
 */
public interface ShoppingCartView extends AbstractView {

    public AppCompatActivity getAppCompatActivity();

    public Toolbar getToolbar();

    public RecyclerView getRecyclerViewShoppingCart();

    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);

    public AppCompatTextView getAppCompatTextViewShoppingCartSummary();

    public AppCompatTextView getAppCompatTextViewShoppingCartSubtotalValue();

    public AppCompatTextView getAppCompatTextViewShoppingCartTotalValue();

    public AppCompatTextView getAppCompatTextViewShoppingCartTotalQuantity();

    public AppCompatButton getAppCompatButtonShoppingCartBuy();

    public Retrofit getRetrofitProductCheckout();

    public MaterialDialog getMaterialDialogAlert(AppCompatActivity appCompatActivity, String title, String content, String positiveText);

}

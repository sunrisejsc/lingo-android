package vn.lingo.marketplace.shopping.views.home;

import net.sourceforge.android.view.autoscrollviewpager.AutoScrollViewPager;

import vn.lingo.marketplace.shopping.views.AbstractView;

/**
 * Created by longtran on 08/11/2015.
 */
public interface HotNewsView extends AbstractView {

    public AutoScrollViewPager getAutoScrollViewPagerHotNews();

}

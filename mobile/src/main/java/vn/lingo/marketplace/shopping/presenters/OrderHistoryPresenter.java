package vn.lingo.marketplace.shopping.presenters;

import android.util.Base64;

import com.thefinestartist.finestwebview.FinestWebView;

import org.apache.commons.lang3.StringUtils;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import vn.lingo.marketplace.shopping.adapter.OrderHistoryRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.OrderHistoryRecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.OrderHistoryLocalStorage;
import vn.lingo.marketplace.shopping.views.OrderHistoryView;

/**
 * Created by longtran on 06/12/2015.
 */
public class OrderHistoryPresenter implements OrderHistoryRecyclerViewItemClickListener {

    private String TAG = OrderHistoryPresenter.class.getName();
    private OrderHistoryView orderHistoryView;

    /***
     * @param orderHistoryView
     */
    public OrderHistoryPresenter(final OrderHistoryView orderHistoryView) {
        this.orderHistoryView = orderHistoryView;
    }

    /****
     *
     */
    public void fetchingOrderHistoryRecyclerViewAdapter() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<OrderHistoryLocalStorage> realmQuery = realm.where(OrderHistoryLocalStorage.class);
        RealmResults<OrderHistoryLocalStorage> realmResultsOrderHistoryLocalStorage = realmQuery.findAll();
        realmResultsOrderHistoryLocalStorage.sort("timeStamp", RealmResults.SORT_ORDER_DESCENDING);
        OrderHistoryRecyclerViewAdapter orderHistoryRecyclerViewAdapter = new OrderHistoryRecyclerViewAdapter(orderHistoryView.getContext(),
                realmResultsOrderHistoryLocalStorage, OrderHistoryPresenter.this);
        orderHistoryView.getRecyclerViewOrderHistory().setAdapter(orderHistoryRecyclerViewAdapter);
    }

    @Override
    public void onItemClicked(int position, OrderHistoryLocalStorage orderHistoryLocalStorage) {
        String orderNumberEndpoint = new String(Base64.decode(orderHistoryLocalStorage.getOrderNumberEndpoint().getBytes(),
                Base64.URL_SAFE | android.util.Base64.NO_WRAP));
        if (!StringUtils.isBlank(orderHistoryLocalStorage.getOrderNumber()) &&
                !orderHistoryLocalStorage.getOrderNumber().equalsIgnoreCase("null")) {
            String orderTracking = orderNumberEndpoint.replaceAll("thanh-cong/(\\d+|\\D+)", "theo-doi-don-hang/" + orderHistoryLocalStorage.getOrderNumber());
            new FinestWebView.Builder(orderHistoryView.getFragmentActivity()).show(orderTracking);
        }
    }
}

package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by zon on 07/04/2016.
 */
public class MobileInfo implements Serializable {
    @SerializedName("IMEI")
    private String imei;
    @SerializedName("LAT")
    private String lat;
    @SerializedName("LON")
    private String lon;
    @SerializedName("DEVICE_TOKEN")
    private String deviceToken;
    @SerializedName("BRAND_NAME ")
    private String brandName;
    @SerializedName("OS_ID")
    private String osID;
    @SerializedName("MODEL")
    private String model;
    @SerializedName("USER_AGENT")
    private String userAgent;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getOsID() {
        return osID;
    }

    public void setOsID(String osID) {
        this.osID = osID;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}

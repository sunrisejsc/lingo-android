package vn.lingo.marketplace.shopping.models;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 08/11/2015.
 */
public interface IndustryAPI {

    @GET("/v1/mobile/Home/GetIndustryFull")
    Call<List<IndustryResponse>> getIndustryFull();

    @GET("/v1/mobile/Home/GetDetailIndustry")
    Call<List<IndustryResponse>> getDetailIndustry(@Query("i_id") long i_id);

    @GET("/v1/mobile/Home/GetGroupProduct")
    Call<List<IndustryResponse>> getGroupProduct(@Query("i_id") long i_id);//i_id ~ ZONE_ID
}

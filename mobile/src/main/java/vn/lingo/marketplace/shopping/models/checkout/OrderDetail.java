package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by zon on 07/04/2016.
 */
public class OrderDetail implements Serializable {
    @SerializedName("PRODUCT_ID")
    private int productID;
    @SerializedName("QUANTITY")
    private int quantity;
    @SerializedName("SAVEBUY_DISCOUNT")
    private int saverBuyDisCount;
    @SerializedName("UNIT_PRICE")
    private int unitPrice;

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSaverBuyDisCount() {
        return saverBuyDisCount;
    }

    public void setSaverBuyDisCount(int saverBuyDisCount) {
        this.saverBuyDisCount = saverBuyDisCount;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }
}

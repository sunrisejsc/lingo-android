package vn.lingo.marketplace.shopping.apicheckout;

import com.google.gson.JsonElement;

import java.util.List;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import vn.lingo.marketplace.shopping.models.ReceiveType;
import vn.lingo.marketplace.shopping.models.checkout.BankVAT;
import vn.lingo.marketplace.shopping.models.checkout.StoreAddress;

/**
 * Created by zon on 31/03/2016.
 */
public interface ApiCheckout {

    @FormUrlEncoded
    @POST("/v1/lingo/mobileapp/GETSHIPTYPE")
    Call<JsonElement> getShipType(@Field("reqData") int cityId);

    @POST("/v1/lingo/mobileapp/GETFULLCITY")
    Call<JsonElement> getLocation();

    @POST("/v1/lingo/mobileapp/GETRECEIVETYPE")
    Call<List<ReceiveType>> getReceiveType();

    @POST("/v1/lingo/mobileapp/GETSTORE")
    Call<JsonElement> getStoreAddress();

    @POST("/v1/lingo/mobileapp/GETBANKVAT")
    Call<List<BankVAT>> getbankVAT();

    @GET("/v1/lingo/ecoupon/verify")
    Call<JsonElement> getCheckCouponCode(@Query("ecouponCode") String ecouponCode ,@Query("reqTime") String reqTime);

    @FormUrlEncoded
    @POST("/v1/lingo/mobileapp/SHIPFEE")
    Call<JsonElement> getShipFree(@Field("reqData") String reqData);

    @FormUrlEncoded
    @POST("/v1/lingo/mobileapp/INSERTORDER")
    Call<JsonElement> getCreateOrder(@Field("reqData") String reqData);

    @FormUrlEncoded
    @POST("/v1/lingo/mobileapp/VERIFYECOUPON")
    Call<JsonElement> getCheckEcouponCode(@Field("reqData") String reqData);

}

package vn.lingo.marketplace.shopping.models;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 11/11/2015.
 */
public interface EventByTypesAPI {

    @GET("/v1/mobile/Home/GetListEventByType")
    Call<List<EventByTypeResponse>> getListEventByType(@Query("i_type") int i_type, @Query("i_size") int i_size);

    @GET("/v1/mobile/Home/GetDetailItemInAllEvent")
    Call<ListHomeEventsResponse> getDetailItemInAllEvent(@Query("i_type") int i_type, @Query("i_size") int i_size, @Query("i_page") int i_page);

    @GET("/v1/mobile/Home/GetDetailItemInOneEvent")
    Call<ListHomeEventsResponse> getDetailItemInOneEvent(@Query("i_event_id") int i_event_id, @Query("i_size") int i_size, @Query("i_page") int i_page);

    @GET("/v1/mobile/Home/GetBanner")
    Call<List<LingoHomePageGetBannerResponse>> getLingoHomePageGetBanner(@Query("i_type") int i_type, @Query("i_mic_type") int i_mic_type);

    @GET("/v1/mobile/Home/GetBigFriday")
    Call<LingoHomePageAllBigFridayResponse> getLingoHomePageGetBigFriday(@Query("i_current_page") int i_current_page, @Query("i_page_size") int i_page_size);
}

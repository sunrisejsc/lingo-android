package vn.lingo.marketplace.shopping.presenters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.Iterator;
import java.util.List;

import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.CategoriesActivity;
import vn.lingo.marketplace.shopping.activities.LeafChildrenCategoryActivity;
import vn.lingo.marketplace.shopping.activities.MoreBestIndustriesActivity;
import vn.lingo.marketplace.shopping.activities.MoreBigFriDayActivity;
import vn.lingo.marketplace.shopping.adapter.home.LingoHomePageIndustryBigFridayAdapter;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.models.EventByTypeResponse;
import vn.lingo.marketplace.shopping.models.EventByTypesAPI;
import vn.lingo.marketplace.shopping.models.LingoHomePageAllBigFridayResponse;
import vn.lingo.marketplace.shopping.models.LingoHomePageBigFridayResponse;
import vn.lingo.marketplace.shopping.models.LingoHomePageGetBannerResponse;
import vn.lingo.marketplace.shopping.models.local.EventByTypeResponseLocalStorage;
import vn.lingo.marketplace.shopping.models.local.LingoHomePageBigFridayResponseLocalStorage;
import vn.lingo.marketplace.shopping.models.local.LingoHomePageGetBannerResponseLocalStorage;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.presenters.home.LingoHomePageBestIndustryService;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.LingoHomePageIndustryView;

/**
 * Created by longtran on 18/12/2015.
 */
public class LingoHomePageIndustryPresenter extends AbstractPresenter<LingoHomePageIndustryView> implements RecyclerViewItemClickListener, View.OnClickListener {

    private String TAG = LingoHomePageIndustryPresenter.class.getName();
    private LingoHomePageIndustryView lingoHomePageIndustryView;
    private boolean reloadsLingoHomePageBanner = false;
    private boolean reloadsHomeBestIndustries = false;
    private boolean reloadsLingoHomePageBigFriday = false;

    /****
     * @param lingoHomePageIndustryView
     */
    public LingoHomePageIndustryPresenter(LingoHomePageIndustryView lingoHomePageIndustryView) {
        super(lingoHomePageIndustryView);
        this.lingoHomePageIndustryView = lingoHomePageIndustryView;
    }

    /****
     *
     */
    private void setLingoHomePageBigFridayAdapter() {
        lingoHomePageIndustryView.getAppCompatTextViewBigFriDayTitle().setText(
                String.format(lingoHomePageIndustryView.getContext().getResources().getString(R.string.promotion), ""));
        RealmQuery<LingoHomePageBigFridayResponseLocalStorage> realmQueryLingoHomePageBigFridayResponseLocalStorage =
                realm.where(LingoHomePageBigFridayResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_HOME_PAGE_BIG_FRIDAY);
        RealmResults<LingoHomePageBigFridayResponseLocalStorage> realmResultsLingoHomePageBigFridayResponseLocalStorage =
                realmQueryLingoHomePageBigFridayResponseLocalStorage.findAll();
        lingoHomePageIndustryView.getAutoScrollViewPagerGoldenBrand().setAdapter
                (new LingoHomePageIndustryBigFridayAdapter(lingoHomePageIndustryView.getContext(), realmResultsLingoHomePageBigFridayResponseLocalStorage,
                        lingoHomePageIndustryView.getDisplayImageOptions(), LingoHomePageIndustryPresenter.this));
        lingoHomePageIndustryView.getAutoScrollViewPagerGoldenBrand().getAdapter().notifyDataSetChanged();
        lingoHomePageIndustryView.getAutoScrollViewPagerGoldenBrand().setInterval(Constant.SLIDE_TIME_OUT);
        lingoHomePageIndustryView.getAutoScrollViewPagerGoldenBrand().startAutoScroll(Constant.SLIDE_TIME_OUT);
        lingoHomePageIndustryView.getRelativeLayoutBigFriDay().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClassName(lingoHomePageIndustryView.getContext(), MoreBigFriDayActivity.class.getName());
                lingoHomePageIndustryView.getContext().startActivity(intent);
            }
        });
        lingoHomePageIndustryView.hideProcessing();
    }

    /****
     *
     */
    public void fetchingLingoHomePageBigFriday() throws Exception {
        lingoHomePageIndustryView.showProcessing();
        RealmQuery<LingoHomePageBigFridayResponseLocalStorage> realmQueryLingoHomePageBigFridayResponseLocalStorage =
                realm.where(LingoHomePageBigFridayResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_HOME_PAGE_BIG_FRIDAY);
        RealmResults<LingoHomePageBigFridayResponseLocalStorage> realmResultsLingoHomePageBigFridayResponseLocalStorage =
                realmQueryLingoHomePageBigFridayResponseLocalStorage.findAll();
        if (realmResultsLingoHomePageBigFridayResponseLocalStorage.size() > 0) {
            Log.i(TAG, "get LingoHomePageBigFriday load data from cache.............");
            setLingoHomePageBigFridayAdapter();
            reloadsLingoHomePageBigFriday = false;
        } else {
            Log.i(TAG, "get LingoHomePageBigFriday load data from server.............");
            reloadsLingoHomePageBigFriday = true;
        }
        EventByTypesAPI eventByTypesAPI = lingoHomePageIndustryView.getRetrofit().create(EventByTypesAPI.class);
        Call<LingoHomePageAllBigFridayResponse> call = eventByTypesAPI.getLingoHomePageGetBigFriday(1, 15);
        call.enqueue(new Callback<LingoHomePageAllBigFridayResponse>() {
            @Override
            public void onResponse(Response<LingoHomePageAllBigFridayResponse> response, Retrofit retrofit) {
                if (response.code() == 200 && null != response.body() && null != lingoHomePageIndustryView.getContext()) {
                    String lable="";
                    if (!response.body().getLabel().equals("Đã kết thúc")){
                        lable=response.body().getLabel();
                    }
                    lingoHomePageIndustryView.getAppCompatTextViewBigFriDayTitle().setText(
                            String.format(lingoHomePageIndustryView.getContext().getResources().getString(R.string.promotion),
                                    lable));
                    try {
                        realm.beginTransaction();
                        LingoHomePageAllBigFridayResponse lingoHomePageAllBigFridayResponse = response.body();
                        Iterator<LingoHomePageBigFridayResponse> iteratorLingoHomePageBigFridayResponse =
                                lingoHomePageAllBigFridayResponse.getListLingoHomePageBigFridayResponse().iterator();
                        while (iteratorLingoHomePageBigFridayResponse.hasNext()) {
                            LingoHomePageBigFridayResponse lingoHomePageBigFridayResponse = iteratorLingoHomePageBigFridayResponse.next();
                            LingoHomePageBigFridayResponseLocalStorage lingoHomePageBigFridayResponseLocalStorage = LingoHomePageBigFridayResponseLocalStorage.
                                    convertFromLingoHomePageBigFridayResponse(lingoHomePageBigFridayResponse);
                            realm.copyToRealmOrUpdate(lingoHomePageBigFridayResponseLocalStorage);
                        }
                        realm.commitTransaction();
                        if (reloadsLingoHomePageBigFriday) {
                            setLingoHomePageBigFridayAdapter();
                        }
                    } catch (Exception exception) {
                        Log.i(TAG, exception.toString());
                    }
                }
                lingoHomePageIndustryView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                lingoHomePageIndustryView.hideProcessing();
            }
        });
    }

    /****
     *
     */
    private void setLingoHomePageBannerAdapter() {
        RealmQuery<LingoHomePageGetBannerResponseLocalStorage> realmQueryLingoHomePageGetBannerResponseLocalStorage =
                realm.where(LingoHomePageGetBannerResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_GET_BANNER);
        RealmResults<LingoHomePageGetBannerResponseLocalStorage> realmResultsLingoHomePageGetBannerResponseLocalStorage =
                realmQueryLingoHomePageGetBannerResponseLocalStorage.findAll();
        if (realmResultsLingoHomePageGetBannerResponseLocalStorage.size() > 3) {
            final LingoHomePageGetBannerResponseLocalStorage lingoHomePageGetBannerResponseFirst = realmResultsLingoHomePageGetBannerResponseLocalStorage.get(0);
            ImageLoader.getInstance().displayImage(Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE + lingoHomePageGetBannerResponseFirst.getAvatar(),
                    lingoHomePageIndustryView.getAppCompatImageViewBannerFirst(), lingoHomePageIndustryView.getDisplayImageOptions(), new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerFirst().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerFirst().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerFirst().setScaleType(ImageView.ScaleType.FIT_XY);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerFirst().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }
                    });
            lingoHomePageIndustryView.getAppCompatImageViewBannerFirst().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        AnalyticsHelper.logEvent(lingoHomePageIndustryView.getContext(), lingoHomePageGetBannerResponseFirst.getMicroSiteKey(),
                                lingoHomePageGetBannerResponseFirst.getMicroSiteKey());
                        AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", lingoHomePageGetBannerResponseFirst.getMicroSiteKey(), true);
                        CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
                        categoriesParcelable.setTitle(lingoHomePageGetBannerResponseFirst.getDescription());
                        categoriesParcelable.setId(Integer.parseInt(lingoHomePageGetBannerResponseFirst.getMicroSiteKey()));
                        categoriesParcelable.setZoneLevel(Integer.parseInt(lingoHomePageGetBannerResponseFirst.getZoneLevel()));
                        categoriesParcelable.setMappingZoneId(Integer.parseInt(lingoHomePageGetBannerResponseFirst.getMicroSiteKey()));
                        Intent intent = new Intent();
                        intent.setClassName(lingoHomePageIndustryView.getContext(), CategoriesActivity.class.getName());
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
                        intent.putExtras(bundle);
                        lingoHomePageIndustryView.getContext().startActivity(intent);
                    } catch (Exception exception) {
                        Log.e(TAG, exception.toString());
                    }
                }
            });
            final LingoHomePageGetBannerResponseLocalStorage lingoHomePageGetBannerResponseSecond = realmResultsLingoHomePageGetBannerResponseLocalStorage.get(1);
            ImageLoader.getInstance().displayImage(Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE + lingoHomePageGetBannerResponseSecond.getAvatar(),
                    lingoHomePageIndustryView.getAppCompatImageViewBannerSecond(), lingoHomePageIndustryView.getDisplayImageOptions(), new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerSecond().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerSecond().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerSecond().setScaleType(ImageView.ScaleType.FIT_XY);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerSecond().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }
                    });
            lingoHomePageIndustryView.getAppCompatImageViewBannerSecond().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        AnalyticsHelper.logEvent(lingoHomePageIndustryView.getContext(), lingoHomePageGetBannerResponseSecond.getMicroSiteKey(),
                                lingoHomePageGetBannerResponseFirst.getMicroSiteKey());
                        AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", lingoHomePageGetBannerResponseSecond.getMicroSiteKey(), true);
                        CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
                        categoriesParcelable.setTitle(lingoHomePageGetBannerResponseSecond.getDescription());
                        categoriesParcelable.setId(Integer.parseInt(lingoHomePageGetBannerResponseSecond.getMicroSiteKey()));
                        categoriesParcelable.setZoneLevel(Integer.parseInt(lingoHomePageGetBannerResponseSecond.getZoneLevel()));
                        categoriesParcelable.setMappingZoneId(Integer.parseInt(lingoHomePageGetBannerResponseSecond.getMicroSiteKey()));
                        Intent intent = new Intent();
                        intent.setClassName(lingoHomePageIndustryView.getContext(), LeafChildrenCategoryActivity.class.getName());
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
                        intent.putExtras(bundle);
                        lingoHomePageIndustryView.getContext().startActivity(intent);
                    } catch (Exception exception) {
                        Log.e(TAG, exception.toString());
                    }
                }
            });

            final LingoHomePageGetBannerResponseLocalStorage lingoHomePageGetBannerResponseThird = realmResultsLingoHomePageGetBannerResponseLocalStorage.get(2);
            ImageLoader.getInstance().displayImage(Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE + lingoHomePageGetBannerResponseThird.getAvatar(),
                    lingoHomePageIndustryView.getAppCompatImageViewBannerThird(), lingoHomePageIndustryView.getDisplayImageOptions(), new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerThird().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerThird().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerThird().setScaleType(ImageView.ScaleType.FIT_XY);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerThird().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }
                    });
            lingoHomePageIndustryView.getAppCompatImageViewBannerThird().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        AnalyticsHelper.logEvent(lingoHomePageIndustryView.getContext(), lingoHomePageGetBannerResponseThird.getMicroSiteKey(),
                                lingoHomePageGetBannerResponseFirst.getMicroSiteKey());
                        AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", lingoHomePageGetBannerResponseThird.getMicroSiteKey(), true);
                        CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
                        categoriesParcelable.setTitle(lingoHomePageGetBannerResponseThird.getDescription());
                        categoriesParcelable.setId(Integer.parseInt(lingoHomePageGetBannerResponseThird.getMicroSiteKey()));
                        categoriesParcelable.setZoneLevel(Integer.parseInt(lingoHomePageGetBannerResponseThird.getZoneLevel()));
                        categoriesParcelable.setMappingZoneId(Integer.parseInt(lingoHomePageGetBannerResponseThird.getMicroSiteKey()));
                        Intent intent = new Intent();
                        intent.setClassName(lingoHomePageIndustryView.getContext(), CategoriesActivity.class.getName());
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
                        intent.putExtras(bundle);
                        lingoHomePageIndustryView.getContext().startActivity(intent);
                    } catch (Exception exception) {
                        Log.e(TAG, exception.toString());
                    }
                }
            });

            final LingoHomePageGetBannerResponseLocalStorage lingoHomePageGetBannerResponseFourth = realmResultsLingoHomePageGetBannerResponseLocalStorage.get(3);
            ImageLoader.getInstance().displayImage(Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE + lingoHomePageGetBannerResponseFourth.getAvatar(),
                    lingoHomePageIndustryView.getAppCompatImageViewBannerFourth(), lingoHomePageIndustryView.getDisplayImageOptions(), new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerFourth().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerFourth().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerFourth().setScaleType(ImageView.ScaleType.FIT_XY);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            lingoHomePageIndustryView.getAppCompatImageViewBannerFourth().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        }
                    });
            lingoHomePageIndustryView.getAppCompatImageViewBannerFourth().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        AnalyticsHelper.logEvent(lingoHomePageIndustryView.getContext(), lingoHomePageGetBannerResponseFourth.getMicroSiteKey(),
                                lingoHomePageGetBannerResponseFirst.getMicroSiteKey());
                        AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", lingoHomePageGetBannerResponseFourth.getMicroSiteKey(), true);
                        CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
                        categoriesParcelable.setTitle(lingoHomePageGetBannerResponseFourth.getDescription());
                        categoriesParcelable.setId(Integer.parseInt(lingoHomePageGetBannerResponseFourth.getMicroSiteKey()));
                        categoriesParcelable.setZoneLevel(Integer.parseInt(lingoHomePageGetBannerResponseFourth.getZoneLevel()));
                        categoriesParcelable.setMappingZoneId(Integer.parseInt(lingoHomePageGetBannerResponseFourth.getMicroSiteKey()));
                        Intent intent = new Intent();
                        intent.setClassName(lingoHomePageIndustryView.getContext(), CategoriesActivity.class.getName());
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
                        intent.putExtras(bundle);
                        lingoHomePageIndustryView.getContext().startActivity(intent);
                    } catch (Exception exception) {
                        Log.e(TAG, exception.toString());
                    }
                }
            });
            lingoHomePageIndustryView.hideProcessing();
        }
    }

    /****
     *
     */
    public void fetchingLingoHomePageBanner() throws Exception {
        lingoHomePageIndustryView.showProcessing();
        RealmQuery<LingoHomePageGetBannerResponseLocalStorage> realmQueryLingoHomePageGetBannerResponseLocalStorage =
                realm.where(LingoHomePageGetBannerResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_GET_BANNER);
        RealmResults<LingoHomePageGetBannerResponseLocalStorage> realmResultsLingoHomePageGetBannerResponseLocalStorage =
                realmQueryLingoHomePageGetBannerResponseLocalStorage.findAll();
        if (realmResultsLingoHomePageGetBannerResponseLocalStorage.size() > 0) {
            Log.i(TAG, "get lingo home page banner load data from cache.............");
            setLingoHomePageBannerAdapter();
            reloadsLingoHomePageBanner = false;
        } else {
            Log.i(TAG, "get lingo home page banner load data from server.............");
            reloadsLingoHomePageBanner = true;
        }

        EventByTypesAPI eventByTypesAPI = lingoHomePageIndustryView.getRetrofit().create(EventByTypesAPI.class);
        Call<List<LingoHomePageGetBannerResponse>> call = eventByTypesAPI.getLingoHomePageGetBanner(2, 2);
        call.enqueue(new Callback<List<LingoHomePageGetBannerResponse>>() {
            @Override
            public void onResponse(Response<List<LingoHomePageGetBannerResponse>> response, Retrofit retrofit) {
                if (response.code() == 200 && null != response.body()) {
                    try {
                        realm.beginTransaction();
                        List<LingoHomePageGetBannerResponse> listLingoHomePageGetBannerResponse = response.body();
                        Iterator<LingoHomePageGetBannerResponse> iteratorLingoHomePageGetBannerResponse = listLingoHomePageGetBannerResponse.iterator();
                        while (iteratorLingoHomePageGetBannerResponse.hasNext()) {
                            LingoHomePageGetBannerResponse industryResponse = iteratorLingoHomePageGetBannerResponse.next();
                            LingoHomePageGetBannerResponseLocalStorage lingoHomePageGetBannerResponseLocalStorage =
                                    LingoHomePageGetBannerResponseLocalStorage.convertFromLingoHomePageGetBannerResponse(industryResponse);
                            realm.copyToRealmOrUpdate(lingoHomePageGetBannerResponseLocalStorage);
                        }
                        realm.commitTransaction();
                        if (reloadsLingoHomePageBanner) {
                            setLingoHomePageBannerAdapter();
                        }
                    } catch (Exception exception) {
                        Log.i(TAG, exception.toString());
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                lingoHomePageIndustryView.hideProcessing();
            }
        });
    }

    /****
     *
     */
    private void setBestIndustriesAdapter() {
        RealmQuery<EventByTypeResponseLocalStorage> realmQueryEventByTypeResponseLocalStorage =
                realm.where(EventByTypeResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_EVENT_BY_TYPE);
        RealmResults<EventByTypeResponseLocalStorage> realmResultsEventByTypeResponseLocalStorage = realmQueryEventByTypeResponseLocalStorage.findAll();
        if (realmResultsEventByTypeResponseLocalStorage.size() > 3) {
            EventByTypeResponseLocalStorage eventByTypeResponseFirst = realmResultsEventByTypeResponseLocalStorage.get(0);
            lingoHomePageIndustryView.getRelativeLayoutFirstLayoutCardTitle().setTag(eventByTypeResponseFirst.getEventId());
            lingoHomePageIndustryView.getRelativeLayoutFirstLayoutCardTitle().setOnClickListener(LingoHomePageIndustryPresenter.this);
            lingoHomePageIndustryView.getAppCompatTextViewBestIndustrialFirst().setText(eventByTypeResponseFirst.getName());
            Log.e("ResponseFirst",eventByTypeResponseFirst.getName());
            fetchingBestIndustrialFirst(eventByTypeResponseFirst.getEventId());

            EventByTypeResponseLocalStorage eventByTypeResponseSecond = realmResultsEventByTypeResponseLocalStorage.get(1);
            lingoHomePageIndustryView.getRelativeLayoutSecondLayoutCardTitle().setTag(eventByTypeResponseSecond.getEventId());
            lingoHomePageIndustryView.getRelativeLayoutSecondLayoutCardTitle().setOnClickListener(LingoHomePageIndustryPresenter.this);
            lingoHomePageIndustryView.getAppCompatTextViewBestIndustrialSecond().setText(eventByTypeResponseSecond.getName());
            fetchingBestIndustrialSecond(eventByTypeResponseSecond.getEventId());

            EventByTypeResponseLocalStorage eventByTypeResponseThird = realmResultsEventByTypeResponseLocalStorage.get(2);
            lingoHomePageIndustryView.getRelativeLayoutThirdLayoutCardTitle().setTag(eventByTypeResponseThird.getEventId());
            lingoHomePageIndustryView.getRelativeLayoutThirdLayoutCardTitle().setOnClickListener(LingoHomePageIndustryPresenter.this);
            lingoHomePageIndustryView.getAppCompatTextViewBestIndustrialThird().setText(eventByTypeResponseThird.getName());
            fetchingBestIndustrialThird(eventByTypeResponseThird.getEventId());

            EventByTypeResponseLocalStorage eventByTypeResponseFourth = realmResultsEventByTypeResponseLocalStorage.get(3);
            lingoHomePageIndustryView.getRelativeLayoutFourthLayoutCardTitle().setTag(eventByTypeResponseFourth.getEventId());
            lingoHomePageIndustryView.getRelativeLayoutFourthLayoutCardTitle().setOnClickListener(LingoHomePageIndustryPresenter.this);
            lingoHomePageIndustryView.getAppCompatTextViewBestIndustrialFourth().setText(eventByTypeResponseFourth.getName());
            fetchingBestIndustrialFourth(eventByTypeResponseFourth.getEventId());

            EventByTypeResponseLocalStorage eventByTypeResponseFifth = realmResultsEventByTypeResponseLocalStorage.get(4);
            lingoHomePageIndustryView.getRelativeLayoutFifthLayoutCardTitle().setTag(eventByTypeResponseFifth.getEventId());
            lingoHomePageIndustryView.getRelativeLayoutFifthLayoutCardTitle().setOnClickListener(LingoHomePageIndustryPresenter.this);
            lingoHomePageIndustryView.getAppCompatTextViewBestIndustrialFifth().setText(eventByTypeResponseFifth.getName());
            fetchingBestIndustrialFifth(eventByTypeResponseFifth.getEventId());
        }
        lingoHomePageIndustryView.hideProcessing();
    }

    /****
     *
     */
    public void fetchingBestIndustries() throws Exception {
        lingoHomePageIndustryView.showProcessing();
        RealmQuery<EventByTypeResponseLocalStorage> realmQueryEventByTypeResponseLocalStorage =
                realm.where(EventByTypeResponseLocalStorage.class).equalTo("moduleId", Constant.MODULE_EVENT_BY_TYPE);
        RealmResults<EventByTypeResponseLocalStorage> realmResultsEventByTypeResponseLocalStorage = realmQueryEventByTypeResponseLocalStorage.findAll();
        if (realmResultsEventByTypeResponseLocalStorage.size() > 0) {
            Log.i(TAG, "get home fetching best industries load data from cache.............");
            setBestIndustriesAdapter();
            reloadsHomeBestIndustries = false;
        } else {
            Log.i(TAG, "get home fetching best industries load data from server.............");
            reloadsHomeBestIndustries = true;
        }
        EventByTypesAPI eventByTypesAPI = lingoHomePageIndustryView.getRetrofit().create(EventByTypesAPI.class);
        Call<List<EventByTypeResponse>> call = eventByTypesAPI.getListEventByType(5, 100);
        call.enqueue(new Callback<List<EventByTypeResponse>>() {
            @Override
            public void onResponse(Response<List<EventByTypeResponse>> response, Retrofit retrofit) {
                if (response.code() == 200 && null != response.body() && response.body().size() > 0) {
                    try {
                        realm.beginTransaction();
                        List<EventByTypeResponse> listEventByTypeResponse = response.body();
                        Iterator<EventByTypeResponse> iteratorEventByTypeResponse = listEventByTypeResponse.iterator();
                        while (iteratorEventByTypeResponse.hasNext()) {
                            EventByTypeResponse eventByTypeResponse = iteratorEventByTypeResponse.next();
                            EventByTypeResponseLocalStorage eventByTypeResponseLocalStorage = EventByTypeResponseLocalStorage.
                                    convertFromEventByTypeResponse(eventByTypeResponse);
                            realm.copyToRealmOrUpdate(eventByTypeResponseLocalStorage);
                        }
                        realm.commitTransaction();
                        if (reloadsHomeBestIndustries) {
                            setBestIndustriesAdapter();
                        }
                    } catch (Exception exception) {
                        Log.i(TAG, exception.toString());
                    }
                }
                lingoHomePageIndustryView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                lingoHomePageIndustryView.hideProcessing();
            }
        });
    }

    /****
     * LingoHomePageBestIndustryService
     *
     * @param eventId
     */
    private void fetchingBestIndustrialFirst(String eventId) {
        new LingoHomePageBestIndustryService(lingoHomePageIndustryView.getContext(),
                lingoHomePageIndustryView.getRetrofit(), lingoHomePageIndustryView.getDisplayImageOptions(), this,
                lingoHomePageIndustryView.getRecyclerViewBestIndustrialFirst())
                .fetchingBestIndustry(Integer.parseInt(eventId));
    }

    /***
     * @param eventId
     */
    private void fetchingBestIndustrialSecond(String eventId) {
        new LingoHomePageBestIndustryService(lingoHomePageIndustryView.getContext(),
                lingoHomePageIndustryView.getRetrofit(), lingoHomePageIndustryView.getDisplayImageOptions(), this,
                lingoHomePageIndustryView.getRecyclerViewBestIndustrialSecond()).
                fetchingBestIndustry(Integer.parseInt(eventId));
    }

    /****
     * @param eventId
     */
    private void fetchingBestIndustrialThird(String eventId) {
        new LingoHomePageBestIndustryService(lingoHomePageIndustryView.getContext(),
                lingoHomePageIndustryView.getRetrofit(), lingoHomePageIndustryView.getDisplayImageOptions(), this,
                lingoHomePageIndustryView.getRecyclerViewBestIndustrialThird())
                .fetchingBestIndustry(Integer.parseInt(eventId));
    }

    /****
     * @param eventId
     */
    private void fetchingBestIndustrialFourth(String eventId) {
        new LingoHomePageBestIndustryService(lingoHomePageIndustryView.getContext(),
                lingoHomePageIndustryView.getRetrofit(), lingoHomePageIndustryView.getDisplayImageOptions(), this,
                lingoHomePageIndustryView.getRecyclerViewBestIndustrialFourth())
                .fetchingBestIndustry(Integer.parseInt(eventId));
    }

    /****
     * @param eventId
     */
    private void fetchingBestIndustrialFifth(String eventId) {
        new LingoHomePageBestIndustryService(lingoHomePageIndustryView.getContext(),
                lingoHomePageIndustryView.getRetrofit(), lingoHomePageIndustryView.getDisplayImageOptions(), this,
                lingoHomePageIndustryView.getRecyclerViewBestIndustrialFifth())
                .fetchingBestIndustry(Integer.parseInt(eventId));
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        lingoHomePageIndustryView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }

    @Override
    public void onClick(View v) {
        int tagValue = Integer.parseInt(v.getTag().toString());
        Intent intent = new Intent();
        intent.setClassName(lingoHomePageIndustryView.getContext(), MoreBestIndustriesActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.EVENT_ID, tagValue);
        intent.putExtras(bundle);
        lingoHomePageIndustryView.getContext().startActivity(intent);
    }
}

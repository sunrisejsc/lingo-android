package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 30/12/2015.
 */
public class EndpointResponse implements Serializable {

    @SerializedName("s3_endpoint")
    private String s3Endpoint;
    @SerializedName("s3_home_page_endpoint")
    private String s3HomePageEndpoint;

    public String getS3Endpoint() {
        return s3Endpoint;
    }

    public void setS3Endpoint(String s3Endpoint) {
        this.s3Endpoint = s3Endpoint;
    }

    public String getS3HomePageEndpoint() {
        return s3HomePageEndpoint;
    }

    public void setS3HomePageEndpoint(String s3HomePageEndpoint) {
        this.s3HomePageEndpoint = s3HomePageEndpoint;
    }
}

package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 08/12/2015.
 */
public class AllEventDealsResponse implements Serializable {

    @SerializedName("items")
    private List<EventDealResponse> listItem;

    @SerializedName("main_event")
    private EventDealResponse eventDealMainEvent;

    public List<EventDealResponse> getListItem() {
        return listItem;
    }

    public void setListItem(List<EventDealResponse> listItem) {
        this.listItem = listItem;
    }

    public EventDealResponse getEventDealMainEvent() {
        return eventDealMainEvent;
    }

    public void setEventDealMainEvent(EventDealResponse eventDealMainEvent) {
        this.eventDealMainEvent = eventDealMainEvent;
    }
}

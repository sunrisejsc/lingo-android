package vn.lingo.marketplace.shopping.launcher;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.MainActivity;
import vn.lingo.marketplace.shopping.models.EndpointAPI;
import vn.lingo.marketplace.shopping.models.EndpointResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;

/**
 * Created by longtran on 15/09/2015.
 */
public class SplashScreen extends Activity {

    private final String TAG = SplashScreen.class.getName();
    /**
     * The package name of your app
     */
    private String package_name;
    private int intCurrentVersion=0;
    private int intResponseVersion =0;
    /**
     * The current version of your app
     */
    private String currentVersion;
    @Bind(R.id.logo_slogan)
    AppCompatTextView appCompatTextView;
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        package_name = getPackageName();
        Log.e("packageName",package_name);
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            intCurrentVersion=Integer.parseInt(currentVersion.replace(".",""));
        } catch (Exception exception) {
            currentVersion = null;
        }
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        retrofit = new Retrofit.Builder()
                .baseUrl(Endpoint.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        EndpointAPI endpointAPI = retrofit.create(EndpointAPI.class);
        Call<EndpointResponse> callEndpointResponse = endpointAPI.getBaseUrl();
        callEndpointResponse.enqueue(new Callback<EndpointResponse>() {
            @Override
            public void onResponse(Response<EndpointResponse> response, Retrofit retrofit) {
                EndpointResponse endpointResponse = response.body();
                if (null != endpointResponse) {
                    Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE = urlFormatter(endpointResponse.getS3HomePageEndpoint(), Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE);
                    Endpoint.LINGO_SIMPLE_STORAGE_SERVICE = urlFormatter(endpointResponse.getS3Endpoint(), Endpoint.LINGO_SIMPLE_STORAGE_SERVICE);
                }
                Log.i(TAG, Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE);
                Log.i(TAG, Endpoint.LINGO_SIMPLE_STORAGE_SERVICE);
                Log.i(TAG, Endpoint.ENDPOINT);
                Log.i(TAG, Endpoint.ENDPOINT_CHECK_OUT);
                new SplashScreenAsyncTask(SplashScreen.this).execute(new String[0]);
            }

            @Override
            public void onFailure(Throwable t) {
                redirectHome(SplashScreen.this);
                finish();
            }
        });
//        Intent serviceIntent = new Intent(this, ForegroundService.class);
//        serviceIntent.setAction(Constant.ACTION.START_FOREGROUND_ACTION);
//        startService(serviceIntent);
    }

    /****
     * @param url
     * @return
     */
    private String urlFormatter(String url, String defaultValue) {
        String urlFormatter = url;
        if (StringUtils.isEmpty(urlFormatter)) {
            return defaultValue;
        }
        if (!url.toLowerCase().matches("^(http|https|ftp)://.*$")) {
            urlFormatter = "http://" + url;
        }
        return urlFormatter;
    }

    /***
     *
     */
    private void redirectHome(Activity activity) {
        Intent intent = new Intent();
        intent.setClassName(activity, MainActivity.class.getName());
        activity.startActivity(intent);
        activity.finish();
    }

    private class SplashScreenAsyncTask extends AsyncTask<String, Integer, String> {

        private Activity activity;

        public SplashScreenAsyncTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String new_version = Jsoup.connect("https://play.google.com/store/apps/details?id=" + package_name + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return new_version;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (!StringUtils.isBlank(currentVersion)
                    && !StringUtils.isBlank(response)) {
                intResponseVersion =Integer.parseInt(response.replace(".",""));
                if (intCurrentVersion< intResponseVersion) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
                        builder.setCancelable(false);
                        builder.setTitle(activity.getString(R.string.you_are_not_updated_title));
                        builder.setIcon(R.drawable.android_app_on_google_play);
                        builder.setMessage(activity.getString(R.string.button_text_message_update_apk));
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + package_name)));
                                dialog.dismiss();
                                finish();
                                System.exit(0);
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                redirectHome(activity);
                            }
                        });
                        builder.show();


//                    new MaterialDialog.Builder(activity)
//                            .cancelable(false)
//                            .iconRes(R.drawable.android_app_on_google_play)
//                            .limitIconToDefaultSize() // limits the displayed icon size to 48dp
//                            .backgroundColorRes(R.color.white)
//                            .title(activity.getString(R.string.you_are_not_updated_title))
//                            .titleColorRes(R.color.black)
//                            .content(activity.getString(R.string.you_are_not_updated_message))
//                            .contentColor(Color.BLACK)
//                            .positiveText(activity.getString(R.string.button_text_message_update_apk))
//                            .positiveColorRes(R.color.black)
//                            .onAny(new MaterialDialog.SingleButtonCallback() {
//                                @Override
//                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + package_name)));
//                                    dialog.dismiss();
//                                    finish();
//                                    System.exit(0);
//                                }
//                            })
//                            .onNegative(new MaterialDialog.SingleButtonCallback() {
//                                @Override
//                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                            .show();


                } else {
                    redirectHome(activity);
                }
            } else {
                redirectHome(activity);
            }
        }
    }
}

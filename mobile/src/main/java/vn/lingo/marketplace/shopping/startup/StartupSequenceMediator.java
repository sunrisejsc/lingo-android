package vn.lingo.marketplace.shopping.startup;

import android.content.Intent;

import vn.lingo.marketplace.shopping.launcher.SplashScreen;

/**
 * Created by longtran on 22/09/2015.
 */
public class StartupSequenceMediator {

    private final StartupActivity mStartupActivity;

    public StartupSequenceMediator(final StartupActivity paramStartupActivity) {
        this.mStartupActivity = paramStartupActivity;
    }

    public void start() {
        Intent intent = new Intent();
        intent.setClassName(mStartupActivity, SplashScreen.class.getName());
        mStartupActivity.startActivity(intent);
        mStartupActivity.finish();
    }
}

package vn.lingo.marketplace.shopping.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.delegate.Singleton;
import vn.lingo.marketplace.shopping.interfaces.PaymentOrderSuccessView;
import vn.lingo.marketplace.shopping.models.checkout.Order;
import vn.lingo.marketplace.shopping.presenters.PaymentOrderSuccessPrecenter;
import vn.lingo.marketplace.shopping.service.RetrieveSiteData;

/**
 * Created by lenhan on 12/04/2016.
 */
public class PaymentOrderSuccessActivity extends AbstractAppCompatActivity implements PaymentOrderSuccessView {
    private final String TAG = PaymentOrderSuccessActivity.class.getName();
    @Bind(R.id.payment_order_success_text_view_code_order)
    AppCompatTextView textViewCodeOrder;
    @Bind(R.id.payment_order_success_text_view_customer_name)
    AppCompatTextView textViewCustomerName;
    @Bind(R.id.payment_order_success_text_view_customer_email)
    AppCompatTextView textViewCustomerEmail;
    @Bind(R.id.payment_order_success_text_view_customer_address)
    AppCompatTextView textViewCustomerAddress;
    @Bind(R.id.payment_order_success_text_view_customer_name_receive)
    AppCompatTextView textViewCustomerNameReceive;
    @Bind(R.id.payment_order_success_text_view_customer_email_receive)
    AppCompatTextView textViewCustomerEmailReceive;
    @Bind(R.id.payment_order_success_text_view_customer_address_receive)
    AppCompatTextView textViewCustomerAddressReceive;
    @Bind(R.id.payment_order_success_text_view_customer_phone)
    AppCompatTextView textViewCustomerPhone;
    @Bind(R.id.payment_order_success_text_view_customer_phone_receive)
    AppCompatTextView textViewCustomerPhoneReceive;
    @Bind(R.id.payment_order_success_text_view_payment_method)
    AppCompatTextView textViewPaymentMethod;
    @Bind(R.id.payment_order_success_button_call_back)
    AppCompatButton buttonCallback;
    @Bind(R.id.payment_order_success_text_view_login_shipping)
    AppCompatTextView textViewLoginShipping;
    private PaymentOrderSuccessPrecenter paymentOrderSuccessPrecenter;

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_payment_order_seccess);
        ButterKnife.bind(this);

        Order order = (Order) getIntent().getExtras().getSerializable("OrderSerializable");
        int isVAT = getIntent().getExtras().getInt("isVAT");
        if (Singleton.getSingleton().getIsStarIntent() == 1) {
            textViewCodeOrder.setText(Singleton.getSingleton().getCodeOrder());
        } else {
            textViewCodeOrder.setText(Singleton.getSingleton().getCodeOrderStorage());
        }

        textViewCustomerName.setText(order.getOrderReceiveName());
        textViewCustomerEmail.setText(order.getOrderReceiveMail());
        textViewCustomerPhone.setText(order.getOrderReceiveMobile());

        textViewCustomerNameReceive.setText(order.getOrderReceiveName());
        textViewCustomerEmailReceive.setText(order.getOrderReceiveMail());
        textViewCustomerPhoneReceive.setText(order.getOrderReceiveMobile());

        textViewPaymentMethod.setText(Singleton.getSingleton().getPaymentMethod());

        if (Singleton.getSingleton().getReceiveGoods() == 0) {
            textViewLoginShipping.setVisibility(View.VISIBLE);

            textViewCustomerAddressReceive.setText(order.getOrderReceiveAddress() + " - " + Singleton.getSingleton().getDistrictName() + " - " + Singleton.getSingleton().getCityName());
            textViewCustomerAddress.setText(order.getOrderReceiveAddress() + " - " + Singleton.getSingleton().getDistrictName() + " - " + Singleton.getSingleton().getCityName());
        } else {
            textViewLoginShipping.setVisibility(View.GONE);
            textViewCustomerAddress.setText(order.getOrderReceiveAddress());
            textViewCustomerAddressReceive.setText(order.getOrderReceiveAddress());
        }

        paymentOrderSuccessPrecenter = new PaymentOrderSuccessPrecenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public AppCompatTextView getTextViewOrderCode() {
        return textViewCodeOrder;
    }

    @Override
    public AppCompatTextView getTextViewCustomerName() {
        return textViewCustomerName;
    }

    @Override
    public AppCompatTextView getTextViewCustomerEmail() {
        return textViewCustomerEmail;
    }

    @Override
    public AppCompatTextView getTextViewCustomerAddress() {
        return textViewCustomerAddress;
    }

    @Override
    public AppCompatTextView getTextViewCustomerPhone() {
        return textViewCustomerPhone;
    }

    @Override
    public AppCompatTextView getTextViewCustomerNameReceive() {
        return textViewCustomerNameReceive;
    }

    @Override
    public AppCompatTextView getTextViewCustomerEmailReceive() {
        return textViewCustomerEmailReceive;
    }

    @Override
    public AppCompatTextView getTextViewCustomerAddressReceive() {
        return textViewCustomerAddressReceive;
    }

    @Override
    public AppCompatTextView getTextViewCustomerPhoneReceive() {
        return textViewCustomerPhoneReceive;
    }

    @Override
    public AppCompatTextView getTextViewMethod() {
        return textViewPaymentMethod;
    }

    @Override
    public AppCompatButton getButtonCallback() {
        return buttonCallback;
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public Context getContext() {
        return getContext();
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {

    }

    @Override
    public void hideProcessing() {

    }

    @Override
    public Retrofit getRetrofit() {
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        paymentOrderSuccessPrecenter.deleteShoppingCart();
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "requestCode : " + requestCode + "resultCode : " + resultCode);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("url");
                Log.i(TAG, "requestCode : " + requestCode + "resultCode : " + resultCode + "result : " + result);
                paymentOrderSuccessPrecenter.deleteShoppingCart();
                new RetrieveSiteData().execute(result);
                Intent intentMainActivity = new Intent(this, MainActivity.class);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intentMainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentMainActivity);
                finishAffinity();
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }
}

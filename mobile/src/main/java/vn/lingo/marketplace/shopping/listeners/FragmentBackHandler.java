package vn.lingo.marketplace.shopping.listeners;

import vn.lingo.marketplace.shopping.views.AbstractFragment;

/**
 * Created by longtran on 13/11/2015.
 */
public interface FragmentBackHandler {
    public abstract boolean onBackPressed(AbstractFragment fragmentStack);
}

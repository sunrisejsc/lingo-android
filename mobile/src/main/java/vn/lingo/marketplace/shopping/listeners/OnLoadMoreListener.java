package vn.lingo.marketplace.shopping.listeners;

/**
 * Created by longtran on 15/11/2015.
 */
public interface OnLoadMoreListener {

    public void onLoadMore(int totalItemCount);
}

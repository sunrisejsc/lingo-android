package vn.lingo.marketplace.shopping.views.home;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 05/11/2015.
 */
public class HotNewsViewHolder {

    @Bind(R.id.item_id)
    public View view;

    @Bind(R.id.item_hot_news)
    public AppCompatTextView appCompatTextViewHotNews;

    @Bind(R.id.item_hot_news_avatar)
    public ImageView imageViewAvatar;

    public HotNewsViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }
}

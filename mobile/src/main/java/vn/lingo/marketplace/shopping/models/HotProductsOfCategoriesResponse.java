package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 07/12/2015.
 */
public class HotProductsOfCategoriesResponse implements Serializable {

    @SerializedName("EXTENT_STYLE")
    private String extentStyle;
    @SerializedName("ZONE_VISIBLEBOTTOM")
    private String zoneVisibleBottom;
    @SerializedName("ZONE_FOOTER")
    private String zoneFooter;
    @SerializedName("ZONE_AVATARMENU")
    private String zoneAvatarMenu;
    @SerializedName("MAPPING_URL")
    private String mappingUrl;
    @SerializedName("ZONE_ALIAS")
    private String zoneAlias;
    @SerializedName("IS_USEBACKGROUND")
    private String isUseBackGround;
    @SerializedName("ZONE_AVATAR")
    private String zoneAvatar;
    @SerializedName("ZONE_TITLE")
    private String zoneTitle;
    @SerializedName("ZONE_LANG")
    private String zoneLang;
    @SerializedName("ZONE_ID")
    private String zoneId;
    @SerializedName("ZONE_MORE")
    private String zoneMore;
    @SerializedName("ZONE_NAME")
    private String zoneName;
    @SerializedName("MAPPING_TYPE")
    private String mappingType;
    @SerializedName("ZONE_VISIBLEINTOP")
    private String zoneVisibleInTop;
    @SerializedName("ZONE_VISIBLEINNAVBAR")
    private String zoneVisibleInNavBar;
    @SerializedName("ZONE_AVATARMAIN")
    private String zoneAvatarMain;
    @SerializedName("PORTAL_TABCONTROLID")
    private String portalTabControlId;
    @SerializedName("VISIBLEONHOME")
    private String visibleOnHome;
    @SerializedName("BACKGROUND_URL")
    private String backgroundUrl;
    @SerializedName("ZONE_LEVEL")
    private String zoneLevel;
    @SerializedName("ZONE_KEYWORDS")
    private String zoneKeywords;
    @SerializedName("MAPPING_ZONEID")
    private String mappingZoneId;
    @SerializedName("ZONE_TYPE")
    private String zoneType;
    @SerializedName("ZONE_HEADER")
    private String zoneHeader;
    @SerializedName("ZONE_DES")
    private String zoneDes;
    @SerializedName("ZONE_PARENTID")
    private String zoneParentId;
    @SerializedName("ZONE_VISIBLECENTER")
    private String zoneVisibleCenter;
    @SerializedName("ZONE_VISIBLEINTAB")
    private String zoneVisibleInTab;
    @SerializedName("ZONE_PAGE")
    private String zonePage;
    @SerializedName("IMG_COORDINATE")
    private String imgCoordinate;
    @SerializedName("ZONE_PRIORITY")
    private String zonePriority;

    public String getExtentStyle() {
        return extentStyle;
    }

    public void setExtentStyle(String extentStyle) {
        this.extentStyle = extentStyle;
    }

    public String getZoneVisibleBottom() {
        return zoneVisibleBottom;
    }

    public void setZoneVisibleBottom(String zoneVisibleBottom) {
        this.zoneVisibleBottom = zoneVisibleBottom;
    }

    public String getZoneFooter() {
        return zoneFooter;
    }

    public void setZoneFooter(String zoneFooter) {
        this.zoneFooter = zoneFooter;
    }

    public String getZoneAvatarMenu() {
        return zoneAvatarMenu;
    }

    public void setZoneAvatarMenu(String zoneAvatarMenu) {
        this.zoneAvatarMenu = zoneAvatarMenu;
    }

    public String getMappingUrl() {
        return mappingUrl;
    }

    public void setMappingUrl(String mappingUrl) {
        this.mappingUrl = mappingUrl;
    }

    public String getZoneAlias() {
        return zoneAlias;
    }

    public void setZoneAlias(String zoneAlias) {
        this.zoneAlias = zoneAlias;
    }

    public String getIsUseBackGround() {
        return isUseBackGround;
    }

    public void setIsUseBackGround(String isUseBackGround) {
        this.isUseBackGround = isUseBackGround;
    }

    public String getZoneAvatar() {
        return zoneAvatar;
    }

    public void setZoneAvatar(String zoneAvatar) {
        this.zoneAvatar = zoneAvatar;
    }

    public String getZoneTitle() {
        return zoneTitle;
    }

    public void setZoneTitle(String zoneTitle) {
        this.zoneTitle = zoneTitle;
    }

    public String getZoneLang() {
        return zoneLang;
    }

    public void setZoneLang(String zoneLang) {
        this.zoneLang = zoneLang;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneMore() {
        return zoneMore;
    }

    public void setZoneMore(String zoneMore) {
        this.zoneMore = zoneMore;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getMappingType() {
        return mappingType;
    }

    public void setMappingType(String mappingType) {
        this.mappingType = mappingType;
    }

    public String getZoneVisibleInTop() {
        return zoneVisibleInTop;
    }

    public void setZoneVisibleInTop(String zoneVisibleInTop) {
        this.zoneVisibleInTop = zoneVisibleInTop;
    }

    public String getZoneVisibleInNavBar() {
        return zoneVisibleInNavBar;
    }

    public void setZoneVisibleInNavBar(String zoneVisibleInNavBar) {
        this.zoneVisibleInNavBar = zoneVisibleInNavBar;
    }

    public String getZoneAvatarMain() {
        return zoneAvatarMain;
    }

    public void setZoneAvatarMain(String zoneAvatarMain) {
        this.zoneAvatarMain = zoneAvatarMain;
    }

    public String getPortalTabControlId() {
        return portalTabControlId;
    }

    public void setPortalTabControlId(String portalTabControlId) {
        this.portalTabControlId = portalTabControlId;
    }

    public String getVisibleOnHome() {
        return visibleOnHome;
    }

    public void setVisibleOnHome(String visibleOnHome) {
        this.visibleOnHome = visibleOnHome;
    }

    public String getBackgroundUrl() {
        return backgroundUrl;
    }

    public void setBackgroundUrl(String backgroundUrl) {
        this.backgroundUrl = backgroundUrl;
    }

    public String getZoneLevel() {
        return zoneLevel;
    }

    public void setZoneLevel(String zoneLevel) {
        this.zoneLevel = zoneLevel;
    }

    public String getZoneKeywords() {
        return zoneKeywords;
    }

    public void setZoneKeywords(String zoneKeywords) {
        this.zoneKeywords = zoneKeywords;
    }

    public String getMappingZoneId() {
        return mappingZoneId;
    }

    public void setMappingZoneId(String mappingZoneId) {
        this.mappingZoneId = mappingZoneId;
    }

    public String getZoneType() {
        return zoneType;
    }

    public void setZoneType(String zoneType) {
        this.zoneType = zoneType;
    }

    public String getZoneHeader() {
        return zoneHeader;
    }

    public void setZoneHeader(String zoneHeader) {
        this.zoneHeader = zoneHeader;
    }

    public String getZoneDes() {
        return zoneDes;
    }

    public void setZoneDes(String zoneDes) {
        this.zoneDes = zoneDes;
    }

    public String getZoneParentId() {
        return zoneParentId;
    }

    public void setZoneParentId(String zoneParentId) {
        this.zoneParentId = zoneParentId;
    }

    public String getZoneVisibleCenter() {
        return zoneVisibleCenter;
    }

    public void setZoneVisibleCenter(String zoneVisibleCenter) {
        this.zoneVisibleCenter = zoneVisibleCenter;
    }

    public String getZoneVisibleInTab() {
        return zoneVisibleInTab;
    }

    public void setZoneVisibleInTab(String zoneVisibleInTab) {
        this.zoneVisibleInTab = zoneVisibleInTab;
    }

    public String getZonePage() {
        return zonePage;
    }

    public void setZonePage(String zonePage) {
        this.zonePage = zonePage;
    }

    public String getImgCoordinate() {
        return imgCoordinate;
    }

    public void setImgCoordinate(String imgCoordinate) {
        this.imgCoordinate = imgCoordinate;
    }

    public String getZonePriority() {
        return zonePriority;
    }

    public void setZonePriority(String zonePriority) {
        this.zonePriority = zonePriority;
    }
}

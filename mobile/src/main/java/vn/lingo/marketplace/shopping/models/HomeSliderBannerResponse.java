package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 05/11/2015.
 */
public class HomeSliderBannerResponse implements Serializable {

    @SerializedName("DES")
    private String description;
    @SerializedName("NAME")
    private String name;
    @SerializedName("GALLERY_ID")
    private String galleryId;
    @SerializedName("IMAGE_PATH")
    private String imagePath;
    @SerializedName("URL")
    private String url;
    @SerializedName("GALLERY_KEY")
    private String galleryKey;
    @SerializedName("PARAM1")
    private String parameter1;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGalleryId() {
        return galleryId;
    }

    public void setGalleryId(String galleryId) {
        this.galleryId = galleryId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public String getGalleryKey() {
        return galleryKey;
    }

    public void setGalleryKey(String galleryKey) {
        this.galleryKey = galleryKey;
    }


}

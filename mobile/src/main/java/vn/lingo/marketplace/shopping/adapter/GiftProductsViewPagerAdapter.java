package vn.lingo.marketplace.shopping.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.sourceforge.android.view.autoscrollviewpager.RecyclingPagerAdapter;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.GiftProductsResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.ProductDetailsGiftProductsViewPagerHolder;

/**
 * Created by longtran on 04/11/2015.
 */
public class GiftProductsViewPagerAdapter extends RecyclingPagerAdapter {

    private Context context;
    private List<GiftProductsResponse> listGiftProductsResponse;
    private DisplayImageOptions displayImageOptions;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;

    public GiftProductsViewPagerAdapter(Context context, List<GiftProductsResponse> listGiftProductsResponse,
                                        DisplayImageOptions displayImageOptions,
                                        RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.context = context;
        this.listGiftProductsResponse = listGiftProductsResponse;
        this.displayImageOptions = displayImageOptions;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
    }

    @Override
    public int getCount() {
        return listGiftProductsResponse.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        ProductDetailsGiftProductsViewPagerHolder productDetailsGiftProductsViewPagerHolder;
        if (convertView == null) {
            /***
             * inflate the layout
             *
             */
            convertView = LayoutInflater.from(container.getContext())
                    .inflate(R.layout.product_details_gift_products_view_pager_item_layout, container, false);
            /***
             * well set up the
             */
            productDetailsGiftProductsViewPagerHolder = new ProductDetailsGiftProductsViewPagerHolder(convertView);
            /***
             * store the holder with the view.
             */
            convertView.setTag(productDetailsGiftProductsViewPagerHolder);
        } else {
            /***
             * we've just avoided calling findViewById() on resource everytimejust use the viewHolder
             */
            productDetailsGiftProductsViewPagerHolder = (ProductDetailsGiftProductsViewPagerHolder) convertView.getTag();
        }
        final GiftProductsResponse giftProductsResponse = listGiftProductsResponse.get(position);
        ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + giftProductsResponse.getAvatar(),
                productDetailsGiftProductsViewPagerHolder.imageView, displayImageOptions);
        productDetailsGiftProductsViewPagerHolder.appCompatTextViewGiftName.setText(giftProductsResponse.getGiftName());
        productDetailsGiftProductsViewPagerHolder.appCompatTextViewProductName.setText(giftProductsResponse.getProductName());
        productDetailsGiftProductsViewPagerHolder.appCompatTextViewProductPrice.setText(giftProductsResponse.getProductPriceFormat());
        productDetailsGiftProductsViewPagerHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(0, Integer.parseInt(giftProductsResponse.getProductId()),
                            Integer.MAX_VALUE, Integer.MAX_VALUE, giftProductsResponse.getProductName());
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        return convertView;
    }
}

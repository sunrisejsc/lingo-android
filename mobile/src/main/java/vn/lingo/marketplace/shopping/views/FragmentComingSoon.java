package vn.lingo.marketplace.shopping.views;

import android.content.res.Resources;

import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 12/12/2015.
 */
public class FragmentComingSoon extends AbstractFragment {

    private final String TAG = FragmentComingSoon.class.getName();

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_coming_soon_screen;
    }

    @Override
    public int getFragmentViewId() {
        return R.id.frame_container;
    }
}

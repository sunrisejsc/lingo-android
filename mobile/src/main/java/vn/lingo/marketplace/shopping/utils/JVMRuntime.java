/**
 * Copyright (c) 2012, SetaCinQ
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 */
package vn.lingo.marketplace.shopping.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.util.Log;
/**
 * Provides an interface to VM-global, Dalvik-specific features.
 * An application cannot create its own Runtime instance, and must obtain
 * one from the getRuntime method.
 *
 * This is an internal Dalvik class that is not appropriate for
 * general use. It will be removed from the public API in a future release.
 *
 * @since Android 1.0
 * Project name : Resolution
 * Package name : com.resolution.utils
 * File name    : VMRuntime.java
 * Author       : longtq6195
 * Create by    : longtq6195
 * Create date  : May 09, 2012 10:38:03 AM
 */
public class JVMRuntime {
	
	private final String TAG = JVMRuntime.class.getName();
    /**
     * Holds the VMRuntime singleton.
     */
    private static final JVMRuntime THE_ONE = new JVMRuntime();
    
	private Class<?> jClass;
	
	private Object runtime = null;
 
    /**
     * Prevents this class from being instantiated.
     */
	private JVMRuntime() {
		initReflection();
	}
	/**
     * Returns the object that represents the VM instance's Dalvik-specific
     * runtime environment.
     *
     * @return the runtime object
     */
	public static JVMRuntime getVMRuntime() {
		return THE_ONE;
	}
	/***
	 * init reflection class dalvik.system.VMRuntime from Dalvik Virtual Machine
	 */
	private void initReflection() {
		try {
			Log.e(TAG, "VMRuntime installation was completed...................... !");
			jClass = Class.forName("dalvik.system.VMRuntime");
			Method methodRuntime = jClass.getMethod("getRuntime", new Class[0]);
			runtime = methodRuntime.invoke(null, new Object[0]);
		} catch (ClassNotFoundException e) {
			Log.i(TAG, "VMRuntime hack does not work! ClassNotFoundException");
			runtime = null;
		} catch (SecurityException e) {
			Log.i(TAG, "VMRuntime hack does not work! SecurityException");
			runtime = null;
		} catch (NoSuchMethodException e) {
			Log.i(TAG, "VMRuntime hack does not work! NoSuchMethodException");
			runtime = null;
		} catch (IllegalArgumentException e) {
			Log.i(TAG, "VMRuntime hack does not work! IllegalArgumentException");
			runtime = null;
		} catch (IllegalAccessException e) {
			Log.i(TAG, "VMRuntime hack does not work! IllegalAccessException");
			runtime = null;
		} catch (InvocationTargetException e) {
			Log.i(TAG, "VMRuntime hack does not work! InvocationTargetException");
			runtime = null;
		}
	}
    /**
     * Requests that the virtual machine collect available memory,
     * and collects any SoftReferences that are not strongly-reachable.
     */
    public synchronized void gcSoftReferences() {
    	if(null == runtime){
    		Log.e(TAG, "VMRuntime hack does not work!");
    		return;
    	}
    	try {
    		Method methodgcSoftReferences = jClass.getMethod("gcSoftReferences", new Class[0]);
			methodgcSoftReferences.invoke(runtime, new Object[0]);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, e.toString());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.toString());
		} catch (InvocationTargetException e) {
			Log.e(TAG, e.toString());
		} catch (SecurityException e) {
			Log.e(TAG, e.toString());
		} catch (NoSuchMethodException e) {
			Log.e(TAG, e.toString());
		}
    }
    /**
     * Does not return until any pending finalizers have been called.
     * This may or may not happen in the context of the calling thread.
     * No exceptions will escape.
     */
    public synchronized void runFinalizationSync() {
    	if(null == runtime) {
    		Log.e(TAG, "VMRuntime hack does not work!");
    		return;
    	}
    	try {
    		Method methodRunFinalizationSync = jClass.getMethod("runFinalizationSync", new Class[0]);
    		methodRunFinalizationSync.invoke(runtime, new Object[0]);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, e.toString());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.toString());
		} catch (InvocationTargetException e) {
			Log.e(TAG, e.toString());
		} catch (SecurityException e) {
			Log.e(TAG, e.toString());
		} catch (NoSuchMethodException e) {
			Log.e(TAG, e.toString());
		}
    }
    /**
     * Removes any growth limits, allowing the application to allocate
     * up to the maximum heap size.
     */
    public synchronized void clearGrowthLimit() {
		Log.e(TAG, "Method clearGrowthLimit() working..........");
    	if(null == runtime) {
    		Log.e(TAG, "Method clearGrowthLimit() does not work!");
    		return;
    	}
    	try {
    		Method methodClearGrowthLimit = jClass.getMethod("clearGrowthLimit", new Class[0]);
    		methodClearGrowthLimit.invoke(runtime, new Object[0]);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, e.toString());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.toString());
		} catch (InvocationTargetException e) {
			Log.e(TAG, e.toString());
		} catch (SecurityException e) {
			Log.e(TAG, e.toString());
		} catch (NoSuchMethodException e) {
			Log.e(TAG, e.toString());
		}
    }
    /**
     * Gets the current ideal heap utilization, represented as a number
     * between zero and one.  After a GC happens, the Dalvik heap may
     * be resized so that (size of live objects) / (size of heap) is
     * equal to this number.
     * http://code.metager.de/source/xref/android/4.1.1/dalvik/vm/native/dalvik_system_VMRuntime.cpp
     * https://android.googlesource.com/platform/libcore-snapshot/+/refs/heads/ics-mr1/dalvik/src/main/java/dalvik/system/VMRuntime.java
     * https://code.google.com/p/android-source-browsing/source/browse/libcore/dalvik/src/main/java/dalvik/system/VMRuntime.java?repo=platform--dalvik&name=froyo&r=886130bc7ff992940e152636f57072e58c91aa2e
     * @return the current ideal heap utilization
     */
    public synchronized float getTargetHeapUtilization() {
    	if(null == runtime) {
    		Log.e(TAG, "Method getTargetHeapUtilization() does not work!");
    		return 0;
    	}
    	try {
    		Method methodTargetHeapUtilization = jClass.getMethod("getTargetHeapUtilization", new Class[0]);
    		Object result = methodTargetHeapUtilization.invoke(runtime, new Object[0]);
    		return (result instanceof Float) ? (Float)result : 0;
		} catch (IllegalArgumentException e) {
			Log.e(TAG, e.toString());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.toString());
		} catch (InvocationTargetException e) {
			Log.e(TAG, e.toString());
		} catch (SecurityException e) {
			Log.e(TAG, e.toString());
		} catch (NoSuchMethodException e) {
			Log.e(TAG, e.toString());
		}
    	return 0;
    }
    /**
     * Sets the desired minimum heap size, and returns the
     * old minimum size.  If size is larger than the maximum
     * size, the maximum size will be used.  If size is zero
     * or negative, the minimum size constraint will be removed.
     *
     * <p>Synchronized to make the order of the exchange reliable.
     *
     * <p>This is only a hint to the garbage collector and may be ignored.
     *
     * @param size the new suggested minimum heap size, in bytes
     * @return the old minimum heap size value
     */
    public synchronized long setMinimumHeapSize(long size) {
    	if(null == runtime) {
    		Log.e(TAG, "Method setMinimumHeapSize(long size) does not work!");
    		return 0;
    	}
    	try {
    		Method methodMinimumHeapSize = jClass.getMethod("setMinimumHeapSize", new Class[] { long.class });
    		Object result = methodMinimumHeapSize.invoke(runtime, Long.valueOf(size));
    		return (result instanceof Long) ? (Long)result : 0;
		} catch (IllegalArgumentException e) {
			Log.e(TAG, e.toString());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.toString());
		} catch (InvocationTargetException e) {
			Log.e(TAG, e.toString());
		} catch (SecurityException e) {
			Log.e(TAG, e.toString());
		} catch (NoSuchMethodException e) {
			Log.e(TAG, e.toString());
		}
    	return 0;
    }

    /**
     * Returns the minimum heap size, or zero if no minimum is in effect.
     *
     * @return the minimum heap size value
     */
    public synchronized long getMinimumHeapSize() {
    	if(null == runtime) {
    		Log.e(TAG, "Method getMinimumHeapSize() does not work!");
    		return 0;
    	}
    	try {
    		Method methodMinimumHeapSize = jClass.getMethod("getMinimumHeapSize", new Class[0]);
    		Object result = methodMinimumHeapSize.invoke(runtime, new Object[0]);
    		return (result instanceof Long) ? (Long)result : 0;
		} catch (IllegalArgumentException e) {
			Log.e(TAG, e.toString());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.toString());
		} catch (InvocationTargetException e) {
			Log.e(TAG, e.toString());
		} catch (SecurityException e) {
			Log.e(TAG, e.toString());
		} catch (NoSuchMethodException e) {
			Log.e(TAG, e.toString());
		}
    	return 0;
    }
    /**
     * Sets the current ideal heap utilization, represented as a number
     * between zero and one.  After a GC happens, the Dalvik heap may
     * be resized so that (size of live objects) / (size of heap) is
     * equal to this number.
     *
     * <p>This is only a hint to the garbage collector and may be ignored.
     *
     * @param newTarget the new suggested ideal heap utilization.
     *                  This value may be adjusted internally.
     * @return the previous ideal heap utilization
     * @throws IllegalArgumentException if newTarget is &lt;= 0.0 or &gt;= 1.0
     */
    public synchronized float setTargetHeapUtilization(float newTarget) {
    	if(null == runtime) {
    		Log.e(TAG, "Method setTargetHeapUtilization(float newTarget) does not work!");
    		return 0;
    	}
    	try {
    		Method methodTargetHeapUtilization = jClass.getMethod("setTargetHeapUtilization", new Class[] { float.class });
    		Object result = methodTargetHeapUtilization.invoke(runtime, Float.valueOf(newTarget));
    		return (result instanceof Float) ? (Float)result : 0;
		} catch (IllegalArgumentException e) {
			Log.e(TAG, e.toString());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.toString());
		} catch (InvocationTargetException e) {
			Log.e(TAG, e.toString());
		} catch (SecurityException e) {
			Log.e(TAG, e.toString());
		} catch (NoSuchMethodException e) {
			Log.e(TAG, e.toString());
		}
    	return 0;
    }
}
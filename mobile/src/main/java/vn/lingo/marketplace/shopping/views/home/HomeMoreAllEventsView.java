package vn.lingo.marketplace.shopping.views.home;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import vn.lingo.marketplace.shopping.views.AbstractView;

/**
 * Created by longtran on 06/12/2015.
 */
public interface HomeMoreAllEventsView extends AbstractView {

    public Toolbar getToolbar();

    public AppCompatActivity getAppCompatActivity();

    public RecyclerView getRecyclerView();

    public void onItemClickedEventDealFixDealsEventByTypes(int position, int id, String micrositeKey, String micrositeType);

}

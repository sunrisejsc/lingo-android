package vn.lingo.marketplace.shopping.adapter.checkout;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ShipType;

/**
 * Created by zon on 31/03/2016.
 */
public class ShipTypeAdapter extends ArrayAdapter<ShipType>{

    private Context context;
    private List<ShipType> lstShipTypes;
    private LayoutInflater layoutInflater;

    public ShipTypeAdapter(Context context, int resource, List<ShipType> objects) {
        super(context, resource, objects);

        this.context = context;
        this.lstShipTypes = objects;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getCustomView(int position, View convertView, ViewGroup parents){
        Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.layout_item_ship_type, null);
            holder.textViewShipType = (AppCompatTextView) convertView.findViewById(R.id.item_ship_type_text_view);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        ShipType shipType = lstShipTypes.get(position);
        holder.textViewShipType.setText(shipType.getName());
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    class Holder {
        AppCompatTextView textViewShipType;
    }
}

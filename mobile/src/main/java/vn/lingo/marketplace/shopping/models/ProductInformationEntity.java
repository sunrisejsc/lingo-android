package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 09/12/2015.
 */
public class ProductInformationEntity implements Serializable {

    @SerializedName("IN_STORE")
    private String in_store;
    @SerializedName("AVATAR")
    private String avatar;
    @SerializedName("IS_BIGTYPE")
    private String isBigType;
    @SerializedName("PRODUCT_ID")
    private String productId;
    @SerializedName("PRODUCT_NAME")
    private String productName;
    @SerializedName("PROMOTION_ID")
    private String promotionId;
    @SerializedName("END_DATE")
    private String endDate;
    @SerializedName("ATTRIBUTES_SET_ID")
    private String attributesSetId;
    @SerializedName("ATTRIBUTE_ID")
    private String attributeId;
    @SerializedName("RATING_NUM")
    private String ratingNum;
    @SerializedName("Logo_Thuonghieu")
    private String logoBrand;
    @SerializedName("DISCOUNTS_TYPE")
    private String discountsType;
    @SerializedName("WEIGHT")
    private String weight;
    @SerializedName("PRODUCT_PRICE")
    private String productPrice;
    @SerializedName("PRODUCT_PRICEDISCOUNT")
    private String productPriceDiscount;
    @SerializedName("ISINSTALLMENT")
    private String isInstallment;
    @SerializedName("SKU")
    private String sku;
    @SerializedName("PRODUCT_QUANTITY")
    private String productQuantity;
    @SerializedName("ISVAT")
    private String isVat;
    @SerializedName("MARKET_PRICE")
    private String marketPrice;
    @SerializedName("RATING_STAR")
    private String ratingStar;
    @SerializedName("NUMBER_VALUE")
    private String numberValue;
    @SerializedName("PROMOTION_DISCOUNT")
    private String promotionDiscount;
    @SerializedName("SHIP_FREE")
    private String shipFree;
    @SerializedName("VAT")
    private String vat;
    @SerializedName("TRADEMARK")
    private String trademark;
    @SerializedName("ISACTIVE")
    private String isActive;
    @SerializedName("MARKET_PRICE_FORMAT")
    private String marketPriceFormat;
    @SerializedName("PRODUCT_PRICE_FORMAT")
    private String productPriceFormat;
    @SerializedName("PRODUCT_PRICEDISCOUNT_FORMAT")
    private String productPriceDiscountFormat;
    @SerializedName("PRODUCT_WISH_LIST")
    private String productWishList;
    @SerializedName("PRODUCT_PlACED_ORDER")
    private String productPlacedOrders;
    @SerializedName("SIZE")
    private String size;
    @SerializedName("COLOR")
    private String color;
    @SerializedName("PRODUCT_GOLD_HOUR")
    private String productGoldHour;
    @SerializedName("PRODUCT_DISCOUNT_LAST")
    private String productDiscount;
    @SerializedName("IS_STOP_SAVEBUY")
    private String stopSaveBuy;
    @SerializedName("IS_PAUSE_SAVEBUY")
    private String pauseSaveBuy;


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getInStore() {
        return in_store;
    }

    public void setInStore(String in_store) {
        this.in_store = in_store;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getIsBigType() {
        return isBigType;
    }

    public void setIsBigType(String isBigType) {
        this.isBigType = isBigType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAttributesSetId() {
        return attributesSetId;
    }

    public void setAttributesSetId(String attributesSetId) {
        this.attributesSetId = attributesSetId;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getRatingNum() {
        return ratingNum;
    }

    public void setRatingNum(String ratingNum) {
        this.ratingNum = ratingNum;
    }

    public String getLogoBrand() {
        return logoBrand;
    }

    public void setLogoBrand(String logoBrand) {
        this.logoBrand = logoBrand;
    }

    public String getDiscountsType() {
        return discountsType;
    }

    public void setDiscountsType(String discountsType) {
        this.discountsType = discountsType;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductPriceDiscount() {
        return productPriceDiscount;
    }

    public void setProductPriceDiscount(String productPriceDiscount) {
        this.productPriceDiscount = productPriceDiscount;
    }

    public String getIsInstallment() {
        return isInstallment;
    }

    public void setIsInstallment(String isInstallment) {
        this.isInstallment = isInstallment;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getIsVat() {
        return isVat;
    }

    public void setIsVat(String isVat) {
        this.isVat = isVat;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(String ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(String numberValue) {
        this.numberValue = numberValue;
    }

    public String getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(String promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public String getShipFree() {
        return shipFree;
    }

    public void setShipFree(String shipFree) {
        this.shipFree = shipFree;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getMarketPriceFormat() {
        return marketPriceFormat;
    }

    public void setMarketPriceFormat(String marketPriceFormat) {
        this.marketPriceFormat = marketPriceFormat;
    }

    public String getProductPriceFormat() {
        return productPriceFormat;
    }

    public void setProductPriceFormat(String productPriceFormat) {
        this.productPriceFormat = productPriceFormat;
    }

    public String getProductPriceDiscountFormat() {
        return productPriceDiscountFormat;
    }

    public void setProductPriceDiscountFormat(String productPriceDiscountFormat) {
        this.productPriceDiscountFormat = productPriceDiscountFormat;
    }

    public String getProductWishList() {
        return productWishList;
    }

    public void setProductWishList(String productWishList) {
        this.productWishList = productWishList;
    }

    public String getProductPlacedOrders() {
        return productPlacedOrders;
    }

    public void setProductPlacedOrders(String productPlacedOrders) {
        this.productPlacedOrders = productPlacedOrders;
    }

    public String getProductGoldHour() {
        return productGoldHour;
    }

    public void setProductGoldHour(String productGoldHour) {
        this.productGoldHour = productGoldHour;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    public String getStopSaveBuy() {
        return stopSaveBuy;
    }

    public void setStopSaveBuy(String stopSaveBuy) {
        this.stopSaveBuy = stopSaveBuy;
    }

    public String getPauseSaveBuy() {
        return pauseSaveBuy;
    }

    public void setPauseSaveBuy(String pauseSaveBuy) {
        this.pauseSaveBuy = pauseSaveBuy;
    }
}

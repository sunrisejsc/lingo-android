package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lenhan on 01/04/2016.
 */
public class ReceiveType implements Serializable {
    @SerializedName("ID")
    private int id;
    @SerializedName("NAME")
    private String name;
    @SerializedName("STORE_ID")
    private String storeID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

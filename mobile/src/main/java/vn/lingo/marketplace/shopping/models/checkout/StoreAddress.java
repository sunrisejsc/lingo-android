package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lenhan on 01/04/2016.
 */
public class StoreAddress extends RealmObject implements Serializable {
    @PrimaryKey
    @SerializedName("STORE_ID")
    private String storeID;
    @SerializedName("STORE_NAME")
    private String storeName;
    @SerializedName("STORE_ADDRESS")
    private String storeAddress;

    private String index;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;

    }

    public StoreAddress(String index,String storeID, String storeName, String storeAddress) {
        this.index=index;
        this.storeID = storeID;
        this.storeName = storeName;
        this.storeAddress = storeAddress;
    }

    public StoreAddress() {
    }
}

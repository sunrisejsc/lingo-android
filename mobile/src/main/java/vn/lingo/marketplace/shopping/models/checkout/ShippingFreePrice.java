package vn.lingo.marketplace.shopping.models.checkout;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;

/**
 * Created by lenhan on 06/04/2016.
 */
public class ShippingFreePrice implements Serializable {
    @SerializedName("orderShipingInfo")
    private OrderShippingInfo orderShipingInfo;
    @SerializedName("listProductShipingInfo")
    private List<ProductShippingInfo> productShippingInfoList;
    @SerializedName("totalAmount")
    private int totalAmount;
    @SerializedName("shipType")
    private int shipType;

    public OrderShippingInfo getOrderShipingInfo() {
        return orderShipingInfo;
    }

    public void setOrderShipingInfo(OrderShippingInfo orderShipingInfo) {
        this.orderShipingInfo = orderShipingInfo;
    }

    public List<ProductShippingInfo> getProductShippingInfoList() {
        return productShippingInfoList;
    }

    public void setProductShippingInfoList(List<ProductShippingInfo> productShippingInfoList) {
        this.productShippingInfoList = productShippingInfoList;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getShipType() {
        return shipType;
    }

    public void setShipType(int shipType) {
        this.shipType = shipType;
    }

    public static String getShippingFreePrice(OrderShippingInfo orderShipingInfo
            ,List<ProductShippingInfo> productShippingInfos,int totalAmount,int shipType){
        ShippingFreePrice shippingFreePrice=new ShippingFreePrice();
        shippingFreePrice.setOrderShipingInfo(orderShipingInfo);
        shippingFreePrice.setProductShippingInfoList(productShippingInfos);
        shippingFreePrice.setTotalAmount(totalAmount);
        shippingFreePrice.setShipType(shipType);

        String jsonObject=new Gson().toJson(shippingFreePrice);

        HashMap<String, String> fetchPhotoStatEventParams = new HashMap<String, String>();
        fetchPhotoStatEventParams.put(AnalyticsHelper.EVENT_PRODUCT_ORDER_DATA_REQUEST, jsonObject);
        AnalyticsHelper.logEvent(AnalyticsHelper.EVENT_PRODUCT_ORDER, fetchPhotoStatEventParams, true);
        Log.e("Json_shipFree: " ,jsonObject);

        return jsonObject;
    }
}

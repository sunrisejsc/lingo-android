package vn.lingo.marketplace.shopping.presenters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.thefinestartist.finestwebview.FinestWebView;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.PaymentOrderSuccessActivity;
import vn.lingo.marketplace.shopping.adapter.checkout.BankVATAdapter;
import vn.lingo.marketplace.shopping.adapter.checkout.CustomerPaymentRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.adapter.checkout.LocationCityAdapter;
import vn.lingo.marketplace.shopping.adapter.checkout.LocationDistrictAdapter;
import vn.lingo.marketplace.shopping.adapter.checkout.ShipTypeAdapter;
import vn.lingo.marketplace.shopping.apicheckout.ApiCheckout;
import vn.lingo.marketplace.shopping.delegate.Singleton;
import vn.lingo.marketplace.shopping.interfaces.CustomerPaymentOrdersView;
import vn.lingo.marketplace.shopping.interfaces.EventClickListenerProductOrder;
import vn.lingo.marketplace.shopping.listeners.CheckoutListeners;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.listeners.ShoppingCartListener;
import vn.lingo.marketplace.shopping.models.City;
import vn.lingo.marketplace.shopping.models.District;
import vn.lingo.marketplace.shopping.models.OrderHistoryLocalStorage;
import vn.lingo.marketplace.shopping.models.ProductCheckoutAPI;
import vn.lingo.marketplace.shopping.models.ProductCheckoutRequest;
import vn.lingo.marketplace.shopping.models.ProductCheckoutResponse;
import vn.lingo.marketplace.shopping.models.ProductInformationEntity;
import vn.lingo.marketplace.shopping.models.ReceiveType;
import vn.lingo.marketplace.shopping.models.ShipType;
import vn.lingo.marketplace.shopping.models.ShoppingCartEntity;
import vn.lingo.marketplace.shopping.models.ShoppingCartLocalStorage;
import vn.lingo.marketplace.shopping.models.checkout.BankVAT;
import vn.lingo.marketplace.shopping.models.checkout.CheckEcoupon;
import vn.lingo.marketplace.shopping.models.checkout.CreateOrder;
import vn.lingo.marketplace.shopping.models.checkout.InstallmentOrder;
import vn.lingo.marketplace.shopping.models.checkout.MobileInfo;
import vn.lingo.marketplace.shopping.models.checkout.Order;
import vn.lingo.marketplace.shopping.models.checkout.OrderDetail;
import vn.lingo.marketplace.shopping.models.checkout.OrderShippingInfo;
import vn.lingo.marketplace.shopping.models.checkout.ProductShippingInfo;
import vn.lingo.marketplace.shopping.models.checkout.ShippingFreePrice;
import vn.lingo.marketplace.shopping.models.checkout.StoreAddress;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.NumberFormat;
import vn.lingo.marketplace.shopping.utils.Utility;
import vn.lingo.marketplace.shopping.views.ShoppingCartView;

/**
 * Created by Administrator on 3/23/2016.
 */
public class CustomerPaymentOrdersPresenter extends AbstractPresenter<ShoppingCartView>
        implements CheckoutListeners, RecyclerViewItemClickListener, ShoppingCartListener, EventClickListenerProductOrder {
    private final String TAG = CustomerPaymentOrdersPresenter.class.getName();
    CustomerPaymentOrdersView customerPaymentOrdersView;
    CustomerPaymentRecyclerViewAdapter customerPaymentRecyclerViewAdapter;
    private String fullNameReceiveOrder = "";
    private String numberPhoneReceiveOrder = "";
    private String mailReceiveOrder = "";
    private String addressReceiveOrder = "";
    private String noteReceiveOrder = "";
    private ApiCheckout apiCheckout;
    private RadioGroup.LayoutParams layoutParams;
    private List<District> lstDistricts;
    private List<ShoppingCartEntity> listShoppingCartEntity;
    private float totalMoneyPayment = 0;
    private int cityID = 0;
    private String cityName = "";
    private int districtID = 0;
    private String districtName = "";
    private String nameCompany = "";
    private String taxCodeCompany = "";
    private String addressCompany = "";
    private int shipType = 1;
    private int shipOrder = 2;
    private float shipAmount = 0;
    private int companyDistrictID = 0;
    private int companyCityID = 0;
    private int totalWeight = 0;
    private String couponCode = "";
    private int isVAT = 0;
    private String storeID = "";
    private String storeIDAtHouse = "";
    private String storeIDAtLingo = "";
    private int pmID = 0;
    private float discountValue = 0;
    private boolean isClickApply = false;
    private boolean isCheckCoupon = false;
    private float couponAmount = 0;
    private int shipOrderAtLingo = 0;
    private int shipOrderAtHouse = 0;
    private float amountShipping = 0;
    private float isShipping = 0;
    private float totalAmount = 0;
    private String orderNumberEndpoint = "";
    private ProductInformationEntity productInformationEntityGlobal;

    public CustomerPaymentOrdersPresenter(final CustomerPaymentOrdersView customerPaymentOrdersView) {
        this.customerPaymentOrdersView = customerPaymentOrdersView;
        Singleton.getSingleton().setReceiveGoods(0);

        listShoppingCartEntity = (ArrayList<ShoppingCartEntity>) customerPaymentOrdersView.getAppCompatActivity()
                .getIntent().getSerializableExtra(Constant.LIST_SHOPPING_CART_ENTITY);

        customerPaymentRecyclerViewAdapter = new CustomerPaymentRecyclerViewAdapter
                (customerPaymentOrdersView.getAppCompatActivity()
                        , listShoppingCartEntity, CustomerPaymentOrdersPresenter.this
                        , CustomerPaymentOrdersPresenter.this, customerPaymentOrdersView.getDisplayImageOptions(), CustomerPaymentOrdersPresenter.this);

        customerPaymentOrdersView.getRecyclerViewProductPurchase().setAdapter(customerPaymentRecyclerViewAdapter);
        customerPaymentRecyclerViewAdapter.notifyDataSetChanged();

//        Total money payment
        for (ShoppingCartEntity shoppingCartEntity : listShoppingCartEntity) {
            int totalPriceProduct = shoppingCartEntity.getProductPrice() * shoppingCartEntity.getOrderQuantity();
            totalAmount = totalMoneyPayment = totalMoneyPayment + totalPriceProduct;

        }

        BigDecimal bigDecimalAllMoneyProduct = new BigDecimal(totalMoneyPayment);

        customerPaymentOrdersView.getTextViewAllMoneyProduct().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                R.string.prices), NumberFormat.priceToString(bigDecimalAllMoneyProduct.floatValue())));

//        event button Apply Coupon code
        customerPaymentOrdersView.getButtonApplyCodeSale().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerPaymentOrdersView.getEditTextCodeSale().setText("");
            }
        });

        customerPaymentOrdersView.getRadioButtonPaymentReceiveGoods().setChecked(true);
        Singleton.getSingleton().setPaymentMethod(customerPaymentOrdersView.getRadioButtonPaymentReceiveGoods().getText().toString());
        pmID = 62;

        customerPaymentOrdersView.getRadioGroupPayments().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.customer_payment_method_radio_internet_banking:
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.GONE);
                        Singleton.getSingleton().setPaymentMethod(customerPaymentOrdersView.getRadioButtonATMInternetBanking().getText().toString());
                        pmID = 41;
                        break;
                    case R.id.customer_payment_method_radio_transfer_get_bill:
                        pmID = 1;
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.VISIBLE);
                        Singleton.getSingleton().setPaymentMethod(customerPaymentOrdersView.getRadioButtonTransferGetBill().getText().toString());
                        break;
                    case R.id.customer_payment_method_radio_account_lingo:
                        pmID = 3;
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.GONE);
                        Singleton.getSingleton().setPaymentMethod(customerPaymentOrdersView.getRadioButtonAccountLingo().getText().toString());
                        break;
                    case R.id.customer_payment_method_radio_visa_mater:
                        pmID = 121;
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.GONE);
                        Singleton.getSingleton().setPaymentMethod(customerPaymentOrdersView.getRadioButtonCard().getText().toString());
                        break;
                    case R.id.customer_payment_method_radio_payment_receipt_goods:
                        pmID = 62;
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.GONE);
                        Singleton.getSingleton().setPaymentMethod(customerPaymentOrdersView.getRadioButtonPaymentReceiveGoods().getText().toString());
                        break;
                }
            }
        });

        customerPaymentOrdersView.getRadioGroupSippingAdress().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.customer_authentication_radio_button_shipping_address_option:
                        customerPaymentOrdersView.getRadioGroupStoreAddress().setVisibility(View.GONE);
                        customerPaymentOrdersView.getLinearLayoutReceiveGoodsLocationDesired().setVisibility(View.VISIBLE);
                        shipOrder = shipOrderAtHouse;
                        shippingFree(shipOrder, cityID, districtID, shipType, totalMoneyPayment, listShoppingCartEntity);
                        Singleton.getSingleton().setReceiveGoods(0);

                        break;
                    case R.id.customer_authentication_radio_button_shipping_lingo_address:
                        customerPaymentOrdersView.getRadioGroupStoreAddress().setVisibility(View.VISIBLE);
                        customerPaymentOrdersView.getLinearLayoutReceiveGoodsLocationDesired().setVisibility(View.GONE);
                        shipOrder = shipOrderAtLingo;
                        Singleton.getSingleton().setReceiveGoods(1);
                        shippingFree(shipOrder, cityID, districtID, shipType, totalMoneyPayment, listShoppingCartEntity);

                        Realm realmStoreAddress = Realm.getDefaultInstance();
                        StoreAddress resultsStoreAddress = realmStoreAddress.where(StoreAddress.class).equalTo("index", String.valueOf(0)).findFirst();
                        if (realmStoreAddress != null) {
                            storeID = resultsStoreAddress.getStoreID();
                            addressReceiveOrder = resultsStoreAddress.getStoreName() + " - " + resultsStoreAddress.getStoreAddress();

                        }

                        customerPaymentOrdersView.getRadioGroupStoreAddress().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {

                                Realm realmStoreAddress = Realm.getDefaultInstance();
                                StoreAddress resultsStoreAddress = realmStoreAddress.where(StoreAddress.class).equalTo("index", String.valueOf(checkedId)).findFirst();
                                if (realmStoreAddress != null) {
                                    storeID = resultsStoreAddress.getStoreID();
                                    addressReceiveOrder = resultsStoreAddress.getStoreName() + " - " + resultsStoreAddress.getStoreAddress();

                                }
                            }
                        });
                        break;
                }
            }
        });

        customerPaymentOrdersView.getCheckBoxGrabBill().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    customerPaymentOrdersView.getLinearLayoutViewReceiveBill().setVisibility(View.VISIBLE);
                    isVAT = 1;

                } else {
                    customerPaymentOrdersView.getLinearLayoutViewReceiveBill().setVisibility(View.GONE);
                    companyDistrictID = 0;
                    companyCityID = 0;
                    isVAT = 0;
                }
            }
        });

        fetchDataSpinner();
        getReceiveType();
        getStoreAddress();
        getBankVAT();
        customerPaymentOrdersView.getButtonApplyCodeSale().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String couponClien = customerPaymentOrdersView.getEditTextCodeSale().getText().toString().trim();
                if (!couponClien.equals("")) {
                    if (!isClickApply) {
                        checkEcouponCode();
                        customerPaymentOrdersView.getButtonApplyCodeSale().setText("Xóa");
                        isClickApply = true;
                    } else {
                        customerPaymentOrdersView.getButtonApplyCodeSale().setText("Áp dụng");
                        customerPaymentOrdersView.getEditTextCodeSale().setText("");
                        customerPaymentOrdersView.getEditTextCodeSale().setEnabled(true);
                        customerPaymentOrdersView.getTextViewPriceDiscounted().setText("");
                        couponCode = "";
                        if (isCheckCoupon) {
                            totalMoneyPayment = totalMoneyPayment + couponAmount;

                            BigDecimal bigDecimal = new BigDecimal(totalMoneyPayment);
                            customerPaymentOrdersView.getTextViewMoneyPay().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                                    R.string.prices), NumberFormat.priceToString(bigDecimal.floatValue())));
                        }
                        isClickApply = false;
                    }

                } else {
                    showDialog("Nhập giảm giá !");
                }
            }
        });

        customerPaymentOrdersView.getButtonPaymentComplete().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCreateOrder();
            }
        });
    }

    public void getCreateOrder() {

        if (validateText()) {
            String phone = customerPaymentOrdersView.getEditTextNumberPhoneConsignee().getText().toString().trim();
            if (validatePhone(phone)) {
                final Order order = getOrder();

                MobileInfo mobileInfo = new MobileInfo();
                TelephonyManager tm = (TelephonyManager) customerPaymentOrdersView.getContext().getSystemService(Context.TELEPHONY_SERVICE);
                String deviceToken = Settings.Secure.getString(customerPaymentOrdersView.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                String model = android.os.Build.MODEL;
                String userAgent = new WebView(customerPaymentOrdersView.getContext()).getSettings().getUserAgentString();


                String imei = "";
                try {
//                    error not get imei
                    imei = tm.getDeviceId();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mobileInfo.setImei(imei);
                mobileInfo.setLat("");
                mobileInfo.setLon("");
                mobileInfo.setBrandName(getDeviceBrandName());
                mobileInfo.setDeviceToken(deviceToken);
                mobileInfo.setOsID(model);
                mobileInfo.setUserAgent(userAgent);

                InstallmentOrder installmentOrder = new InstallmentOrder("", "", 1);

                List<OrderDetail> listOrderDetail = new ArrayList<>();
                for (ShoppingCartEntity shoppingCartEntity : listShoppingCartEntity) {
                    OrderDetail orderDetail = new OrderDetail();
                    orderDetail.setProductID(shoppingCartEntity.getCartID());
                    orderDetail.setQuantity(shoppingCartEntity.getOrderQuantity());
                    orderDetail.setSaverBuyDisCount(0);
                    orderDetail.setUnitPrice(shoppingCartEntity.getProductPrice());
                    listOrderDetail.add(orderDetail);
                }

                // 62     : COD – Thanh toán khi nhận hàng
//                        1      : Chuyện khoảng lấy hóa đơn VAT ngân hàng
//
//                        41     : ATM Internet Banking
//                        121    : Visa master card
//                        3      : Tài khoản Lingo
//
//                        201    : SacombankGateway
//                        141    : Installment – Thanh toán trả góp

                if (pmID == 62 || pmID == 1) {
                    customerPaymentOrdersView.showProcessing();
                    ApiCheckout apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
                    Call<JsonElement> call = apiCheckout.getCreateOrder(CreateOrder.getJsonCreateOrder(order, installmentOrder, mobileInfo, listOrderDetail));
                    call.enqueue(new Callback<JsonElement>() {
                        @Override
                        public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                            customerPaymentOrdersView.hideProcessing();
                            if (response.code() == 200) {
                                JsonElement jsonElement = response.body();
                                JsonObject jsonObject = jsonElement.getAsJsonObject();
                                String codeOrder = jsonObject.get("OrderCode").getAsString();

                                orderNumberEndpoint = "http://lingo.com.vn/thanh-cong/488371";
                                byte[] byteBase64 = orderNumberEndpoint.getBytes();
                                String value = Base64.encodeToString(byteBase64, Base64.URL_SAFE | android.util.Base64.NO_WRAP);
                                if (!StringUtils.isBlank(value) &&
                                        !StringUtils.isBlank(codeOrder)) {
                                    OrderHistoryLocalStorage orderHistoryLocalStorage = new OrderHistoryLocalStorage();
                                    orderHistoryLocalStorage.setOrderNumberEndpoint(value);
                                    orderHistoryLocalStorage.setOrderNumber(codeOrder);
                                    orderHistoryLocalStorage.setOrderNumberId(codeOrder);
                                    orderHistoryLocalStorage.setTimeStamp(System.currentTimeMillis());
                                    Realm realm = Realm.getDefaultInstance();
                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(orderHistoryLocalStorage);
                                    realm.commitTransaction();
                                }

                                Singleton.getSingleton().setCodeOrder(codeOrder);
                                Singleton.getSingleton().setIsStarIntent(1);
                                Intent intent = new Intent();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("OrderSerializable", order);
                                intent.setClass(customerPaymentOrdersView.getContext(), PaymentOrderSuccessActivity.class);
                                intent.putExtras(bundle);
                                customerPaymentOrdersView.getContext().startActivity(intent);
                                customerPaymentOrdersView.getAppCompatActivity().finish();

                            } else if (response.code() == 400) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_400));
                            } else if (response.code() == 411) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_411));
                            } else if (response.code() == 412) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_412));
                            } else if (response.code() == 413) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_413));
                            } else if (response.code() == 414) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_414));
                            } else if (response.code() == 415) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_415));
                            } else if (response.code() == 416) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_416));
                            } else if (response.code() == 417) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_417));
                            } else if (response.code() == 418) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_418));
                            } else if (response.code() == 419) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_419));
                            } else if (response.code() == 420) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_420));
                            } else if (response.code() == 421) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_421));
                            } else if (response.code() == 422) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_422));
                            } else if (response.code() == 423) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_423));
                            } else if (response.code() == 424) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_424));
                            } else if (response.code() == 425) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_425));
                            } else if (response.code() == 426) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_426));
                            } else if (response.code() == 510) {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_510));
                            } else {
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_511));
                            }

                        }

                        @Override
                        public void onFailure(Throwable t) {
                            customerPaymentOrdersView.hideProcessing();
                            showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
                        }
                    });
                } else {
                    customerPaymentOrdersView.showProcessing();
                    try {
//                    RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class).equalTo("cartID",
//                            Integer.parseInt(productInformationEntityGlobal.getProductId()));
//                    RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
//
//                    if (realmResultsShoppingCartLocalStorage.size() <= 0) {
//                        // updateShoppingCart(valueWeight,1, productInformationEntityGlobal);
//                    }
//                    AnalyticsHelper.logEvent(customerPaymentOrdersView.getContext(), "PURCHASE",
//                            productInformationEntityGlobal.getProductName() + ":" + productInformationEntityGlobal.getProductId());
                        RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorageBuyNow = realm.where(ShoppingCartLocalStorage.class).equalTo("isOrdered", 1);
                        RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorageBuyNow = realmQueryShoppingCartLocalStorageBuyNow.findAll();
                        ProductCheckoutAPI productCheckoutAPI = customerPaymentOrdersView.getRetrofit().create(ProductCheckoutAPI.class);
                        Call<ProductCheckoutResponse> callProductCheckoutResponse = productCheckoutAPI.productCheckoutWeb(ProductCheckoutRequest
                                .getProductCheckoutRequest(realmResultsShoppingCartLocalStorageBuyNow, order));
                        callProductCheckoutResponse.enqueue(new Callback<ProductCheckoutResponse>() {
                            @Override
                            public void onResponse(Response<ProductCheckoutResponse> response, Retrofit retrofit) {
                                ProductCheckoutResponse productCheckoutResponse = response.body();
                                if (response.code() == 200) {
                                    if (!StringUtils.isBlank(productCheckoutResponse.getRedirectWapURL())) {
                                      // Intent i = new Intent(Intent.ACTION_VIEW);
//                                        i.setData(Uri.parse(productCheckoutResponse.getRedirectWapURL()));
//                                         i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        customerPaymentOrdersView.getAppCompatActivity().startActivityForResult(i,1);
                                       new FinestWebView.Builder(customerPaymentOrdersView.getAppCompatActivity()).show(productCheckoutResponse.getRedirectWapURL());
                                    }
                                } else {
                                    customerPaymentOrdersView.getMaterialDialogAlert(customerPaymentOrdersView.getAppCompatActivity(),
                                            customerPaymentOrdersView.getContext().getResources().getString(R.string.app_name),
                                            customerPaymentOrdersView.getContext().getResources().getString(R.string.product_invalidate),
                                            customerPaymentOrdersView.getContext().getResources().getString(R.string.material_dialog_positive_text));
                                }
                                customerPaymentOrdersView.hideProcessing();
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                customerPaymentOrdersView.hideProcessing();
                                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
                            }
                        });
                    } catch (Exception exception) {
                        exception.fillInStackTrace();
                        Log.e(TAG, exception.toString());
                        customerPaymentOrdersView.hideProcessing();
                    }
                }

            } else {
                showDialog("Số điện thoại không chính xác !");
                customerPaymentOrdersView.getEditTextNumberPhoneConsignee().findFocus();
            }
        } else {
            showDialog("Hãy nhập đầy đủ các thông tin !");
        }
    }

    @NonNull
    public Order getOrder() {
        final Order order = new Order();
        order.setSex(1);
        order.setOrderReceiveMail(mailReceiveOrder);
        order.setShipType(shipType);
        order.setOrderReceiveAddress(addressReceiveOrder);
        order.setOrderReceiveName(fullNameReceiveOrder);
        order.setOrderReceiveMobile(numberPhoneReceiveOrder);
        order.setOrderReceiveNote(noteReceiveOrder);
        order.setPmID(pmID);
        order.setStoreGuid(storeID);
        order.setReceiveOrderType(shipOrder);
        order.setReceiveCityID(cityID);
        order.setReceiveDistrictID(districtID);
        order.setReceiveCityName(cityName);
        order.setReceiveDistrictName(districtName);
        order.setShipMount((int) shipAmount);
        order.setCompanyDistrictID(companyDistrictID);
        order.setTotalWeight(totalWeight);
        order.setCompanyCityID(companyCityID);
        order.setIsVat(isVAT);
        order.setEcopounCode(couponCode);
        order.setHadUserLogin(true);
        order.setUserLoginMail("");
        order.setUserLoginGender(0);
        order.setUserLoginID(0);
        order.setUserLoginFullName("");
        order.setUserLoginMobile("");
        order.setReceiveFullAddress("");
        order.setOrderOwnerCompany(nameCompany);
        order.setOrderOwnerCompanyAddress(addressCompany);
        order.setOrderOwnerCompanyTaxCode(taxCodeCompany);
        return order;
    }

    public boolean validatePhone(String phone) {
        String subPhone = phone.substring(0, 2);
        if (subPhone.equals("09") || subPhone.equals("04") || subPhone.equals("08")) {
            if (phone.length() == 10) {
                return true;
            }
        } else if (subPhone.equals("01")) {
            if (phone.length() == 11) {
                return true;
            }
        }
        return false;
    }

    public String getDeviceBrandName() {
        if (android.os.Build.MODEL != null) {
            return java.net.URLEncoder.encode(android.os.Build.MODEL);
        }

        return "";
    }

    private boolean validateText() {
        boolean val = true;
        fullNameReceiveOrder = customerPaymentOrdersView.getEditTextFullNameConsignee().getText().toString().trim();
        numberPhoneReceiveOrder = customerPaymentOrdersView.getEditTextNumberPhoneConsignee().getText().toString().trim();
        mailReceiveOrder = customerPaymentOrdersView.getEditTextEmailConsignee().getText().toString().trim();


        if (isVAT == 1) {
            addressCompany = customerPaymentOrdersView.getEditTextAddressCompany().getText().toString().trim();
            nameCompany = customerPaymentOrdersView.getEditTextNameCompany().getText().toString().trim();
            taxCodeCompany = customerPaymentOrdersView.getEditTextTaxCompany().getText().toString().trim();

            if (addressCompany.isEmpty()) {
                customerPaymentOrdersView.getEditTextAddressCompany().setError(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_address_company_empty));
                val = false;
            }

            if (nameCompany.isEmpty()) {
                customerPaymentOrdersView.getEditTextNameCompany().setError(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_name_company_empty));
                val = false;
            }

            if (taxCodeCompany.isEmpty()) {
                customerPaymentOrdersView.getEditTextTaxCompany().setError(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_tax_company_empty));
                val = false;
            }

        }
        if (shipOrder == 2) {
            addressReceiveOrder = customerPaymentOrdersView.getEditTextAddressReceiptGoods().getText().toString().trim();
            if (addressReceiveOrder.isEmpty()) {
                customerPaymentOrdersView.getEditTextAddressReceiptGoods().setError(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_address_empty));
                val = false;
            }
        }

        if (fullNameReceiveOrder.isEmpty()) {
            customerPaymentOrdersView.getEditTextFullNameConsignee().setError(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_fullName_empty));
            val = false;
        }
        if (numberPhoneReceiveOrder.isEmpty()) {
            customerPaymentOrdersView.getEditTextNumberPhoneConsignee().setError(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_numberPhone_empty));
            val = false;
        }

        return val;
    }

    public void shippingFree(int orderType, int cityID, int districtID, final int shipType, final float totalMoneyPaymentShipping, List<ShoppingCartEntity> listShoppingCartEntity) {
        OrderShippingInfo orderShippingInfo = new OrderShippingInfo(0, shipType, orderType, cityID, districtID);
        List<ProductShippingInfo> listProductShippingInfos = new ArrayList<>();
        for (ShoppingCartEntity shoppingCartEntity : listShoppingCartEntity) {
            ProductShippingInfo productShippingInfo = new ProductShippingInfo();
            productShippingInfo.setBigType(shoppingCartEntity.getBigType());
            productShippingInfo.setComboID(0);
            productShippingInfo.setComboQuantity(0);
            productShippingInfo.setOnlineOrderID(0);
            productShippingInfo.setProductID(shoppingCartEntity.getCartID());
            productShippingInfo.setProductWeight(shoppingCartEntity.getValueWeight());
            totalWeight = totalWeight + shoppingCartEntity.getValueWeight();
            productShippingInfo.setQuantity(shoppingCartEntity.getOrderQuantity());
            productShippingInfo.setShippingExtraTimes(0);
            productShippingInfo.setUnitPrice(shoppingCartEntity.getProductPrice());
            productShippingInfo.setComboIsFreeShip(0);
            productShippingInfo.setFreeShip(0);
            listProductShippingInfos.add(productShippingInfo);
        }
        customerPaymentOrdersView.showProcessing();
        ApiCheckout apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
        Call<JsonElement> call = apiCheckout.getShipFree(ShippingFreePrice.getShippingFreePrice(orderShippingInfo, listProductShippingInfos, (int) totalMoneyPayment, shipType));
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                customerPaymentOrdersView.hideProcessing();
                if (response.code() == Constant.REQUEST_SEREVE) {
                    JsonElement jsonElement = response.body();
                    JsonObject jsonObject = jsonElement.getAsJsonObject();
                    float shipAmount = jsonObject.get("ShipingValue").getAsFloat();
                    BigDecimal bigDecimalShipPrice = new BigDecimal(shipAmount);
                    totalMoneyPayment = totalMoneyPaymentShipping - isShipping + shipAmount;

                    BigDecimal bigDecimal = new BigDecimal(totalMoneyPayment);
                    if (shipAmount == 0.0) {
                        customerPaymentOrdersView.getTextViewMoneyPay().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                                R.string.prices), NumberFormat.priceToString(bigDecimal.floatValue())));
                        customerPaymentOrdersView.getTextViewTransportFee().setText("Miễn phí");
                        customerPaymentOrdersView.getTextViewTransportFee().setTextColor(customerPaymentOrdersView.getContext().getResources().getColor(R.color.green));
                        isShipping = 0;
                    } else {
                        customerPaymentOrdersView.getTextViewTransportFee().setTextColor(customerPaymentOrdersView.getContext().getResources().getColor(R.color.black));
                        customerPaymentOrdersView.getTextViewTransportFee().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                                R.string.prices), NumberFormat.priceToString(bigDecimalShipPrice.floatValue())));
                        customerPaymentOrdersView.getTextViewMoneyPay().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                                R.string.prices), NumberFormat.priceToString(bigDecimal.floatValue())));
                        isShipping = shipAmount;
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                customerPaymentOrdersView.hideProcessing();
                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
            }
        });
    }

    public void fetchingShoppingCart() {
        customerPaymentOrdersView.getToolbar().setTitle("");
        customerPaymentOrdersView.getToolbar().setTitleTextColor(customerPaymentOrdersView.getContext().getResources().getColor(R.color.primary));
        customerPaymentOrdersView.getAppCompatActivity().setSupportActionBar(customerPaymentOrdersView.getToolbar());
        customerPaymentOrdersView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        customerPaymentOrdersView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        customerPaymentOrdersView.getToolbar().setNavigationIcon(R.drawable.close_delete_remove_icon);
        customerPaymentOrdersView.getToolbar().invalidate();// restore toolbar
    }

    public void invalidateToolbar() {
        customerPaymentOrdersView.getToolbar().setTitle("");
        customerPaymentOrdersView.getToolbar().setTitleTextColor(customerPaymentOrdersView.getContext().getResources().getColor(R.color.primary));
        customerPaymentOrdersView.getToolbar().invalidate();// restore toolbar
    }

    public void getShippingType(final int cityId) {
        final List<ShipType> listShipType = new ArrayList<>();
        customerPaymentOrdersView.showProcessing();
        apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
        final Call<JsonElement> call = apiCheckout.getShipType(cityId);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                customerPaymentOrdersView.hideProcessing();
                if (response.code() == Constant.REQUEST_SEREVE) {
                    JsonElement jsonElement = response.body();
                    JsonArray jsonArray = jsonElement.getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                        int id = jsonObject.get("ID").getAsInt();
                        String name = jsonObject.get("NAME").getAsString();
                        ShipType shipType = new ShipType(id, name, String.valueOf(cityId));
                        listShipType.add(shipType);
                    }

                    ShipTypeAdapter shipTypeAdapter = new ShipTypeAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, listShipType);
                    customerPaymentOrdersView.getSpinnerMethodShippingGoods().setAdapter(shipTypeAdapter);
                    customerPaymentOrdersView.getSpinnerMethodShippingGoods().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ShipType ship = listShipType.get(position);
                            shipType = ship.getId();
                            shippingFree(shipOrder, cityId, districtID, shipType, totalMoneyPayment, listShoppingCartEntity);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Throwable t) {
                customerPaymentOrdersView.hideProcessing();
                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
            }
        });
    }

    public void getLocation() {
        final List<City> lstCities = new ArrayList<>();
        lstDistricts = new ArrayList<>();
        apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
        Call<JsonElement> call = apiCheckout.getLocation();
        customerPaymentOrdersView.showProcessing();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                customerPaymentOrdersView.hideProcessing();
                if (response.code() == Constant.REQUEST_SEREVE) {
                    JsonElement jsonElement = response.body();
                    JsonArray jsonArray = jsonElement.getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                        int id = jsonObject.get("LOCATION_ID").getAsInt();
                        String name = jsonObject.get("LOCATION_NAME").getAsString();
                        City city = new City(id, name);
                        writeRealmCity(city);
                        lstCities.add(city);
                        JsonArray arrayDistrict = jsonObject.get("LIST_DISTRICT").getAsJsonArray();
                        for (int j = 0; j < arrayDistrict.size(); j++) {
                            JsonObject objectDistrict = arrayDistrict.get(j).getAsJsonObject();
                            int dId = objectDistrict.get("LOCATION_ID").getAsInt();
                            String dName = objectDistrict.get("LOCATION_NAME").getAsString();
                            District district = new District(dId, dName, String.valueOf(id));
                            writeRealmDistrict(district);
                            lstDistricts.add(district);
                        }
                    }
                    LocationCityAdapter locationCityAdapter = new LocationCityAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, lstCities);
                    customerPaymentOrdersView.getSpinnerCityReceiveGoods().setAdapter(locationCityAdapter);


//                    load district and  ShippingType at city position = 0
                    customerPaymentOrdersView.getSpinnerCityReceiveGoods().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            City city = lstCities.get(position);
                            cityID = city.getId();
                            findDistrictByCityId(city.getId());
                            cityName = city.getName();
                            Singleton.getSingleton().setCityName(cityName);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

//                    setAdapter city company
                    customerPaymentOrdersView.getSpinnerCityCompany().setAdapter(locationCityAdapter);
                    locationCityAdapter.notifyDataSetChanged();
                    customerPaymentOrdersView.getSpinnerCityCompany().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            City cityCompany = lstCities.get(position);
                            findDistrictCompanyByCityId(cityCompany.getId());
                            companyCityID = cityCompany.getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Throwable t) {
                customerPaymentOrdersView.hideProcessing();
                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
            }
        });
    }

    public void fetchDataSpinner() {
        if (getAllCity().isEmpty()) {
            getLocation();
        } else {
            final LocationCityAdapter locationCityAdapter = new LocationCityAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, getAllCity());
            customerPaymentOrdersView.getSpinnerCityReceiveGoods().setAdapter(locationCityAdapter);
//            load district and shippingType at position city = 0
            customerPaymentOrdersView.getSpinnerCityReceiveGoods().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    City city = getAllCity().get(position);
                    findDistrictByCityId(city.getId());
                    cityName = city.getName();
                    Singleton.getSingleton().setCityName(cityName);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

//            load city Company
            customerPaymentOrdersView.getSpinnerCityCompany().setAdapter(locationCityAdapter);
            customerPaymentOrdersView.getSpinnerCityCompany().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    City cityCompany = getAllCity().get(position);
                    findDistrictCompanyByCityId(cityCompany.getId());
                    companyCityID = cityCompany.getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }

    public void getBankVAT() {
        if (customerPaymentOrdersView.getRetrofit() != null) {
            ApiCheckout apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
            Call<List<BankVAT>> call = apiCheckout.getbankVAT();
            call.enqueue(new Callback<List<BankVAT>>() {
                @Override
                public void onResponse(Response<List<BankVAT>> response, Retrofit retrofit) {
                    if (response.code() == Constant.REQUEST_SEREVE && response.body() != null) {
                        final List<BankVAT> listBankVAT = response.body();
                        List<String> listNameBank = new ArrayList<String>();
                        for (BankVAT bankVAT : listBankVAT) {
                            listNameBank.add(bankVAT.getBankName().split("-")[0]);
                        }
                        BankVATAdapter bankVATAdapter = new BankVATAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, listNameBank);
                        customerPaymentOrdersView.getSpinnerNameBanking().setAdapter(bankVATAdapter);
                        bankVATAdapter.notifyDataSetChanged();

                        customerPaymentOrdersView.getSpinnerNameBanking().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                BankVAT bankVAT = listBankVAT.get(position);
                                customerPaymentOrdersView.getTextViewNameBanking().setText(bankVAT.getBankName().toString().trim());
                                customerPaymentOrdersView.getTextViewNumberAccountBanking().setText(bankVAT.getBankNunber().toString().trim());
                                customerPaymentOrdersView.getTextViewAccountOwner().setText(bankVAT.getAccountName().toString().trim());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
                }
            });
        }
    }

    public float checkEcouponCode() {
        couponCode = customerPaymentOrdersView.getEditTextCodeSale().getText().toString().trim();
        List<OrderDetail> listOrderDetail = new ArrayList<>();
        for (ShoppingCartEntity shoppingCartEntity : listShoppingCartEntity) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setProductID(shoppingCartEntity.getCartID());
            orderDetail.setQuantity(shoppingCartEntity.getOrderQuantity());
            orderDetail.setSaverBuyDisCount(0);
            orderDetail.setUnitPrice(shoppingCartEntity.getProductPrice());
            listOrderDetail.add(orderDetail);
        }
        customerPaymentOrdersView.showProcessing();
        ApiCheckout apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
        Call<JsonElement> call = apiCheckout.getCheckEcouponCode(CheckEcoupon.getCheckEcoupon(couponCode, listOrderDetail));
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                customerPaymentOrdersView.hideProcessing();
                if (response.code() == 200) {
                    isCheckCoupon = true;
                    customerPaymentOrdersView.getEditTextCodeSale().setEnabled(false);
                    couponCode = customerPaymentOrdersView.getEditTextCodeSale().getText().toString().trim();
                    JsonElement jsonElement = response.body();
                    JsonObject jsonObject = jsonElement.getAsJsonObject();
                    couponAmount = jsonObject.get("DiscountValue").getAsFloat();
                    totalMoneyPayment = totalMoneyPayment - couponAmount;

                    BigDecimal bigDecimalPayment = new BigDecimal(totalMoneyPayment);
                    customerPaymentOrdersView.getTextViewMoneyPay().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                            R.string.prices), NumberFormat.priceToString(bigDecimalPayment.floatValue())));
                    BigDecimal bigDecimal = new BigDecimal(couponAmount);
                    customerPaymentOrdersView.getTextViewPriceDiscounted().setText("-" + String.format(customerPaymentOrdersView.getContext().getResources().getString(
                            R.string.prices), NumberFormat.priceToString(bigDecimal.floatValue())));
                } else if (response.code() == 500) {
                    isCheckCoupon = false;
                    customerPaymentOrdersView.getEditTextCodeSale().setEnabled(false);
                    couponCode = "";
                    customerPaymentOrdersView.getTextViewPriceDiscounted().setText("");
                    showDialog("Có lỗi xảy ra !");
                } else if (response.code() == 510) {
                    couponCode = "";
                    isCheckCoupon = false;
                    customerPaymentOrdersView.getEditTextCodeSale().setEnabled(false);
                    customerPaymentOrdersView.getTextViewPriceDiscounted().setText("");
                    showDialog("Mã giảm giá không hợp lệ !");
                } else if (response.code() == 513) {
                    isCheckCoupon = false;
                    customerPaymentOrdersView.getEditTextCodeSale().setEnabled(false);
                    couponCode = "";
                    customerPaymentOrdersView.getTextViewPriceDiscounted().setText("");
                    showDialog("Mã giảm giá không áp dụng cho Mobile !");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                isCheckCoupon = false;
                customerPaymentOrdersView.getEditTextCodeSale().setEnabled(false);
                couponCode = "";
                customerPaymentOrdersView.hideProcessing();
                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
            }
        });
        return couponAmount;
    }

    public Date converStringToDateTime(String dateString) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void writeRealmCity(City city) {
        Realm realmCity = Realm.getDefaultInstance();
        realmCity.beginTransaction();
        realmCity.copyToRealmOrUpdate(city);
        realmCity.commitTransaction();
    }

    public void deleteRealmCity() {
        Realm realmCity = Realm.getDefaultInstance();
        realmCity.beginTransaction();
        RealmResults<City> results = realmCity.where(City.class).findAll();
        results.removeAll(results);
        realmCity.commitTransaction();
    }

    public void writeRealmDistrict(District district) {
        Realm realmDistrict = Realm.getDefaultInstance();
        realmDistrict.beginTransaction();
        realmDistrict.copyToRealmOrUpdate(district);
        realmDistrict.commitTransaction();
    }

    public void deleteRealmDistrict() {
        Realm realmDistrict = Realm.getDefaultInstance();
        realmDistrict.beginTransaction();
        RealmResults<District> results = realmDistrict.where(District.class).findAll();
        results.removeAll(results);
        realmDistrict.commitTransaction();
    }

    public List<City> getAllCity() {
        List<City> lstCities = new ArrayList<>();
        Realm realmAllCity = Realm.getDefaultInstance();
        RealmResults<City> results = realmAllCity.where(City.class).findAll();

        for (City city : results) {
            lstCities.add(city);
        }
        return lstCities;
    }

    public void findDistrictByCityId(final int cityId) {
        this.cityID = cityId;
        final List<District> lstDistricts = new ArrayList<>();
        Realm realmDistrict = Realm.getDefaultInstance();
        RealmResults<District> results = realmDistrict.where(District.class).equalTo("cityId", String.valueOf(cityId)).findAll();
        for (District district : results) {
            lstDistricts.add(district);
        }
//        load spinner District and shppingFree when event click spinner City
        LocationDistrictAdapter locationDistrictAdapter = new LocationDistrictAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, lstDistricts);
        customerPaymentOrdersView.getSpinnerDistrictReceiptGoods().setAdapter(locationDistrictAdapter);
        customerPaymentOrdersView.getSpinnerDistrictReceiptGoods().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                District district = lstDistricts.get(position);
                districtID = district.getId();
                districtName = district.getName();
                Singleton.getSingleton().setDistrictName(districtName);
                getShippingType(cityId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void findDistrictCompanyByCityId(final int cityId) {
        this.cityID = cityId;
        final List<District> lstDistricts = new ArrayList<>();
        Realm realmDistrict = Realm.getDefaultInstance();
        RealmResults<District> results = realmDistrict.where(District.class).equalTo("cityId", String.valueOf(cityId)).findAll();
        for (District district : results) {
            lstDistricts.add(district);
        }

        LocationDistrictAdapter locationDistrictAdapter = new LocationDistrictAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, lstDistricts);
        customerPaymentOrdersView.getSpinnerDistrictCompany().setAdapter(locationDistrictAdapter);
        customerPaymentOrdersView.getSpinnerDistrictCompany().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                District district = lstDistricts.get(position);
                companyDistrictID = district.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getReceiveType() {
        apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
        Call<List<ReceiveType>> call = apiCheckout.getReceiveType();
        call.enqueue(new Callback<List<ReceiveType>>() {
            @Override
            public void onResponse(Response<List<ReceiveType>> response, Retrofit retrofit) {
                if (response.code() == Constant.REQUEST_SEREVE && response.body() != null) {
                    List<ReceiveType> listReceiveType = response.body();
                    for (ReceiveType receiveType : listReceiveType) {
                        if (receiveType.getId() == 1) {
                            customerPaymentOrdersView.getRadioButtonReceiveAtLingo().setText(receiveType.getName());
                            storeIDAtLingo = receiveType.getStoreID();
                            shipOrderAtLingo = receiveType.getId();
                        } else if (receiveType.getId() == 2) {
                            customerPaymentOrdersView.getRadioButtonReceiveAtHouse().setText(receiveType.getName());
                            shipOrderAtHouse = receiveType.getId();
                            storeIDAtHouse = receiveType.getStoreID();
                        }
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Throwable t) {
                showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
            }
        });
    }

    private void getStoreAddress() {
        if (customerPaymentOrdersView.getRetrofit() != null) {
            apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
            Call<JsonElement> call = apiCheckout.getStoreAddress();
            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                    if (response.code() == Constant.REQUEST_SEREVE && response.body() != null) {
                        JsonElement jsonElement = response.body();
                        JsonArray jsonArray = jsonElement.getAsJsonArray();

                        for (int i = 0; i < jsonArray.size(); i++) {
                            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                            StoreAddress storeAddress = new StoreAddress(i + "", jsonObject.get("STORE_ID").getAsString()
                                    , jsonObject.get("STORE_NAME").getAsString(), jsonObject.get("STORE_ADDRESS").getAsString());
                            writeRealmStoreAddress(storeAddress);
                            RadioButton radioButton = new RadioButton(customerPaymentOrdersView.getContext());
                            if (i == 0) {
                                radioButton.setText(storeAddress.getStoreName() + "  (" + storeAddress.getStoreAddress() + ")");
                                radioButton.setId(i);
                                radioButton.setChecked(true);
                            }
                            radioButton.setText(storeAddress.getStoreName() + "  (" + storeAddress.getStoreAddress() + ")");
                            radioButton.setId(i);
                            layoutParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            customerPaymentOrdersView.getRadioGroupStoreAddress().addView(radioButton, layoutParams);

                        }

                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    showDialog(customerPaymentOrdersView.getContext().getResources().getString(R.string.error_connect_internet));
                }
            });
        }
    }

    @Override
    public void onclickItemRecyclerView(int position) {

    }

    public void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(customerPaymentOrdersView.getContext());
        builder.setTitle("Thông báo !");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {

    }

    @Override
    public void setOnListener(int actionID, int position, ShoppingCartEntity shoppingCartEntity) {
//        listShoppingCartEntity.remove(position);
//        customerPaymentRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void clickItemAdapter(int event, int position, int quantity, float price, ShoppingCartEntity shoppingCartEntity) {
        switch (event) {
            case Constant.EVENT_ADD_PRODUCT:
                int cardIdAdd = shoppingCartEntity.getCartID();
                for (int i = 0; i < listShoppingCartEntity.size(); i++) {
                    if (listShoppingCartEntity.get(i).getCartID() == cardIdAdd) {
                        listShoppingCartEntity.get(i).setOrderQuantity(quantity);
                    }
                }
                totalAmount = totalAmount + price;
                totalMoneyPayment = totalMoneyPayment + price;
                shippingFree(shipOrder, cityID, districtID, shipType, totalMoneyPayment, listShoppingCartEntity);
                BigDecimal bigDecimalAdd = new BigDecimal(totalAmount);
                customerPaymentOrdersView.getTextViewAllMoneyProduct().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(R.string.prices)
                        , NumberFormat.priceToString(bigDecimalAdd.floatValue())));
                Log.e("amountShipping", amountShipping + "");
                updateItemShoppingCart(shoppingCartEntity);
                break;
            case Constant.EVENT_SUB_PRODUCT:
                int cardIdSub = shoppingCartEntity.getCartID();
                for (int i = 0; i < listShoppingCartEntity.size(); i++) {
                    if (listShoppingCartEntity.get(i).getCartID() == cardIdSub) {
                        listShoppingCartEntity.get(i).setOrderQuantity(quantity);
                    }
                }
                totalAmount = totalAmount - price;
                totalMoneyPayment = totalMoneyPayment - price;
                shippingFree(shipOrder, cityID, districtID, shipType, totalMoneyPayment, listShoppingCartEntity);
                BigDecimal bigDecimalSub = new BigDecimal(totalAmount);
                customerPaymentOrdersView.getTextViewAllMoneyProduct().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(R.string.prices)
                        , NumberFormat.priceToString(bigDecimalSub.floatValue())));

                updateItemShoppingCart(shoppingCartEntity);
                break;
            case Constant.EVENT_REMOVE_PRODUCT:
                for (int i = 0; i < listShoppingCartEntity.size(); i++) {
                    if (i == position) {
                        if (listShoppingCartEntity.size() > 1) {
                            listShoppingCartEntity.remove(position);
                            customerPaymentRecyclerViewAdapter.notifyDataSetChanged();
                            totalAmount = totalAmount - (quantity * price);
                            totalMoneyPayment = totalMoneyPayment - (quantity * price);
                            shippingFree(shipOrder, cityID, districtID, shipType, totalMoneyPayment, listShoppingCartEntity);
                            BigDecimal bigDecimalRemove = new BigDecimal(totalAmount);
                            customerPaymentOrdersView.getTextViewAllMoneyProduct().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(R.string.prices)
                                    , NumberFormat.priceToString(bigDecimalRemove.floatValue())));

                            deleteItemShoppingCart(shoppingCartEntity);
                        }
                    }
                }
                break;
        }
    }

    private void deleteItemShoppingCart(ShoppingCartEntity shoppingCartEntity) {
        RealmQuery<ShoppingCartLocalStorage> realmQuery = realm.where(ShoppingCartLocalStorage.class)
                .equalTo("cartID", shoppingCartEntity.getCartID());
        RealmResults<ShoppingCartLocalStorage> realmResults = realmQuery.findAll();
        realm.beginTransaction();
        realmResults.clear();
        realm.commitTransaction();
        Intent intent = new Intent();
        intent.setAction(Constant.ACTION.ADD_TO_CART_ACTION);
        customerPaymentOrdersView.getAppCompatActivity().sendBroadcast(intent);

        Intent intentUpdateListCart = new Intent();
        intentUpdateListCart.setAction(Constant.ACTION.UPDATE_LIST_CART);
        customerPaymentOrdersView.getAppCompatActivity().sendBroadcast(intentUpdateListCart);
    }


    public void writeRealmStoreAddress(StoreAddress storeAddress) {
        Realm realmCity = Realm.getDefaultInstance();
        realmCity.beginTransaction();
        realmCity.copyToRealmOrUpdate(storeAddress);
        realmCity.commitTransaction();
    }

    private void updateItemShoppingCart(ShoppingCartEntity shoppingCartEntity) {
        ShoppingCartLocalStorage shoppingCartLocalStorage = new ShoppingCartLocalStorage();
        shoppingCartLocalStorage.setCartID(shoppingCartEntity.getCartID());
        shoppingCartLocalStorage.setOrderQuantity(shoppingCartEntity.getOrderQuantity());
        shoppingCartLocalStorage.setAvatar(shoppingCartEntity.getAvatar());
        shoppingCartLocalStorage.setProductName(shoppingCartEntity.getProductName());
        shoppingCartLocalStorage.setMarketPrice(shoppingCartEntity.getMarketPrice());
        shoppingCartLocalStorage.setProductPrice(shoppingCartEntity.getProductPrice());
        shoppingCartLocalStorage.setIsOrdered(shoppingCartEntity.getIsOrdered());
        shoppingCartLocalStorage.setMarketPriceFormat(shoppingCartEntity.getMarketPriceFormat());
        shoppingCartLocalStorage.setProductPriceFormat(shoppingCartEntity.getProductPriceFormat());
        shoppingCartLocalStorage.setSku(shoppingCartEntity.getSku());
        shoppingCartLocalStorage.setTimeStamp(shoppingCartEntity.getTimeStamp());
        shoppingCartLocalStorage.setBigType(shoppingCartEntity.getBigType());
        shoppingCartLocalStorage.setValue(shoppingCartEntity.getValueWeight());
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(shoppingCartLocalStorage);
            realm.commitTransaction();
        } catch (Exception exception) {
            exception.printStackTrace();
            Log.e(TAG, exception.toString());
        }
        Intent intent = new Intent();
        intent.setAction(Constant.ACTION.ADD_TO_CART_ACTION);
        customerPaymentOrdersView.getAppCompatActivity().sendBroadcast(intent);

        Intent intentUpdateListCart = new Intent();
        intentUpdateListCart.setAction(Constant.ACTION.UPDATE_LIST_CART);
        customerPaymentOrdersView.getAppCompatActivity().sendBroadcast(intentUpdateListCart);
    }

}

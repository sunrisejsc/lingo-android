package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionListBoxSettings;

/**
 * Created by longtran on 17/10/2015.
 */
public class EventDealFixDealsCollectionFilterAdapter extends ArrayAdapter<EventDealFixDealsCollectionListBoxSettings> {
    /**
     * This is our own custom constructor (it doesn't mirror a superclass constructor).
     * The context is used to inflate the layout file, and the List is the data we want
     * to populate into the lists
     *
     * @param context     The current context. Used to inflate the layout file.
     * @param listPrinter A List of PrinterEntity objects to display in a list
     */
    public EventDealFixDealsCollectionFilterAdapter(Context context, List<EventDealFixDealsCollectionListBoxSettings> listPrinter) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, listPrinter);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    /***
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    private View getCustomView(int position, View convertView, ViewGroup parent) {
        EventDealFixDealsCollectionFilterViewHolder viewHolder;
        final EventDealFixDealsCollectionListBoxSettings eventDealFixDealsCollectionListBoxSettings = getItem(position);
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.event_deal_fix_deals_collection_spinner_rows, parent, false);
            viewHolder = new EventDealFixDealsCollectionFilterViewHolder();
            viewHolder.textViewItem = (TextView) convertView.findViewById(R.id.batch_display);
            viewHolder.relativeLayoutSpinnerItem = (RelativeLayout) convertView.findViewById(R.id.relative_layout_spinner_item);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (EventDealFixDealsCollectionFilterViewHolder) convertView.getTag();
        }
        viewHolder.textViewItem.setText(eventDealFixDealsCollectionListBoxSettings.getBoxTitle());
        return convertView;
    }

    /****
     *
     */
    private class EventDealFixDealsCollectionFilterViewHolder {
        TextView textViewItem;
        RelativeLayout relativeLayoutSpinnerItem;
    }
}

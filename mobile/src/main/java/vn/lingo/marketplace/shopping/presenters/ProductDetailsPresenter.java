package vn.lingo.marketplace.shopping.presenters;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.thefinestartist.finestwebview.FinestWebView;

import net.sourceforge.widgets.CountdownView;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.CustomerPaymentOrdersActivity;
import vn.lingo.marketplace.shopping.activities.MoreProductReviewsActivity;
import vn.lingo.marketplace.shopping.activities.ProductDetailInformationActivity;
import vn.lingo.marketplace.shopping.adapter.GiftProductsViewPagerAdapter;
import vn.lingo.marketplace.shopping.adapter.ProductDetailsGeneralProductInformationRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.adapter.ProductDetailsImageMetadataPagerAdapter;
import vn.lingo.marketplace.shopping.adapter.ProductDetailsProductReviewsRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.adapter.ProductDetailsRelatedProductsRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.delegate.Singleton;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.ProductCheckoutAPI;
import vn.lingo.marketplace.shopping.models.ProductCheckoutRequest;
import vn.lingo.marketplace.shopping.models.ProductCheckoutResponse;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.models.ProductDetailsRelatedProductsEntity;
import vn.lingo.marketplace.shopping.models.ProductDetailsResponse;
import vn.lingo.marketplace.shopping.models.ProductInformationEntity;
import vn.lingo.marketplace.shopping.models.ProductInformationImageMetadata;
import vn.lingo.marketplace.shopping.models.ProductReviewsAPI;
import vn.lingo.marketplace.shopping.models.ProductReviewsResponse;
import vn.lingo.marketplace.shopping.models.ProductsAPI;
import vn.lingo.marketplace.shopping.models.ShoppingCartEntity;
import vn.lingo.marketplace.shopping.models.ShoppingCartLocalStorage;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.ProductDetailsView;

/**
 * Created by longtran on 09/12/2015.
 */
public class ProductDetailsPresenter extends AbstractPresenter<ProductDetailsView> implements RecyclerViewItemClickListener {

    private final String TAG = ProductDetailsPresenter.class.getName();
    private final int totalDocumentOfPageProductReviews = 3;
    private ProductDetailsView productDetailsView;
    private ProductInformationEntity productInformationEntityGlobal;
    private int orderQuantity = 1;
    private int currentPageProductReviews = 1;
    private Object OBJECT = new Object();
    private int valueWeight = 0;
    private List<ShoppingCartEntity> listShoppingCartEntity;

    /****
     * @param productDetailsView
     */
    public ProductDetailsPresenter(final ProductDetailsView productDetailsView) {
        this.productDetailsView = productDetailsView;
        productDetailsView.getAppCompatButtonAddToCart().setEnabled(false);
        productDetailsView.getAppCompatButtonBuyNow().setEnabled(false);
        productDetailsView.getToolbar().setTitle("");
        productDetailsView.getToolbar().setTitleTextColor(ScrollUtils.getColorWithAlpha(0.28f, productDetailsView.getContext().getResources().getColor(R.color.primary)));
        productDetailsView.getToolbar().setBackgroundColor(ScrollUtils.getColorWithAlpha(0.28f, productDetailsView.getContext().getResources().getColor(R.color.primary)));
        productDetailsView.getAppCompatActivity().setSupportActionBar(productDetailsView.getToolbar());
        productDetailsView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        productDetailsView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        productDetailsView.getToolbar().invalidate();// restore toolbar

        productDetailsView.getAppCompatButtonBuyNow().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productInformationEntityGlobal == null) {
                    return;
                }
//                productDetailsView.showProcessing();
//                try {
//                    RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class).equalTo("cartID",
//                            Integer.parseInt(productInformationEntityGlobal.getProductId()));
//                    RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
//
//                    if (realmResultsShoppingCartLocalStorage.size() <= 0) {
//                        updateShoppingCart(valueWeight, 1, productInformationEntityGlobal);
//                    }
//                    AnalyticsHelper.logEvent(productDetailsView.getContext(), "PURCHASE",
//                            productInformationEntityGlobal.getProductName() + ":" + productInformationEntityGlobal.getProductId());
//                    RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorageBuyNow = realm.where(ShoppingCartLocalStorage.class).equalTo("isOrdered", 1);
//                    RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorageBuyNow = realmQueryShoppingCartLocalStorageBuyNow.findAll();
//                    ProductCheckoutAPI productCheckoutAPI = productDetailsView.getRetrofitProductCheckout().create(ProductCheckoutAPI.class);
//                    Call<ProductCheckoutResponse> callProductCheckoutResponse = productCheckoutAPI.productCheckout(ProductCheckoutRequest
//                            .getProductCheckoutRequest(realmResultsShoppingCartLocalStorageBuyNow));
//                    callProductCheckoutResponse.enqueue(new Callback<ProductCheckoutResponse>() {
//                        @Override
//                        public void onResponse(Response<ProductCheckoutResponse> response, Retrofit retrofit) {
//                            ProductCheckoutResponse productCheckoutResponse = response.body();
//                            if (response.code() == 200) {
//                                if (!StringUtils.isBlank(productCheckoutResponse.getRedirectWapURL())) {
//                                    new FinestWebView.Builder(productDetailsView.getAppCompatActivity()).show(productCheckoutResponse.getRedirectWapURL());
//                                }
//                            } else {
//                                productDetailsView.getMaterialDialogAlert(productDetailsView.getAppCompatActivity(),
//                                        productDetailsView.getContext().getResources().getString(R.string.app_name),
//                                        productDetailsView.getContext().getResources().getString(R.string.product_invalidate),
//                                        productDetailsView.getContext().getResources().getString(R.string.material_dialog_positive_text));
//                            }
//                            productDetailsView.hideProcessing();
//                        }
//
//                        @Override
//                        public void onFailure(Throwable t) {
//                            productDetailsView.hideProcessing();
//                        }
//                    });
//                } catch (Exception exception) {
//                    exception.fillInStackTrace();
//                    Log.e(TAG, exception.toString());
//                    productDetailsView.hideProcessing();
//                }

                RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorageFromList = realm.where(ShoppingCartLocalStorage.class).equalTo("cartID",
                        Integer.parseInt(productInformationEntityGlobal.getProductId()));
                ShoppingCartLocalStorage realmResultsShoppingCartLocalStorageFromList = realmQueryShoppingCartLocalStorageFromList.findFirst();

                if (realmResultsShoppingCartLocalStorageFromList == null) {
                    updateShoppingCart(valueWeight, 1, productInformationEntityGlobal);
                    listShoppingCartEntity = new ArrayList<ShoppingCartEntity>();
                    RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class);
                    RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
                    realmResultsShoppingCartLocalStorage.sort("timeStamp", RealmResults.SORT_ORDER_DESCENDING);
                    Iterator<ShoppingCartLocalStorage> iterator = realmResultsShoppingCartLocalStorage.iterator();
                    while (iterator.hasNext()) {
                        ShoppingCartLocalStorage shoppingCartLocalStorage = iterator.next();
                        ShoppingCartEntity shoppingCartEntity = new ShoppingCartEntity();
                        shoppingCartEntity.setCartID(shoppingCartLocalStorage.getCartID());
                        shoppingCartEntity.setOrderQuantity(shoppingCartLocalStorage.getOrderQuantity());
                        shoppingCartEntity.setValueWeight(shoppingCartLocalStorage.getValue());
                        shoppingCartEntity.setProductName(shoppingCartLocalStorage.getProductName());
                        shoppingCartEntity.setMarketPrice(shoppingCartLocalStorage.getMarketPrice());
                        shoppingCartEntity.setProductPrice(shoppingCartLocalStorage.getProductPrice());
                        shoppingCartEntity.setAvatar(shoppingCartLocalStorage.getAvatar());
                        shoppingCartEntity.setIsOrdered(shoppingCartLocalStorage.getIsOrdered());
                        shoppingCartEntity.setProductPriceFormat(shoppingCartLocalStorage.getProductPriceFormat());
                        shoppingCartEntity.setMarketPriceFormat(shoppingCartLocalStorage.getMarketPriceFormat());
                        shoppingCartEntity.setSku(shoppingCartLocalStorage.getSku());
                        shoppingCartEntity.setTimeStamp(shoppingCartLocalStorage.getTimeStamp());
                        shoppingCartEntity.setBigType(shoppingCartLocalStorage.getBigType());
                        listShoppingCartEntity.add(shoppingCartEntity);
                    }
                }else {
                        updateShoppingCart(valueWeight, realmResultsShoppingCartLocalStorageFromList.getOrderQuantity(), productInformationEntityGlobal);
                        listShoppingCartEntity = new ArrayList<ShoppingCartEntity>();
                        RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class);
                        RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
                        realmResultsShoppingCartLocalStorage.sort("timeStamp", RealmResults.SORT_ORDER_DESCENDING);
                        Iterator<ShoppingCartLocalStorage> iterator = realmResultsShoppingCartLocalStorage.iterator();
                        while (iterator.hasNext()) {
                            ShoppingCartLocalStorage shoppingCartLocalStorage = iterator.next();
                            ShoppingCartEntity shoppingCartEntity = new ShoppingCartEntity();
                            shoppingCartEntity.setCartID(shoppingCartLocalStorage.getCartID());
                            shoppingCartEntity.setOrderQuantity(shoppingCartLocalStorage.getOrderQuantity());
                            shoppingCartEntity.setValueWeight(shoppingCartLocalStorage.getValue());
                            shoppingCartEntity.setProductName(shoppingCartLocalStorage.getProductName());
                            shoppingCartEntity.setMarketPrice(shoppingCartLocalStorage.getMarketPrice());
                            shoppingCartEntity.setProductPrice(shoppingCartLocalStorage.getProductPrice());
                            shoppingCartEntity.setAvatar(shoppingCartLocalStorage.getAvatar());
                            shoppingCartEntity.setIsOrdered(shoppingCartLocalStorage.getIsOrdered());
                            shoppingCartEntity.setProductPriceFormat(shoppingCartLocalStorage.getProductPriceFormat());
                            shoppingCartEntity.setMarketPriceFormat(shoppingCartLocalStorage.getMarketPriceFormat());
                            shoppingCartEntity.setSku(shoppingCartLocalStorage.getSku());
                            shoppingCartEntity.setTimeStamp(shoppingCartLocalStorage.getTimeStamp());
                            shoppingCartEntity.setBigType(shoppingCartLocalStorage.getBigType());
                            listShoppingCartEntity.add(shoppingCartEntity);
                      

                    }
                }
                Intent intent = new Intent();
                intent.setClassName(productDetailsView.getContext(), CustomerPaymentOrdersActivity.class.getName());
                intent.putExtra(Constant.LIST_SHOPPING_CART_ENTITY, (Serializable) listShoppingCartEntity);
                productDetailsView.getContext().startActivity(intent);
                productDetailsView.getAppCompatActivity().finish();
            }
            }

            );

            productDetailsView.getAppCompatButtonAddToCart().

                    setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (productInformationEntityGlobal == null) {
                                return;
                            } else {

                            }
                            try {
                                AnalyticsHelper.logEvent(productDetailsView.getContext(), "ADD_TO_CART",
                                        productInformationEntityGlobal.getProductName() + ":" + productInformationEntityGlobal.getProductId());
                                RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class)
                                        .equalTo("cartID", Integer.parseInt(productInformationEntityGlobal.getProductId()));
                                RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
                                orderQuantity = realmResultsShoppingCartLocalStorage.sum("orderQuantity").intValue();
                                orderQuantity++;
                                orderQuantity = Math.min(orderQuantity, 5);
                                updateShoppingCart(valueWeight, orderQuantity, productInformationEntityGlobal);
                            } catch (Exception exception) {
                                exception.printStackTrace();
                                Log.e(TAG, exception.toString());
                                productDetailsView.hideProcessing();
                            }
                        }
                    });

        productDetailsView.getLinearLayoutMoreProductProductReviews().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClassName(productDetailsView.getAppCompatActivity(), MoreProductReviewsActivity.class.getName());
                Bundle bundle = new Bundle();
                bundle.putString(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productInformationEntityGlobal.getProductId());
                intent.putExtras(bundle);
                productDetailsView.getAppCompatActivity().startActivity(intent);
            }
        });
    }

    /***
     *
     */
    public void invalidateToolbar() {
        //mainView.getToolbar().setLogo(R.drawable.lingo_logo_tool_bar);
        productDetailsView.getToolbar().setTitle("");
        productDetailsView.getToolbar().setTitleTextColor(ScrollUtils.getColorWithAlpha(0.28f, productDetailsView.getContext().getResources().getColor(R.color.primary)));
        productDetailsView.getToolbar().invalidate();// restore toolbar
    }

    /****
     *
     */
    private void updateShoppingCart(int value, int orderQuantityValue, ProductInformationEntity productInformationEntityGlobalValue) {
        synchronized (OBJECT) {
            ShoppingCartLocalStorage shoppingCartLocalStorage = new ShoppingCartLocalStorage();
            try {
                shoppingCartLocalStorage.setCartID(Integer.parseInt(productInformationEntityGlobalValue.getProductId()));
            } catch (Exception exception) {
                exception.printStackTrace();
                Log.e(TAG, exception.toString());
            }
            if (productInformationEntityGlobalValue.getIsBigType()==null){
                shoppingCartLocalStorage.setBigType(0);
            }else {
                shoppingCartLocalStorage.setBigType(Integer.parseInt(productInformationEntityGlobalValue.getIsBigType()));
            }
            shoppingCartLocalStorage.setValue(value);
            shoppingCartLocalStorage.setOrderQuantity(orderQuantityValue);
            shoppingCartLocalStorage.setAvatar(productInformationEntityGlobalValue.getAvatar());
            shoppingCartLocalStorage.setProductName(productInformationEntityGlobalValue.getProductName());
            try {
                shoppingCartLocalStorage.setMarketPrice(Integer.parseInt(productInformationEntityGlobalValue.getMarketPrice()));
            } catch (Exception exception) {
                exception.fillInStackTrace();
                Log.e(TAG, exception.toString());
            }
            try {
                shoppingCartLocalStorage.setProductPrice(Integer.parseInt(productInformationEntityGlobalValue.getProductPrice()));
            } catch (Exception exception) {
                exception.fillInStackTrace();
                Log.e(TAG, exception.toString());
            }
            shoppingCartLocalStorage.setIsOrdered(1);
            shoppingCartLocalStorage.setProductPriceFormat(productInformationEntityGlobalValue.getProductPriceFormat());
            shoppingCartLocalStorage.setMarketPriceFormat(productInformationEntityGlobalValue.getMarketPriceFormat());
            shoppingCartLocalStorage.setSku(productInformationEntityGlobalValue.getSku());
            shoppingCartLocalStorage.setTimeStamp(System.currentTimeMillis());
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(shoppingCartLocalStorage);
            realm.commitTransaction();
            Intent intent = new Intent();
            intent.setAction(Constant.ACTION.ADD_TO_CART_ACTION);
            productDetailsView.getAppCompatActivity().sendBroadcast(intent);
        }
    }

    /***
     *
     */
    public void retrievingProductInformation(ProductDetailsParcelable productDetailsParcelable) {
        Log.i(TAG, "productDetailsParcelable.getId() " + productDetailsParcelable.getId());
        productDetailsView.showProcessing();
        ProductsAPI productsAPI = productDetailsView.getRetrofit().create(ProductsAPI.class);
        Call<ProductDetailsResponse> callProductDetailsResponse = productsAPI.getProductDetails(productDetailsParcelable.getId());
        callProductDetailsResponse.enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Response<ProductDetailsResponse> response, Retrofit retrofit) {
                ProductDetailsResponse productDetailsResponse = response.body();
                if (response.code() == 200 && null != productDetailsResponse) {
                    List<ProductInformationEntity> listProductInformationEntity = productDetailsResponse.getListProductInformationEntity();
                    for (ProductInformationEntity productInformationEntity : listProductInformationEntity) {
                        productInformationEntityGlobal = productInformationEntity;
                        productInformationDataBinding(productInformationEntityGlobal);
                        productionCheckCondition(productInformationEntityGlobal);
                    }
                    List<ProductInformationImageMetadata> listImageMetadata = productDetailsResponse.getProductDetailsImageMetadata();
                    if (null != listImageMetadata) {
                        productDetailsView.getAutoScrollViewPager().setAdapter(new ProductDetailsImageMetadataPagerAdapter(productDetailsView.getContext(), listImageMetadata,
                                productDetailsView.getDisplayImageOptions()));
                    }
                    List<ProductDetailsRelatedProductsEntity> productDetailsRelatedProductsEntity = productDetailsResponse.getProductDetailsRelatedProductsEntity();
                    if (null != productDetailsRelatedProductsEntity) {
                        fetchingRelatedProducts(productDetailsRelatedProductsEntity);
                    }
                    try {
                        ProductDetailsResponse.ProductDetailsGeneralProductInformation productDetailsGeneralProductInformation =
                                productDetailsResponse.getProductDetailsGeneralProductInformation().get(0);

                        for (ProductDetailsResponse.GeneralProductInformationNameValue generalProductInformationNameValue
                                : productDetailsGeneralProductInformation.getListGeneralProductInformationNameValue()) {

                            if (generalProductInformationNameValue.getAttributesKey().equals("Khac_KhoiLuong")) {
                                valueWeight = Integer.parseInt(generalProductInformationNameValue.getValue());
                            }

                        }

                        fetchingProductDetailsGeneralProductInformation(productDetailsGeneralProductInformation.getListGeneralProductInformationNameValue());
                    } catch (Exception exception) {
                        Log.e(TAG, exception.toString());
                    }
                    try {
                        ProductDetailsResponse.ProductDetailsGeneralProductInformation productDetailsDetailedInformation =
                                productDetailsResponse.getProductDetailsGeneralProductInformation().get(1);
                        fetchingProductDetailsDetailedInformation(productDetailsDetailedInformation.getListGeneralProductInformationNameValue());
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                    productDetailsView.getCirclePageIndicator().setViewPager(productDetailsView.getAutoScrollViewPager());
                    productDetailsView.getAutoScrollViewPager().setInterval(250000L);
                    productDetailsView.getAutoScrollViewPager().startAutoScroll();
                    productDetailsView.getAutoScrollViewPager().setCurrentItem(0/*Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % listImageMetadata.size()*/);
                    if (null != productDetailsResponse.getGiftProductsResponse() && productDetailsResponse.getGiftProductsResponse().size() > 0) {
                        productDetailsView.getLinearLayoutViewPagerAutoScrollGiftProducts().setVisibility(View.VISIBLE);
                        GiftProductsViewPagerAdapter giftProductsViewPagerAdapter = new GiftProductsViewPagerAdapter(productDetailsView.getContext(),
                                productDetailsResponse.getGiftProductsResponse(),
                                productDetailsView.getDisplayImageOptions(), ProductDetailsPresenter.this);
                        productDetailsView.getAutoScrollViewPagerGiftProducts().setAdapter(giftProductsViewPagerAdapter);
                        productDetailsView.getAutoScrollViewPagerGiftProducts().setInterval(100000L);
                        productDetailsView.getAutoScrollViewPager().startAutoScroll(5000);
                    } else {
                        productDetailsView.getLinearLayoutViewPagerAutoScrollGiftProducts().setVisibility(View.GONE);
                    }
                    try {
                        fetchingProductReviews();
                    } catch (Exception exception) {
                        exception.fillInStackTrace();
                        Log.e(TAG, exception.toString());
                    }
                }
                productDetailsView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                productDetailsView.hideProcessing();
            }
        });
    }

    /****
     *
     */
    private void productInformationDataBinding(ProductInformationEntity productInformationEntityGlobal) {
        AnalyticsHelper.logEvent(productDetailsView.getContext(), productInformationEntityGlobal.getProductName(),
                productInformationEntityGlobal.getProductId());
        AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY_FETCHING_PRODUCT_INFORMATION", productInformationEntityGlobal.getProductName() + ":" +
                productInformationEntityGlobal.getProductId(), true);
        productDetailsView.getProductDetailsDescription().setText(productInformationEntityGlobal.getProductName());
        productDetailsView.getProductCode().setText(String.format(productDetailsView.getContext().getResources().getString(R.string.product_code),
                productInformationEntityGlobal.getSku()));
        productDetailsView.getProductPrice().setText(productInformationEntityGlobal.getProductPriceFormat());
        if (StringUtils.isBlank(productInformationEntityGlobal.getMarketPriceFormat())) {
            productDetailsView.getProductMarketPrice().setVisibility(View.INVISIBLE);
        } else {
            productDetailsView.getProductMarketPrice().setVisibility(View.VISIBLE);
            productDetailsView.getProductMarketPrice().setText(productInformationEntityGlobal.getMarketPriceFormat());
        }
        productDetailsView.getRatingBar().setRating(Constant.TOTAL_STAR);
        productDetailsView.getRatingBar().setNumStars(Integer.parseInt(productInformationEntityGlobal.getRatingStar()));
        productDetailsView.getProductBrand().setText(String.format(productDetailsView.getContext().getResources().getString(R.string.product_brand),
                productInformationEntityGlobal.getTrademark()));
        productDetailsView.getAppCompatTextViewScores().setText(productInformationEntityGlobal.getRatingStar());
        productDetailsView.getCountdownViewGoldenHour().setTag("PRODUCT_GOLD_HOUR");
        try {
            long millisecond = Long.parseLong(productInformationEntityGlobal.getProductGoldHour());
            if (millisecond > 0) {
                productDetailsView.getProductPrice().setTextColor(0xFFF7A700);
                productDetailsView.getProductPriceGoldenHourLabel().setVisibility(View.VISIBLE);
                productDetailsView.getLinearLayoutCountDownGoldenHour().setVisibility(View.VISIBLE);
                productDetailsView.getCountdownViewGoldenHour().start(millisecond * 1000);
            } else {
                productDetailsView.getLinearLayoutCountDownGoldenHour().setVisibility(View.GONE);
                productDetailsView.getProductPriceGoldenHourLabel().setVisibility(View.GONE);
            }
        } catch (Exception exception) {
            productDetailsView.getLinearLayoutCountDownGoldenHour().setVisibility(View.GONE);
            productDetailsView.getProductPriceGoldenHourLabel().setVisibility(View.GONE);
            exception.printStackTrace();
            Log.e(TAG, exception.toString());
        }
        productDetailsView.getCountdownViewGoldenHour().setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
            @Override
            public void onEnd(CountdownView cv) {
                productDetailsView.getLinearLayoutCountDownGoldenHour().setVisibility(View.GONE);
            }
        });
        if (!StringUtils.isBlank(productInformationEntityGlobal.getProductDiscount())) {
            productDetailsView.getProductDiscount().setText(productInformationEntityGlobal.getProductDiscount());
            productDetailsView.getProductDiscount().setVisibility(View.VISIBLE);
        }
    }

    /***
     * @param listProductDetailsRelatedProductsEntity
     */
    private void fetchingRelatedProducts(List<ProductDetailsRelatedProductsEntity> listProductDetailsRelatedProductsEntity) {
        final ProductDetailsRelatedProductsRecyclerViewAdapter productDetailsRelatedProductsRecyclerViewAdapter =
                new ProductDetailsRelatedProductsRecyclerViewAdapter(
                        productDetailsView.getContext(), listProductDetailsRelatedProductsEntity, ProductDetailsPresenter.this,
                        productDetailsView.getDisplayImageOptions());
        productDetailsView.getRecyclerViewRelatedProducts().setAdapter(productDetailsRelatedProductsRecyclerViewAdapter);
    }

    /***
     * @param listProductDetailsResponseGeneralProductInformationNameValue
     */
    private void fetchingProductDetailsGeneralProductInformation(List<ProductDetailsResponse.GeneralProductInformationNameValue>
                                                                         listProductDetailsResponseGeneralProductInformationNameValue) {
        final ProductDetailsGeneralProductInformationRecyclerViewAdapter productDetailsRelatedProductsRecyclerViewAdapter =
                new ProductDetailsGeneralProductInformationRecyclerViewAdapter(
                        productDetailsView.getContext(), listProductDetailsResponseGeneralProductInformationNameValue);
        productDetailsView.getRecyclerViewProductDetailsGeneralProductInformation().setAdapter(productDetailsRelatedProductsRecyclerViewAdapter);
        for (ProductDetailsResponse.GeneralProductInformationNameValue generalProductInformationNameValue
                : listProductDetailsResponseGeneralProductInformationNameValue) {
            String s = generalProductInformationNameValue.getAttributesKey();

        }
    }

    /*****
     * http://stackoverflow.com/questions/6652687/strip-leading-and-trailing-spaces-from-java-string
     *
     * @param listProductDetailsResponseGeneralProductInformationNameValue
     */
    private void fetchingProductDetailsDetailedInformation(List<ProductDetailsResponse.GeneralProductInformationNameValue>
                                                                   listProductDetailsResponseGeneralProductInformationNameValue) {
        final String html = listProductDetailsResponseGeneralProductInformationNameValue.get(0).getValue();
        if (!StringUtils.isBlank(html)) {
            productDetailsView.getAppCompatTextViewDetailedInformation().setMaxLines(2);
            productDetailsView.getAppCompatTextViewDetailedInformation().setText(Jsoup.parse(html).text().replaceAll("^\\s+", ""));
            productDetailsView.getAppCompatTextViewProductDetailedInformationViewMore().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClassName(productDetailsView.getAppCompatActivity(), ProductDetailInformationActivity.class.getName());
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.ProductDetails.PRODUCT_DETAIL_INFORMATION, html);
                    intent.putExtras(bundle);
                    productDetailsView.getAppCompatActivity().startActivity(intent);
                }
            });
        }
    }

    /***
     *
     */
    private void fetchingProductReviews() throws Exception {
        ProductReviewsAPI productReviewsAPI = productDetailsView.getRetrofit().create(ProductReviewsAPI.class);
        Call<ProductReviewsResponse> callProductReviewsResponse = productReviewsAPI.getListComments(currentPageProductReviews, totalDocumentOfPageProductReviews, Integer.parseInt(productInformationEntityGlobal.getProductId()));
        callProductReviewsResponse.enqueue(new Callback<ProductReviewsResponse>() {
            @Override
            public void onResponse(Response<ProductReviewsResponse> response, Retrofit retrofit) {
                ProductReviewsResponse productReviewsResponse = response.body();
                if (response.code() == 200 && null != productReviewsResponse && null != productReviewsResponse.getLisProductReviewsResponse()
                        && productReviewsResponse.getLisProductReviewsResponse().size() > 0) {
                    int totalRecord = 0;
                    try {
                        totalRecord = Integer.parseInt(productReviewsResponse.getTotalRecord());
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Log.e(TAG, exception.toString());
                    }
                    if (totalRecord > 3) {
                        productDetailsView.getLinearLayoutMoreProductProductReviews().setVisibility(View.VISIBLE);
                    } else {
                        productDetailsView.getLinearLayoutMoreProductProductReviews().setVisibility(View.GONE);
                    }
                    ProductDetailsProductReviewsRecyclerViewAdapter productDetailsProductReviewsRecyclerViewAdapter =
                            new ProductDetailsProductReviewsRecyclerViewAdapter(productDetailsView.getContext(), productReviewsResponse.getLisProductReviewsResponse(),
                                    productDetailsView.getDisplayImageOptions());
                    productDetailsView.getRecyclerViewProductForReview().setAdapter(productDetailsProductReviewsRecyclerViewAdapter);
                }
                productDetailsView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                productDetailsView.hideProcessing();
            }
        });
    }

    /****
     * The production check-condition
     */
    private void productionCheckCondition(ProductInformationEntity productInformationEntityGlobal) {
        try {
            if (Integer.parseInt(productInformationEntityGlobal.getStopSaveBuy()) > 0 ||
                    Integer.parseInt(productInformationEntityGlobal.getPauseSaveBuy()) > 0) {
                productDetailsView.getAppCompatButtonAddToCart().setEnabled(false);
                productDetailsView.getAppCompatButtonAddToCart().setText(productDetailsView.getContext().getResources().getString(R.string.sold_out_product));
                productDetailsView.getAppCompatButtonBuyNow().setEnabled(false);
                productDetailsView.getAppCompatButtonBuyNow().setText(productDetailsView.getContext().getResources().getString(R.string.sold_out_product));
            } else {
                productDetailsView.getAppCompatButtonAddToCart().setEnabled(true);
                productDetailsView.getAppCompatButtonBuyNow().setEnabled(true);
                productDetailsView.getAppCompatButtonAddToCart().setText(productDetailsView.getContext().getResources().getString(R.string.add_to_cart));
                productDetailsView.getAppCompatButtonBuyNow().setText(productDetailsView.getContext().getResources().getString(R.string.buy_now));
            }
        } catch (Exception exception) {
            exception.fillInStackTrace();
            Log.e(TAG, exception.toString());
        }
    }

    /****
     *
     */
    public void truncateShoppingCart() {
        RealmQuery<ShoppingCartLocalStorage> realmQueryShoppingCartLocalStorage = realm.where(ShoppingCartLocalStorage.class).equalTo("isOrdered", 1);
        RealmResults<ShoppingCartLocalStorage> realmResultsShoppingCartLocalStorage = realmQueryShoppingCartLocalStorage.findAll();
        // All changes to data must happen in a transaction
        realm.beginTransaction();
        realmResultsShoppingCartLocalStorage.clear();
        realm.commitTransaction();
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        productDetailsView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}

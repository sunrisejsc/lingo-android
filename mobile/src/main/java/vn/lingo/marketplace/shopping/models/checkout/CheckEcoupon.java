package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;

/**
 * Created by zon on 07/04/2016.
 */
public class CheckEcoupon implements Serializable {
    @SerializedName("EcouponCode")
    private String ecouponCode;
    @SerializedName("ListOrderDetail")
    List<OrderDetail> listOrderDetail;

    public String getEcouponCode() {
        return ecouponCode;
    }

    public void setEcouponCode(String ecouponCode) {
        this.ecouponCode = ecouponCode;
    }

    public List<OrderDetail> getListOrderDetail() {
        return listOrderDetail;
    }

    public void setListOrderDetail(List<OrderDetail> listOrderDetail) {
        this.listOrderDetail = listOrderDetail;
    }

    public static String getCheckEcoupon(String ecouponCode, List<OrderDetail> listOrderDetail){
        CheckEcoupon checkEcoupon=new CheckEcoupon();
        checkEcoupon.setEcouponCode(ecouponCode);
        checkEcoupon.setListOrderDetail(listOrderDetail);
        String jsonObject=new Gson().toJson(checkEcoupon);

        HashMap<String, String> fetchPhotoStatEventParams = new HashMap<String, String>();
        fetchPhotoStatEventParams.put(AnalyticsHelper.EVENT_PRODUCT_ORDER_DATA_REQUEST, jsonObject);
        AnalyticsHelper.logEvent(AnalyticsHelper.EVENT_PRODUCT_ORDER, fetchPhotoStatEventParams, true);

        return jsonObject;
    }
}
